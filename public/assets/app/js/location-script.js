$(document).ready(function () {
    var $country = $("#country"),
        $state = $("#state");

    $state.change(function () {
        if (this.value === '') {
            $state.val('');
        }
    });

    $country.change(function () {
        $.get('http://app.dealmanagerpro.com/countries/' + this.value + '/states', function (states) {
            // Clear state select
            $state.find('option').remove().end();

            $state.append('<option value="">-- Select --</option>');

            $(states).each(function (key, state) {
                $state.append('<option value="' + state + '">' + state + '</option>');
            });
        });
    });

    $country.val('United States').change();
});