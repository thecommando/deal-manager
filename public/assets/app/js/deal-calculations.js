var vm = new Vue({
	el: 'body',

	data: {
		sqft: null,
		asking_price_flag: false,
		estimated_rehab_flag: false,
		appraised_value_flag: false,
		equity_position: 0,
		asking_price: null,
		asking_price_formatted: null,
		appraised_value: null,
		appraised_value_formatted: null,
		fair_market_rent: null,
		fair_market_rent_formatted: null,
		actual_monthly_rent: null,
		actual_monthly_rent_formatted: null,
		annual_taxes: null,
		annual_taxes_formatted: null,
		annual_insurance: null,
		annual_insurance_formatted: null,
		rehab_estimate: null,
		rehab_estimate_formatted: null,
		offer_price: null,
		offer_price_formatted: null,
		wholesale_fee: '',
		wholesale_fee_formatted: '',
		purchase: null,
		rehab: null,
		comparables: [],
		deal_type: '',
		rehab_estimate_category: '',
		offer_scale_percentage: 60,
		seller_mobile: '',
		seller_phone: ''
	},

	computed: {
		estimated_rehab: function() {
			if (parseInt(this.rehab_estimate)) {
				return this.rehab_estimate;
			}

			if (this.rehab_estimate_category && this.sqft) {
				return this.sqft * parseFloat(this.rehab_estimate_category);
			}

			return 0;
		},

		comparable_price_per_sqft: function() {
			let totalSqftPrice = 0,
				totalComparables = 0;

			for(let i = 0; i < this.comparables.length; i++) {
				if (this.comparables[i].sold_price && this.comparables[i].sqft) {
					totalSqftPrice += this.comparables[i].sold_price / this.comparables[i].sqft;
					totalComparables++;
				}
			}

			let value = totalSqftPrice / totalComparables;

			return value || 0;
		},

		after_repair_value: function() {
			if (this.sqft && this.comparable_price_per_sqft) {
				return this.sqft * this.comparable_price_per_sqft;
			}

			return 0;
		},

		suggested_maximum_offer: function() {
			if (this.after_repair_value && this.estimated_rehab) {
				return (this.after_repair_value * (this.offer_scale_percentage / 100)) - this.estimated_rehab - this.wholesale_fee;
			}

			return 0;
		},

		annual_expenses: function () {
			let value = 0;

			value += parseInt(this.annual_taxes || 0);
			value += parseInt(this.annual_insurance || 0);

			return value;
		},

		annual_gross_income: function () {
			if (parseInt(this.actual_monthly_rent)) {
				return parseInt(this.actual_monthly_rent) * 12;
			}

			return parseInt(this.fair_market_rent || 0) * 12;
		},

		annual_net_operating_income: function () {
			return this.annual_gross_income - this.annual_expenses;
		},

		wholesale_value: function () {
			/* return parseInt(this.after_repair_value * (this.offer_scale_percentage/100) || 2); */
			return parseInt(this.offer_price || 0) + parseInt(this.wholesale_fee || 0);
		},

		retail_value: function () {
			return parseInt(this.after_repair_value || 0);
		},

		arv_estimate: function () {
			return parseInt(this.after_repair_value || 0);
		},

		equity_position: function () {
			if (! this.after_repair_value) {
				return 0;
			}
			/* Updated 6/5/17 to wholesale value instead of offer price */
			return parseInt(((this.after_repair_value - this.wholesale_value)/this.after_repair_value) * 100);
		},
		
		purchase: function () {
            if (! this.after_repair_value) {
            	return 0;
			}
			return parseInt(((this.offer_price / this.after_repair_value)) * 100);
		},
		
		rehab: function () {
            if (! this.after_repair_value) {
            	return 0;
			}
			return parseInt(((this.estimated_rehab / this.after_repair_value)) * 100);
		}
		
	},

	ready: function() {
		this.initializeComparable();

		this.maskCurrencyFields();
	},

	methods: {
		initializeComparable: function() {
			this.addComparable();
		},

		maskCurrencyFields: function() {
			this.asking_price_formatted = this.asking_price;
            this.appraised_value_formatted = this.appraised_value;
            this.fair_market_rent_formatted = this.fair_market_rent;
            this.actual_monthly_rent_formatted = this.actual_monthly_rent;
            this.annual_taxes_formatted = this.annual_taxes;
            this.annual_insurance_formatted = this.annual_insurance;
            this.rehab_estimate_formatted = this.rehab_estimate;
            this.wholesale_fee_formatted = this.wholesale_fee;
            this.offer_price_formatted = this.offer_price;
            this.seller_mobile_formatted = this.seller_mobile;
            this.seller_phone_formatted = this.seller_phone;

			$('#asking_price_formatted').maskMoney();
			$('#appraised_value_formatted').maskMoney();
			$('#fair_market_rent_formatted').maskMoney();
			$('#actual_monthly_rent_formatted').maskMoney();
			$('#annual_taxes_formatted').maskMoney();
			$('#annual_insurance_formatted').maskMoney();
			$('#rehab_estimate_formatted').maskMoney();
			$('#wholesale_fee_formatted').maskMoney();
			$('#offer_price_formatted').maskMoney();

			$('#seller_mobile_formatted').mask('(000) 000-0000');
			$('#seller_phone_formatted').mask('(000) 000-0000');
		},

		addComparable: function() {
			this.comparables.push({
				address: null,
				br: null,
				ba: null,
				sqft: null,
				sold_price: null
			});
		},

		onSubmit: function (e) {
			e.preventDefault();

			this.$http.post('/api/properties/save-comparable', {comparables: this.comparables}).then(function (response) {
				$("#deal-form").submit();
			});
		},

		set_asking_price: function() {
			this.asking_price = $('#asking_price_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_appraised_value: function() {
			this.appraised_value = $('#appraised_value_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_fair_market_rent: function() {
			this.fair_market_rent = $('#fair_market_rent_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_actual_monthly_rent: function() {
			this.actual_monthly_rent = $('#actual_monthly_rent_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_annual_taxes: function() {
			this.annual_taxes = $('#annual_taxes_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_annual_insurance: function() {
			this.annual_insurance = $('#annual_insurance_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_rehab_estimate: function() {
			this.rehab_estimate = $('#rehab_estimate_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_wholesale_fee: function() {
			this.wholesale_fee = $('#wholesale_fee_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_offer_price: function() {
			this.offer_price = $('#offer_price_formatted').maskMoney('unmasked')[0] * 1000;
		},

		set_seller_mobile: function() {
			this.seller_mobile = $('#seller_mobile_formatted').cleanVal();
		},

		set_seller_phone: function() {
			this.seller_phone = $('#seller_phone_formatted').cleanVal();
		}
	}
});