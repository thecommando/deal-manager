<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Event::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 43,
        'property_id' => 128,
        'title' => $faker->name,
        'date' => $faker->dateTimeBetween('now', '+1 month'),
        'remind_at' => $faker->dateTimeBetween('now', '+1 month'),
        'link' => null,
        'is_global' => $faker->boolean,
        'color' => $faker->safeHexColor,
    ];
});