<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

	        // Property Information
	        $table->string('address');
	        $table->string('city');
	        $table->string('state');
	        $table->string('country');
	        $table->string('postal_code');
	        $table->integer('bedrooms')->unsigned();
	        $table->integer('bathrooms')->unsigned();
	        $table->integer('type')->unsigned();
	        $table->string('year_built')->nullable();
	        $table->double('square_feet')->nullable();
	        $table->double('lot_size')->nullable();
	        $table->text('description')->nullable();
            $table->text('reason_for_selling')->nullable();
            $table->string('notes')->nullable();

	        // Property Status
	        $table->integer('status')->unsigned()->nullable();
	        $table->integer('deal_type')->unsigned()->nullable();
	        $table->integer('stage')->unsigned()->nullable();
            $table->integer('condition')->unsigned()->nullable();
	        $table->boolean('contract_status')->default(false);
	        $table->boolean('inspection_status')->default(false);

	        // Financial Information
	        $table->boolean('asking_price_flag')->default(false);
	        $table->double('asking_price')->nullable();
	        $table->boolean('estimated_rehab_flag')->default(false);
	        $table->double('rehab_estimate')->nullable();
	        $table->string('rehab_estimate_category')->nullable();
	        $table->boolean('appraised_value_flag')->default(false);
	        $table->double('appraised_value')->nullable();
	        $table->double('wholesale_fee')->nullable();
	        $table->boolean('asking_price_source')->default(false);

	        // Wholesale & Rehab Calculations
	        $table->double('estimated_rehab')->nullable();
	        $table->double('comparable_price_per_sqft')->nullable();
	        $table->double('after_repair_value')->nullable();
	        $table->double('offer_scale_percentage')->nullable();
	        $table->double('suggested_maximum_offer')->nullable();

	        // Buy & Hold Calculations
	        $table->double('fair_market_rent')->nullable();
	        $table->double('actual_monthly_rent')->nullable();
	        $table->double('annual_taxes')->nullable();
	        $table->double('annual_insurance')->nullable();
	        $table->double('delinquent_mortgage_amount')->nullable();
	        $table->double('annual_expenses')->nullable();
	        $table->double('annual_gross_income')->nullable();
	        $table->double('annual_net_operating_income')->nullable();

	        // Equity Position
	        $table->double('equity_position')->nullable();
	        $table->integer('signal')->unsigned()->nullable();

	        // Special Note
	        $table->double('wholesale_value')->nullable();
	        $table->double('retail_value')->nullable();
	        $table->double('offer_price')->nullable();
	        $table->double('purchase')->nullable();
	        $table->double('rehab')->nullable();

	        // Seller Information
            $table->string('seller_name')->nullable();
	        $table->string('seller_email')->nullable();
	        $table->string('seller_phone')->nullable();
	        $table->string('seller_mobile')->nullable();
            $table->string('seller_address')->nullable();
            $table->string('seller_city')->nullable();
            $table->string('seller_state')->nullable();
            $table->string('seller_country')->nullable();
            $table->string('seller_postal_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
