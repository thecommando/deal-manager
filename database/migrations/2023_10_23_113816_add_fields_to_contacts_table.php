<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->renameColumn('email', 'email_address');
            $table->renameColumn('mobile_phone', 'phone');
            $table->renameColumn('address', 'site_address');
            $table->renameColumn('city', 'site_city');
            $table->renameColumn('state', 'site_state');
            $table->renameColumn('postal_code', 'site_zip_code');

            $table->string('mailing_city')->nullable()->after('postal_code');
            $table->string('mailing_address')->nullable()->after('mailing_city');
            $table->string('mailing_state')->nullable()->after('mailing_address');
            $table->string('mailing_zip')->nullable()->after('mailing_state');
            $table->string('site_county')->nullable()->after('mailing_zip');

            // remove not column
            $table->dropColumn('note');
            $table->dropColumn('country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {

            $table->renameColumn('email_address', 'email');
            $table->renameColumn('phone', 'mobile_phone');
            $table->renameColumn('site_address', 'address');
            $table->renameColumn('site_city', 'city');
            $table->renameColumn('site_state', 'state');
            $table->renameColumn('site_zip_code', 'postal_code');

            $table->dropColumn('mailing_city');
            $table->dropColumn('mailing_address');
            $table->dropColumn('mailing_state');
            $table->dropColumn('mailing_zip');

            $table->text('note')->nullable();
            $table->string('country')->nullable();
        });
    }
}
