<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broadcast_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('session_id');
            $table->string('phone');
            $table->string('status');
            $table->string('failure_reason')->nullable();
            $table->string('delivery_time');
            $table->string('carrier');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broadcast_statuses');
    }
}
