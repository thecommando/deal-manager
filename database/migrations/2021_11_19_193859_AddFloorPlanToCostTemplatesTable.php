<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFloorPlanToCostTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cost_templates', function (Blueprint $table) {            
            $table->string('path')->nullable()->after('total_cost');
            $table->string('template_name')->nullable()->after('total_cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cost_templates', function (Blueprint $table) {
            $table->dropColumn('path');
            $table->dropColumn('template_name');
        });
    }
}
