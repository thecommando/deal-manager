<?php

use App\Country;
use App\State;
use Illuminate\Database\Seeder;

class StateCountrySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Country::truncate();
		State::truncate();

		foreach ($this->getSeeder() as $country => $states) {
			$country = Country::create(['name' => $country]);

			foreach ($states as $state) {
				$country->states()->create(['name' => $state]);
			}
		}
	}

	/**
	 * Get Countries with States
	 *
	 * @return array
	 */
	public function getSeeder()
	{
		return [
			"Canada"         => [],
			"United Kingdom" => [],
			"United States"  => [
				"Alabama",
				"Alaska",
				"Arizona",
				"Arkansas",
				"California",
				"Colorado",
				"Connecticut",
				"Delaware",
				"District Of Columbia",
				"Florida",
				"Georgia",
				"Hawaii",
				"Idaho",
				"Illinois",
				"Indiana",
				"Iowa",
				"Kansas",
				"Kentucky",
				"Louisiana",
				"Maine",
				"Maryland",
				"Massachusetts",
				"Michigan",
				"Minnesota",
				"Mississippi",
				"Missouri",
				"Montana",
				"Nebraska",
				"Nevada",
				"New Hampshire",
				"New Jersey",
				"New Mexico",
				"New York",
				"North Carolina",
				"North Dakota",
				"Ohio",
				"Oklahoma",
				"Oregon",
				"Pennsylvania",
				"Rhode Island",
				"South Carolina",
				"South Dakota",
				"Tennessee",
				"Texas",
				"Utah",
				"Vermont",
				"Virginia",
				"Washington",
				"West Virginia",
				"Wisconsin",
				"Wyoming",
			],
		];
	}
}
