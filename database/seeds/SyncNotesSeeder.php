<?php

use Illuminate\Database\Seeder;

class SyncNotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $properties = \App\Property::get();

        foreach ($properties as $property) {
            if($property->notes) {
                $property->saveNotes([$property->notes]);
            }
        }
    }
}
