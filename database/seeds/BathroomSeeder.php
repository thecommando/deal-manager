<?php

use App\Bathroom;
use Illuminate\Database\Seeder;

class BathroomSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Bathroom::truncate();

		foreach ($this->getSeeder() as $name) {
			Bathroom::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
			'1',
			'1.5',
			'2',
			'2.5',
			'3',
			'3.5',
			'4',
			'4.5',
			'5+',
		];
	}
}
