<?php

use App\User;
use Illuminate\Database\Seeder;

class APIKeySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$users = User::whereNull('api_key')->get();

		$users->map(function (User $user) {
			$user->api_key = generateUUID();
			$user->save();
		});
	}
}
