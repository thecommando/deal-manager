<?php

use App\PropertySignal;
use Illuminate\Database\Seeder;

class PropertySignalSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		PropertySignal::truncate();

		foreach ($this->getSeeder() as $name) {
			PropertySignal::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
			'Platinum',
			'Gold',
			'Silver',
		];
	}
}
