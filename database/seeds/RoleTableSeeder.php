<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Role::truncate();
        DB::table('role_user')->truncate();

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Master Admin';
        $admin->description  = 'Admin is the owner of a given project';
        $admin->save();

        $superUser = new Role();
        $superUser->name         = 'super-user';
        $superUser->display_name = 'Team Admin';
        $superUser->description  = 'Team Admin is allowed to manage and edit other users';
        $superUser->save();

        $user = new Role();
        $user->name         = 'user';
        $user->display_name = 'User';
        $user->description  = 'User is allowed to manage his own deals';
        $user->save();
        
        $user = new Role();
        $user->name         = 'finder';
        $user->display_name = 'Deal Finder';
        $user->description  = 'Deal Finder is allowed to manage his own deals';
        $user->save();
        
        $user = new Role();
        $user->name         = 'facilitator';
        $user->display_name = 'Deal Facilitator';
        $user->description  = 'Facilitator is allowed to manage his own deals';
        $user->save();
        
        $user = new Role();
        $user->name         = 'partner';
        $user->display_name = 'Deal Partner';
        $user->description  = 'Partner is allowed to manage a team of users';
        $user->save();

        $coach = new App\Role();
        $coach->name         = 'coach';
        $coach->display_name = 'Coach';
        $coach->description  = 'Coach is allowed to manage a team of users';
        $coach->save();*/
    }
}
