<?php

use Illuminate\Database\Seeder;
use Spatie\Activitylog\Models\Activity;

class ClearRemovedModelActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activity::where('causer_id', NULL)
            ->orWhere('subject_id', NULL)
            ->delete();
    }
}
