<?php

use App\ContactCategory;
use Illuminate\Database\Seeder;

class ContactCategorySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		ContactCategory::truncate();

		foreach ($this->getSeeder() as $name) {
			ContactCategory::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
			'Attorney',
			'Broker',
			'Bird Dog/Deal Finder',
			'Builder',
			'Buyer Lead',
			'Cash Buyer',
			'Contractor',
			'Hard Money Lender',
			'Inspector',
			'Insurance',
			'Mentor',
			'Private Money Investor',
			'Property Manager',
			'Real Estate Agent',
			'REI Club Member',
			'Seller',
			'Title Agent',
            'Team Member/Friend',
		];
	}
}
