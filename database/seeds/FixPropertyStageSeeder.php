<?php

use App\Property;
use App\PropertyStage;
use Illuminate\Database\Seeder;

class FixPropertyStageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // change offer accepted to under contract
        PropertyStage::find(3)->update(['name' => 'Under Contract']);

        // remove unnecessary stages
        PropertyStage::whereIn('id', [4, 5, 6])->delete();

        // replace stage in properties
        Property::where('stage', 5)->update(['stage' => 3]);
    }
}
