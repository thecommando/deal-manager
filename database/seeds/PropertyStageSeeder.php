<?php

use App\PropertyStage;
use Illuminate\Database\Seeder;

class PropertyStageSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		PropertyStage::truncate();

		foreach ($this->getSeeder() as $name) {
			PropertyStage::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
			'New',
			'Offer Submitted',
			'Offer Accepted',
			'Contract Assigned',
			'Funding Requested',
			'Funded',
			'On Hold',
			'Closed',
            'No Deal',
		];
	}
}
