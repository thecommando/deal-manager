<?php

use App\Events\RoleWasUpdated;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $this->createAdmins();
        $this->createSuperUsers();
        $this->createDealFinders();
        $this->createUsers();
    }

    private function createAdmins()
    {
        $roleAdmin = Role::whereName('admin')->first();

        $admin = User::create([
            'name'     => 'Chad',
            'email'    => 'chad@yourlifeofwealth.com',
            'username' => 'chad',
            'password' => bcrypt('password'),
        ]);

        $admin->attachRole($roleAdmin);
    }

    private function createSuperUsers()
    {
        $roleSuperUser = Role::whereName('super-user')->first();

        $superUser = User::create([
            'name'     => "Ricardo Clarke",
            'email'    => "ricardo.clarke@gmail.com",
            'username' => "ricardo",
            'password' => bcrypt('password'),
        ]);

        $superUser->attachRole($roleSuperUser);

        $superUser->updateTeamIfSuperUser();
    }

    private function createDealFinders()
    {
        $roleFinder = Role::whereName('finder')->first();

        $dealFinder = User::create([
            'name'     => "Finder User",
            'email'    => "finder@ricardoclarke.com",
            'username' => "finder",
            'password' => bcrypt('password'),
        ]);

        $dealFinder->attachRole($roleFinder);
    }

    private function createUsers()
    {
        $roleUser = Role::whereName('user')->first();

        $user = User::create([
            'name'     => "Ricardo User1",
            'email'    => "hello@ricardoclarke.com",
            'username' => "user1",
            'password' => bcrypt('password'),
        ]);

        $user->attachRole($roleUser);

        $user2 = User::create([
            'name'     => "Ricardo User2",
            'email'    => "ricardo@stackglobalmedia.com",
            'username' => "user2",
            'password' => bcrypt('password'),
        ]);

        $user2->attachRole($roleUser);
    }
}
