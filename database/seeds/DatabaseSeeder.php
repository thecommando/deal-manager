<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(StateCountrySeeder::class);

        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(APIKeySeeder::class);

        $this->call(PropertySignalSeeder::class);
        $this->call(PropertyTypeSeeder::class);
        $this->call(PropertyStatusSeeder::class);
        $this->call(PropertyStageSeeder::class);
        $this->call(PropertyConditionSeeder::class);
        $this->call(DealTypeSeeder::class);
        $this->call(BedroomSeeder::class);
        $this->call(BathroomSeeder::class);

        $this->call(ContactCategorySeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
