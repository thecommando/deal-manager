<?php

use App\PropertyStatus;
use Illuminate\Database\Seeder;

class PropertyStatusSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		PropertyStatus::truncate();

		foreach ($this->getSeeder() as $name) {
			PropertyStatus::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
			'Foreclosure',
			'Short Sale',
			'Pre-foreclosure',
			'MLS',
			'REO',
			'Probate',
			'Rented',
			'Vacant',
			'Unknown',
            'FSBO',
            'Bankruptcy',
            'Tax Lien',
            'Tax Deed',
            'Lease Option',
            'Subject To',
            'I Don\'t Know',
		];
	}
}
