<?php

use App\Bedroom;
use Illuminate\Database\Seeder;

class BedroomSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Bedroom::truncate();

		foreach ($this->getSeeder() as $name) {
			Bedroom::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7+',
		];
	}
}
