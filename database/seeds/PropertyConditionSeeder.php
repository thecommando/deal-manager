<?php

use App\PropertyCondition;
use Illuminate\Database\Seeder;

class PropertyConditionSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		PropertyCondition::truncate();

		foreach ($this->getSeeder() as $name) {
			PropertyCondition::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
			'Good',
            'Fair',
            'Poor',
		];
	}
}
