<?php

use App\DealType;
use Illuminate\Database\Seeder;

class DealTypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
//		DealType::truncate();

		foreach ($this->getSeeder() as $name) {
			DealType::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
//            'Wholesale',
//            'Fix & Flip',
//            'Buy & Hold',
//            'Lease Option',
//            'Subject To',
//            'Short Sale',
//            'BRRRR',
            'Assignment',
		];
	}
}
