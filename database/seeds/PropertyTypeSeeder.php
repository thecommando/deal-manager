<?php

use App\PropertyType;
use Illuminate\Database\Seeder;

class PropertyTypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		PropertyType::truncate();

		foreach ($this->getSeeder() as $name) {
			PropertyType::create([
				'name' => $name,
			]);
		}
	}

	public function getSeeder()
	{
		return [
			'Single Family',
			'Townhouse',
			'Condominium',
			'Commercial',
			'Land',
			'MultiUnit',
		];
	}
}
