module.exports = {
    computed: {
        /**
         * Access the global Spark object.
         */
        application() {
            return window.Application;
        },
    }
};
