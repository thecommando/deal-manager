export default {
    data() {
        return {
            date_format: 'YYYY-MM-DD',
            dates: [],
            from: moment().startOf('month'),
            to: moment().endOf('month'),
            params: {
                from: '',
                to: '',
            }
        }
    },

    mounted() {

    },

    methods: {
        prepareDates() {
            let from = this.from.clone().subtract(this.from.get('d'), 'd');

            this.dates = Array.from(
                moment.range(from, this.to).by('days')
            );
        },

        isToday(date) {
            return moment().format('YYYY-MM-DD') === date.format('YYYY-MM-DD');
        },

        isCurrentMonth(date) {
            return moment(date).month() === this.from.month();
        },

        isWeekend(date) {
            return _.includes([0, 6], moment(date).day());
        },

        today() {
            this.from = moment().startOf('month');
            this.to = moment().endOf('month');

            this.fetch();
        },

        previous() {
            this.from.subtract(1, 'month').startOf('month');
            this.to.subtract(1, 'month').endOf('month');

            this.fetch();
        },

        next() {
            this.from.add(1, 'month').startOf('month');
            this.to.add(1, 'month').endOf('month');

            this.fetch();
        }
    },
}
