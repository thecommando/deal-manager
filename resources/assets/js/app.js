
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('deal-calculations', require('./components/DealCalculations.vue'));
Vue.component('deal-email', require('./components/DealEmail.vue'));
Vue.component('deal-blacklist', require('./components/DealBlacklist.vue'));
Vue.component('contact-email', require('./components/ContactEmail.vue'));
Vue.component('contact-message', require('./components/ContactMessage.vue'));
Vue.component('user-notifications', require('./components/UserNotifications.vue'));
Vue.component('create-document', require('./components/CreateDocument.vue'));
Vue.component('users', require('./components/Users.vue'));
Vue.component('broadcasts', require('./components/Broadcasts.vue'));
Vue.component('calendar', require('./components/Calendar.vue'));
Vue.component('cost-calculator', require('./components/RepairCostCalculator.vue'));
Vue.component('modal-box', require('./components/ModalComponent.vue'));

const app = new Vue({
    mixins: [require('./application')],
});
