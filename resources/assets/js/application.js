/**
 * Export the root Application application.
 */
module.exports = {
    el: '#app',

    data: {
        user: Application.user
    },

    created() {
        let self = this;

        Bus.$on('updateUser', function() {
            self.getUser();
        });
    },

    mounted() {
        console.log('Application Ready.');

        this.whenReady();
    },

    methods: {

        whenReady() {
            
        },

        /*
         * Get the current user of the application.
         */
        getUser() {
            this.$http.get('/api/user/current')
                .then(response => {
                    this.user = response.data;
                });
        }
    },

    computed: {
       
    }
};
