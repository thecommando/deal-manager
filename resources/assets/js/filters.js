/**
 * Date time format.
 */
Vue.filter('datetime', value => {
    return moment.utc(value).local().format('Do MMMM YYYY - HH:mm');
});

/**
 * Time ago from now
 */
Vue.filter('ago', value => {
    return moment.utc(value).local().fromNow();
});

Vue.filter('currency', (value, currency, decimals) => {
    var digitsRE = /(\d{3})(?=\d)/g
    value = parseFloat(value)
    if (!isFinite(value) || (!value && value !== 0)) return ''
    currency = currency != null ? currency : '$'
    decimals = decimals != null ? decimals : 2
    var stringified = Math.abs(value).toFixed(decimals)
    var _int = decimals
        ? stringified.slice(0, -1 - decimals)
        : stringified
    var i = _int.length % 3
    var head = i > 0
        ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
        : ''
    var _float = decimals
        ? stringified.slice(-1 - decimals)
        : ''
    var sign = value < 0 ? '-' : ''
    return sign + currency + head +
        _int.slice(i).replace(digitsRE, '$1,') +
        _float
});

Vue.filter('format', (value, format) => {
    if (!value) return '';

    return moment(value).format(format);
});