window.flash = function (message) {
    Bus.$emit('flash', message);
};


window.isNumeric = function(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
};

window.percentageDifference = function(firstValue, secondValue) {
    return ((firstValue - secondValue) / firstValue) * 100;
}