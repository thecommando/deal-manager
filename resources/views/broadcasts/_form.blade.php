<div class="card-box">
	<div class="row">
		<!--- Campaign Name Field --->
		<div class="form-group col-sm-12">
			{!! Form::label('campaign_name', 'Campaign Name:') !!} <span class="red">*</span>
			{!! Form::text('campaign_name', null, ['class' => 'form-control']) !!}
		</div>

		<!--- Phone Field --->
		<div class="form-group col-sm-12">
			{!! Form::label('phone', 'Phone Numbers (ONE NUMBER PER LINE):') !!} <span class="red">*</span>
			{!! Form::textarea('phone', null, ['class' => 'form-control', 'rows' => 5]) !!}
		</div>

		<div class="form-group col-sm-12">
			{!! Form::label('file', 'Audio File:') !!} <span class="red">*</span>
			<div class="clearfix"></div>
			{!! Form::file('file') !!}
		</div>

	</div>

	<!--- Save Changes Field --->
	<div class="form-group input-group-sm input-group-sm m-t-30 text-right">
		{!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
		<button type="button" onclick="goBack()" class="btn btn-default">Cancel</button>
	</div>
</div>