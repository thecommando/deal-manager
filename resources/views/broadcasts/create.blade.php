@extends('layouts.default')

@section('content')

<!-- Page-Title -->
<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-2">
        <h3 class="content-header-title mb-0">New Broadcast</h3>
        <div class="row breadcrumbs-top">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
              </li>
                <li class="breadcrumb-item"><a href="{{ route('broadcasts.index') }}">Broadcasts</a>
              </li>
              <li class="breadcrumb-item active">New Broadcast</li>
            </ol>
          </div>
            <div class="col-xs-12">@include('partials.errors')</div>
        </div>
     </div>         
</div>

<div class="content-detached content-right">
    <div class="content-body">

        <!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">New Broadcast</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            {!! Form::open(['route' => 'broadcasts.store', 'files' => true]) !!}
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Email Address <span class="red">*</span></label>
                                <input type="email" name="slybroadcast_email" value="{{ $slybroadcast['email'] }}" class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Password <span class="red">*</span></label>
                                <input type="password" name="slybroadcast_password" value="{{ $slybroadcast['password'] }}" class="form-control">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Caller ID <span class="red">*</span></label>
                                <input type="text" name="slybroadcast_caller_id" value="{{ $slybroadcast['caller_id'] }}" class="form-control">
                            </div>
                        </div>

                        @include('broadcasts._form', ['submitButtonText' => 'Create'])
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </section>
        <!--/ Main Content Area -->

    </div>
</div>

<!-- Sidebar Content Area -->
<div class="sidebar-detached sidebar-left">
          <div class="sidebar">
              <div class="sidebar-content card">
                <div class="card-block">
                    <div class="category-title pb-1">
                       <h4 class="card-title">Instructions</h4>
                    </div>
                    <p class="card-text">
                        {!! nl2br(ucwords($credits)) !!}
                    </p>
                </div>
             </div>

          </div>
</div>
<!--/ Sidebar Content Area -->

	
@endsection

@section('scripts')
	@include('partials.location-script', ['country' => null, 'state' => null])

    <script src="/assets/js/jquery.maskMoney.min.js"></script>
    <script src="/assets/js/jquery.mask.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#mobile_phone').mask('(000) 000-0000');
            $('#alternate_phone').mask('(000) 000-0000');
        });
    </script>
@endsection