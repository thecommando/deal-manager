@extends('layouts.default')

@section('content')

    <broadcasts :data="{{ $broadcasts }}" inline-template>
        <div>
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-xs-12 mb-2">
                    <h3 class="content-header-title mb-0">Broadcasts</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">Broadcasts</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="content-header-right col-md-6 col-xs-12">
                    <div class="media width-250 float-xs-right">
                        <div class="media-left media-middle">

                        </div>
                        <div class="media-body media-right text-xs-right">
                            <div class="btn-group pull-right m-t-15 hidden-print">
                                <a href="{{ route('broadcasts.create') }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="New Broadcast"><span class=""><i class="fa fa-plus"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section id="mobile-support">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport">
                                        <thead>
                                        <tr>
                                            <th>Campaign Name</th>
                                            <th>Phone</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="broadcast in broadcasts" :key="broadcast.id">
                                            <td>@{{ broadcast.campaign_name }}</td>
                                            <td>@{{ broadcast.phone }}</td>
                                            <td>@{{ broadcast.status }}</td>
                                            <td>@{{ broadcast.created_at | datetime }}</td>
                                            <td>
                                                <a :href="broadcast.file_url" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
                                                <a href="#" @click.prevent="showStatuses(broadcast)" class="btn btn-primary btn-sm">Status</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Campaign Name</th>
                                            <th>Phone</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div id="statusesModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">Broadcast Status</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport" v-if="statuses.length">
                                <thead>
                                <tr>
                                    <th>Session ID</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Failure Reason</th>
                                    <th>Delivery Time</th>
                                    <th>Carrier</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="status in statuses" :key="status.id">
                                    <td>@{{ status.session_id }}</td>
                                    <td>@{{ status.phone }}</td>
                                    <td>@{{ status.status }}</td>
                                    <td>@{{ status.failure_reason }}</td>
                                    <td>@{{ status.delivery_time }}</td>
                                    <td>@{{ status.carrier }}</td>
                                </tr>
                                </tbody>
                            </table>
                            <p class="text-center" v-else>Statuses not found.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </broadcasts>

@endsection