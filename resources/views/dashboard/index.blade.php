@extends('layouts.default')

@section('content')

@inject('dashboard', 'App\Services\Dashboard')

@role('finder')
    @include('dashboard._finder_dashboard')
@endrole

@role(['admin', 'super-user', 'user', 'bird-dog-deal-finder'])
    <div class="row match-height">
    <div class="col-xl-4 col-lg-6 col-md-12">
        <div class="card" style="padding-bottom: 0">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title" style="margin-bottom: 8px">Quick Links</h4>
                    <span style="color: #838383"><em>Quickly access commonly used links below</em></span>
                </div>
                <div class="list-group list-group-flush">
                    <a href="{{ route('dashboard.onboarding') }}" class="list-group-item">
                        <span class="tag tag-default tag-pill bg-warning float-xs-right">GO</span>
                        <strong> ONBOARDING TUTORIAL</strong>
                    </a>
                    <a href="{{ route('deals.create') }}" class="list-group-item">
                        <span class="tag tag-default tag-pill bg-warning float-xs-right">GO</span>
                        <strong> ADD A NEW DEAL</strong>
                    </a>
                    <a href="{{ route('deals.index') }}" class="list-group-item">
                        <span class="tag tag-default tag-pill bg-warning float-xs-right">GO</span>
                        <strong> MANAGE EXISTING DEALS</strong>
                    </a>
                    <a href="{{ route('contacts.create') }}" class="list-group-item">
                        <span class="tag tag-default tag-pill bg-warning float-xs-right">GO</span>
                        <strong>ADD A NEW CONTACT</strong>
                    </a>
                    <a href="{{ route('resources.support') }}" class="list-group-item">
                        <span class="tag tag-default tag-pill bg-warning float-xs-right">GO</span>
                        <strong>GET HELP FROM THE SUPPORT TEAM</strong>
                    </a>
                    <a href="{{ route('calendar.index') }}" class="list-group-item">
                        <span class="tag tag-default tag-pill bg-warning float-xs-right">GO</span>
                        <strong>CALENDAR <small>({{ \App\Event::forUser()->today()->count() }} events today)</small></strong>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xs-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                        @include('dashboard.partials.total_contacts')   
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                        @include('dashboard.partials.total_deals')     
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                        @include('dashboard.partials.total_closed_deals')
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                        @include('dashboard.partials.total_wholesale_deals')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endrole

@role('admin')
    <div class="row match-height">
        <div class="col-xl-12 col-lg-6 col-md-12">
            <div class="card card-inverse card-success">
                <div class="card-body">
                    
                    <div class="card-block p-b-0">
                        <h4 class="card-title">ADMIN STATISTICS</h4>
                        
                            <div class="row my-1 p-b-0">
                                <div class="col-lg-2 col-xs-12">
                                    <div class="text-xs-left card-text">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalContactsAll() }}</span></h3>
                                        <p class="text-muted-light">Total Contacts</p>
                                        <div id="sp-tristate-bar-total-revenue"></div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left ">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalCashBuyersAll() }}</span></h3>
                                        <p class="text-muted-light">Total Cash Buyers <span class="danger"></span></p>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalUsers }}</span></h3>
                                        <p class="text-muted-light">Total Users</p>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left">
                                        <h3><span class="tag tag-default tag-pill bg-primary"> {{$dashboard->totalTeams }}</span></h3>
                                        <p class="text-muted-light">Total Investor Teams</p>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalClosedDealsAll() }}</span></h3>
                                        <p class="text-muted-light">Total Closed Deals </p>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left">
                                        <h3><span class="tag tag-default tag-pill bg-primary">
                                        {{ formatPrice($dashboard->totalWholeSaleFeesCollectedAll()) }}
                                            </span></h3>
                                        <p class="text-muted-light">Wholesale Income</p>
                                        
                                    </div>
                                </div>
                            </div>   
                    </div>
  
                </div>
            </div>
            
        </div>
    </div>
@endrole

@role(['admin', 'super-user', 'user', 'bird-dog-deal-finder'])
    <div class="row match-height">
        <div class="col-xl-8 col-xs-12">
            <div class="card">
                <div class="card-header no-border">
                    <h4 class="card-title">Recent Contacts</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div id="goal-list-scroll" class="table-responsive height-250 position-relative">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Category</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dashboard->latestContacts as $contact)
                                <tr>
                                    <td>{{ $contact->full_name }}</td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->mobile_phone }}</td>
{{--                                    <td>{{ $contact->category->name ?? ''}}</td>--}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card">
                <div class="card-header no-border">
                    <h4 class="card-title">Deal Finders</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="col-md-12">
                        <p>Your confirmed Deal Finders will be listed below (if available).</p>
                    </div>

                    <div class="w-100 d-inline-block height-250 overflow-auto">
                        @foreach($dashboard->dealFinders() as $finder)
                            <div>
                                <div class="col-md-3 m-t-20">
                                    <img src="{{ $finder->photoURL() }}" alt="{{ $finder->name }}" class="avatar-lrg avatar user-img">
                                </div>
                                <div class="col-md-9 m-t-20">
                                    <span class="user-name">{{ $finder->name }}</span>
                                    <div class="clearfix"></div>
                                    <span class="user-name">Deals: {{ $dashboard->dealsCountByFinder($finder->id) }}</span>
                                    <div class="clearfix"></div>

                                    @if($finder->state && $finder->country)
                                        <span class="tag tag-default tag-pill bg-warning">{{ $finder->state }}, {{ $finder->country }}</span>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row match-height">
        <div class="col-xl-12 col-xs-12">
            <div class="card">
                <div class="card-header no-border">
                    <h4 class="card-title">Contract Period Timer</h4>
                </div>
                <div class="card-body">
                    <div id="goal-list-scroll" class="table-responsive height-250 position-relative">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Address</th>
                                <th>Contract Period</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dashboard->dealsOnContractPeriod() as $deal)
                                <tr class="font-small-3">
                                    <td class="font-small-2"><a href="{{ route('deals.show', $deal->id) }}">{{ $deal->getFullAddress() }}</a></td>
                                    <td>
                                        Expires in {{ $deal->contract_period->diffInDays(Carbon\Carbon::today()) }} Days on {{ $deal->contract_period->format('m/d/Y') }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    @include('dashboard.partials.latest_deals')
        
    </div>   
        
        
     <div class="row">
         <div class="col-lg-12 col-xs-12">
            <div class="card">
        <div class="card-header no-border">
            <h4 class="card-title">Deals Map</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-body">
             <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAnV96rN0iDZCoSf5UTQPXXV43J5Hzb9HE' type='text/javascript'></script><script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
                <script type='text/javascript' src='https://www.google.com/jsapi'></script>
                <script type='text/javascript'>google.charts.load('42', {'packages':['geochart']});
                google.charts.setOnLoadCallback(drawVisualization);

                  function drawVisualization() {var data = new google.visualization.DataTable();

                 data.addColumn('string', 'Country');
                 data.addColumn('number', 'Value'); 
                 data.addColumn({type:'string', role:'tooltip'});var ivalue = new Array();

                 @php($map_addresses = getMapAddresses())
                         var insertingData = [
                             @foreach($map_addresses as $property)
                             [{v:'{{ $property->address }} {{ $property->city }} {{$property->state }}',f:'{{ $property->address }} {{ $property->city }}, {{$property->state }} {{$property->postal_code }}'},0,'Investor: {{ $property->user->first_name }} {{$property->deal_type }} Deal ({{$property->stage }}) {{ formatPrice($property->wholesale_fee) }}'],
                             @endforeach
                         ];
                        data.addRows(insertingData);

                 var options = {
                 backgroundColor: {fill:'#FFFFFF',stroke:'#FFFFFF' ,strokeWidth:0 },
                 colorAxis:  {minValue: 0, maxValue: 1,  colors: ['#59d853','#FF0000']},
                 legend: 'none',	
                 datalessRegionColor: '#f5f5f5',
                 displayMode: 'markers', 
                 enableRegionInteractivity: 'true', 
                 resolution: 'provinces',
                 sizeAxis: {minValue: 1, maxValue:1,minSize:3,  maxSize: 3},
                 region:'US',
                 keepAspectRatio: true,
                 width:null,
                 height:null,
                 tooltip: {textStyle: {color: '#444444'}, trigger:'focus', isHtml: false}	
                 };
                  var chart = new google.visualization.GeoChart(document.getElementById('visualization')); 
                 chart.draw(data, options);
                 }
                 window.onresize = function(event) {
                     drawVisualization();
                 };
                 </script>
                 <div id='visualization'></div>
        </div>
             </div>

             <div class="card">
                 <div class="card-body">
                     <div class="col-md-12">
                         <form action="{{ route('users.update-map-link', $user->id) }}" method="POST">
                             {!! csrf_field() !!}

                             <div class="form-group">
                                 <label>Google Map Link</label>
                                 <input type="text" name="google_map_link" value="{{ $user->google_map_link }}" class="form-control input-sm">
                             </div>
                             <div class="form-group">
                                 <button class="btn btn-primary btn-sm">Save</button>
                             </div>
                         </form>
                     </div>
                     <div class="clearfix"></div>

                     @if($user->google_map_link)
                         <div class="col-md-12">
                             <iframe src="{{ $user->google_map_link }}" style="height: 480px; width: 100%; border: none;"></iframe>
                         </div>
                        <div class="clearfix"></div>
                     @endif
                 </div>
             </div>
        </div>
    </div>
@endrole
	
@endsection

@section('scripts')	

	<script src="/app-assets/vendors/js/extensions/jquery.knob.min.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/extensions/knob.js" type="text/javascript"></script>

@endsection