
@extends('layouts.default')

 @section('content')

	<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Onboarding</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item active">Onboarding
                  </li>
                </ol>
              </div>
            </div>
          </div>
          <div class="content-header-right col-md-6 col-xs-12">
            
          </div>
    </div>

<div class="content-body"><!-- Open main content section -->
    <section id="number-tabs">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Welcome </h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in ">
                    <div class="card-block">
                        
                        <div class="col-md-6">
                                <iframe src="https://player.vimeo.com/video/532922519" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                        </div>
                        
                        <div class="col-md-6 p-l-40">
                            <h4 >Quick Introduction </h4>
                            We're happy to have you become a part of the Deal Manager Pro family. <BR><BR>Please take a look at the welcome video to the left and the startup checklist below. We are in the process of adding additional videos and we will be conducting a series of webinars to get you up to speed.<BR><BR>
                            In the meantime, take a look at the startup checklist and complete them so you get familiar with the application.
                        </div>
                            
                        <div class="col-md-12 m-t-40">
                            <hr>
                        </div>
                        
                        <div class="col-md-6">
                        <h4>Dashboard Walkthrough </h4>
                            Take a look at the video below for an overview of the Dashboard.
                            <iframe class="p-t-20" src="https://player.vimeo.com/video/532922498" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                            
                        <div class="col-md-6 p-l-40">
                            <h4>Add New Contacts </h4>
                            Take a look at the video below for the steps how to add new contacts into DMP.
                            <iframe class="p-t-20" src="https://player.vimeo.com/video/532922478" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                            
                        <div class="col-md-12 m-t-40">
                            <hr>
                        </div>
                            
                        <div class="col-md-6">
                        <h4>Add New Contact Groups </h4>
                            Take a look at the video below for the steps how to add new contacts into DMP
                            <iframe class="p-t-20" src="https://player.vimeo.com/video/532922489" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                            
                        <div class="col-md-6 p-l-40">
                            
                            
                        </div>
                            
                        
                        
                            
                            
                            
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End main content section -->
</div>
    

 @endsection