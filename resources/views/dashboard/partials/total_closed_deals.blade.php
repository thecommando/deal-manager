{{-- Total users --}}

<div class="my-1 text-xs-center">
    <div class="card-header mb-2 pt-0">
        <h5 class="warning">Closed Deals</h5>
        <h3 class="font-large-2 text-bold-200">{{ $dashboard->totalClosedDeals }} 
            <!--<span class="font-medium-1 grey darken-1 text-bold-400">kcal</span>-->
        </h3>
    </div>
    <div class="card-body">
        <input type="text" value="{{ $dashboard->totalClosedDeals }}" class="knob hide-value responsive angle-offset" data-angleOffset="20" data-thickness=".15" data-linecap="round" data-width="130" data-height="130" data-inputColor="#BABFC7" data-readOnly="true" data-fgColor="#FFA87D" data-knob-icon="icon-energy">
        <ul class="list-inline clearfix pt-1 mb-0">
            <li>
                <h2 class="grey darken-1 text-bold-400">
                    <span>{{ formatPrice($dashboard->totalWholeSaleFeesCollected()) }}</span>
                </h2>
                <span class="warning"><span class="fa fa-money"></span> Wholesale Income</span>
            </li>
        </ul>
    </div>
</div>
