<div class="col-lg-12 col-xs-12">
    <div class="card">
        <div class="card-header no-border">
            <h4 class="card-title">Latest Deals</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <div id="goal-list-scroll" class="table-responsive height-250 position-relative">
                <table class="table">
            <thead>
                <tr>
                    <th>Address</th>
                    <th>Property Type</th>
                    <th>Deal Stage</th>
                    <th>Offer Price</th>
                    <th>Estimated ARV</th>
                    <th>Equity Position</th>
                    <th>Added By</th>
                </tr>
            </thead>
            <tbody>
                @foreach($dashboard->latestDeals as $deal)
                <tr class="font-small-3">
                    <td class="font-small-2"><a href="{{ route('deals.show', $deal->id) }}">{{ $deal->getFullAddress() }}</a></td>
                    <td>{{ $deal->type }}</td>
                    <td><span class="label label-success">{{ $deal->stage }}</span></td>
                    <td>{{ formatPrice($deal->offer_price) }}</td>
                    <td>{{ formatPrice($deal->after_repair_value) }}</td>
                    <td>{{ $deal->equity_position }}%</td>
                    <td>{{ $deal->finder ? $deal->finder->name : $deal->user->name }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
            </div>
        </div>
    </div>
</div>
