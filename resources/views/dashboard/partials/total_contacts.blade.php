{{-- Total users --}}

<div class="my-1 text-xs-center">
    <div class="card-header mb-2 pt-0">
        <h5 class="primary">Contacts</h5>
        <h3 class="font-large-2 text-bold-200">{{ $dashboard->totalContacts() }}</h3>
    </div>
    <div class="card-body">
        <input type="text" value="{{ $dashboard->totalContacts() }}" class="knob hide-value responsive angle-offset" data-angleOffset="40" data-thickness=".15" data-linecap="round" data-width="130" data-height="130" data-inputColor="#BABFC7" data-readOnly="true" data-fgColor="#00B5B8" data-knob-icon="icon-trophy">
        <ul class="list-inline clearfix pt-1 mb-0">
            <li class="">
                
                <h2 class="grey darken-1 text-bold-400" data-plugin="counterup">{{ $dashboard->totalCashBuyers() }}</h2>
                <span class="primary"><span class="fa fa-credit-card"></span> Cash Buyers </span>
            </li>

        </ul>
    </div>
</div>