{{-- Total users --}}
<div class="col-md-4">
	<div class="card-box">
		<h4 class="header-title m-t-0 m-b-30">Total Deals Closed</h4>

		<div class="widget-chart-1">
			<div class="widget-chart-box-1">
				<input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
				       data-bgColor="#F9B9B9" value="{{ $dashboard->totalClosedDealsAll() }}"
				       data-skin="tron" data-angleOffset="180" data-readOnly=true
				       data-thickness=".15"/>
			</div>

			<div class="widget-detail-1">
				<h2 class="p-t-10 m-b-0">$<span data-plugin="counterup"> {{ $dashboard->totalWholeSaleFeesCollectedAll() }}</span> </h2>
				<p class="text-muted">Wholesale Income</p>
			</div>
		</div>
	</div>
</div><!-- end col -->