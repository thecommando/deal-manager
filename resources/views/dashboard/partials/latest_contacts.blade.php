<div class="col-md-12">
	<div class="card-box">
		<h4 class="header-title m-t-0 m-b-30">Latest Contacts</h4>

		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Mobile</th>
						<th>Category</th>
					</tr>
				</thead>
				<tbody>
					@foreach($dashboard->latestContacts as $contact)
					<tr>
						<td>{{ $contact->full_name }}</td>
						<td>{{ $contact->email }}</td>
						<td>{{ $contact->mobile_phone }}</td>
						<td>{{ $contact->category->name }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div><!-- end col -->