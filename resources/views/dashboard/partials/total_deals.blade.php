{{-- Total users --}}

<div class="my-1 text-xs-center">
    <div class="card-header mb-2 pt-0">
        <h5 class="danger">Open Deals</h5>
        <h3 class="font-large-2 text-bold-200">{{ $dashboard->totalOpenDeals() }} 
            <!--<span class="font-medium-1 grey darken-1 text-bold-400">deals</span> -->
        </h3>
    </div>
    <div class="card-body">
        <input type="text" value="{{ $dashboard->totalOpenDeals() }}" class="knob hide-value responsive angle-offset" data-angleOffset="0" data-thickness=".15" data-linecap="round" data-width="130" data-height="130" data-inputColor="#BABFC7" data-readOnly="true" data-fgColor="#FF7588" data-knob-icon="icon-pointer">
        <ul class="list-inline clearfix pt-1 mb-0">
            <li>
                <h2 class="grey darken-1 text-bold-400" data-plugin="counterup"> {{ $dashboard->totalDealsOnContract() }} </h2>
                <span class="danger"><span class="fa fa-file-text-o"></span> Under Contract Deals</span>
            </li>
        </ul>
    </div>
</div>