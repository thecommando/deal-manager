@extends('layouts.default')

@section('content')
	@inject('dashboard', 'App\Services\CoachDashboard')

	<div class="row match-height">
        <div class="col-xl-4 col-lg-6 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h4 class="card-title">Quick Links</h4>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="{{ route('dashboard.onboarding') }}">
                                <span class="tag tag-default tag-pill bg-warning float-xs-right">GO</span></a>
                                <strong> ONBOARDING TUTORIAL</strong>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('deals.create') }}">
                                <span class="tag tag-default tag-pill bg-primary float-xs-right">GO</span></a>
                                <strong> ADD A NEW DEAL</strong>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('deals.index') }}">
                                <span class="tag tag-default tag-pill bg-info float-xs-right">GO</span></a>
                                <strong> MANAGE EXISTING DEALS</strong>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('contacts.create') }}">
                                <span class="tag tag-default tag-pill bg-danger float-xs-right">GO</span></a>
                                <strong>ADD A NEW CONTACT</strong>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('resources.support') }}">
                                <span class="tag tag-default tag-pill bg-success float-xs-right">GO</span></a>
                                <strong>GET HELP FROM THE SUPPORT TEAM</strong>
                        </li>
                    </ul>
                    <div class="card-block">
                        <p class="card-text"><em>Quickly access commonly used links above</em></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                            @include('dashboard.partials.total_contacts')
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                            @include('dashboard.partials.total_deals')
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                            @include('dashboard.partials.total_closed_deals')
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                            @include('dashboard.partials.total_wholesale_deals')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @role('coach')
    <div class="row match-height">
        <div class="col-xl-12 col-lg-6 col-md-12">
            <div class="card card-inverse card-success">
                <div class="card-body">

                    <div class="card-block p-b-0">
                        <h4 class="card-title">COACH ADMIN STATISTICS</h4>

                            <div class="row my-1 p-b-0">
                                <div class="col-lg-2 col-xs-12">
                                    <div class="text-xs-left card-text">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalContactsAll() }}</span></h3>
                                        <p class="text-muted-light">Total Contacts</p>
                                        <div id="sp-tristate-bar-total-revenue"></div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left ">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalCashBuyersAll() }}</span></h3>
                                        <p class="text-muted-light">Total Cash Buyers <span class="danger"></span></p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalUsers }}</span></h3>
                                        <p class="text-muted-light">Total Team Members</p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalNewDealsAll() }}</span></h3>
                                        <p class="text-muted-light">Total Open Deals* </p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left">
                                        <h3><span class="tag tag-default tag-pill bg-primary">{{ $dashboard->totalClosedDealsAll() }}</span></h3>
                                        <p class="text-muted-light">Total Closed Deals </p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-12 card-text">
                                    <div class="text-xs-left">
                                        <h3>
                                            <span class="tag tag-default tag-pill bg-primary">
                                                {{ formatPrice($dashboard->totalWholeSaleFeesCollectedAll()) }}
                                            </span>
                                        </h3>
                                        <p class="text-muted-light">Wholesale Income</p>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    @endrole
    
    <div class="row match-height">
        <div class="col-xl-8 col-xs-12">
            <div class="card">
                <div class="card-header no-border">
                    <h4 class="card-title">Recent Contacts</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div id="goal-list-scroll" class="table-responsive height-250 position-relative">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Category</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dashboard->latestContacts as $contact)
                                <tr>
                                    <td>{{ $contact->full_name }}</td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->mobile_phone }}</td>
                                    <td>{{ $contact->category->name }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card">
                <div class="card-header no-border">
                    <h4 class="card-title">Information Card</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">

                </div>
            </div>
        </div>

    </div>

    <div class="row">
        @include('dashboard.partials.latest_deals')
    </div>
@endsection

@section('scripts')	
	<script src="/app-assets/vendors/js/extensions/jquery.knob.min.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/extensions/knob.js" type="text/javascript"></script>
@endsection