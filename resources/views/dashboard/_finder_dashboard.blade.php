<div class="row match-height">

    <div class="col-xl-4 col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <p class="card-text">
                    <div class="my-1 text-xs-center">
                        <div class="card-header mb-0 m-b-0 pt-0">
                            <h5 class="primary">INVESTOR PARTNERS</h5>
                            <span class="card-text font-small-1">Number of investors you are submitting deals for</span>
                            <h3 class="font-large-2 text-bold-200">{{ $dashboard->totalInvestorPartners() }}</h3>
                        </div>

                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <p class="card-text">
                    <div class="my-1 text-xs-center">
                        <div class="card-header mb-0 m-b-0 pt-0">
                            <h5 class="primary">TOTAL DEALS SENT</h5>
                            <span class="card-text font-small-1">Property leads you've submitted</span>
                            <h3 class="font-large-2 text-bold-200">{{ $dashboard->finderDealsCount() }}</h3>
                        </div>

                    </div>
                    </p>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <p class="card-text">
                    <div class="my-1 text-xs-center">
                        <div class="card-header mb-0 m-b-0 pt-0">
                            <h5 class="primary">TOTAL DEALS CLOSED</h5>
                            <span class="card-text font-small-1">Property leads that have been purchased</span>
                            <h3 class="font-large-2 text-bold-200">{{ $dashboard->finderClosedDealsCount() }}</h3>
                        </div>

                    </div>
                    </p>
                </div>

            </div>
        </div>
    </div>

</div>
<div class="row match-height">
    <div class="col-xl-4 col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">Investor Partners</h4>
                    <p class="card-text">The following are the investors you have partnered up with to submit deals. The deals you've submited for these investors will be listed to the right.</p>

                    @foreach($dashboard->investorPartners() as $partner)
                        <div class="col-md-3 m-t-20">
                            <img src="{{ $partner->photoURL() }}" alt="{{ $partner->first_name }}" class="avatar-lrg avatar user-img">
                        </div>
                        <div class="col-md-9 m-t-20">
                            <span class="user-name">
                                <h5>{{ $partner->first_name }} <span class="font-small-1 tag tag-default tag-pill tag-success"><a href="{{ route('integration.deals.create', ['api_key' => $partner->api_key, 'finder_key' => $dashboard->user->api_key]) }}" target="_blank"> Add New Deal <i class="fa fa-external-link"> </i> </a></span></h5>
                            </span>

                            <div class="clearfix"></div>
                            @if($partner->state && $partner->country)
                            <p class="font-small-2 m-t-0">Currently based in {{ $partner->city }}, {{ $partner->state }}.</p>
                            @endif
                            <div class="clearfix"></div>
                            
                        </div>
                        
                    
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-8">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">Current Deal Status</h4>
                </div>

                <div id="goal-list-scroll card-block" class="table-responsive height-250 position-relative">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Address</th>
                            <th>City</th>
                            <th>BR/BA</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dashboard->finderDeals() as $deal)
                            <tr>
                                <td>{{ $deal->address }}</td>
                                <td>{{ $deal->city }}</td>
                                <td></td>
                                <td>{{ $deal->status }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row match-height">
    <div class="col-xl-4 col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">Deal Finder Training</h4>

                    <p class="card-text">The best skill you can have is finding great deals for your Investor Partners. The videos below will help get you on the right track.
                    </p>

                </div>

            </div>
        </div>
    </div>

    <div class="col-xs-8">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">Documents</h4>

                    <p class="card-text">The following documents have been added to your account.
                    </p>

                </div>


            </div>
        </div>
    </div>
</div>