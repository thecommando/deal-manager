@extends('layouts.default')

@section('content')
    <div class="card-box">
        <h4 class="m-t-0 header-title"><b>Welcome</b></h4>
        <p>Your Application's Landing Page.</p>
    </div>
@endsection
