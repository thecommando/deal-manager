@extends('layouts.default')

@section('styles')
    <style>
        .ms-container {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')
<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Edit Group Members</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="{{ route('contact-groups.index') }}">Manage Contact Groups</a></li>
                  <li class="breadcrumb-item active">Edit Group Members</li>
                </ol>
              </div>
            </div>
          </div>    
    </div>


<div class="row">
    
    <div class="col-md-6">
        <div class="content-body">

        <!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Edit: {{ $contactGroup->name }}'S MEMBERS</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        {!! Form::open(['route' => ['contact-groups.update-members', $contactGroup->id], 'method' => 'PUT']) !!}

                        <div class="form-group">
                            <select name="contacts[]" class="multi-select" id="contact-list" multiple>
                                @foreach($allContacts as $contact)
                                    <option value="{{ $contact['id'] }}" {{ $contactGroup->contacts->contains($contact) ? 'selected' : '' }}>
                                        {{ $contact['first_name'] }} {{ $contact['last_name'] }}
                                        @if($contact['price_range_min'] && $contact['price_range_max'] && $contact['location'])
                                            (${{ $contact['price_range_min'] }} - ${{ $contact['price_range_max'] }} at {{ $contact['location'] }})
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <!--- Submit Field --->
                        <div class="form-group">
                            {!! Form::submit('Save Changes', ['class' => 'btn btn-success']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

        </div>
    </div>
    
    <div class="col-md-6">
        <div class="content-body">

        <!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">ADDITIONAL INSTRUCTIONS</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        <p>Use the dialog box to the left to add members to {{ $contactGroup->name }} group.</p>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
		$(document).ready(function () {
			$('#contact-list').multiSelect({
				selectableHeader: "<p>All Contacts</p><input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
				selectionHeader: "<p>Group Contacts</p><input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
				afterInit: function (ms) {
					var that = this,
							$selectableSearch = that.$selectableUl.prev(),
							$selectionSearch = that.$selectionUl.prev(),
							selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
							selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

					that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
							.on('keydown', function (e) {
								if (e.which === 40) {
									that.$selectableUl.focus();
									return false;
								}
							});

					that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
							.on('keydown', function (e) {
								if (e.which == 40) {
									that.$selectionUl.focus();
									return false;
								}
							});
				},
				afterSelect: function () {
					this.qs1.cache();
					this.qs2.cache();
				},
				afterDeselect: function () {
					this.qs1.cache();
					this.qs2.cache();
				}
			});
		});
	</script>
@endsection