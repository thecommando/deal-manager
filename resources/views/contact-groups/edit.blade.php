@extends('layouts.default')

@section('content')

	<!-- Page-Title -->
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Edit Contact Group</h4>
		</div>
	</div>

	{!! Form::model($contactGroup, ['route' => ['contact-groups.update', $contactGroup->id], 'method' => 'PUT']) !!}
	<div class="row">

		<div class="col-md-12">
			@include('partials.errors')
		</div>

		@include('contact-groups._form', ['submitButtonText' => 'Update'])

	</div>
	<!-- end row -->
	{!! Form::close() !!}
@endsection