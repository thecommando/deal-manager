@extends('layouts.default')

@section('content')

    <!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Manage Contact Groups</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('contact-groups.index') }}">Contacts</a>
                  </li>
                  <li class="breadcrumb-item active">My Contact Groups
                  </li>
                </ol>
              </div>
            </div>
          </div>
          <div class="content-header-right col-md-6 col-xs-12">
            <div class="media width-250 float-xs-right">
              <div class="media-left media-middle"></div>
              <div class="media-body media-right text-xs-right">
                <div class="btn-group pull-right m-t-15 hidden-print">
                <!-- Add Contacts Button -->
                <a href="{{ route('contact-groups.create') }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="left" title="" data-original-title="Add New Contact Group"><span class=""><i class="fa fa-plus"></i></span></a>
            	</div>
              </div>
            </div>
          </div>
    </div>

    <section id="mobile-support">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<p class="card-text">The following contact groups are in your database.</p>
						<table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport">
							<thead>
								<tr>
                                    <th>Name</th>
                                    <th>Created At</th>
                                    <th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($contactGroups as $contactGroup)
									<tr>
										<td>{{ $contactGroup->name }}</td>
										<td>{{ $contactGroup->created_at->format('M d, Y h:i a') }}</td>
										<td>
											<a href="{{ route('contact-groups.edit', $contactGroup->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{ $contactGroup->first_name }}'s Details"><i class="fa fa-pencil"></i></a>
											<a href="{{ route('contact-groups.edit-members', $contactGroup->id) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Add/Edit Group Members"><i class="fa fa-user"></i></a>
											<a href="{{ route('contact-groups.delete', $contactGroup->id) }}" class="btn btn-sm btn-danger" onclick="if(!confirm('Are you sure to delete this contact group?')){return false;};"><i class="fa fa-close"></i></a>
										</td>
									</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
                                    <th>Name</th>
                                    <th>Created At</th>
                                    <th>Action</th>
								</tr>
							</tfoot>
						</table>				
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection