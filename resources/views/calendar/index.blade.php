@extends('layouts.default')

@section('styles')
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- Page-Title -->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-xs-12 mb-0.5">
                    <h3 class="content-header-title mb-0">Calendar</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Calendar</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-deck">
                {!! Form::open(['route' => 'events.store']) !!}
                    <div class="form-group input-group-sm">
                        {!! Form::label('title', 'Title:') !!} <span class="red">*</span>
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group input-group-sm">
                        {!! Form::label('date', 'Date:') !!} <span class="red">*</span>
                        {!! Form::date('date', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group input-group-sm">
                        {!! Form::label('remind_at', 'Remind At:') !!}
                        {!! Form::date('remind_at', null, ['class' => 'form-control']) !!}
                    </div>

                    @role(['admin', 'super-user'])
                    <div class="form-group input-group-sm">
                        <label>
                            {!! Form::checkbox('is_global', 1, null) !!} Global?
                        </label>
                    </div>
                    @endrole

                    <div>@include('partials.errors')</div>

                    <div class="form-group input-group-sm text-right">
                        <a type="button" href="{{route('calendar.index')}}" class="btn btn-default">Cancel</a>
                        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
                    </div>
                {!! Form::close() !!}
            </div>

            <div class="card card-deck">
                @php
                    $clientID =  env('GOOGLE_CLIENT_ID');
                    $redirectURL = env('GOOGLE_CLIENT_REDIRECT_URL');

                    $login_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . $redirectURL . '&response_type=code&client_id=' . $clientID . '&access_type=offline';

                    $login_url_refresh = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($redirectURL) . '&response_type=code&client_id=' . $clientID . '&access_type=offline&prompt=consent';                   
                @endphp

                @if (!empty(session()->get('access_token_expiry')) && time() > session()->get('access_token_expiry'))                   
                     <a href="{{ $login_url_refresh }}" class="btn btn-lg btn-primary">Google Login</a>
                @else
                    @if (!empty(session()->get('access_token')))
                        <a href="{{ route('sync-gcalendar.create') }}" class="btn btn-lg btn-primary">Sync Google Calendar</a>
                    @else
                         <a href="{{ $login_url }}" class="btn btn-lg btn-primary">Google Login</a>
                    @endif
                @endif
            </div>
        </div>

        <div class="col-md-9">
            <calendar></calendar>
        </div>
    </div>
@endsection