@extends('layouts.default')

@section('styles')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/select.dataTables.min.css">

@endsection


@section('content')
<users inline-template>
    <div>
        <!-- Page-Title -->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Build My Team</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ route('my-team.index') }}">Build My Team</a>
                            </li>
                            <li class="breadcrumb-item active">list
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-xs-12">
                <div class="media width-250 float-xs-right">
                    <div class="media-left media-middle">
                    </div>
                    <div class="media-body media-right text-xs-right">
                        <div class="btn-group pull-right m-t-15 hidden-print">
                            <!-- Add Templates Button -->
                            <a href="{{ route('my-team.create') }}" class="btn btn-success waves-effect waves-light m-r-5"><span class="">Add New</span></a>                 
                        </div>
                    </div>
                </div>
            </div> 
        </div>


        <section id="compact-style">
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">My Team</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="card-body collapse in">
                            <div class="card-block card-dashboard">
                                <p></p>
                                <table class="table table-striped table-bordered compact data-table">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Active</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>
                                                <img src="{{ $user->photoURL() }}" class="img img-responsive img-avatar" width="40px" alt="{{ $user->name }}">
                                            </td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->username }}</td>
                                            
                                            <td><span class="small">{{ $user->email }}</span></td>
                                            <td>
                                                @foreach($user->roles as $role)
                                                    <span class="tag tag-default">{{ $role->display_name }}</span>
                                                @endforeach
                                            </td>
                                            <td>
                                                <input type="checkbox" data-size="small" v-on:change="toggle({!! $user->id !!})" data-plugin="switchery" data-color="#98A6AD" {!! $user->isActive() ? 'checked' : '' !!}>
                                            </td>
                                            <td>
                                                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit {{ $user->name }}'s Details"><i class="fa fa-pencil"></i></a>
                                                
                                                @if($user->trashed())
                                                    <a href="{{ route('users.restore', $user->id) }}" class="btn btn-sm btn-success" onclick="if(!confirm('Are you sure to restore this user?')){return false;};" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Restore {{ $user->name }}"><i class="fa fa-refresh"></i></a>
                                                @else
                                                    <a href="{{ route('users.delete', $user->id) }}" class="btn btn-sm btn-danger" onclick="if(!confirm('Are you sure to delete this user?')){return false;};" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete {{ $user->name }}"><i class="fa fa-close"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Base -->
    </div>
</users>

@endsection