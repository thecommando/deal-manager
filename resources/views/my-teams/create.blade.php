@extends('layouts.default')

@section('content')

<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Add New</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Build My Team</a>
                  </li>
                  <li class="breadcrumb-item active">Add New
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>

	{!! Form::open(['route' => 'my-team.store', 'files' => true]) !!}
	<section id="compact-style">
    <div class="row">

		<div class="col-md-12">
			@include('partials.errors')
		</div>		
		<div class="col-md-12">
			<div class="card">
                <div class="card-header">
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						</ul>
					</div>
				</div>
                
                <div class="card-body collapse in">
					<div class="card-block">
                        <h4 class="subtitle-text">Personal Information</h4>

                        <div class="row">
                            <div class="col-md-3">
                                <label>Profile Picture:</label>
                                <div class="clearfix"></div>
                                <label class="btn btn-warning btn-xs m-t-10 btn-file">
                                    Upload Photo <input type="file" name="profile" style="display: none;" />
                                </label>
                            </div>

                            <div class="col-md-9">
                                <!--- Name Field --->
                                <div class="form-group input-group-sm col-sm-4">
                                    {!! Form::label('name', 'Name:') !!} <span class="red">*</span>
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>

                                <!--- Email Field --->
                                <div class="form-group input-group-sm col-sm-8">
                                    {!! Form::label('email', 'Email:') !!} <span class="red">*</span>
                                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                </div>

                                <!--- Username Field --->
                                <div class="form-group input-group-sm col-sm-4">
                                    {!! Form::label('username', 'Username:') !!} <span class="red">*</span>
                                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                                </div>

                                <!--- Mobile phone Field --->
                                <div class="form-group input-group-sm col-sm-4">
                                    {!! Form::label('mobile_phone', 'Mobile Phone:') !!}
                                    {!! Form::text('mobile_phone', null, ['class' => 'form-control']) !!}
                                </div>

                                <!--- Office phone Field --->
                                <div class="form-group input-group-sm col-sm-4">
                                    {!! Form::label('office_phone', 'Office Phone:') !!}
                                    {!! Form::text('office_phone', null, ['class' => 'form-control']) !!}
                                </div>

                                <!--- Website Field --->
                                <div class="form-group input-group-sm col-sm-12">
                                    {!! Form::label('website', 'Website:') !!}
                                    {!! Form::text('website', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <hr>

                        <h4 class="m-t-0 m-b-20 subtitle-text">Location Information</h4>

                        <div class="row">
                            <!--- Address Field --->
                            <div class="form-group input-group-sm col-sm-8">
                                {!! Form::label('address', 'Address:') !!}
                                {!! Form::text('address', null, ['class' => 'form-control']) !!}
                            </div>

                            <!--- City Field --->
                            <div class="form-group input-group-sm col-sm-4">
                                {!! Form::label('city', 'City:') !!}
                                {!! Form::text('city', null, ['class' => 'form-control']) !!}
                            </div>

                            <!--- State Field --->
                            <div class="form-group input-group-sm col-sm-4">
                                {!! Form::label('state', 'State:') !!}
                                {!! Form::select('state', [], null, ['class' => 'form-control']) !!}
                            </div>

                            <!--- Country Field --->
                            <div class="form-group input-group-sm col-sm-4">
                                {!! Form::label('country', 'Country:') !!}
                                {!! Form::select('country', country_options(), null, ['class' => 'form-control']) !!}
                            </div>

                            <!--- Postal code Field --->
                            <div class="form-group input-group-sm col-sm-4">
                                {!! Form::label('postal_code', 'Postal Code:') !!}
                                {!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <hr/>

                        <!-- <h4 class="m-t-0 m-b-20 font-600">Select Access Roles <span class="red">*</span></h4> -->

                        <div class="form-group">
                            <select name="roles[]" class="select2 select2-multiple d-none" data-placeholder="Roles ...">
                                @foreach($roles as $id => $role)
                                    <option value="{{ $id }}" @if($id == 3) selected @endif>{{ $role }}</option>
                                @endforeach
                            </select>
                        </div>

                        <!--- Save Changes Field --->
                        <div class="form-group input-group-sm m-t-30 text-right">
                            {!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
                            <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                        </div>
                    </div>
                </div>
                
			</div>
		</div>
	</div>
    </section>
	<!-- end row -->
	{!! Form::close() !!}
@endsection

@section('scripts')
	@include('partials.location-script', ['country' => null, 'state' => null])

    <script src="/assets/js/jquery.maskMoney.min.js"></script>
    <script src="/assets/js/jquery.mask.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#mobile_phone').mask('(000) 000-0000');
            $('#office_phone').mask('(000) 000-0000');
        });
    </script>
@endsection