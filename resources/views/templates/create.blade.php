@extends('layouts.default')

@section('content')

    <!-- Page-Title -->
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Create Template</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                    <li class="breadcrumb-item"><a href="{{ route('manage-templates.index') }}">Templates</a>
                  </li>
                  <li class="breadcrumb-item active">Create Template</li>
                </ol>
              </div>
                <div class="col-xs-12">@include('partials.errors')</div>
            </div>
         </div>
    </div>

    <div class="content-detached">
        <div class="content-body">

            <!-- Main Content Area -->
            <section id="description" class="card">
                <div class="card-header">
                    <h4 class="card-title">Add New Template</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                {!! Form::open(['route' => 'manage-templates.store']) !!}
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="card-text">
                            @include('templates._form', ['submitButtonText' => 'Save'])
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </section>
            <!--/ Main Content Area -->

        </div>
    </div>

@endsection
@section('scripts')
	<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
    <script>
        class MyUploadAdapter {
        constructor( loader ) {
            // CKEditor 5's FileLoader instance.
            this.loader = loader;

            // URL where to send files.
            this.url = '{{route("image.upload")}}';
        }

        // Starts the upload process.
        upload() {
            return this.loader.file
                .then( file => new Promise( ( resolve, reject ) => {
                    this._initRequest();
                    this._initListeners( resolve, reject, file );
                    this._sendRequest( file );
                } ) );
        }

        // Aborts the upload process.
        abort() {
            if ( this.xhr ) {
                this.xhr.abort();
            }
        }


        _initRequest() {
            const xhr = this.xhr = new XMLHttpRequest();

            xhr.open( 'POST', this.url ,true);
            xhr.responseType = 'json';
        }


        _initListeners( resolve, reject,file ) {
            const xhr = this.xhr;
            const loader = this.loader;
            const genericErrorText = 'Couldn\'t upload file:' + ` ${ file.name }.`;

            xhr.addEventListener( 'error', () => reject( genericErrorText ) );
            xhr.addEventListener( 'abort', () => reject() );
            xhr.addEventListener( 'load', () => {
                const response = xhr.response;

                if ( !response || response.error ) {
                    return reject( response && response.error ? response.error.message : genericErrorText );
                }
                //console.log(response);
                // If the upload is successful, resolve the upload promise with an object containing
                // at least the "default" URL, pointing to the image on the server.
                resolve( {
                    default: response.url
                } );
            } );

            if ( xhr.upload ) {
                xhr.upload.addEventListener( 'progress', evt => {
                    if ( evt.lengthComputable ) {
                        loader.uploadTotal = evt.total;
                        loader.uploaded = evt.loaded;
                    }
                } );
            }
        }

        // Prepares the data and sends the request.
        _sendRequest(file) {
            const data = new FormData();
            //console.log(this.loader.file);
            data.append('upload', file );
        //csrf_token CSRF protection
            //data.append('csrf_token', requestToken);

            this.xhr.send( data );
        }
    }
    function MyCustomUploadAdapterPlugin( editor ) {
        editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
            return new MyUploadAdapter( loader );
        };
    }

    function init_ckeditor(selector,timeout){
        ClassicEditor
            .create( document.querySelector(selector ),{
                extraPlugins: [ MyCustomUploadAdapterPlugin ],
            } )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    }   

    init_ckeditor('#message' );
		// ClassicEditor
        //     .create( document.querySelector( '#message' ), {
        //         ckfinder: {
        //             uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
        //         },
		// 		removePlugins: [ 'Blockquote' ],
        //         // plugins: [ ... , ImageInsert ],
		// 		// toolbar: [
		// 		// 	'Image',
		// 		// 	'bold',
		// 		// 	'italic',
		// 		// 	'link',
		// 		// 	'unlink'
		// 		// ]
		// 	})
        //     .catch( error => {
        //         console.error( error );
        //     } );
    </script>
@endsection