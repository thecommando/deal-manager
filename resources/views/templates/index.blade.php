@extends('layouts.default')

@section('content')
<!-- Page-Title -->
<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-2">
      <h3 class="content-header-title mb-0">Manage Templates</h3>
      <div class="row breadcrumbs-top">
        <div class="breadcrumb-wrapper col-xs-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('manage-templates.index') }}">Templates</a>
            </li>
            <li class="breadcrumb-item active">My Templates
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div class="content-header-right col-md-6 col-xs-12">
      <div class="media width-250 float-xs-right">
        <div class="media-left media-middle">

        </div>
        <div class="media-body media-right text-xs-right">
          <div class="btn-group pull-right m-t-15 hidden-print">
						<!-- Add Contacts Button -->
						<a href="{{ route('manage-templates.create') }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="left" title="" data-original-title="Add New Template"><span class=""><i class="fa fa-plus"></i></span></a>
					</div>
        </div>
      </div>
    </div>
</div>

<section id="mobile-support">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<p class="card-text">The following templates are in your database. You can use these message template list to send email contacts.</p>
						<table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport">
							<thead>
								<tr>									
									<th>Template Name</th>									
									<th>Message</th>									
									<th>Created At</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($templates as $template)								
									<tr>
										<td>{{ $template->name ?? 'N/A' }}</td>
										<td>{!! nl2br(html_entity_decode(e($template->message))) !!}</td>										
										<td>{{ $template->created_at->format('M d, Y h:i a') }}</td>
										<td>
											<a href="{{ route('manage-templates.edit', $template->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{ $template->name }}'s Details"><i class="fa fa-pencil"></i></a>		

											<a href="{{ route('copyTemplate', $template->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Copy Template Row"><i class="fa fa-clone"></i></a>

											<a  class="btn btn-sm btn-danger"
											    href="{{ route('manage-templates.destroy', ['template' => $template]) }}"
											    onclick="if(confirm('Are you sure to delete this template?')){event.preventDefault();
											    document.getElementById('delete-form-{{ $template->id }}').submit();}">
											    <i class="fa fa-trash"></i>
											</a>
											{!! Form::open(['route' => ['manage-templates.destroy', $template->id], 'method' => 'DELETE', 'style' => 'display:none;', 'id' => "delete-form-".$template->id]) !!}
											
											    {{ csrf_field() }}    											
											{!! Form::close() !!}
										</td>
									</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Template Name</th>									
									<th>Message</th>									
									<th>Created At</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>				
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection