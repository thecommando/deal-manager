@if (Session::has('flash_message'))
    <script type="text/javascript">
        swal({
            title: "{{ session('flash_message.title') }}",
            text: "{{ session('flash_message.text') }}",
            type: "{{ session('flash_message.type') }}",
            timer: 1700,
            showConfirmButton: false
        });
    </script>
@endif

@if (Session::has('flash_overlay'))
    <script type="text/javascript">
        function htmlDecode(input){
            var e = document.createElement('textarea');
            e.innerHTML = input;
            // handle case of empty input
            return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
        }
        let overlayMessage = htmlDecode("{{ session('flash_overlay.text') }}");
        swal({
            title: "{{ session('flash_overlay.title') }}",
            text: overlayMessage,
            type: "{{ session('flash_overlay.type') }}",
            confirmButtonText: "Ok"
        });
    </script>
@endif