<script>
	$(document).ready(function () {
		var $country = $("#{{ isset($country_id) ? $country_id : 'country' }}"),
			$state = $("#{{ isset($state_id) ? $state_id : 'state' }}");

		$state.change(function () {
			if (this.value === '') {
				$state.val('');
			}
		});

		$country.change(function () {
			$.get('/countries/' + this.value + '/states', function (states) {
				// Clear state select
				$state.find('option').remove().end();

				$state.append('<option value="">-- Select --</option>');

				$(states).each(function (key, state) {
					$state.append('<option value="' + state + '">' + state + '</option>');
				});

				// Set state if provided
				$state.val('{{ $state ?: '' }}').change();

				Bus.$emit('countries-fetched');
			});
		});

		$country.val('{{ $country ?: 'United States' }}').change();
	});
</script>