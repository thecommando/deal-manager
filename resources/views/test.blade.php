@extends('layouts.default')

@section('content')

	<div class="container">
		<form method="POST" action="http://deal-manager.dev/integration/form/contacts" accept-charset="UTF-8">
			<div class="row">
				<input name="api_key" type="hidden" value="e9030098def6270cdf609a3e2783a855">
				<input name="redirect_to" type="hidden" value="http://example.com/contacts">

				<!--- Category id Field -->
				<div class="form-group input-group-sm col-sm-12">
					<select class="form-control hidden" name="category_id"><option value="1">Attorney</option><option value="2">Broker</option><option value="3">Bird Dog</option><option value="4">Builder</option><option value="5">Buyer Lead</option><option value="6" selected="selected">Cash Buyer</option><option value="7">Contractor</option><option value="8">Hard Money Lender</option><option value="9">Inspector</option><option value="10">Insurance</option><option value="11">Mentor</option><option value="12">Private Money Investor</option><option value="13">Property Manager</option><option value="14">Real Estate Agent</option><option value="15">REI Club Member</option><option value="16">Seller</option><option value="17">Title Agent</option></select>
				</div>

				<!--- First Name Field -->
				<div class="form-group input-group-sm col-sm-6">
					<label for="first_name">First Name:</label>
					<input class="form-control" name="first_name" type="text" id="first_name">
				</div>

				<!--- Last name Field -->
				<div class="form-group input-group-sm col-sm-6">
					<label for="last_name">Last Name:</label>
					<input class="form-control" name="last_name" type="text" id="last_name">
				</div>

				<!--- Email Field -->
				<div class="form-group input-group-sm col-sm-6">
					<label for="email">Email:</label>
					<input class="form-control" name="email" type="email" id="email">
				</div>

				<!--- Mobile phone Field -->
				<div class="form-group input-group-sm col-sm-6">
					<label for="mobile_phone">Mobile Phone:</label>
					<input class="form-control" name="mobile_phone" type="text" id="mobile_phone">
				</div>

			</div>

			<!--- Save Changes Field -->
			<div class="form-group input-group-sm input-group-sm m-t-20 text-right">
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

		</form>

	</div>

@endsection