<div class="card-box">
	<h5 class="m-t-0 m-t-20 subtitle-text">PERSONAL INFORMATION</h5>

	<div class="row">
		<!--- Category id Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('category_id', 'Category:') !!} <span class="red">*</span>
			{!! Form::select('category_id', ['' => '-- Select --'] + \App\ContactCategory::pluck('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
		</div>

		<!--- First Name Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('first_name', 'First Name:') !!} <span class="red">*</span>
			{!! Form::text('first_name', null, ['class' => 'form-control']) !!}
		</div>

		<!--- Last name Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('last_name', 'Last Name:') !!}
			{!! Form::text('last_name', null, ['class' => 'form-control']) !!}
		</div>
	</div>
	<div class="row">

		<!--- Email Field --->
		<div class="form-group input-group-sm col-sm-8">
			{!! Form::label('email_address', 'Email:') !!} <span class="red">*</span>
			{!! Form::email('email_address', null, ['class' => 'form-control']) !!}
		</div>

		<!--- Mobile phone Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('phone', 'Mobile Phone:') !!}
			{!! Form::text('phone', null, ['class' => 'form-control']) !!}
		</div>

		<!--- Alternate phone Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('alternate_phone', 'Alternate Phone:') !!}
			{!! Form::text('alternate_phone', null, ['class' => 'form-control']) !!}
		</div>

	</div>

	<h5 class="m-t-0 m-t-20 subtitle-text">Location Information</h5>

	<div class="row">
		<!--- Address Field --->
		<div class="form-group input-group-sm col-sm-8">
			{!! Form::label('site_address', 'Address:') !!}
			{!! Form::text('site_address', null, ['class' => 'form-control']) !!}
		</div>

		<!--- City Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('site_city', 'City:') !!}
			{!! Form::text('site_city', null, ['class' => 'form-control']) !!}
		</div>

		<!--- State Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('site_state', 'State:') !!}
			{!! Form::select('site_state', [], null, ['class' => 'form-control']) !!}
		</div>

		<!--- Country Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('country', 'Country:') !!}
			{!! Form::select('country', country_options(), null, ['class' => 'form-control']) !!}
		</div>

		<!--- Postal code Field --->
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('site_zip_code', 'Postal Code:') !!}
			{!! Form::text('site_zip_code', null, ['class' => 'form-control']) !!}
		</div>


	</div>

	<h5 class="m-t-0 m-t-20 subtitle-text">Additional Information</h5>
    
	<div class="row">
		<!--- Note Field --->
		<div class="form-group input-group-sm col-sm-12">
			{!! Form::label('note', 'Notes:') !!}
			{!! Form::textarea('note', null, ['class' => 'form-control']) !!}
		</div>
        
		{{-- Price range minimum --}}
        
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('price_range_min', 'Minimum Price Range:') !!}
            <div class="clearfix m-t-0 m-b-0"></div>
            <span class="font-small-2 m-t-0 text-muted">Enter your low end price for properties </span>
			{!! Form::text('price_range_min', null, ['class' => 'form-control']) !!}
		</div>

		{{-- Price range maximum --}}
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('price_range_max', 'Maximum Price Range:') !!}
            <div class="clearfix"></div>
            <span class="font-small-2 m-t-0 text-muted">Enter your high end price limit for properties </span>
			{!! Form::text('price_range_max', null, ['class' => 'form-control']) !!}
		</div>

		{{-- Location --}}
		<div class="form-group input-group-sm col-sm-4">
			{!! Form::label('location', 'Location:') !!}
            <div class="clearfix"></div>
            <span class="font-small-2 m-t-0 text-muted">Enter your prefered location(s) for properties </span>
			{!! Form::text('location', null, ['class' => 'form-control']) !!}
		</div>
	</div>

	<!--- Save Changes Field --->
	<div class="form-group input-group-sm input-group-sm m-t-30 text-right">
		{!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
		<button type="button" onclick="goBack()" class="btn btn-default">Cancel</button>
	</div>
</div>