@extends('layouts.default')

@section('content')
<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Contact Upload</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                    <li class="breadcrumb-item"><a href="{{ route('contacts.index') }}">Contact</a>
                  </li>
                  <li class="breadcrumb-item active">Upload
                  </li>
                </ol>
              </div>
                <div class="col-xs-12">@include('partials.errors')</div>
            </div>
          </div> 
          <div class="content-header-right col-md-6 col-xs-12">
            
          </div>
    </div>

{!! Form::open(['route' => 'contacts.upload', 'files' => true, 'class' => 'filUploadForm']) !!}
<div class="content-body"><!-- Card headings examples section start -->
<section id="card-headings">
    
	<div class="row card-deck-wrapper match-height">
		<div class="loadingImg">
			<img src="../images/loading.gif" alt="">
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="card card-deck">
				<div class="card-header" id="heading-links">
					<h4 class="card-title">DOWNLOAD XLS TEMPLATE</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="card-block">
					<h4 class="m-t-0 m-b-20 font-300">Contact Template </h4>
				    <p class="font-light">Click the icon below to download the template that should be used to upload contacts into the system. Once you've downloaded the template, open MS Excel, add your contacts in and then use the form to the right to load them into the system. </p>
				
				    <div class="text-center">
				        <a href="{{ url('samples/contacts.xlsx') }}"><i class="fa fa-file-excel-o fa-4x"></i></a>
				    </div>
						<br>
						<p><strong>NOTE</strong>: Once you've downloaded the template then please cross check the latest category list before any upload.</p>
						<ul>
							<li><small><strong>{{ implode(', ', $categoryList) }}</strong></small></li>
							<li><small><strong>Please don't create the sheet 1, sheet 2 at bottom right in excel sheet template (there should be simple excel sheet)</strong></small></li>
						</ul>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="card card-deck">
				<div class="card-header">
					<h4 class="card-title" id="heading-labels">UPLOAD XLS FILE</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
        			<div class="heading-elements">
						<ul class="list-inline mb-0">
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
					</div>
				</div>

				<div class="form-group categoriesUploadPages">
					<div class="col-md-4 mb-4">
						<label for="validationCustom04">Contact Group</label>
						<select name="contacts_group" class="contactGroupOptions custom-select form-control col-sm-2">
							<option selected value="">Select Contact Group</option>
							@foreach($contactGroups as $contact)
								<option value="{{ $contact->id }}" >
									{{ $contact->name }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="btn-group m-t-15 hidden-print">
						<a href="#" id="createContactGroup" data-target="#createContactGroupModal" data-placement="bottom" title="" data-original-title="Add New Contact" class="btn btn-success waves-effect waves-light m-r-5">
							<span><i class="fa fa-plus"></i></span>
						</a>
					</div>
				</div>
				<div class="form-group categoriesUploadPages">
					<div class="col-md-4 mb-4">
						<label for="validationCustom04">Categories</label>
						<select name="category" class="categoryOptions custom-select form-control col-sm-2">
							<option selected value="">Select Category</option>
							@foreach($categoryList as $key => $value)
								<option value="{{ $key }}" >
									{{ $value }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="btn-group m-t-15 hidden-print">
						<a href="#" id="createCategory" data-toggle="tooltip" data-target="#createCategoryModal"  data-placement="bottom" title="" data-original-title="Add New Category" class="btn btn-success waves-effect waves-light m-r-5">
							<span><i class="fa fa-plus"></i></span>
						</a>
					</div>
				</div>

				<div class="card-block">
					<div class="form-group">
					   <h4 class="m-t-0 m-b-20 font-300">Upload XLS File:</h4>
					   <p class="font-light">Make sure you only upload your contacts using the file template to the left. No other template will work with this system. </p>
					
					   {!! Form::file('file', ['class' => 'fileToUpload dropify']) !!}
{{--						hidden input --}}
						<input type="hidden" name="file_name" id="fileUploadRouteUrl" value="{{ route('contacts.upload-ajax') }}">
				    </div>

					<div id="differentValues">
						<div>
							<table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport">
								<thead>
									<tr>
										<th>Your Fields</th>
										<th>Upload Fields</th>
									</tr>
								</thead>
								<tbody id="uploadFieldsTableBody">

								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group input-group-sm">
						<label for="stackedCheck1">Excel has heading</label>
						<input id="stackedCheck1" name="heading" type="checkbox" value="1" checked>
					</div>

					<!--- Submit Field --->
                    <div class="form-group input-group-sm m-t-30 text-right">
                        {!! Form::submit('Upload', ['class' => 'btn btn-success']) !!}
                        <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                    </div>
				</div>

			</div>
		</div>
	</div>
    </section>
</div>
{!! Form::close() !!}
<div class="modal fade" id="createContactGroupModal" tabindex="-1" role="dialog" aria-labelledby="inactiveModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			{!! Form::open(['route' => 'contact-groups.store', 'id' => 'contactGroupForm']) !!}
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						@include('contact-groups._modal-form', ['submitButtonText' => 'Save'])
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="createCategoryModal" tabindex="-1" role="dialog" aria-labelledby="inactiveModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			{!! Form::open(['route' => 'categories.store', 'id' => 'categoryForm']) !!}
			<div class="card-body collapse in">
				<div class="card-block">
					<div class="card-text">
						@include('categories._modal-form', ['submitButtonText' => 'Save'])
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection