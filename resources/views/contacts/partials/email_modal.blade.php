@inject('emailTemplate', 'App\Services\ContactEmailTemplate')

<contact-email inline-template>
<div id="emailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Email</h4>
			</div>
			{!! Form::open(['route' => ['contacts.email']]) !!}
			<div class="modal-body">

				<div class="form-group">
					<p><label>Send To?</label></p>
					<label>
						<input type="radio" v-model="to" name="to" value="contacts">
						Contacts
					</label>

					<label class="ml-1">
						<input type="radio" v-model="to" name="to" value="group">
						Contact Group
					</label>

					<label class="ml-1">
						<input type="radio" v-model="to" name="to" value="contact_category">
						Contact Category
					</label>

					<label class="ml-1">
						<input type="radio" v-model="to" name="to" value="other">
						Other Contact
					</label>
				</div>

				<!--- Contact Field --->
				<div class="form-group" v-show="to === 'contacts'">
				    {!! Form::label('contact', 'Contact:') !!}
				    {!! Form::select('contact[]', get_contacts(), null, ['class' => 'form-control select2 select2-multiple', 'multiple']) !!}
				</div>

				{{-- Contact Group --}}
				<div class="form-group" v-show="to === 'group'">
					{!! Form::label('group', 'Contact Group:') !!}
					{!! Form::select('group', ['-- Select --'] + \App\ContactGroup::where('user_id', auth()->id())->pluck('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
				</div>

				{{-- Contact Category --}}
				<div class="form-group" v-show="to === 'contact_category'">
					{!! Form::label('contact_category', 'Contact Category:') !!}
					{!! Form::select('contact_category', ['-- Select --'] + \App\ContactCategory::pluck('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
				</div>

				{{-- Other --}}
				<div class="form-group" v-show="to === 'other'">
					{!! Form::label('other', 'Email:') !!}
					{!! Form::email('other', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Subject Field --->
				<div class="form-group">
				    {!! Form::label('subject', 'Subject:') !!}
				    {!! Form::text('subject', null, ['class' => 'form-control']) !!}
				</div>

				
				<!--- Message Field --->
				<div class="form-group">
				    {!! Form::label('message', 'Message:') !!}
					<select id="m-t" v-model="selectedTemplate" class="form-control" @change="selectTemplate($event)">
						<option value="">-- Select message template --</option>
						@foreach($emailTemplate->all() as $title => $template)
							<option value="{{ $template }}" data-title="{{ $title }}">{{ $title }}</option>
						@endforeach
						{{--<option v-for="template in templates" :value="template">@{{ template }}</option>--}}
					</select>				   
				</div>
				<div class="form-group">
					 {!! Form::textarea('message', null, ['class' => 'form-control', 'id' => 'mail-message', 'v-model' => 'message']) !!}
					<strong>NOTE: <small>Please <a target="_blank" href="/manage-templates">click here</a> to modify template details.</small></strong>
					</div>
				<div class="form-group" v-show="template_title == '' || template_title == null">
					<label>Template Name</label>
				   <input type="text" name="title" class="form-control" v-model="new_title">
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" id="save-template-btn" class="btn btn-primary waves-effect" style="float: left;" @click.prevent="saveAsTemplate()">Save As Template</button>
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary waves-effect waves-light">Send</button>
			</div>
			{!! Form::close() !!}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</contact-email>