<div id="transferContactModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Transfer Contact</h4>
			</div>
			{!! Form::open(['route' => ['contacts.transfer']]) !!}
			<div class="modal-body">

				{!! Form::hidden('contact_id', null, ['id' => 'contact_id']) !!}

				<!--- Contact Field --->
				<div class="form-group">
				    {!! Form::label('user_id', 'User:') !!}
				    {!! Form::select('user_id', \App\User::orderBy('name')->pluck('name', 'id'), null, ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary waves-effect waves-light">Transfer</button>
			</div>
			{!! Form::close() !!}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->