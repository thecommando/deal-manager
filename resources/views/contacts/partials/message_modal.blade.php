<contact-message inline-template>
<div id="messageModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Message</h4>
			</div>
			{!! Form::open(['route' => ['contacts.message']]) !!}
			<div class="modal-body">
				<div class="form-group">
					<p><label>Send To?</label></p>
					<label>
						<input type="radio" v-model="to" name="to" value="contacts">
						Contacts
					</label>

					<label class="ml-1">
						<input type="radio" v-model="to" name="to" value="group">
						Contact Group
					</label>

					<label class="ml-1">
						<input type="radio" v-model="to" name="to" value="contact_category">
						Contact Category
					</label>
				</div>

				<!--- Contact Field --->
				<div class="form-group" v-show="to === 'contacts'">
				    {!! Form::label('contact', 'Contact:') !!}
				    {!! Form::select('contact[]', get_contacts(), null, ['class' => 'form-control select2 select2-multiple', 'multiple']) !!}
				</div>

				{{-- Contact Group --}}
				<div class="form-group" v-show="to === 'group'">
					{!! Form::label('group', 'Contact Group:') !!}
					{!! Form::select('group', ['-- Select --'] + \App\ContactGroup::where('user_id', auth()->id())->pluck('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
				</div>

				{{-- Contact Category --}}
				<div class="form-group" v-show="to === 'contact_category'">
					{!! Form::label('contact_category', 'Contact Category:') !!}
					{!! Form::select('contact_category', ['-- Select --'] + \App\ContactCategory::pluck('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
				</div>

				{{-- Other --}}
				<div class="form-group" v-show="to === 'other'">
					{!! Form::label('other', 'Number:') !!}
					{!! Form::text('other', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Message Field --->
				<div class="form-group">
				    {!! Form::label('message', 'Message:') !!}
				    {!! Form::textarea('message', null, ['class' => 'form-control', 'id' => 'sms-message', 'v-model' => 'message', 'placeholder' => 'Enter message...']) !!}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary waves-effect waves-light">Send</button>
			</div>
			{!! Form::close() !!}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</contact-message>