@extends('layouts.default')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">

@endsection
@section('content')

	@include('contacts.partials.email_modal')
	@include('contacts.partials.message_modal')
	@include('contacts.partials.transfer_contact_modal')

		<!-- Page-Title -->
	<div class="content-header row">
					<div class="content-header-left col-md-6 col-xs-12 mb-2">
						<h3 class="content-header-title mb-0">Contacts</h3>
						<div class="row breadcrumbs-top">
							<div class="breadcrumb-wrapper col-xs-12">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ route('contacts.index') }}">Contacts</a>
									</li>
									<li class="breadcrumb-item active">My Contacts
									</li>
								</ol>
							</div>
						</div>
					</div>
					<div class="content-header-right col-md-6 col-xs-12">
						<div class="media width-250 float-xs-right">
							<div class="media-left media-middle">
							</div>
							<div class="media-body media-right text-xs-right">
								<div class="btn-group pull-right m-t-15 hidden-print">
					<!-- Add Contacts Button -->
					<a href="{{ route('contacts.create') }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Add New Contact"><span class=""><i class="fa fa-plus"></i></span></a>

					<!-- Upload Contacts Button -->
					<a href="{{ route('contacts.upload') }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Upload Contact Sheet"><span class=""><i class="fa fa-cloud-upload"></i></span></a>

					<!-- Message Template -->
					<a href="{{ route('manage-templates.index') }}" class="btn btn-success waves-effect waves-light m-r-5 p-0" >
						<span style="display:block; padding: .75rem 1rem !important;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Manage Templates">
							<i class="fa fa-list-ul"></i>
						</span>
					</a>
					<!-- Email Contacts  -->
					<button type="button" class="btn btn-success waves-effect waves-light m-r-5 p-0" data-toggle="modal" data-target="#emailModal">
						<span style="display:block; padding: .75rem 1rem !important;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Send Email">
							<i class="fa fa-envelope-o"></i>
						</span>
					</button>

					<!-- Message Contacts  -->
					<button type="button" class="btn btn-success waves-effect waves-light m-r-5 p-0" data-toggle="modal" data-target="#messageModal">
						<span style="display:block; padding: .75rem 1rem !important;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Send SMS">
							<i class="fa fa-comment-o"></i>
						</span>
					</button>

				</div>
							</div>
						</div>
					</div>
		</div>

		<section id="mobile-support">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<p class="card-text">The following people are in your database. Do your best to keep in contact with them so they don't go cold.</p>
						<button type="button" class="btn btn-success waves-effect waves-light delete_all mb-1"><span class="">Delete All</span></button>
						<table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport" id="dataTable" style="width:100%">
							<thead>
								<tr>
									<th width="50px"><input type="checkbox" id="select-all-chk"></th>
									<th>Category</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Contact Groups</th>
									<th>Email</th>
									<th>Owner</th>
									<th>Mobile</th>
									<th>Created At</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
	<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: "{{ route('contacts.index', request()->has('all') ? ['all' => 'true'] : '') }}",
				type: 'GET',
			},
			columns: [
				{data: 'id', name: 'id' , render: function (data, type, row, meta) {
						return '<input type="checkbox" class="row-check" data-id="' + data + '">';
					}, orderable: false, searchable: false},
				{data: 'category', name: 'category.name', render: function(data, type, row, meta) {
						if (data && data.name) {
							return data.name;
						} else {
							return "";
						}
					}},
				{data: 'first_name', name: 'first_name'},
				{data: 'last_name', name: 'last_name'},
				{data: 'assigned_group', name: 'assigned_group',searchable:false, orderable:false},
				{data: 'email_address', name: 'email_address'},
				{data: 'user', name: 'user.name', render: function(data, type, row, meta) {
						if (data && data.name) {
							return data.name;
						} else {
							return "";
						}
					}},
				{data: 'phone', name: 'phone'},
				{data: 'created_at', name: 'created_at'},
				{data: 'action', name: 'action', orderable: false, searchable: false, render: function(data, type, row, meta) {
					return data;
					}
				}
			],
		});
	});






		let theEditor;

		ClassicEditor
			.create( document.querySelector( '#mail-message' ), {
				removePlugins: [ 'Blockquote', 'Image' ],
                // plugins: [ ... , ImageInsert ],
				toolbar: [
					// 'insertImage',
					'bold',
					'italic',
					'link',
					'unlink'
				]
			})
			.then(editor => {
				theEditor = editor;

			})
            .catch( error => {
                console.error( error );
            } );

			function getDataFromTheEditor() {
				return theEditor.getData();
			}
			function send_1(data) {
				var testing = data;
				theEditor.setData(testing);
			}
			$('#m-t').on('change', function(e) {
				send_1($(this).val());
			});
    </script>
	<script>
		$(document).ready(function() {

			$(document).on('click', ".contact-transfer", function(e) {
				$("#contact_id").val($(this).data("contact-id"));
			})

			$('#save-tempalte').on('click', function() {
					$.ajax({
							method: 'POST',
							url: '{{route("saveTemplate")}}',
							data: {
								subject: $('#subject').val(),
								message: $('#mail-message').val(),
								_token: "{{ csrf_token() }}"
							},
							success: function () {
								location.reload();
							},
							error: function(errors) {
								alert(errors);
							}
					});
			});
		});

		$(document).ready(function () {
			$('#select-all-chk').on('click', function(e) {
				if($(this).is(':checked',true)) {
					$(".row-check").prop('checked', true);
				} else {
					$(".row-check").prop('checked',false);
				}
			});

			$('.delete_all').on('click', function(e) {
				var checkedValues = [];

				$(".row-check:checked").each(function() {
						checkedValues.push($(this).attr('data-id'));
				});

				if(checkedValues.length <=0) {
					bootbox.alert("Please select atleast one contact to delete.");
				}  else {
					bootbox.confirm({
						title: "Delete Contacts?",
						size: "large",
						message: "Warning: This action cannot be undone. Are you sure?",
						buttons: {
							confirm: {
								label: 'Delete',
								className: 'btn-success ml-1'
							},
							cancel: {
								label: 'Cancel',
								className: 'btn-danger'
							}
						},
						callback: function (result) {
							if (result) {
								/* your callback code */
								var concated_values = checkedValues.join(",");

								$.ajax({
									url: '{{route("delete-all-contacts")}}',
									type:'DELETE',
									headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
									beforeSend:function() {
										$('.delete_all').text('Please wait...');
									},
									data: {
										'ids' : concated_values,
										'_token': $('meta[name="csrf-token"]').attr('content')
									},
									success: function (data) {
										if (data['success']) {
											$(".row-check:checked").each(function() {
													$(this).parents("tr").remove();
											});
											bootbox.alert(data['success']);
											window.location.reload();
										} else if (data['error']) {
											bootbox.alert(data['error']);
										} else {
											bootbox.alert('Whoops Something went wrong!!');
										}
										$('.delete_all').text('Delete All');
									},
									error: function (data) {
										$('.delete_all').text('Delete All');
										bootbox.alert(data.responseText);
									}
								});

								// $.each(checkedValues, function( index, value ) {
								// 		$('table tr').filter("[data-row-id='" + value + "']").remove();
								// });
							}
						}
					});
				}
			});
		});
	</script>
@endsection