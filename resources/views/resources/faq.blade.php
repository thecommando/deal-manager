@extends('layouts.default')

@section('content')

<!-- Page-Title -->
<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Frequently Asked Questions</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Resources</a>
                  </li>
                  <li class="breadcrumb-item active">FAQs
                  </li>
                </ol>
              </div>
            </div>
          </div>    
</div>

<div class="content-detached content-left">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block center">
                    <div class="card-text ">
                        
                        <div class="center">
                                       
                            <p class="text-muted"> The following are frequently asked questions that have been asked before. Review the topics below to see if your question is answered. If not, then feel free to use the button below to email us your specific question.</p>

                            <hr>

                        </div>
                        <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    
                                </div><!-- end col -->
                            </div><!-- end row -->

                            <div class="row m-t-40">
                                <div class="col-lg-6">
                                    <div class="p-20">
                                        <div class="panel-group m-b-0" id="accordion" role="tablist"
                                             aria-multiselectable="true">
                                            <div class="panel panel-default bx-shadow-none">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse"
                                                           data-parent="#accordion" href="#collapseOne"
                                                           aria-expanded="true" aria-controls="collapseOne">
                                                            What is Lorem Ipsum? <i class="fa fa-arrow-right"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in"
                                                     role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        Lorem Ipsum is simply dummy text of the printing and
                                                        typesetting industry. Lorem Ipsum has been the industry's
                                                        standard dummy text ever since the 1500s, when an unknown
                                                        printer took a galley of type and scrambled it to make a
                                                        type specimen book.

                                                        <br/> <br/>

                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                        accusamus terry richardson ad squid. 3 wolf moon officia
                                                        aute, non cupidatat skateboard dolor brunch. Food truck
                                                        quinoa nesciunt laborum eiusmod.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default bx-shadow-none">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion" href="#collapseTwo"
                                                           aria-expanded="false" aria-controls="collapseTwo">
                                                            Is safe use Lorem Ipsum? <i class="fa fa-arrow-right"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse"
                                                     role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        Lorem Ipsum is simply dummy text of the printing and
                                                        typesetting industry. Lorem Ipsum has been the industry's
                                                        standard dummy text ever since the 1500s, when an unknown
                                                        printer took a galley of type and scrambled it to make a
                                                        type specimen book.

                                                        <br/> <br/>

                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                        accusamus terry richardson ad squid. 3 wolf moon officia
                                                        aute, non cupidatat skateboard dolor brunch. Food truck
                                                        quinoa nesciunt laborum eiusmod.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default bx-shadow-none">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion" href="#collapseThree"
                                                           aria-expanded="false" aria-controls="collapseThree">
                                                            Why use Lorem Ipsum? <i class="fa fa-arrow-right"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse"
                                                     role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        Lorem Ipsum is simply dummy text of the printing and
                                                        typesetting industry. Lorem Ipsum has been the industry's
                                                        standard dummy text ever since the 1500s, when an unknown
                                                        printer took a galley of type and scrambled it to make a
                                                        type specimen book.

                                                        <br/> <br/>

                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                        accusamus terry richardson ad squid. 3 wolf moon officia
                                                        aute, non cupidatat skateboard dolor brunch. Food truck
                                                        quinoa nesciunt laborum eiusmod.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end col -->

                                <div class="col-lg-6">
                                    <div class="p-20">
                                        <div class="panel-group m-b-0" id="accordion1" role="tablist"
                                             aria-multiselectable="true">
                                            <div class="panel panel-default bx-shadow-none">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse"
                                                           data-parent="#accordion1" href="#faq4">
                                                            License & Copyright <i class="fa fa-arrow-right"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="faq4" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        Lorem Ipsum is simply dummy text of the printing and
                                                        typesetting industry. Lorem Ipsum has been the industry's
                                                        standard dummy text ever since the 1500s, when an unknown
                                                        printer took a galley of type and scrambled it to make a
                                                        type specimen book.

                                                        <br/> <br/>

                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                        accusamus terry richardson ad squid. 3 wolf moon officia
                                                        aute, non cupidatat skateboard dolor brunch. Food truck
                                                        quinoa nesciunt laborum eiusmod.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default bx-shadow-none">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion1" href="#faq5">
                                                            How many variations exist? <i class="fa fa-arrow-right"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="faq5" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Lorem Ipsum is simply dummy text of the printing and
                                                        typesetting industry. Lorem Ipsum has been the industry's
                                                        standard dummy text ever since the 1500s, when an unknown
                                                        printer took a galley of type and scrambled it to make a
                                                        type specimen book.

                                                        <br/> <br/>

                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                        accusamus terry richardson ad squid. 3 wolf moon officia
                                                        aute, non cupidatat skateboard dolor brunch. Food truck
                                                        quinoa nesciunt laborum eiusmod.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default bx-shadow-none">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion1" href="#faq6">
                                                            What is Lorem Ipsum? <i class="fa fa-arrow-right"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="faq6" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Lorem Ipsum is simply dummy text of the printing and
                                                        typesetting industry. Lorem Ipsum has been the industry's
                                                        standard dummy text ever since the 1500s, when an unknown
                                                        printer took a galley of type and scrambled it to make a
                                                        type specimen book.

                                                        <br/> <br/>

                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                        accusamus terry richardson ad squid. 3 wolf moon officia
                                                        aute, non cupidatat skateboard dolor brunch. Food truck
                                                        quinoa nesciunt laborum eiusmod.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end col -->
                            </div><!-- end row -->

                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->
                        

                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>

<!-- Sidebar Content Area -->
<div class="sidebar-detached sidebar-right">
    <div class="sidebar">
        <div class="sidebar-content card">
            <div class="card-block">
                <div class="category-title pb-1">
                    
                </div>
        
        <div class="text-xs-center">
            <img class="card-img-top mb-1 img-fluid img-circle" data-src="holder.js/100px180/" src="/app-assets/images/portfolio/portfolio-1.jpg" alt="Card image cap">
            <h4 class="card-title center">Support Team</h4>
        </div>
        
                <p class="card-text">If you have a question you think others would benefit from, email us at <span class="text-highlight">{{ config('application.support_email') }}</span> and we will review and add it to this page if we feel enough people would benefit. </p>
        

            </div>
        </div>

    </div>
</div>
<!--/ Sidebar Content Area -->
                
                
@endsection