@extends('layouts.default')

@section('content')

<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Support</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Resources</a>
                  </li>
                  <li class="breadcrumb-item active">Support
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>

<div class="content-detached content-left">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Get Support</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        
                    <div class="col-md-8">
                                <iframe src="https://player.vimeo.com/video/532922512" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                    </div>

                    <div class="col-md-4 p-l-40">
                        <h4 >Getting Support</h4>
                        <p>
                            Please review the FAQs below or use the chat icon in the lower right corner. <BR><BR>You may also email us at <strong><a href="mailto:{{ config('application.support_email') }}">{{ config('application.support_email') }}</a></strong> <BR><BR>
                            The <a href="{{ route('dashboard.onboarding') }}">onboarding page</a> has a list of videos you can use to get up to speed quickly.
                        </p>
                    </div>

                    <div class="col-md-12 m-t-40">
                        <hr>
                    </div>
                            
                    <div class="col-md-12 m-t-40">
                         <h2 class="m-b-20">Frequently Asked Questions</h2>
                    
                        <div class="panel-group m-b-0" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default bx-shadow-none">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseOne"
                                               aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-plus-square"></i> How do I use technical support?  
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in m-b-10"
                                         role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            You can click the support icon in the bottom-right corner of this page. Or email {{ config('application.support_email') }}
                                            <br/>
                                        </div>
                                    </div>
                                 </div>
                                <div class="panel panel-default bx-shadow-none m-t-20">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseTwo"
                                               aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="fa fa-plus-square"></i> When is Support Available?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse m-b-10"
                                         role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            Monday - Friday, 9:00AM - 6:00PM MST

                                            <br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default bx-shadow-none m-t-20">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseThree"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                <i class="fa fa-plus-square"></i> Is there anything I should do before contacting technical support?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse m-b-10"
                                         role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            To make the most use of time prior to contacting support, please make sure that you search through our FAQ’s and the Training section of the website.
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="panel panel-default bx-shadow-none m-t-20">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseFour"
                                               aria-expanded="false" aria-controls="collapseFour">
                                                <i class="fa fa-plus-square"></i> Can technical support help me with Marketing / Strategy / Traffic Questions?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse m-b-10"
                                         role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            It’s important for you to know that our technical support staff will not be able to provide you with any marketing or strategic advice.
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel panel-default bx-shadow-none m-t-20">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseFive"
                                               aria-expanded="false" aria-controls="collapseFive">
                                                <i class="fa fa-plus-square"></i> Where can I ask questions regarding billing or cancellation of my membership?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse m-b-10"
                                         role="tabpanel" aria-labelledby="headingFive">
                                        <div class="panel-body">
                                            You can click the support icon in the bottom-right corner of this page, in the app, or email us directly. Please email {{ config('application.support_email') }} for any questions regarding payments or cancellation of your DMP account. 
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="panel panel-default bx-shadow-none m-t-20">
                                    <div class="panel-heading" role="tab" id="headingSix">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseSix"
                                               aria-expanded="false" aria-controls="collapseSix">
                                                <i class="fa fa-plus-square"></i> Can I integrate my current website and have leads go directly into Deal Manager Pro? 
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse m-b-10"
                                         role="tabpanel" aria-labelledby="headingSix">
                                        <div class="panel-body">
                                            Yes. You can integrate your existing website and allow the data to transfer directly into DMP. This information can be found under your “Profile” tab and choose the “Integration” option.
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel panel-default bx-shadow-none m-t-20">
                                    <div class="panel-heading" role="tab" id="headingSeven">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseSeven"
                                               aria-expanded="false" aria-controls="collapseSeven">
                                                <i class="fa fa-plus-square"></i> Can I send emails from DMP? 
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse m-b-10"
                                         role="tabpanel" aria-labelledby="headingSeven">
                                        <div class="panel-body">
                                            Yes. Using our email wizard allows you to email out directly to your contacts entered or not entered into DMP. You can also choose our template options for easy to use emails.
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="panel panel-default bx-shadow-none m-t-20">
                                    <div class="panel-heading" role="tab" id="headingEight">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseEight"
                                               aria-expanded="false" aria-controls="collapseEight">
                                                <i class="fa fa-plus-square"></i> Can I manage a team under my account?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse m-b-10"
                                         role="tabpanel" aria-labelledby="headingEight">
                                        <div class="panel-body">
                                            Yes. DMP allows you to build a team under your user account. This is the perfect option for having property scouts/bird dogs looking for deals. They can send them directly into your database by using your referral link. The instructions on how to build a team of bird dogs are in the “Training” section.
                                        </div>
                                    </div>
                                </div>
                            
                            
                            </div>   
                        
                        
                    </div>      
                    

                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>

<!-- Sidebar Content Area -->
<div class="sidebar-detached sidebar-right">
          <div class="sidebar">
              
              <div class="sidebar-content card hidden-md-down">
                 
                    <div class="card-block">
                        <div class="text-xs-center">
                            
                            <i class="fa fa-cogs fa-5x"></i>
                            
                            <h4 class="card-title center p-t-20">SUPPORT TEAM</h4>
                            <p class="mb-1">{{ config('application.support_email') }}</p>
                        </div>

                        <p class="card-text">Please use the above email address for all support questions and issues you may have. We will have a series of webinars to help you get onboard with the software.</p>


                    </div>
              </div>

          </div>
</div>
<!--/ Sidebar Content Area -->

@endsection