@if($name == '564374668')
    <iframe style="width: 100%;height: 100%;" class="p-t-20" src="https://player.vimeo.com/video/564374668" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
@elseif($name == '565075814')
    <iframe style="width: 100%;height: 100%;" class="p-t-20" src="https://player.vimeo.com/video/565075814" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
@else
    <video controls="true" class="p-t-20" style="width: 100%;height: 100%;">
        <source src="/samples/{{ $name }}" type="video/mp4" />
    </video>
@endif