@extends('layouts.default')

@section('content')

<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Training Videos</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Resources</a>
                  </li>
                  <li class="breadcrumb-item active">Training Videos
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>

<div class="content-detached content-left">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Training Resources</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        
                        <p>We are developing our training videos with advanced instruction from our experts. This section will be updated frequently and we suggest to please check this area often. Please feel free to email us at <strong>{{ config('application.support_email') }}</strong> if you have any questions or issues. <BR><BR>
                        </p>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <table class="table table-bordered table-sm">
                                  <thead>
                                    <tr style="background-color:#5b9bd5; color:#fff;">
                                      <th scope="col">#</th>
                                      <th scope="col">Topic</th>
                                      <th scope="col">Link to video</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>                                      
                                      <td></td>
                                      <th>How to search for deals & use the Markerting Centre</th>
                                      <td></td>
                                    </tr>
                                    <tr>                                      
                                      <th>1</th>
                                      <td>Basic list search & export</td>
                                      <td><a href="{{ route('watch-training-video', 'Basic-list-search-and-export.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>                                      
                                      <th>2</th>
                                      <td>How to get detailed property information</td>
                                      <td><a href="{{ route('watch-training-video', 'detailed-property-info-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>                                      
                                      <th>3</th>
                                      <td>How to use Direct Mail</td>
                                      <td><a href="{{ route('watch-training-video', 'how-to-direct-mail-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>                                      
                                      <th>4</th>
                                      <td>Advanced list search & export</td>
                                      <td><a href="{{ route('watch-training-video', 'Advanced-list-search-and-export.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>                                      
                                      <th>5</th>
                                      <td>Where's my list?</td>
                                      <td><a href="{{ route('watch-training-video', 'wheres-my-list-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>                                      
                                      <th>6</th>
                                      <td>How to upload your lists</td>
                                      <td><a href="{{ route('watch-training-video', 'uploading-lists-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>                                      
                                      <th>7</th>
                                      <td>How to find comparables</td>
                                      <td><a href="{{ route('watch-training-video', 'quick-comps-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>                                      
                                      <td></td>
                                      <th>How to set up DMP to send emails</th>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <th scope="row">8</th>
                                      <td>How to set up a Send In Blue Account and integrate to DMP</td>
                                      <td><a href="{{ route('watch-training-video', 'send_in_blue_setup.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>
                                      <th scope="row">9</th>
                                      <td>How to send out emails to contacts</td>
                                      <td><a href="{{ route('watch-training-video', 'sending-email-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>                                      
                                      <td></td>
                                      <th>How to set up DMP to send text messages</th>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <th scope="row">10</th>
                                      <td>How to set up a Twillio Account and integrate to DMP</td>
                                      <td><a href="{{ route('watch-training-video', 'twilio_setup_2022.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>
                                      <th scope="row">11</th>
                                      <td>How to send SMS out to contacts</td>
                                      <td><a href="{{ route('watch-training-video', 'sending-sms-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>                                      
                                      <td></td>
                                      <th>How to set up DMP to send ringless voicemail</th>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <th scope="row">12</th>
                                      <td>Ringless VM 1 - How to set up a Sly Broadcasting account</td>
                                      <td><a href="{{ route('watch-training-video', 'ringless-VM-v1-setup-sly-broadcast account-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                    <tr>
                                      <th scope="row">13</th>
                                      <td>Ringless VM 2 - How to record your audio file to send out</td>
                                      <td><a href="{{ route('watch-training-video', 'ringless-VM-v2-recording-audio-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr>  
                                    <tr>
                                      <th scope="row">14</th>
                                      <td>Ringless VM 3 - How to send your ringless voicemail message in DMP</td>
                                      <td><a href="{{ route('watch-training-video', 'ringless-VM-v3- number-prep-and send-hb.mp4') }}" target="_blank">Click here to watch video</a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>                                      
                                      <td></td>
                                      <th>How to use the Deal Simulator</th>
                                      <td></td>
                                    </tr>
                                    <tr>                                      
                                      <th>15</th>
                                      <td>Deal Simulator</td>
                                      <td><a href="{{ route('watch-training-video', '564374668') }}" target="_blank">Click here to watch video</a></td>
                                    </tr>
                                    <tr>                                      
                                      <td></td>
                                      <th>How to use the Calendar</th>
                                      <td></td>
                                    </tr> 
                                    <tr>                                      
                                      <th>16</th>
                                      <td>Using Calendar</td>
                                      <td><a href="{{ route('watch-training-video', '565075814') }}" target="_blank">Click here to watch video</a></td>
                                    </tr> 
                                  </tbody>
                                </table>
                            </div>
                            <!-- <div class="col-md-6">
                                <h5>1. How to set up a Send In Blue Account and integrate to DMP</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/send_in_blue_setup.mp4" type="video/mp4" />
                                </video>
                            </div>
                            <div class="col-md-6">
                                <h5>2. How to send out emails to contacts</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/send_in_blue_setup.mp4" type="video/mp4" />
                                </video>
                            </div> -->
                        </div>
                        <!-- <div class="row mt-3">
                            <div class="col-md-12">
                                <h3>How to set up DMP to send text messages </h3>
                            </div>
                            <div class="col-md-6">
                                <h5>3. How to set up a Twillio Account and integrate to DMP</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/twilio_setup_2022.mp4" type="video/mp4" />
                                </video>
                            </div>
                            <div class="col-md-6">
                                <h5>4. How to send SMS out to contacts</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/sending-sms-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <h3>How to set up DMP to send ringless voicemail</h3>
                            </div>
                            <div class="col-md-6">
                                <h5>5. Ringless VM 1 - How to set up a Sly Broadcasting account</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/ringless-VM-v1-setup-sly-broadcast account-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                            <div class="col-md-6">
                                <h5>6. Ringless VM 2 - How to record your audio file to send out</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/ringless-VM-v2-recording-audio-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <h5>7. Ringless VM 3 - How to send your ringless voicemail message in DMP</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/ringless-VM-v2-recording-audio-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                        </div>
                        <div class="row mt-3">                           
                            <div class="col-md-6">
                                <h5>8. Basic list search & export</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/Basic-list-search-and-export.mp4" type="video/mp4" />
                                </video>
                            </div>
                            <div class="col-md-6">
                                <h5>9. How to get detailed property information</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/detailed-property-info-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                        </div>
                        <div class="row mt-3">                           
                            <div class="col-md-6">
                                <h5>10. How to use Direct Mail</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/how-to-direct-mail-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                            <div class="col-md-6">
                                <h5>11. Advanced list search & export</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/Advanced-list-search-and-export.mp4" type="video/mp4" />
                                </video>
                            </div>
                        </div>
                        <div class="row mt-3">                           
                            <div class="col-md-6">
                                <h5>12. Where's my list?</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/wheres-my-list-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                            <div class="col-md-6">
                                <h5>13. How to upload your lists</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/uploading-lists-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                        </div>
                        <div class="row mt-3">                           
                            <div class="col-md-6">
                                <h5>14. How to find comparables</h5>
                                <video controls="true" class="p-t-20" width="500" height="289">
                                    <source src="/samples/quick-comps-hb.mp4" type="video/mp4" />
                                </video>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <h4>15. Deal Simulator</h4>
                                Take a look at the video below for deal simulator.
                                <iframe class="p-t-20" src="https://player.vimeo.com/video/564374668" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                            <div class="col-md-6">
                                <h4>16. Using Calendar</h4>
                                Take a look at the video below for calendar usage.
                                <iframe class="p-t-20" src="https://player.vimeo.com/video/565075814" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>                            
                        </div> -->
                        <!-- <div class="row mt-3">
                            <div class="col-md-6">
                                <h4>Text Messaging with Twilio</h4>
                                Take a look at the video below for twilio integration.
                                <iframe class="p-t-20" src="https://player.vimeo.com/video/564476059" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>        
                            <div class="col-md-6">
                                <h4>Marketing Center Welcome Video</h4>
                                Take a look at the video below for marketing center welcome video.
                                <iframe class="p-t-20" src="https://player.vimeo.com/video/534127042" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <h4>1-2-3 Lead List Building</h4>
                                Easily build a lead list for marketing in 3 steps.
                                <iframe class="p-t-20" src="https://player.vimeo.com/video/585586844" width="500" height="289" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>

<!-- Sidebar Content Area -->
<div class="sidebar-detached sidebar-right">
          <div class="sidebar">
              <div class="sidebar-content card">
                    <div class="card-block">
                        <div class="text-xs-center">
                            
                            <i class="fa fa-cogs fa-5x"></i>
                            
                            <h4 class="card-title center p-t-20">SUPPORT TEAM</h4>
                            <p class="mb-1">support@dealmanagerpro.com</p>
                        </div>

                        <p class="card-text">Please use the above email address for all support questions and issues you may have. We will have a series of training webinars to help you get onboard with the software.</p>


                    </div>
              
              </div>

          </div>
</div>
<!--/ Sidebar Content Area -->

@endsection