@extends('errors.template')

@section('content')
    <div class="text-error">503</div>
    <h3 class="text-uppercase font-600">Be right back.</h3>
    <p class="text-muted">
        The site is under maintenance.
    </p>
@endsection
