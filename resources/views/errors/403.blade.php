@extends('errors.template')

@section('content')
	<div class="text-error">403</div>
	<h3 class="text-uppercase font-600">Unauthorized</h3>
	<p class="text-muted">
		It's looking like you may have taken a wrong turn. You don't have rights to access this page.
	</p>
	<br>
	<a class="btn btn-success waves-effect waves-light" href="{{ route('dashboard') }}"> Return Home</a>
@endsection