@extends('errors.template')

@section('content')
	<div class="text-error">404</div>
	<h3 class="text-uppercase font-600">Page not Found</h3>
	<p class="text-muted">
		It's looking like you may have taken a wrong turn. The requested page not found.
	</p>
	<br>
	<a class="btn btn-success waves-effect waves-light" href="{{ route('dashboard') }}"> Return Home</a>
@endsection