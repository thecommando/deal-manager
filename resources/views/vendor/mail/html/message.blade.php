@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ url('assets/images/dmp-logo.png') }}" alt="Deal Manager Pro" style="width: 40%">
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <center> &copy; {{ date('Y') }} {{ config('app.name') }}. All Rights Reserved.<BR>
            <p>Deal Manager Pro, Inc | 809 W Riordan Rd #100-100 Flagstaff, Az 86001 | 928-569-3790</p>
            <p>If you would like to stop receiving these emails, click the unsubscribe link. <a href="#UN_SUB#">Unsubscribe</a></p></center>
                
        @endcomponent
    @endslot
@endcomponent
