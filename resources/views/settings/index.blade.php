@extends('layouts.default')

@section('content')

    <!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Settings</h3>
          </div>
    </div>

    <div class="content-body">
        @if (Session::has('error_settings'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <strong>{{ Session::get('error_settings') }} </strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>            
        @endif
        <!-- Main Content Area -->
        <section class="card">
            <div class="card-header">
                <h4 class="card-title">Twilio Credentials</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text col-md-8 col-xs-12">
                        <blockquote class="blockquote blockquote-success font-size-16">
                        <p>Set your twilio credentials to send SMS to your contacts or contact groups.</p>
                    </blockquote>

                    <form method="POST" action="{!! route('settings.store', 'twilio') !!}">

                        {!! csrf_field() !!}

                        <!-- Account SID -->
                        <div class="form-group">
                            <label for="account_sid">Account SID</label>
                            <input type="text" id="account_sid" name="account_sid" value="{{ array_get($settings, 'account_sid') }}" class="form-control" placeholder="Account SID" required>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <!-- Auth Token -->
                                <div class="col-md-6">
                                    <label for="auth_token">Auth Token</label>
                                    <input type="text" id="auth_token" name="auth_token" value="{{ array_get($settings, 'auth_token') }}" class="form-control" placeholder="Auth Token" required>
                                </div>
                                <!-- From Number -->
                                <div class="col-md-6">
                                    <label for="from">From Number</label>
                                    <input type="text" id="from" name="from" value="{{ array_get($settings, 'from') }}" class="form-control" placeholder="From Number" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-success">Update</button> <span class="payment-errors margin-left-10 font-size-16" style="color: red"></span>
                            <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

        <section class="card">
            <div class="card-header">
                <h4 class="card-title">SendInBlue Credentials</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text col-md-8 col-xs-12">
                        <blockquote class="blockquote blockquote-success font-size-16">
                            <p>Set your sendinblue credentials to send automated emails to your contacts or contact groups.</p>
                        </blockquote>

                        <form method="POST" action="{!! route('settings.store', 'sendinblue') !!}">

                            {!! csrf_field() !!}

                            <!-- From email -->
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="email" id="username" name="username" value="{{ array_get($settings, 'username') }}" class="form-control" placeholder="Username" required>
                            </div>

                            <!-- API Key -->
                            <div class="form-group">
                                <label for="password">Sendinblue SMTP Password</label>
                                <input type="text" id="password" name="password" value="{{ array_get($settings, 'password') }}" class="form-control" placeholder="Password" required>
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success">Update</button> <span class="payment-errors margin-left-10 font-size-16" style="color: red"></span>
                                <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>        
    </div>
@endsection