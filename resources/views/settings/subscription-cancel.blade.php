@extends('layouts.default')

@section('content')
	<!-- Page-Title -->
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Settings</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			@include('settings.partials.sidemenu')
		</div>

		<div class="col-md-9">
			<div class="card-box">
				<h4 class="m-t-0 m-b-20 font-600"><b>Cancel Your Subscription</b></h4>

				<hr/>

				{!! Form::open(['route' => 'settings.subscription.cancel']) !!}

					<p>Are you really sure that you want to cancel?</p>

					<!--- Update Plan Field --->
					<div class="form-group text-right">
					    {!! Form::submit("Yes, I'm Sure", ['class' => 'btn btn-danger', 'onclick' => "if(!confirm('Are you sure?')){return false;};"]) !!}
						<button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection