@extends('layouts.default')

@section('content')

    <!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Billing Settings</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Settings</a>
                  </li>
                  <li class="breadcrumb-item active">Billing
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>

<div class="content-detached content-left">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card ">
            <div class="card-header">
                <h4 class="card-title">Update Your Credit Card Information</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text col-md-8 col-xs-12">
                        <blockquote class="blockquote blockquote-success font-size-16">
                        <p>If you need to update the credit card that we have on file, provide the new details below. Don't worry; your card information will never touch our servers.</p>
                    </blockquote>

                    <form method="POST" action="{!! route('settings.card') !!}" id="payment-form">

                    {!! csrf_field() !!}

                    <!-- Credit Card Number -->
                        <div class="form-group">
                            <label for="cc-number">Credit Card Number</label>
                            <input type="text" id="cc-number" class="form-control" data-stripe="number" placeholder="**** **** **** {!! $user->card_last_four ?:'****' !!}" required>
                        </div>

                        <!-- Expiration Date -->
                        <div class="form-group">
                            

                            <div class="row">
                                <div class="col-md-4">
                                    <label>Expiration Month</label>
                                    <select class="form-control" data-stripe="exp-month">
                                        <option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Expiration Year</label>
                                    <select class="form-control" data-stripe="exp-year">
                                        @for ($year = \Carbon\Carbon::today()->year; $year <= \Carbon\Carbon::today()->addYears(15)->year; $year++)
                                            <option value="{{ $year }}">{{ $year }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="cvv">CVV Number</label>
                                    <input type="text" id="cvv" class="form-control" data-stripe="cvc" required>
                                </div>
                            </div>
                        </div>

                        <!-- CVV Number -->
                        

                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-success">Update Credit Card</button> <span class="payment-errors margin-left-10 font-size-16" style="color: red"></span>
                            <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->
        
        @if(auth()->user()->hasStripeId())
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Your Subscription Plan</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        {!! Form::open(['route' => 'settings.subscription']) !!}

                <!--- Plan Field --->
                    <div class="form-group">
                        {!! Form::label('plan', 'Want to Modify Your Subscription?') !!}
                        {!! Form::select('plan', [
                            '' => '-- Select --',
                            'dmp_monthly' => 'Deal Manager Pro ($49)',
                        ], null, ['class' => 'form-control']) !!}
                    </div>

                    <!--- Coupon code Field --->
                    <div class="form-group">
                        {!! Form::text('coupon_code', null, ['class' => 'form-control', 'placeholder' => 'Have a coupon code?']) !!}
                    </div>

                    <!--- Update Plan Field --->
                    <div class="form-group text-right">
                        {!! Form::submit('Update Plan', ['class' => 'btn btn-success']) !!}
                        <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                    </div>

                    {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </section>
            
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Your Payment History</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        <hr/>

                    <table class="table table-bordered table-striped">
                        @foreach ($invoices as $invoice)
                            <tr>
                                <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                <td>{{ $invoice->total() }}</td>
                                <td><a href="/settings/invoice/{{ $invoice->id }}"><i class="fa fa-download"></i> Download</a></td>
                            </tr>
                        @endforeach
                    </table>

                    </div>
                </div>
            </div>
        </section>
            
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Cancel Your Subscription</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                    <hr/>

                        {!! Form::open(['route' => 'settings.subscription.cancel']) !!}

                        <p>Are you really sure that you want to cancel?</p>

                        <!--- Update Plan Field --->
                        <div class="form-group text-right">
                            {!! Form::submit("Yes, I'm Sure", ['class' => 'btn btn-danger', 'onclick' => "if(!confirm('Are you sure?')){return false;};"]) !!}
                            <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </section>
        
        @endif    
            
    </div>
</div> 
    
<div class="sidebar-right">
    <div class="sidebar">
        <div class="sidebar-content card">
            
            <div class="card-header">
                <h4 class="card-title">Secure Processing</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <p class="card-text">We process all cards using Stripe. Your information is never saved or stored on our servers. </p>       
                        
                    <img src="/assets/images/poweredby_stripe.png" width="100%"> 
                        
                    <div class="clearfix"></div>
                    
                        
                                

                                <!-- /Card sample -->

                </div>
        </div>
    </div>
    </div>
    
</div>

@endsection

@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script type="text/javascript">
        // This identifies your website in the createToken call below
        Stripe.setPublishableKey('{!! env('STRIPE_PUBLIC') !!}');

        jQuery(function($) {
            $('#payment-form').submit(function(event) {
                var $form = $(this);

                // Disable the submit button to prevent repeated clicks
                $form.find('button').prop('disabled', true);

                Stripe.card.createToken($form, stripeResponseHandler);

                // Prevent the form from submitting with the default action
                return false;
            });
        });

        function stripeResponseHandler(status, response) {
            var $form = $('#payment-form');

            if (response.error) {
                // Show the errors on the form
                $form.find('.payment-errors').text(response.error.message);
                $form.find('button').prop('disabled', false);
            } else {
                // response contains id and card, which contains additional card details
                var token = response.id;
                // Insert the token into the form so it gets submitted to the server
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                // and submit
                $form.get(0).submit();
            }
        }
    </script>
@stop