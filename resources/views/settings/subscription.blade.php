@extends('layouts.default')

@section('content')
	<!-- Page-Title -->
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Settings</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			@include('settings.partials.sidemenu')
		</div>

		<div class="col-md-9">
			<div class="card-box">
				<h4 class="m-t-0 m-b-20 font-600"><b>Your Subscription Plan</b></h4>

				<hr/>

				{!! Form::open(['route' => 'settings.subscription']) !!}

					<!--- Plan Field --->
					<div class="form-group">
					    {!! Form::label('plan', 'Want to Modify Your Subscription?') !!}
					    {!! Form::select('plan', [
					        '' => '-- Select --',
					        'dmp_monthly' => 'Monthly ($29)',
					    ], null, ['class' => 'form-control']) !!}
					</div>

					<!--- Coupon code Field --->
					<div class="form-group">
					    {!! Form::text('coupon_code', null, ['class' => 'form-control', 'placeholder' => 'Have a coupon code?']) !!}
					</div>

					<!--- Update Plan Field --->
					<div class="form-group text-right">
					    {!! Form::submit('Update Plan', ['class' => 'btn btn-success']) !!}
					    <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection