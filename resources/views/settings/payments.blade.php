@extends('layouts.default')

@section('content')
	<!-- Page-Title -->
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Settings</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			@include('settings.partials.sidemenu')
		</div>

		<div class="col-md-9">
			<div class="card-box">
				<h4 class="m-t-0 m-b-20 font-600"><b>Your Payment History</b></h4>

				<hr/>

				<table class="table table-bordered table-striped">
					@foreach ($invoices as $invoice)
						<tr>
							<td>{{ $invoice->date()->toFormattedDateString() }}</td>
							<td>{{ $invoice->total() }}</td>
							<td><a href="/settings/invoice/{{ $invoice->id }}"><i class="fa fa-download"></i> Download</a></td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
@endsection