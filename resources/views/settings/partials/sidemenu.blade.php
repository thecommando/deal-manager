<div class="list-group">
	@if(auth()->user()->hasStripeId())
		<a href="{{ route('settings.card') }}" class="list-group-item">Credit Card</a>
		<a href="{{ route('settings.subscription') }}" class="list-group-item">Subscription</a>
		<a href="{{ route('settings.payments') }}" class="list-group-item">Payments</a>
		<a href="{{ route('settings.subscription.cancel') }}" class="list-group-item">Cancel</a>
	@else
		<a href="{{ route('settings.card') }}" class="list-group-item">Add Card</a>
	@endif
</div>