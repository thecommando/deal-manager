@extends('layouts.default')

@section('content')
	<!-- Page-Title -->
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Settings</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			@include('settings.partials.sidemenu')
		</div>

		<div class="col-md-9">
			<div class="card-box">
				<h4 class="m-t-0 m-b-20 font-600"><b>Update Your Credit Card</b></h4>

				<hr/>

				<blockquote class="blockquote blockquote-success font-size-16">
					<p>Want to update the credit card that we have on file? Provide the new details here. Don't worry; your card information will never touch our servers.</p>
				</blockquote>

				<form method="POST" action="{!! route('settings.card') !!}" id="payment-form">

					{!! csrf_field() !!}

					<!-- Credit Card Number -->
					<div class="form-group">
						<label for="cc-number">Credit Card Number</label>
						<input type="text" id="cc-number" class="form-control" data-stripe="number" placeholder="**** **** **** {!! $user->card_last_four ?:'****' !!}" required>
					</div>

					<!-- Expiration Date -->
					<div class="form-group">
						<label>Expiration Date</label>

						<div class="row">
							<div class="col-md-3">
								<select class="form-control" data-stripe="exp-month">
									<option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option>
								</select>
							</div>
							<div class="col-md-2">
								<select class="form-control" data-stripe="exp-year">
									@for ($year = \Carbon\Carbon::today()->year; $year <= \Carbon\Carbon::today()->addYears(15)->year; $year++)
										<option value="{{ $year }}">{{ $year }}</option>
									@endfor
								</select>
							</div>
						</div>
					</div>

					<!-- CVV Number -->
					<div class="form-group">
						<label for="cvv">CVV Number</label>
						<input type="text" id="cvv" class="form-control" data-stripe="cvc" required>
					</div>

					<div class="form-group text-right">
						<button type="submit" class="btn btn-success">Update Credit Card</button> <span class="payment-errors margin-left-10 font-size-16" style="color: red"></span>
					    <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

	<script type="text/javascript">
		// This identifies your website in the createToken call below
		Stripe.setPublishableKey('{!! env('STRIPE_PUBLIC') !!}');

		jQuery(function($) {
			$('#payment-form').submit(function(event) {
				var $form = $(this);

				// Disable the submit button to prevent repeated clicks
				$form.find('button').prop('disabled', true);

				Stripe.card.createToken($form, stripeResponseHandler);

				// Prevent the form from submitting with the default action
				return false;
			});
		});

		function stripeResponseHandler(status, response) {
			var $form = $('#payment-form');

			if (response.error) {
				// Show the errors on the form
				$form.find('.payment-errors').text(response.error.message);
				$form.find('button').prop('disabled', false);
			} else {
				// response contains id and card, which contains additional card details
				var token = response.id;
				// Insert the token into the form so it gets submitted to the server
				$form.append($('<input type="hidden" name="stripeToken" />').val(token));
				// and submit
				$form.get(0).submit();
			}
		}
	</script>
@stop