@extends('layouts.default')

@section('styles')
    <link rel="stylesheet" type="text/css"
          href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/select.dataTables.min.css">
    <style>
        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 30px;
            height: 17px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 13px;
            width: 13px;
            left: 2px;
            bottom: 2px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(13px);
            -ms-transform: translateX(13px);
            transform: translateX(13px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 17px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endsection


@section('content')
    <users inline-template>
        <div>
            <!-- Page-Title -->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-xs-12 mb-2">
                    <h3 class="content-header-title mb-0">@if(request()->has('archived')) Archive @else Active @endif Users</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">@if(request()->has('archived')) Archive @else Active @endif Users
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="content-header-right col-md-6 col-xs-12">
                    <div class="media width-250 float-xs-right">
                        <div class="media-left media-middle">
                        </div>
                        <div class="media-body media-right text-xs-right">
                            <div class="btn-group pull-right m-t-15 hidden-print">
                                <!-- Add Templates Button -->
                                <a href="{{ route('export-users') }}"
                                   class="btn btn-success waves-effect waves-light m-r-5"><span
                                            class="">Export</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section id="compact-style">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Current @if(request()->has('archived')) Archive @else Active @endif Users</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <p></p>
                                    <table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport" id="dataTable" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Deals (Open/Closed)</th>
                                            <th>Email</th>
                                            <th>Roles</th>
                                            <th>Active</th>
                                            <th>Bypass</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Base -->
        </div>
    </users>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('users.index', request()->has('archived') ? ['archived' => 'true'] : '') }}",
                    type: 'GET',
                },
                columns: [
                    {
                        data: 'image',
                        name: 'image',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    },
                    {data: 'name', name: 'name'},
                    {data: 'username', name: 'username'},
                    {
                        data: 'deals',
                        name: 'deals',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    },
                    {
                        data: 'email',
                        name: 'email',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    },
                    {
                        data: 'roles',
                        name: 'roles',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    },
                    {
                        data: 'active',
                        name: 'active',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    },
                    {
                        data: 'bypass',
                        name: 'bypass',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    },
                    {
                        data: 'actions',
                        name: 'actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    }
                ],
            });
        });

        $(document).on('change', '.active_checkbox', function () {
            let id = $(this).data('id');
            $.ajax({
                url: '{{route('users.status.toggle', ['user' => '_user'])}}'.replace('_user', id),
                method: 'GET',
                data: {
                    _token: '{{ csrf_token() }}'
                },
            })
        });

        $(document).on('change', '.bypass_checkbox', function () {
            let id = $(this).data('id');
            $.ajax({
                url: '{{route('users.bypassPayment.toggle', ['user' => '_user'])}}'.replace('_user', id),
                method: 'GET',
                data: {
                    _token: '{{ csrf_token() }}'
                },
            })
        });
    </script>
@endsection