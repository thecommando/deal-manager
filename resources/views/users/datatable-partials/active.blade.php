<label class="switch">
    <input class="active_checkbox" type="checkbox"
           data-plugin="switchery"
           data-id="{{$user->id}}" {!! $user->isActive() ? 'checked' : '' !!}>
    <span class="slider round"></span>
</label>