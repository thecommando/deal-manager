<label class="switch">
    <input class="bypass_checkbox" type="checkbox" data-id="{{$user->id}}" {!! $user->canBypassPayment() ? 'checked' : '' !!}>
    <span class="slider round"></span>
</label>