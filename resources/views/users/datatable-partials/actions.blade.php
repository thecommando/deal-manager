<a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit {{ $user->name }}'s Details"><i class="fa fa-pencil"></i></a>
<a href="{{ route('user.deals', $user->id) }}" class="btn btn-sm btn-success"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View {{ $user->name }}'s Deals"><i class="fa fa-eye"></i></a>
@if($user->trashed())
    <a href="{{ route('users.restore', $user->id) }}" class="btn btn-sm btn-success" onclick="if(!confirm('Are you sure to restore this user?')){return false;};" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Restore {{ $user->name }}"><i class="fa fa-refresh"></i></a>
@else
    <a href="{{ route('users.delete', $user->id) }}" class="btn btn-sm btn-danger" onclick="if(!confirm('Are you sure to delete this user?')){return false;};" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete {{ $user->name }}"><i class="fa fa-close"></i></a>
@endif