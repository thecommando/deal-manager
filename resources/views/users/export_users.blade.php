<table class="table table-striped table-bordered compact data-table">
	<thead>
	<tr>
		<th><strong>Name</strong></th>
		<th><strong>Username</th>
		<th><strong>Deals (Open/Closed)</strong></th>
		<th><strong>Email</strong></th>
		<th><strong>Roles</strong></th>
		<th><strong>Active</strong></th>
		<th><strong>Bypass</strong></th>
	</tr>
	</thead>
	<tbody>
	@foreach($users as $user)
		<tr>
			<td>{{ $user->name }}</td>
			<td>{{ $user->username }}</td>
			<td>
				{{ $user->openDeals() }} / {{ $user->closedDeals() }}
			</td>
			<td>{{ $user->email }}</td>
			<td>
				@foreach($user->roles as $role)
					<span class="tag tag-default">{{ $role->display_name }}</span>
				@endforeach
			</td>
			<td>
				{!! $user->isActive() ? 'Active' : 'Inactive' !!}
			</td>
			<td>
				{!! $user->canBypassPayment() ? 'Yes' : 'No' !!}
			</td>
		</tr>
	@endforeach
	</tbody>

</table>