<div class="row">
	<div class="col-md-3">
		<h4 class="m-t-0 header-title"><b>Personal Information</b></h4>

		<div>
			<img src="{{ $user->photoURL() }}" class="img-responsive img-circle" width="75" alt="{{ $user->name }}">
		</div>
		<div class="clearfix"></div>
		<label class="btn btn-warning btn-xs m-t-10 m-l-15 btn-file">
			Browse <input type="file" name="profile" style="display: none;" />
		</label>
		
		<hr class="m-t-20 m-b-20">
		
		<h4 class="m-t-0 header-title"><b>Change Password</b></h4>

		<!--- Password Field --->
		<div class="form-group">
			{!! Form::label('password', 'Password:') !!}
			{!! Form::password('password', ['class' => 'form-control']) !!}
		</div>

		<!--- Password confirmation Field --->
		<div class="form-group">
			{!! Form::label('password_confirmation', 'Confirm Password:') !!}
			{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
		</div>

		<!--- Submit Field --->
		<div class="form-group input-group-sm">
			{!! Form::submit('Update Password', ['class' => 'btn btn-success']) !!}
		</div>
	</div>
	
	<div class="col-md-1"></div>

	<div class="col-md-8">
		<h4 class="m-t-0 header-title"><b>Personal Information</b></h4>
		<hr>

		<div class="row">
			<!--- Name Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('name', 'Name:') !!} <span class="red">*</span>
				{!! Form::text('name', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Email Field --->
			<div class="form-group input-group-sm col-sm-8 p-l-0">
				{!! Form::label('email', 'Email:') !!} <span class="red">*</span>
				{!! Form::email('email', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Username Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('username', 'Username:') !!} <span class="red">*</span>
				{!! Form::text('username', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Mobile phone Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('mobile_phone', 'Mobile Phone:') !!}
				{!! Form::text('mobile_phone', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Alternate phone Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('office_phone', 'Alternate Phone:') !!}
				{!! Form::text('office_phone', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Website Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('website', 'Website:') !!}
				{!! Form::text('website', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<h4 class="m-t-20 header-title"><b>Location Information</b></h4>
		<hr>

		<div class="row">
			<!--- Address Field --->
			<div class="form-group input-group-sm col-sm-8 p-l-0">
				{!! Form::label('address', 'Address:') !!}
				{!! Form::text('address', null, ['class' => 'form-control']) !!}
			</div>

			<!--- City Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('city', 'City:') !!}
				{!! Form::text('city', null, ['class' => 'form-control']) !!}
			</div>

			<!--- State Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('state', 'State:') !!}
				{!! Form::select('state', [], null, ['class' => 'form-control']) !!}
			</div>

			<!--- Country Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('country', 'Country:') !!}
				{!! Form::select('country', country_options(), null, ['class' => 'form-control']) !!}
			</div>

			<!--- Postal code Field --->
			<div class="form-group input-group-sm col-sm-4 p-l-0">
				{!! Form::label('postal_code', 'Postal Code:') !!}
				{!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
			</div>

			<div class="clearfix"></div>

			<div class="form-group input-group-sm col-sm-12">
				{!! Form::submit($submit, ['class' => 'btn btn-success']) !!}
				<button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
			</div>
		</div>
	</div>
</div>