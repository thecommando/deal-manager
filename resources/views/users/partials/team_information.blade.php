<h4 class="m-t-0 header-title"><b>Team Information</b></h4>

<!--- Name Field --->
<div class="form-group">
	{!! Form::label('team_name', 'Name:') !!} <span class="red">*</span>
	{!! Form::text('team_name', $user->teamName(), ['class' => 'form-control', 'placeholder' => 'Team Name']) !!}
</div>

<!--- Submit Field --->
<div class="form-group">
	{!! Form::submit('Save Changes', ['class' => 'btn btn-success']) !!}
</div>