@extends('layouts.default')

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Edit: {{ $user->name }}</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				@include('partials.errors')

				{!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT', 'files' => true]) !!}
					@include('users.form', ['submit' => 'Update'])
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<hr>
	
	<div class="row">
		<div class="card-box col-md-4">
			<h4 class="m-t-0 header-title"><b>User Roles <span class="red">*</span></b></h4>

			{!! Form::open(['route' => ['users.roles.update', $user->id], 'method' => 'PUT']) !!}
				<div class="form-group">
					<select name="roles[]" class="select2 select2-multiple form-control" multiple="multiple" data-placeholder="Roles ...">
						@foreach($roles as $id => $role)
							<option value="{{ $id }}" {{ $userRoles->contains($id) ? 'selected' : '' }}>{{ $role }}</option>
						@endforeach
					</select>
				</div>

				<!--- Submit Field --->
				<div class="form-group input-group-sm m-t-30 text-right">
					{!! Form::submit('Update Roles', ['class' => 'btn btn-success']) !!}
					<button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
				</div>
			{!! Form::close() !!}
		</div>

		@if($user->hasRole('user'))
			<div class="card-box col-md-4">
				<h4 class="m-t-0 header-title"><b>Deal Finders</b></h4>

				{!! Form::open(['route' => ['users.deal-finders.update', $user->id], 'method' => 'PUT']) !!}
				<div class="form-group">
					<select name="finders[]" class="select2 select2-multiple form-control" multiple="multiple" data-placeholder="Deal Finders ...">
						@foreach($dealFinders as $id => $name)
							<option value="{{ $id }}" {{ $userDealFinders->contains($id) ? 'selected' : '' }}>{{ $name }}</option>
						@endforeach
					</select>
				</div>

				<!--- Submit Field --->
				<div class="form-group input-group-sm m-t-30 text-right">
					{!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
					<button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
				</div>
				{!! Form::close() !!}
			</div>

			<div class="card-box col-md-4">
				<h4 class="m-t-0 header-title"><b>Coaches</b></h4>

				{!! Form::open(['route' => ['users.coaches.update', $user->id], 'method' => 'PUT']) !!}
				<div class="form-group">
					<select name="coaches[]" class="select2 select2-multiple form-control" multiple="multiple" data-placeholder="Coaches ...">
						@foreach($coaches as $id => $name)
							<option value="{{ $id }}" {{ $userCoaches->contains($id) ? 'selected' : '' }}>{{ $name }}</option>
						@endforeach
					</select>
				</div>

				<!--- Submit Field --->
				<div class="form-group input-group-sm m-t-30 text-right">
					{!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
					<button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
				</div>
				{!! Form::close() !!}
			</div>
		@endif

		@if($user->hasRole('super-user'))
			<div class="card-box col-md-4">
				{!! Form::model($user->team, ['route' => ['users.team', $user->id]]) !!}
					@include('users.partials.team_information')
				{!! Form::close() !!}
			</div>
		@endif
	</div>

@endsection

@section('scripts')
	@include('partials.location-script', ['country' => $user->country, 'state' => $user->state])

	<script src="/assets/js/jquery.maskMoney.min.js"></script>
	<script src="/assets/js/jquery.mask.min.js"></script>

	<script type="text/javascript">
        $(function() {
            $('#mobile_phone').mask('(000) 000-0000');
            $('#office_phone').mask('(000) 000-0000');
        });
	</script>
@endsection