@component('mail::message')

{{ $user->first_name }} requested seller locator for <strong>{{ $property->full_address }}</strong>.

<strong>Username:</strong> {{ $user->username }}

<strong>Email:</strong> {{ $user->email }}

@component('mail::button', ['url' => route('deals.show', $property->id)])
View Deal
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
