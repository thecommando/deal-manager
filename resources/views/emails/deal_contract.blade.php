@extends('emails.layout.email')

@section('content')

<p>{{ $user->first_name }} has sent you contract for property <strong>{{ $property->getFullAddress() }}</strong></p>

<p>Please check attached PDF.</p>

@endsection