@extends('emails.layout.email')

@section('content')
<p>{!! $body !!}</p>

<h3>{{ $property->getFullAddress() }}</h3>

{{--<a href="{{ route('deals.download.pdf', $property->id) }}">Download Deal PDF</a>--}}
@endsection