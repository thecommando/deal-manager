@component('mail::message')

DMP user {{ $data['auth_user'] }} has requested deal funding.

<strong>Name:</strong> {{ $data['name'] }}

<strong>Email:</strong> {{ $data['email'] }}

<strong>Phone:</strong> {{ $data['phone'] }}

<strong>What is the After Repair Value?</strong> {{ dollars((float)$data['repair_value']) }}

<strong>What is the Estimated Repair Cost?</strong> {{ dollars((float)$data['estimated_price']) }}

<strong>How much money do you need to do this deal?</strong> {{ dollars((float)$data['amount_need']) }}

<strong>In your opinion, do you think there is a possibility to do this deal without funding? If so, would you like our help to structure this for you?</strong> {{ $data['note'] }}


Thanks,<br>
{{ config('app.name') }}
@endcomponent
