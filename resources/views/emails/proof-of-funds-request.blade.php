@component('mail::message')

{{ $user->first_name }} requested proof of funds for <strong>{{ $property->full_address }}</strong>.

@foreach($property->comparable_properties as $comparable_property)

<strong>BR/BA:</strong> {{ $comparable_property->br }}/{{ $comparable_property->ba }}

<strong>Sqft:</strong> {{ $comparable_property->sqft }}

@endforeach

<strong>After Repair Value:</strong> {{ dollars($property->after_repair_value) }}

<strong>ARV $/sqft:</strong> {{ dollars($property->comparable_price_per_sqft) }}

<strong>Rehab Estimate:</strong> {{ dollars($property->rehab_estimate) }}

<strong>Annual Gross Income:</strong> {{ dollars($property->annual_gross_income) }}

<strong>Annual Expenses:</strong> {{ dollars($property->annual_expenses) }}

<strong>Annual Net Operating Income:</strong> {{ dollars($property->annual_net_operating_income) }}

<strong>Suggested Max Offer:</strong> {{ dollars($property->suggested_maximum_offer) }}

<strong>Purchase Price:</strong> {{ dollars($property->offer_price) }}

<strong>Adjusted Sales Price:</strong> {{ dollars($property->wholesale_value) }}

<strong>Username:</strong> {{ $user->username }}

<strong>Email:</strong> {{ $user->email }}

@component('mail::button', ['url' => route('deals.show', $property->id)])
View Deal
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
