@component('mail::message')
# Hi {{ $user->first_name }},

You are successfully subscribed to a Deal Manager Pro plan.

@component('mail::button', ['url' => route('login')])
Click to Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
