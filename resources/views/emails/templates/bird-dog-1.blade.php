Hello (name),

I'm really glad you were interested in helping me find a few properties here in {{ $property->city }}. This should be really easy for you and won't require any extra driving around wasting your time.

I have a few things that I wanted you to focus on so we can get a deal closed quickly and put some cash in your pocket.  The main thing to look for are vacant properties. If you see any house that has overgrown or unkempt lawns, a stack of newspapers on the front steps, no curtains in the windows, any boarded up windows and basically just a "nobody lives here" feel to them. You will know what they are since you are starting to look for them. You will be surprised how many you end up finding. There are also seller’s that just run into hard times like divorce, relocating, or even a death in the family. If anyone you know needs my help let them know you have a friend that specializes in their situation.

I want this to be easy as possible, so all I need is the address of a property and a picture if possible. If you want to do a little more like get the owner’s info for me, talk to the neighbors about the house, etc. That will be great!

Keep in mind I’m paying you on every deal we close and can’t wait to get started working some deals from you. If you need any help let me know.

Thank so much.

@include('emails.templates._seller_details')