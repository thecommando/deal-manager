Hello [Enter Name Here],

You asked me to stay in touch if I had any properties in {{ $property->city }} that you may be interested in.
I am reaching out to let you know about a deal I have ran across a proper in an area you are looking for good deals.
I can't keep any properties exclusive but wanted to let you know about it today.

If you want additional details I'd love to go over them with you. I attached a profile for you to look at. The details on this deal that are worth checking out are:

ARV estimate: <strong>{{ $property->after_repair_value }}</strong>
Estimated Rehab: <strong>{{ $property->estimated_rehab }}</strong>
Additional Info: <strong>{{ $property->bedrooms }} Bedrooms, {{ $property->bathrooms }} Bathrooms, {{ $property->square_feet }} Square Feet, {{ $property->year_built }} Year Built.</strong>

<strong>SUBMITTING AN OFFER</strong>
This property will not be available for a long time, so if you are interested contact us using the information below.

@include('emails.templates._seller_details')

Thanks,
Investor Name

PS. I'll keep you posted on anything else that comes up in the area.
If you are looking in other areas just let me know and I can keep you up to date on deals elsewhere.