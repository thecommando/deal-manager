<strong>CONTACT DETAILS</strong>
<strong>{{ $property->seller_name }}</strong>
<i class="fa fa-phone-square fa-fw"></i> {{ $property->seller_mobile }} (mobile)
@if($property->seller_phone)
    <i class="fa fa-phone-square fa-fw"></i> {{ $property->seller_phone }} (phone)
@endif
<i class="fa fa-envelope fa-fw  m-t-20"></i> {{ $property->seller_email }}
