Fixer Upper! Rare opportunity at $_____________ or Best Offer. Bids accepted until 12:00 noon, (day), (Month). This {{ $property->bedrooms }} bedroom {{ $property->bathrooms }} bath has lots of potential for a rehabber to come in and make the necessary repairs in a short period of time.

Rental rates in the area are ________ so this property can be used for a few different opportunities. If you are interested please let us know quickly.

Thanks,

@include('emails.templates._seller_details')