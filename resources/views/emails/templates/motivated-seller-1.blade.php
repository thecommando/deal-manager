Hello [name],

I had a pleasure speaking with you today about your property on {{ $property->address }}. I really do want to figure out something that can make this a win win for us both.

Can you please confirm a few things for me just to make sure we are on the same page with the property?

<strong>{{ $property->bedrooms }} Bedrooms, {{ $property->bathrooms }} Bathrooms, {{ $property->square_feet }} Square Feet, {{ $property->year_built }} Year Built.</strong>

Thank so much,

@include('emails.templates._seller_details')