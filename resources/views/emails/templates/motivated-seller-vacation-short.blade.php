Hello [name],

I was looking at your listing on <a href="{{ route('deals.show', $property) }}">{{ route('deals.show', $property) }}</a>. I'd like to see if you have any plans of selling the property. I would be interested in paying all cash if we can figure out how to make this a win win for us both.

Thank so much.

@include('emails.templates._seller_details')