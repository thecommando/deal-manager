Hello [seller_name],

I wanted to reach out to you about the property over on {{ $property->address }}. I noticed it's been vacant for quite some time now. Can you call me about your plans with the house?

I have a few ideas of how I can help with it. I'm just down the road from the house and I can meet you there any time or you can call me at {{ $property->user->mobile_phone }}.

Thanks,

{{ $property->user->name }}

PS. If you are looking to keep the house and having some troubles or sell the home, either way I can help you quickly.