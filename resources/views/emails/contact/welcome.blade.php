@component('mail::message')
# Hello {{ $contact->first_name }} !

You have been added as a Deal Finder by {{ $contact->user->name }} in Deal Manager PRO.

{{--The best way to capture potential deals on the go is with the Deal Manager PRO app on your iPhone or Android.--}}

{{--Click below to download now.--}}

{{--@component('mail::button', ['url' => 'https://apkcombo.com/deal-manager-pro/com.rightprix.propertymanager/'])--}}
{{--Download for iPhone from the AppStore--}}
{{--@endcomponent--}}

{{--@component('mail::button', ['url' => 'https://apps.apple.com/in/app/dealmanagerpro/id1570052194'])--}}
{{--Get it for Android on GooglePlay--}}
{{--@endcomponent--}}
<br>
@if($data['category_id'] == 3)
DMP Login credentials are given as below:<br>

<strong>Email:</strong> {{ $data['email'] }}

<strong>Password:</strong> {{ $data['password'] }}
@endif

Thank you for using Deal Manager PRO!

Regards,<br>
The Deal Manager PRO Team
@endcomponent
<hr>