@component('mail::message')
# Hello {{ $user->first_name }} !

You have been added as a team member by {{ $user->manager->name }} in Deal Manager PRO.

Your password for your account is "<strong>{{ $password }}</strong>".

Log in to the application by clicking button below. If for some reason this link doesn't work, go to {{ route('login') }}.

@component('mail::button', ['url' => route('login')])
    Click to Login
@endcomponent

{{--The best way to capture potential deals on the go is with the Deal Manager PRO app on your iPhone or Android.--}}

{{--Click below to download now.--}}

{{--@component('mail::button', ['url' => 'https://apkcombo.com/deal-manager-pro/com.rightprix.propertymanager/'])--}}
{{--Download for iPhone from the AppStore--}}
{{--@endcomponent--}}

{{--@component('mail::button', ['url' => 'https://apps.apple.com/in/app/dealmanagerpro/id1570052194'])--}}
{{--Get it for Android on GooglePlay--}}
{{--@endcomponent--}}

Thank you for using Deal Manager PRO!

Regards,<br>
The Deal Manager PRO Team
@endcomponent
<hr>