@extends('layouts.default')

@section('styles')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/select.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/fonts/simple-line-icons/style.min.css">
<link rel="stylesheet" href="/app-assets/vendors/css/ui/prism.min.css">
@endsection

@section('content')
    @inject('dashboard', 'App\Services\CoachDashboard')

<!-- Page-Title -->
<div class="content-header row">
      <div class="content-header-left col-md-6 col-xs-12 mb-2">
        <h3 class="content-header-title mb-0">Manage Students</h3>
        <div class="row breadcrumbs-top">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
              </li>
                <li class="breadcrumb-item">User Management
              </li>
              <li class="breadcrumb-item active">Manage Students
              </li>
            </ol>
          </div>
        </div>
      </div>    
</div>


<div class="row">
        <div class="col-xl-4 col-lg-6 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-body text-xs-left">
                                <h1 class="warning">{{ $dashboard->totalNewDealsAll() }}</h1>
                                <span>Open Deals</span>
                            </div>
                            <div class="media-right media-middle">
                                <i class="icon-rocket danger font-large-2 float-xs-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-body text-xs-left">
                                <h1 class="danger">{{ $dashboard->totalClosedDealsAll() }}</h1>
                                <span>Closed Deals</span>
                            </div>
                            <div class="media-right media-middle">
                                <i class="icon-pie-chart warning font-large-2 float-xs-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-body text-xs-left">
                                <h1 class="primary">{{ $dashboard->totalUsers }}</h1>
                                <span>Total Students</span>
                            </div>
                            <div class="media-right media-middle">
                                <i class="icon-user primary font-large-2 float-xs-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        <div class="col-xl-2 col-lg-4 col-xs-6">
            <div class="card bg-success">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-body white text-xs-left">
                                <h1 class="white">{{ formatPrice($dashboard->totalWholeSaleFeesCollectedAll()) }}</h1>
                                <span>Income Generated</span>
                            </div>
                            <div class="media-right media-middle">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






<div class="row match-height">
    <div class="col-xl-4 col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">Students Management</h4>
                
                     
                    <p> <strong>Registration Link:<BR> </strong>
                    <pre><BR><code class="language-markup"><a href="{{ url("register?suid=" . auth()->id()) }}">{{ url("register?suid=" . auth()->id()) }}</a></code></pre>    
                        
                        
                    </p>
                        
                    <p class="card-text">Use the registration link above to refer students directly into your account.
                    </p>
                    
                </div>
                
            </div>
        </div>
    </div>
    
    <div class="col-xs-8">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">Students</h4>
                    
                    <p class="card-text"> </p>
                        <table class="table table-striped table-bordered data-table">
                        <thead>
                        <tr class="font-small-3">
                            <th></th>
                            <th>Name</th>
                            <th>Deals (Open/Closed)</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <img src="{{ $user->photoURL() }}" class="avatar user-img" width="40px" alt="{{ $user->name }}">
                                </td>
                                <td>{{ $user->name }}</td>
                                <td><span class="tag tag-md btn-primary">
                                        {{ $user->openDeals() }} / {{ $user->closedDeals() }}
                                    </span>
                                </td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if($user->isActive())
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('user.deals', $user->id) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                    {{--<a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>--}}
                                    {{--<a href="#" class="btn btn-danger"><i class="fa fa-close"></i></a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>            
</div>

@endsection

@section('scripts')	
	<script src="/app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>
@endsection
