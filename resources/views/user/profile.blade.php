@extends('layouts.default')

@section('content')

	<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Profile</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item active">Profile
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>
    

    <div class="content-detached content-right">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Profile Information</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text col-md-8 col-xs-12">
                        <div class="col-md-12">@include('partials.errors')</div>
                            {!! Form::model($user, ['route' => 'profile']) !!}
                            @include('user.partials.personal_information')

                            <hr>

                            @include('user.partials.location_information')

                            <!--- Save Changes Field --->
                            <div class="form-group input-group-sm m-t-30 text-right">
                                {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
                            <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
					       </div>
					
				{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>

<!-- Sidebar Content Area -->
<div class="sidebar-detached sidebar-left">
    <div class="sidebar">
        <div class="sidebar-content card hidden-md-down">
            <div class="card-block">
                
                <div class="text-xs-center">
                    
                </div>
                
                <div class="card-box widget-user">
				{!! Form::open(['route' => 'profile.picture', 'files' => true]) !!}
					@include('user.partials.profile_photo')
				{!! Form::close() !!}
			</div>

			@role('super-user')
				<div class="card-box">
					{!! Form::model($user->team, ['route' => 'profile.team']) !!}
						@include('user.partials.team_information')
					{!! Form::close() !!}
				</div>
			@endrole

			<div class="card-box">
				{!! Form::open(['route' => 'profile.password']) !!}
					@include('user.partials.change_password')
				{!! Form::close() !!}
			</div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('scripts')
	@include('partials.location-script', ['country' => $user->country, 'state' => $user->state])

    <script src="/assets/js/jquery.maskMoney.min.js"></script>
    <script src="/assets/js/jquery.mask.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#mobile_phone').mask('(000) 000-0000');
            $('#office_phone').mask('(000) 000-0000');
        });
    </script>
@endsection
