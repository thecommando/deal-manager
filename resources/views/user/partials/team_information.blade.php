<h4 class="m-t-0 m-b-20 font-600"><b>Team Information</b></h4>

<!--- Name Field --->
<div class="form-group">
	{!! Form::label('team_name', 'Name:') !!} <span class="red">*</span>
	{!! Form::text('team_name', $user->teamName(), ['class' => 'form-control', 'placeholder' => 'Team Name']) !!}
</div>

<!--- Submit Field --->
<div class="form-group text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
</div>