<div>

	<img src="{{ auth()->user()->photoURL() }}" class="img-responsive img-circle" alt="{{ $user->name }}">
	<div class="wid-u-info">
		<h4 class="media-heading m-b-0">{{ $user->name }}</h4>
		@foreach($user->roles as $role)
		<span class="label label-danger"><b>{{ $role->display_name }}</b></span>
		@endforeach
	</div>
</div>
<div class="clearfix"></div>
<label class="btn btn-warning btn-xs m-t-10 m-l-15 btn-file">
	Edit <input type="file" name="profile" onchange="this.form.submit()" style="display: none;" />
</label>
