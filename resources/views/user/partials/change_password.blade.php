<h4 class="m-t-0 m-b-20 font-600"><b>Change Password</b></h4>

<!--- Password Field --->
<div class="form-group input-group-sm">
	{!! Form::label('password', 'New Password:') !!}
	{!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!--- Password confirmation Field --->
<div class="form-group input-group-sm">
	{!! Form::label('password_confirmation', 'Confirm Password:') !!}
	{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>

<!--- Change Password Field --->
<div class="form-group input-group-sm text-right">
	{!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
	<button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
</div>