<h4 class="m-t-0 m-b-20 font-600"><b>Personal Information</b></h4>

<div class="row">
	<!--- Name Field --->
	<div class="form-group input-group-sm col-sm-4">
		{!! Form::label('name', 'Name:') !!} <span class="red">*</span>
		{!! Form::text('name', null, ['class' => 'form-control']) !!}
	</div>

	<!--- Email Field --->
	<div class="form-group input-group-sm col-sm-8">
		{!! Form::label('email', 'Email:') !!} <span class="red">*</span>
		{!! Form::email('email', null, ['class' => 'form-control']) !!}
	</div>

	<!--- Username Field --->
	<div class="form-group input-group-sm col-sm-4">
		{!! Form::label('username', 'Username:') !!} <span class="red">*</span>
		{!! Form::text('username', null, ['class' => 'form-control']) !!}
	</div>

	<!--- Mobile phone Field --->
	<div class="form-group input-group-sm col-sm-4">
		{!! Form::label('mobile_phone', 'Mobile Phone:') !!}
		{!! Form::text('mobile_phone', null, ['class' => 'form-control']) !!}
	</div>

	<!--- Office phone Field --->
	<div class="form-group input-group-sm col-sm-4">
	<!--Need to update field in database to be alternate_phone -->
		{!! Form::label('office_phone', 'Alternate Phone:') !!}
		{!! Form::text('office_phone', null, ['class' => 'form-control']) !!}
	</div>

	<!--- Website Field --->
	<div class="form-group input-group-sm col-sm-4">
		{!! Form::label('website', 'Website:') !!}
		{!! Form::text('website', null, ['class' => 'form-control']) !!}
	</div>
</div>