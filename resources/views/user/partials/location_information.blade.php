<h4 class="m-t-0 m-b-20 font-600"><b>Location Information</b></h4>

<div class="row">
	<!--- Address Field --->
	<div class="form-group input-group-sm col-sm-8">
		{!! Form::label('address', 'Address:') !!}
		{!! Form::text('address', null, ['class' => 'form-control']) !!}
	</div>

	<!--- City Field --->
	<div class="form-group input-group-sm col-sm-4">
		{!! Form::label('city', 'City:') !!}
		{!! Form::text('city', null, ['class' => 'form-control']) !!}
	</div>

	<!--- State Field --->
	<div class="form-group input-group-sm col-sm-4">
		{!! Form::label('state', 'State:') !!}
		{!! Form::select('state', [], null, ['class' => 'form-control']) !!}
	</div>

	<!--- Country Field --->
	<div class="form-group input-group-sm col-sm-4">
		{!! Form::label('country', 'Country:') !!}
		{!! Form::select('country', country_options(), null, ['class' => 'form-control']) !!}
	</div>

	<!--- Postal code Field --->
	<div class="form-group input-group-sm col-sm-4">
		{!! Form::label('postal_code', 'Postal Code:') !!}
		{!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
	</div>
</div>