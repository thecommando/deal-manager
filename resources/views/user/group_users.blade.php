@extends('layouts.default')

@section('content')
<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Manage Group Members</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                    <li class="breadcrumb-item"><a href="{{ route('groups.index') }}">User Management</a>
                  </li>
                  <li class="breadcrumb-item active">Manage Group <small>({{ auth()->user()->groupName() }})</small>
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>


<div class="content-detached content-left">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Group Members ({{ auth()->user()->groupName() }})</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        
                        <div id="goal-list-scroll" class="table-responsive height-250 position-relative">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                        <img src="{{ $user->photoURL() }}" class="img img-responsive img-avatar" width="40px" alt="{{ $user->name }}">
                                    </td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if($user->isActive())
                                            <span class="tag tag-success">Active</span>
                                        @else
                                            <span class="tag tag-danger">Inactive</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('user.deals', $user->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                        {{--<a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>--}}
                                        {{--<a href="#" class="btn btn-danger"><i class="fa fa-close"></i></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </table>
                        </div>
                        

                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>
<!-- Sidebar Content Area -->
<div class="sidebar-detached sidebar-right">
          
        
        <div class="sidebar">
              <div class="sidebar-content card">
                  <div class="card-header">
                <h4 class="card-title">Admin Information</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                
            </div>
                <div class="card-block">
                   
                        <p class="card-text"><strong>Special Registration Link: </strong></p>
                        <p class="card-text">When users use the link below, they will automatically be registered to you.
                        <a href="{{ url("register?suid=" . auth()->id()) }}">{{ url("register?suid=" . auth()->id()) }}</a></p>

                </div>
              </div>

          </div>
</div>
<!--/ Sidebar Content Area -->
	
@endsection
