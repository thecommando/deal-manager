@extends('layouts.default')

@section('styles')
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/select.dataTables.min.css">
@endsection

@section('content')
    <section id="compact-style">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">PENDING DEALS</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <p></p>
                            <table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport"
                                   id="dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Address</th>
                                    <th>Deal Owner</th>
                                    <th>Date Added</th>
                                    <th>Transfer From</th>
                                    <th>Bedroom(s)</th>
                                    <th>Bathroom(s)</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('deals.pending-properties') }}",
                    type: 'GET',
                },
                columns: [
                    {
                        data: 'image',
                        name: 'image',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    },
                    {data: 'address', name: 'Address'},
                    {
                        data: 'owner',
                        name: 'owner',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return data
                        }
                    },
                    {data: 'created_at', name: 'created_at'},
                    {
                        data: 'transfer_from',
                        name: 'transfer_from',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    },
                    {data: 'bedrooms', name: 'bedrooms'},
                    {data: 'bathrooms', name: 'bathrooms'},
                    {
                        data: 'actions',
                        name: 'actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return $('<div/>').html(data).text();
                        }
                    }
                ],
            });
        });
    </script>
@endsection