@extends('layouts.default')

@section('content')
	<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Deals Upload</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                    <li class="breadcrumb-item"><a href="{{ route('deals.index') }}">Deal Management</a>
                  </li>
                  <li class="breadcrumb-item active">Upload
                  </li>
                </ol>
              </div>
                <div class="col-xs-12">@include('partials.errors')</div>
            </div>
          </div> 
          <div class="content-header-right col-md-6 col-xs-12">
            <div class="media width-250 float-xs-right">
              <div class="media-left media-middle">
                <div id="sp-bar-total-sales"></div>
              </div>
              <div class="media-body media-right text-xs-right">
                <h3 class="m-0">$5,668</h3><span class="text-muted">Sales</span>
              </div>
            </div>
          </div>
    </div>

{!! Form::open(['route' => 'deals.upload', 'files' => true]) !!}
<div class="content-body"><!-- Card headings examples section start -->
<section id="card-headings">
    
	<div class="row card-deck-wrapper match-height">
		<div class="col-md-6 col-sm-12">
			<div class="card card-deck">
				<div class="card-header" id="heading-links">
					<h4 class="card-title">DOWNLOAD XLS TEMPLATE</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
				</div>
				<div class="card-block">
					<h4 class="m-t-0 m-b-20 font-300">Deals Template </h4>
				    <p class="font-light">Click the icon below to download the template that should be used to upload deals into the system. Once you've downloaded the template, open MS Excel, add your deals in and then use the form to the right to load your deals into the system. </p>
				
				    <div class="text-center">
				        <a href="{{ url('samples/deals.xlsx') }}"><i class="fa fa-file-excel-o fa-4x"></i></a>
				    </div>
				    <br>
						<p><strong>NOTE</strong>: Once you've downloaded the template then please cross check the latest stage list before any upload.</p>
						<ul>
							<li><small><strong>{{ implode(', ', $stagesList) }}</strong></small></li>
							<li><small><strong>Please don't create the sheet 1, sheet 2 at bottom right in excel sheet template (there should be simple excel sheet)</strong></small></li>
						</ul>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="card card-deck">
				<div class="card-header">
					<h4 class="card-title" id="heading-labels">UPLOAD XLS FILE</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
        			<div class="heading-elements">
						<ul class="list-inline mb-0">
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
					</div>
				</div>
				<div class="card-block">
					<div class="form-group">
					   <h4 class="m-t-0 m-b-20 font-300">Upload XLS File:</h4>
					   <p class="font-light">Make sure you only upload your contacts using the file template to the left. No other template will work with this system. </p>
					
					   {!! Form::file('file', ['class' => 'dropify']) !!}
				    </div>

                    <!--- Submit Field --->
                    <div class="form-group input-group-sm m-t-30 text-right">
                        {!! Form::submit('Upload', ['class' => 'btn btn-success']) !!}
                        <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
    </section>
</div>
{!! Form::close() !!}

@endsection