<table class="table highlighted" v-if="zillow_comparables.length">
    <thead>
    <tr>
        <th>Address</th>
        <th>Area</th>
        <th>BR/BA</th>
        <th>SqFt</th>
        <th>Year Built</th>
        <th>Zestimate</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <td class="font-small-3 text-primary"><em>Please verify comparable values.</em></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td><img src="/assets/images/zillow-logo.png" width="75px"></td>
    </tr>
  </tfoot>
    <tbody>
    <tr v-for="comparable in zillow_comparables" class="font-small-3">
        <td>@{{ comparable.address.street }} @{{ comparable.address.city }}, @{{ comparable.address.state }} @{{ comparable.address.zipcode }}</td>
        <td>@{{ comparable.localRealEstate.region['@attributes'].name }}</td>
        <td>@{{ comparable.bedrooms }}/@{{ comparable.bathrooms }}</td>
        <td>@{{ comparable.finishedSqFt }}</td>
        <td>@{{ comparable.yearBuilt }}</td>
        <td>@{{ comparable.zestimate.amount | currency }}</td>
    </tr>
    </tbody>
</table>
<div></div>