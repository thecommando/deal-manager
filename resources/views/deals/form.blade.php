<deal-calculations :old-input="{{ json_encode(array_filter(session()->getOldInput())) }}" inline-template>
	<div>
		<div class="row">

			<h5 class="form-section"><span class="tag tag-warning">STEP 01</span> PROPERTY INFORMATION</h5>

			<input type="hidden" id="deal-id" value="{{ isset($property) ? $property->id : 0 }}">

			<!--- Address Field --->
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('address', 'Address:') !!} <span class="red">*</span>
				{!! Form::text('address', null, ['class' => 'form-control', 'v-model' => 'address']) !!}
			</div>

			<!--- City Field --->
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::label('city', 'City:') !!} <span class="red">*</span>
				{!! Form::text('city', null, ['class' => 'form-control', 'v-model' => 'city']) !!}
			</div>

			<!--- State Field --->
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::label('state', 'State:') !!} <span class="red">*</span>
				{!! Form::select('state', [], null, ['class' => 'form-control', 'v-model' => 'state']) !!}
			</div>

			<!--- Country Field --->
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::label('country', 'Country:') !!}
				{!! Form::select('country', country_options(), null, ['class' => 'form-control']) !!}
			</div>

			<!--- Postal code Field --->
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::label('postal_code', 'Postal Code:') !!} <span class="red">*</span>
				{!! Form::text('postal_code', null, ['class' => 'form-control', 'v-model' => 'postal_code ']) !!}
			</div>

			<div class="clearfix"></div>

			<!--- Bedrooms Field --->
			<div class="form-group input-group-sm col-sm-2 small">
				{!! Form::label('bedrooms', 'Bedrooms:') !!}
				{!! Form::select('bedrooms', bedrooms(), null, ['class' => 'form-control']) !!}
			</div>

			<!--- Bathrooms Field --->
			<div class="form-group input-group-sm col-sm-2 small">
				{!! Form::label('bathrooms', 'Bathrooms:') !!}
				{!! Form::select('bathrooms', bathrooms(), null, ['class' => 'form-control']) !!}
			</div>

			<!--- Type Field --->
			<div class="form-group input-group-sm col-sm-2 small">
				{!! Form::label('type', 'Type:') !!} <span class="red">*</span>
				{!! Form::select('type', property_types(), null, ['class' => 'form-control']) !!}
			</div>

			<!--- Year built Field --->
			<div class="form-group input-group-sm col-sm-2 small">
				{!! Form::label('year_built', 'Year Built:') !!}
				{!! Form::select('year_built', year_built(), null, ['class' => 'form-control']) !!}
			</div>

			<!--- Square feet Field --->
			<div class="form-group input-group-sm col-sm-2 small">
				{!! Form::label('square_feet', 'Square Feet:') !!} <span class="red">*</span>
				{!! Form::number('square_feet', null, ['class' => 'form-control', 'v-model' => 'sqft']) !!}
			</div>

			<!--- Lot size Field --->
			<div class="form-group input-group-sm col-sm-2 small">
				{!! Form::label('lot_size', 'Lot Size:') !!}
				{!! Form::text('lot_size', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Description Field --->
			<div class="form-group input-group-sm col-sm-12">
				{!! Form::label('description', 'Description (This information will be displayed on property card & report):') !!} <span class="red">*</span>
				{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Notes Field --->
			<div class="form-group input-group-sm col-sm-12">
				{!! Form::label('notes', 'Internal Notes (This information will NOT be displayed on property card & report):') !!}

				<div class="row" v-for="(note, index) in notes" v-if="notes.length">
					<div class="col-md-11">
						<div class="input-group-sm">
							<input type="text" name="notes[]" v-model="note.text" class="form-control" placeholder="Note...">
							<small v-if="note.created_at" class="text-muted">@{{ note.created_at | datetime }}</small>
						</div>
					</div>
					<div class="col-md-1">
						<a href="#" v-if="index !== 0" class="btn btn-danger btn-sm" @click.prevent="removeNote(index)">Remove</a>
					</div>
					<div class="clearfix"></div>
					<br>
				</div>

				<a href="javascript:void(0);" @click.prevent="addNote" class="btn btn-primary btn-sm">+ Add Note</a>
			</div>

			<div class="clearfix"></div>

			<div class="form-group input-group-sm col-sm-12">
			<p>Property Photos: <small class="muted">(The first photo will be used as your property cover photo)</small></p>

				<div class="row">
					<div id="address_map_image_block" class="col-md-2" style="display: none;">
						<input type="file" name="images[]" id="address_map_image_input" style="display: none;"/>
						<img id="address_map_image" style="width: 210px; height: auto;" src="" alt="Address Map Image"/>
					</div>
					@if(isset($property) && $property->images)
						@foreach($property->images as $image)
							<div class="col-md-2">
								<input type="file" id="image-{{ $image->id }}" data-image-id="image-{{ $image->id }}" class="dropify" data-height="110" data-default-file="{{ url($image->thumbnail_path) }}" data-id="{{ $image->id }}" />
							</div>
						@endforeach

						@for($col = 1; $col <= (5 - $property->images->count()); $col++)
							<div class="col-md-2">
								<input type="file" name="images[]" class="dropify" data-height="140"/>
							</div>
						@endfor
					@else
						@for($col = 1; $col <= 5; $col++)
							<div class="col-md-2">
								<input type="file" name="images[]" class="dropify" data-height="140"/>
							</div>
						@endfor
					@endif
				</div>

			</div>

		</div>
		<div class="clearfix"></div>

		<div class="row">
			<!--- Seller Information --->
			<h5 class="form-section m-t-40"><span class="tag tag-primary">STEP 02</span> SELLER INFORMATION</h5>


			<!--- Seller name Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::label('seller_name', 'Name:') !!}
				{!! Form::text('seller_name', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Seller email Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::label('seller_email', 'Email:') !!}
				{!! Form::email('seller_email', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Seller mobile Field --->
			<div class="form-group input-group-sm col-sm-2 small">
				{!! Form::label('seller_mobile', 'Mobile Phone:') !!}
				{!! Form::hidden('seller_mobile', null, ['v-model' => 'seller_mobile']) !!}
				{!! Form::text('seller_mobile_formatted', isset($property) ? $property->seller_mobile : null, ['class' => 'form-control', 'id' => 'seller_mobile_formatted', '@blur' => 'set_seller_mobile']) !!}
			</div>

			<!--- Seller phone Field --->
			<div class="form-group input-group-sm col-sm-2 small">
				{!! Form::label('seller_phone', 'Alternate Phone:') !!}
				{!! Form::hidden('seller_phone', null, ['v-model' => 'seller_phone']) !!}
				{!! Form::text('seller_phone_formatted', isset($property) ? $property->seller_phone : null, ['class' => 'form-control', 'id' => 'seller_phone_formatted', '@blur' => 'set_seller_phone']) !!}
			</div>

			<!--- Seller address Field --->
			<div class="form-group input-group-sm col-sm-8 small">
				{!! Form::label('seller_address', 'Address:') !!}
				{!! Form::text('seller_address', null, ['class' => 'form-control']) !!}
			</div>

			<!--- Seller city Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::label('seller_city', 'City:') !!}
				{!! Form::text('seller_city', null, ['class' => 'form-control']) !!}
			</div>

			<!--- State Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::label('seller_state', 'State:') !!}
				{!! Form::select('seller_state', [], null, ['class' => 'form-control']) !!}
			</div>

			<!--- Country Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::label('seller_country', 'Country:') !!}
				{!! Form::select('seller_country', country_options(), null, ['class' => 'form-control']) !!}
			</div>

			<!--- Seller postal code Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::label('seller_postal_code', 'Postal Code:') !!}
				{!! Form::text('seller_postal_code', null, ['class' => 'form-control']) !!}
			</div>

		</div>
		<div class="clearfix"></div>

		<div class="row">

			<!--- Property Status Information --->
			<h5 class="form-section m-t-40"><span class="tag tag-danger">STEP 03</span> PROPERTY STATUS</h5>

			<!--- Deal Stage Field --->
			<div class="form-group input-group-sm col-sm-8 ">
				{!! Form::label('stage', 'What stage are you in closing this deal?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::select('stage', ['' => '-- Select --'] + property_stages(), null, ['class' => 'form-control', 'v-model' => 'stage']) !!}
			</div>

			<!--- Deal Type Field --->
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('deal_type', 'What type of deal do you anticipate this being?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::select('deal_type', ['' => '-- Select --'] + deal_types(), null, ['class' => 'form-control', 'v-model' => 'deal_type']) !!}
			</div>


			<!--- Status Field (Needs to be changed to Property Type)--->
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('status', 'How would you best categorize this type of property?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::select('status', ['' => '-- Select --'] + property_statuses(), null, ['class' => 'form-control']) !!}
			</div>

				<!--- Contract Status Field --->
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('contract_status', 'Is the property currently under contract?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4">
				<input type="hidden" name="contract_status" v-model="contract_status">

				<strong>
					<span v-if="contract_status">Yes</span>
					<span v-else>No</span>
				</strong>
				{{--No {!! Form::checkbox('contract_status', 1, null, ['data-size' => 'small', 'data-plugin' => 'switchery', 'data-secondary-color' => '#FF0000', 'data-color' => '#80C341', 'disabled', 'v-model' => 'contract_status']) !!} Yes--}}
			</div>

			<div v-if="contract_status">
				<div class="form-group input-group-sm col-sm-8">
					{!! Form::label('contract_period_days', 'Contract Period') !!}
					@isset($property)
						@if($property->contract_period)
						<strong>(Contract Expires in {{ $property->contract_period->diffInDays(Carbon\Carbon::today()) }} Days on {{ $property->contract_period->format('m/d/Y') }})</strong>
						@endif
					@endisset
				</div>
				<div class="form-group input-group-sm col-sm-4">
					{!! Form::select('contract_period_days', ['' => '-- Select --'] + contract_periods(), null, ['class' => 'form-control']) !!}
				</div>
			</div>

			<!--- Inspection Status Field --->
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('inspection_status', 'Have you had an inspection done on the property?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4">
				No {!! Form::checkbox('inspection_status', 1, null, ['data-size' => 'small', 'data-plugin' => 'switchery', 'data-secondary-color' => '#FF0000', 'data-color' => '#80C341']) !!} Yes
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">

			<h5 class="form-section m-t-40"><span class="tag tag-success">STEP 04</span> FINANCIAL INFORMATION</h5>


			<!--- Asking price Flag --->
			<!--
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('asking_price_flag', 'Do you know the current asking price for the property?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::checkbox('asking_price_flag', 1, null, ['data-size' => 'small', 'data-plugin' => 'switchery', 'data-secondary-color' => '#FF0000', 'data-color' => '#80C341', 'v-model' => 'asking_price_flag']) !!}
			</div>
			-->

			<!--- Asking price Field  --->
			<!--<div v-show="asking_price_flag"> -->
				<div class="form-group input-group-sm col-sm-8">
					{!! Form::label('asking_price', 'What is the asking price of the property (if available)?') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 small">
					{!! Form::hidden('asking_price', null, ['v-model' => 'asking_price']) !!}
					<money v-model="asking_price" class="form-control"></money>
				</div>
			<!--</div> -->

			<!--- Delinquent mortgage amount Field --->
			<div v-show="deal_type == 4 || deal_type == 5 || deal_type == 6">
				<div class="form-group input-group-sm col-sm-8">
					{!! Form::label('delinquent_mortgage_amount', 'What is the Mortgage Delinquent amount (if applicable)?') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 small">
					{!! Form::hidden('delinquent_mortgage_amount', null, ['v-model' => 'delinquent_mortgage_amount']) !!}
					<money v-model="delinquent_mortgage_amount" class="form-control"></money>
				</div>
			</div>

			<!--- Appraised Flag  --->

			<!--
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('appraised_value_flag', 'Do you have an official appraisal amount for this property?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::checkbox('appraised_value_flag', 1, null, ['data-size' => 'small', 'data-plugin' => 'switchery', 'data-secondary-color' => '#FF0000', 'data-color' => '#80C341', 'v-model' => 'appraised_value_flag']) !!}
			</div>
			-->
			<!--<div v-show="appraised_value_flag"> -->
				<!--- Appraised value Field --->
				<div class="form-group input-group-sm col-sm-8 font-light">
					{!! Form::label('appraised_value', 'What is the documented appraised value of this property?') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 small">
					{!! Form::hidden('appraised_value', null, ['v-model' => 'appraised_value']) !!}
					<money v-model="appraised_value" class="form-control"></money>
				</div>
			<!--</div>-->

				<!--- Fair market rent Field --->
				<div class="form-group input-group-sm col-sm-8 font-light">
					{!! Form::label('fair_market_rent', 'What is the Fair Market Rent?') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 small">
					{!! Form::hidden('fair_market_rent', null, ['v-model' => 'fair_market_rent']) !!}
					<money v-model="fair_market_rent" class="form-control"></money>
				</div>


				<!--- Actual monthly rent Field --->
			<div>
				<div class="form-group input-group-sm col-sm-8">
					{!! Form::label('actual_monthly_rent', 'What is the Actual Monthly Rent (if available)?') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 small">
					{!! Form::hidden('actual_monthly_rent', null, ['v-model' => 'actual_monthly_rent']) !!}
					<money v-model="actual_monthly_rent" class="form-control"></money>
				</div>
			</div>

			<!--- Annual taxes Field --->
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('annual_taxes', 'What are the Annual Taxes?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::hidden('annual_taxes', null, ['v-model' => 'annual_taxes']) !!}
				<money v-model="annual_taxes" class="form-control"></money>
			</div>

			<!--- Annual insurance Field --->
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('annual_insurance', 'What is the Annual Insurance amount?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::hidden('annual_insurance', null, ['v-model' => 'annual_insurance']) !!}
				<money v-model="annual_insurance" class="form-control"></money>
			</div>

			<!--- Estimated Rehab Amt Flag --->
			<!--
			<div class="form-group input-group-sm col-sm-8">
				{!! Form::label('estimated_rehab_flag', 'Do you know the current rehab estimate for the property?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4">
				No {!! Form::checkbox('estimated_rehab_flag', 1, null, ['data-size' => 'small', 'data-plugin' => 'switchery', 'data-secondary-color' => '#FF0000', 'data-color' => '#80C341', 'v-model' => 'estimated_rehab_flag']) !!} Yes
			</div>
			-->

			

			<!--- Estimated Rehab Category Field --->
				<!--
				<div class="form-group input-group-sm col-sm-8">
					{!! Form::label('rehab_estimate_category', 'If you had to choose, what level of rehab work is needed on this property?') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 small">
					{!! Form::select('rehab_estimate_category', [
						'' => '-- Select --',
						10 => 'Light Rehab',
						15 => 'Medium Rehab',
						25 => 'Full Rehab',
					], null, ['v-model' => 'rehab_estimate_category', 'class' => 'form-control']) !!}
				</div>
				-->

			<div v-show="deal_type == 1 || deal_type == 8">
				<!--- Wholesale Fee Field (NEW) --->
				<div class="form-group input-group-sm col-sm-8 font-light">
					{!! Form::label('wholesale_fee', 'What is your wholesale fee for this property?') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 small">
					{!! Form::hidden('wholesale_fee', null, ['v-model' => 'wholesale_fee']) !!}
					<money v-model="wholesale_fee" class="form-control"></money>
				</div>
			</div>

		</div>
		<div class="clearfix"></div>

		<div class="row">

			<!--- After Repair Value Comparables--->
			<h5 class="form-section m-t-40"><span class="tag tag-default">STEP 05</span> AFTER REPAIR VALUE COMPARABLES</h5>

			<div class="form-group input-group-sm col-sm-12">
				<p class="m-t-10 m-b-10 font-300">This section is used to calculate the After Repair Value of this property which will be used to determine how much you should pay for it. Enter 3 recent "sold" comparables below.</p>
			</div>

			{{--@if(isset($property))
				@foreach($property->comparable_properties as $key => $comparable)
					<input type="hidden" v-model="comparables[{{ $key }}].address" value="{{ $comparable->address }}">
					<input type="hidden" v-model="comparables[{{ $key }}].br" value="{{ $comparable->br }}">
					<input type="hidden" v-model="comparables[{{ $key }}].ba" value="{{ $comparable->ba }}">
					<input type="hidden" v-model="comparables[{{ $key }}].sqft" value="{{ $comparable->sqft }}">
					<input type="hidden" v-model="comparables[{{ $key }}].sold_price" value="{{ $comparable->sold_price }}">
				@endforeach
			@endif--}}

			<div v-for="(comparable, index) in comparables" v-if="comparables.length">
				<div class="form-group input-group-sm col-sm-4">
					<label class="small muted">Street Address</label>
					<input type="text" v-model="comparable.address" class="form-control">
				</div>
				<div class="form-group input-group-sm col-sm-1">
					<label class="small muted">BR</label>
					<input type="text" v-model="comparable.br" class="form-control">
				</div>
				<div class="form-group input-group-sm col-sm-1">
					<label class="small muted">BA</label>
					<input type="text" v-model="comparable.ba" class="form-control">
				</div>
				<div class="form-group input-group-sm col-sm-2">
					<label class="small muted">Sqft</label>
					<input type="number" v-model="comparable.sqft" class="form-control">
				</div>
				<div class="form-group input-group-sm col-sm-2">
					<label class="small muted">Sold Price</label>
					<money v-model="comparable.sold_price" class="form-control"></money>
					{{--<input type="text" v-model="comparable.sold_price" class="form-control">--}}
				</div>
				
				<div class="form-group input-group-sm col-sm-1 text-sm-center">
					<label class="small muted display-none">Remove</label>
					<a href="javascript:void(0);"  @click.prevent="removeComparable(index)">
						<span class="add-new-plus "><br><i class="fa fa-2x fa-minus-circle"></i> </span>
					</a>
				</div>
			</div>
			<div class="row">
			<div class="form-group input-group-sm col-sm-1 text-sm-center">
				<label class="small muted display-none">Add Comp</label>
				<a href="javascript:void(0);" @click.prevent="addComparable">
					<span class="add-new-plus "><br><i class="fa fa-2x fa-plus-circle"></i> </span>
				</a>
			</div>
			</div>

			@include('deals._zillow_comparables')

			<!--- Asking price Source (MLS Comp Field) --->
			<div class="form-group input-group-sm col-sm-8 m-t-20">
				{!! Form::label('asking_price_source', 'Are your comparable values from the MLS?') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4 m-t-20">
				No {!! Form::checkbox('asking_price_source', 1, null, ['data-size' => 'small', 'data-plugin' => 'switchery', 'data-secondary-color' => '#FF0000', 'data-color' => '#80C341']) !!} Yes
			</div>


		</div>
		<div class="clearfix"></div>

		<div class="row">

			<!--- Deal Calculations --->
			<h5 class="form-section m-t-40"><span class="tag tag-success">STEP 06</span> DEAL CALCULATIONS</h5>

				<div id="goal-list-scroll" class="table-responsive position-relative">
					<table class="table table-sm">

						<tbody>

							<tr>
								<td><p class="font-md-2 text-bold-700">AFTER REPAIR VALUE:</p></td>
								<td><p class="pull-left text-bold-400">@{{ after_repair_value | currency }}</p></td>
								<td><p class="font-md-2 text-bold-700">ARV $/SQFT:</p></td>
								<td><p class="pull-left text-bold-400">@{{ comparable_price_per_sqft | currency }}</p></td>
							</tr>
							<tr>
								<td><p class="font-md-2 text-bold-700">REHAB ESTIMATE:</p></td>
								<td><p class="pull-left text-bold-400">@{{ estimated_rehab | currency }}</p></td>
								<td><p class="font-md-2 text-bold-700">ANNUAL GROSS INCOME:</p></td>
								<td><p class="pull-left text-bold-400">@{{ annual_gross_income | currency }}</p></td>
							</tr>
							<tr>
								<td><p class="font-md-2 text-bold-700">ANNUAL EXPENSES:</p></td>
								<td><p class="pull-left text-bold-400">@{{ annual_expenses | currency }}</p></td>
								<td><p class="font-md-2 text-bold-700">ANNUAL NET OPERATING INCOME:</p></td>
								<td><p class="pull-left text-bold-400">@{{ annual_net_operating_income | currency }}</p></td>
							</tr>

						</tbody>
					</table>
				</div>

			
            
            <!--- Estimated Rehab Amt Field --->
			<!--<div v-show="estimated_rehab_flag"> -->
				<div class="form-group input-group-sm col-sm-8 m-t-40">
					{!! Form::label('rehab_estimate', 'What is the estimated rehab amount for this property?') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 m-t-40 small">
					@if(!empty($property->id))
						<a href="{{ route('calculator.template.create', $property->id) }}" target="_blank" class="btn btn-sm btn-info" style="color:#fff; margin-bottom:2px !important;">Rehab Calculator</a>
					@else
						<a href="{{ route('calculator.template.create') }}" target="_blank" class="btn btn-sm btn-info" style="color:#fff; margin-bottom:2px !important;">Rehab Calculator</a>
					@endif
					<a @click="openTemplateModalBox" class="btn btn-sm btn-info" style="color:#fff; margin-bottom:2px !important;">Rehab Templates</a>
					{!! Form::hidden('rehab_estimate', null, ['v-model' => 'rehab_estimate']) !!}
					{!! Form::hidden('cost_template_id', null, ['v-model' => 'cost_template_id']) !!}

					<money v-model="rehab_estimate" class="form-control"></money>
				</div>
			<!--</div>-->
            
            <div class="form-group input-group-sm col-sm-8 font-light">
					{!! Form::label('offer_scale_percentage', 'Select the offer scale value you will use for this deal.') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 small">
					{!! Form::select('offer_scale_percentage', [
					60 => '60%',
					65 => '65%',
					70 => '70%',
					75 => '75%',
					80 => '80%',
					85 => '85%',
				], null, ['v-model' => 'offer_scale_percentage', 'class' => 'form-control col-sm-2']) !!}
			</div>

			<!--- Offer Price Field --->
			<div class="form-group input-group-sm col-sm-8 m-t-10 font-light">
				{!! Form::label('offer_price', 'Enter the price you will pay for this property? ') !!}
			</div>
			<div class="form-group input-group-sm col-sm-4 m-t-10 small">
				{!! Form::hidden('offer_price', null, ['v-model' => 'offer_price']) !!}
				<money v-model="offer_price" class="form-control"></money>
			</div>

			<!--- Actual Net Profit Field --->
			<div v-if="deal_type == 2 && stage == 8">
				<div class="form-group input-group-sm col-sm-8 m-t-10 font-light">
					{!! Form::label('actual_net_profit', 'Enter the actual net profit of this property? ') !!}
				</div>
				<div class="form-group input-group-sm col-sm-4 m-t-10 small">
					{!! Form::hidden('actual_net_profit', null, ['v-model' => 'actual_net_profit']) !!}
					<money v-model="actual_net_profit" class="form-control"></money>
				</div>
			</div>

			<div class="clearfix"></div>
			<hr>
			<div class="clearfix"></div>

			<!--- Suggested Max Offer Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				<h5 class="m-t-20 m-b-10 font-300">Suggested Max Offer:<BR>
				<span class="font-small-2">Highest amount you should offer</span></h5>
				<h3 class="font-light m-t-5">@{{ suggested_maximum_offer | currency }}</h3>

			</div>

			<!--- Offer Price (Printed) --->
			<div class="form-group input-group-sm col-sm-4 small">
				<h5 class="m-t-20 m-b-10 font-300">Purchase Price:<BR>
				<span class="font-small-2">Property purchase price </span></h5>
				<h3 class="font-light m-t-5">@{{ offer_price | currency }}</h3>

			</div>
            
            <div v-if="deal_type == 1 || deal_type == 8">
                <!--- Sales Price Field (Wholesale Deals)--->
                <div class="form-group input-group-sm col-sm-4 small">
                    <h5 class="m-t-20 m-b-10 font-300">Adjusted Sales Price:<BR>
                    <span class="font-small-2">Adjusted sales price</span></h5>
                    <h3 class="font-light m-t-5">@{{ wholesale_value | currency }}</h3>
                    {!! Form::hidden('wholesale_value', null, ['v-model' => 'wholesale_value', 'readonly' => 'true']) !!}
                </div>
            </div>
            
            <div v-if="deal_type == 2">
                <!--- Gross profit Field --->
                <div class="form-group input-group-sm col-sm-3 small">
                    <h5 class="m-t-20 m-b-10 font-300">Gross Profit:<BR>
                        <span class="font-small-2">Gross profit before cost & fees</span></h5>
                    <h3 class="font-light m-t-5">@{{ gross_profit | currency }}</h3>
                    {!! Form::hidden('gross_profit', null, ['v-model' => 'gross_profit', 'readonly' => 'true']) !!}
                </div>
            </div>
                
            <div v-if="deal_type == 3">
                <!--- Cap Rate Field --->
                <div class="form-group input-group-sm col-sm-3 small">
                    <h5 class="m-t-20 m-b-10 font-300">Cap Rate:<BR>
                        <span class="font-small-2">Capitlization Rate</span></h5>
                    <h3 class="font-light m-t-5">@{{ cap_rate }}%</h3>
                    {!! Form::hidden('cap_rate', null, ['v-model' => 'cap_rate', 'readonly' => 'true']) !!}
                </div>
            </div>
            
                
            <div class="clearfix"></div>
			<hr>
			<div class="clearfix"></div>

            <div id="calculation-box p-t-40">    
                <div v-if="deal_type == 2 || deal_type == 3">
                    <div class="form-group input-group-sm col-sm-2 small">
                        <label for="">Retail Price</label>
                        {!! Form::hidden('retail_value', null, ['v-model' => 'retail_value']) !!}
                        <money v-model="retail_value" class="form-control"></money>
                    </div>

                    <div class="form-group input-group-sm col-sm-2 small">
                        <label for="">Commission Rate (%)</label>
                        {!! Form::text('commission_fee_rate', null, ['v-model' => 'commission_fee_rate', 'class' => 'form-control']) !!}
                    </div>

                    <div class="form-group input-group-sm col-sm-2 small">
                        <label for="">Closing Cost Rate (%)</label>
                        {!! Form::text('closing_cost_rate', null, ['v-model' => 'closing_cost_rate', 'class' => 'form-control']) !!}
                    </div>
                </div>
                <div v-if="deal_type == 2">
                    <div class="form-group input-group-sm col-sm-2 small">
                    	<label for="">Funding Fee</label>
                    	<select name="funding_fee_type" class="form-control col-sm-2" v-model="funding_fee_type">
                    		<option value="flat">Flat Fee</option>
                    		<option value="percent">Percentage Rate Fee</option>
                    	</select>
                    </div>
                    <div class="form-group input-group-sm col-sm-1 small" v-if="funding_fee_type == 'flat'">
                        <label for="">Funding Fee</label>
                        {!! Form::text('funding_fee', null, ['v-model' => 'funding_fee', 'class' => 'form-control']) !!}
                    </div>

                    <div class="form-group input-group-sm col-sm-1 small" v-if="funding_fee_type == 'percent'">
                        <label for="">Funding Fee</label>
                        {!! Form::text('funding_fee_rate', null, ['v-model' => 'funding_fee_rate', 'class' => 'form-control']) !!}
                    </div>

                    <div class="form-group input-group-sm col-sm-2 small">
                    	<label for="">Loan Origination Fee</label>
                    	<select name="loan_fee_type" class="form-control col-sm-2" v-model="loan_fee_type">
                    		<option value="flat">Flat Fee</option>
                    		<option value="percent">Percentage Rate Fee</option>
                    	</select>
                    </div>

                    <div class="form-group input-group-sm col-sm-1 small" v-if="loan_fee_type == 'flat'">
                        <label for="">Loan Fee</label>
                        {!! Form::text('loan_fee', null, ['v-model' => 'loan_fee', 'class' => 'form-control']) !!}
                    </div>

                    <div class="form-group input-group-sm col-sm-1 small" v-if="loan_fee_type == 'percent'">
                        <label for="">Loan Fee</label>
                        {!! Form::text('loan_fee_rate', null, ['v-model' => 'loan_fee_rate', 'class' => 'form-control']) !!}
                    </div>

                    <div class="form-group input-group-sm col-sm-2 small">
                        <label for="">Holding Days</label>
                        {!! Form::text('holding_days', null, ['v-model' => 'holding_days', 'class' => 'form-control']) !!}
                    </div>
                    
                    <div class="clearfix"></div>
			         <hr>
                    <div class="clearfix"></div>
                </div>
                
                
            </div>

			<div v-if="deal_type == 2">
				<div class="form-group input-group-sm col-sm-3 small">
					<h5 class="m-t-20 m-b-10 font-300">Commission Fee:<BR>
						<span class="font-small-2"></span></h5>
					<h3 class="font-light m-t-5">@{{ commission_fee | currency }}</h3>
					{!! Form::hidden('commission_fee', null, ['v-model' => 'commission_fee', 'readonly' => 'true']) !!}
				</div>

				<div class="form-group input-group-sm col-sm-3 small">
					<h5 class="m-t-20 m-b-10 font-300">Closing Cost:<BR>
						<span class="font-small-2"></span></h5>
					<h3 class="font-light m-t-5">@{{ closing_cost | currency }}</h3>
					{!! Form::hidden('closing_cost', null, ['v-model' => 'closing_cost', 'readonly' => 'true']) !!}
				</div>

				<div class="form-group input-group-sm col-sm-3 small">
					<h5 class="m-t-20 m-b-10 font-300">Funding Fee:<BR>
						<span class="font-small-2"></span></h5>
					<h3 class="font-light m-t-5">@{{ total_fee_amount | currency }}</h3>
					{!! Form::hidden('funding_fee_amount', null, ['v-model' => 'total_fee_amount', 'readonly' => 'true']) !!}
				</div>

				<div class="form-group input-group-sm col-sm-3 small">
					<h5 class="m-t-20 m-b-10 font-300">Estimated Net Profit:<BR>
						<span class="font-small-2"></span></h5>
					<h3 class="font-light m-t-5">@{{ estimated_net_profit | currency }}</h3>
					{!! Form::hidden('estimated_net_profit', null, ['v-model' => 'estimated_net_profit', 'readonly' => 'true']) !!}
				</div>
		</div>

			<div class="clearfix"></div>
			<!--- Equity position Field --->
<!--			<div class="form-group input-group-sm col-sm-12 m-t-20">
				<span class="pull-left">{!! Form::label('equity_position', 'Equity Position:') !!}</span>
					<span class="success font-600 m-t-10"> <span class="tag tag-success pull-right m-l-5"> @{{ equity_position }}%</span></span>
					<progress class="progress progress-success mt-1 mb-0 m-t-40" :value="equity_position" max="100"></progress>
				{!! Form::hidden('equity_position', null, ['v-model' => 'equity_position', 'readonly' => 'true']) !!}

				<p class="font-muted font-small-2">Important note: The Equity position is based on the Sales Price of the property.</p>
			<BR><BR>
			</div>-->

			<!--- Offer % Field --->
			<div class="form-group input-group-sm col-sm-6">
				<span class="pull-left">{!! Form::label('purchase', 'Offer Percentage:') !!}</span>
					<span class="success font-600 m-t-10"> <span class="tag tag-warning pull-right m-l-5"> @{{ purchase }}%</span></span>
					<progress class="progress progress-warning mt-1 mb-0 m-t-40" :value="purchase" max="100"></progress>
				{!! Form::hidden('purchase', null, ['v-model' => 'purchase', 'readonly' => 'true']) !!}
			</div>

			<!--- Rehab % Field --->
			<div class="form-group input-group-sm col-sm-6 ">
				<span class="pull-left">{!! Form::label('rehab', 'Rehab Percentage:') !!}</span>
					<span class="success font-600 m-t-10"> <span class="tag tag-primary pull-right m-l-5"> @{{ rehab }}%</span></span>
					<progress class="progress progress-primary mt-1 mb-0 m-t-40" :value="rehab" max="100"></progress>
				{!! Form::hidden('purchase', null, ['v-model' => 'purchase', 'readonly' => 'true']) !!}
				{!! Form::hidden('rehab', null, ['v-model' => 'rehab', 'readonly' => 'true']) !!}
			</div>

			<!--- Signal Field --->
			<!--
			<div class="form-group input-group-sm col-sm-6 small">
				{!! Form::label('signal', 'Based on the current equity position, your Deal Signal is') !!}
				{!! Form::select('signal', property_signals(), null, ['class' => 'form-control']) !!}
			</div>
			-->

			<!--- Hidden Form Fields --->

			<!--- Comparable Price Per SQ FT Field --->
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::hidden('comparable_price_per_sqft', null, ['v-model' => 'comparable_price_per_sqft']) !!}
			</div>

			<!--- After Repair Value Field --->
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::hidden('after_repair_value', null, ['v-model' => 'after_repair_value']) !!}
			</div>

			<!--- Estimated Rehab Field --->
			<div class="form-group input-group-sm col-sm-4">
				{!! Form::hidden('estimated_rehab', null, ['v-model' => 'estimated_rehab']) !!}
			</div>

			<!--- Annual expenses Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::hidden('annual_expenses', null, ['v-model' => 'annual_expenses']) !!}
			</div>

			<!--- Annual gross income Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::hidden('annual_gross_income', null, ['v-model' => 'annual_gross_income']) !!}
			</div>

			<!--- Annual net operating income Field --->
			<div class="form-group input-group-sm col-sm-4 small">
				{!! Form::hidden('annual_net_operating_income', null, ['v-model' => 'annual_net_operating_income']) !!}
			</div>

			<!--- Retail Price Field --->
				{!! Form::hidden('retail_value', null, ['v-model' => 'retail_value', 'readonly' => 'true']) !!}

			<!--- After Repair Value Field --->
				{!! Form::hidden('after_repair_value', null, ['v-model' => 'arv_estimate', 'readonly' => 'true']) !!}

			<!--- Purchase Field (Offer %)--->
				{!! Form::hidden('purchase', null, ['v-model' => 'purchase', 'readonly' => 'true']) !!}

			<!--- Rehab Field --->
				{!! Form::hidden('rehab', null, ['v-model' => 'rehab', 'readonly' => 'true']) !!}


			<div class="form-group input-group-sm col-sm-4 small">
				<label>API Key (If you wanna transfer this deal to another user)</label>
				{!! Form::text('transfer_key', null, ['class' => 'form-control', 'placeholder' => 'Optional']) !!}
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">

			<!--- Deal Simulator --->
			<h5 class="form-section m-t-40"><span class="tag tag-default">STEP 07</span> DEAL SIMULATOR</h5>

			<div class="form-group input-group-sm col-sm-12">
				<p class="m-t-10 m-b-10 font-300">The Deal Simulator uses our proprietary algorithm to determine which of the various investing strategies are the best fit for your deal.</p>
			</div>

			<div class="card input-group-sm col-sm-12 m-t-20" style="margin-bottom: 30px">
				<div class="row">
					<div v-if="is_good_wholesale" class="col-md-2">
						<img style="width: 50px" class="block img-responsive" src="/images/wholesale.svg" alt="Wholesale">
						<div class="mt-1">Wholesale</div>
					</div>
					<div v-if="is_good_fix_flip" class="col-md-2">
						<img style="width: 50px" class="block img-responsive" src="/images/fix-n-flip.svg" alt="Fix-N-Flip">
						<div class="mt-1">Fix-N-Flip</div>
					</div>
					<div v-if="is_good_buy_hold" class="col-md-2">
						<img style="width: 50px" class="block img-responsive" src="/images/buy-n-hold.svg" alt="Buy & Hold">
						<div class="mt-1">Buy & Hold</div>
					</div>
					<div v-if="is_good_brrrr" class="col-md-2">
						<img style="width: 50px" class="block img-responsive" src="/images/brrrr.svg" alt="BRRRR">
						<div class="mt-1">BRRRR</div>
					</div>
					<div v-if="is_good_lease_option" class="col-md-2">
						<img style="width: 50px" class="block img-responsive" src="/images/lease-option.svg" alt="Lease Option">
						<div class="mt-1">Lease Option</div>
					</div>
					<div v-if="is_good_short_sale" class="col-md-2">
						<img style="width: 50px" class="block img-responsive" src="/images/short-sale.svg" alt="Short Sale">
						<div class="mt-1">Short Sale</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 m-t-20">
				<!--- Submit Field --->
				<div class="text-right m-t-30 form-group pull-right">
					{!! Form::submit('Submit', ['class' => 'btn btn-success waves-light', 'v-on:click.prevent' => 'onSubmit']) !!}
					<a href="{{ route('deals.index') }}" class="btn btn-default waves-effect">Cancel</a>
				</div>
			</div>
		</div>
	  	 <modal-box v-if="showModal" @close="showModal = false" :my-templates-list="myTemplatesList" :use-template-selected="useTemplateSelected">
		    <!--
		      you can use custom content here to overwrite
		      default content
		    -->
		  </modal-box>
	</div>

	
</deal-calculations>