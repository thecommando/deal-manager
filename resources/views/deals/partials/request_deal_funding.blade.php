<div id="RequestDealModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="RequestDealModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Request Deal Funding</h4>
			</div>
			{!! Form::open(['method' => 'POST', 'route' => ['deals.request.funding', $property->id]]) !!}
			<div class="modal-body">

				{{-- Section 1 --}}
				<div class="row">
					<div class="col-md-12">
						<!--- Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('name', 'Name:') !!}
							{!! Form::text('name', $user->name, ['class' => 'form-control' , 'required' => 'required']) !!}
						</div>

						<!--- Email Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('email', 'Email:') !!}
							{!! Form::email('email', $user->email, ['class' => 'form-control', 'required' => 'required']) !!}
						</div>

						<!--- Phone Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('phone', 'Phone:') !!}
							{!! Form::text('phone', $user->mobile_phone, ['class' => 'form-control', 'required' => 'required']) !!}
						</div>

						<!--- Repair Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('repair_value', 'What is the After Repair Value?') !!}
							{!! Form::text('repair_value', null, ['class' => 'form-control', 'required' => 'required']) !!}
						</div>

						<!--- Estimated price Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('estimated_price', 'What is the Estimated Repair Cost?') !!}
							{!! Form::text('estimated_price', null, ['class' => 'form-control', 'required' => 'required']) !!}
						</div>

						<!--- Deal Amount Need --->
						<div class="form-group input-group-sm">
							{!! Form::label('amount_need', 'How much money do you need to do this deal?') !!}
							{!! Form::text('amount_need', null, ['class' => 'form-control', 'required' => 'required']) !!}
						</div>

						<!--- Note/Opinion --->
						<div class="form-group input-group-sm">
							{!! Form::label('note', 'In your opinion, do you think there is a possibility to do this deal without funding? If so, would you like our help to structure this for you?') !!}
							{!! Form::textarea('note', null, ['class' => 'form-control', 'required' => 'required']) !!}
						</div>
					</div>
				</div>
				<hr/>										
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary waves-effect waves-light">Send</button>
			</div>
			{!! Form::close() !!}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->