<div>
    @php($is_admin = auth()->user()->hasRole('admin'))
    @foreach($properties->chunk(4) as $chunk)
        <div class="row">
            @foreach($chunk as $property)
                <div class="col-xs-12 col-md-3">
                    <div class="card-deck-wrapper">
                        <div class="card-deck">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="m-b-0 m-t-0" title="{{ $property->address }}">
                                        <strong>
                                            <a href="{{ route('deals.show', $property->id) }}">{{ $property->short_address }}</a>
                                        </strong>
                                    </h5>
                                    <h6 class="m-b-10 m-t-0 m-l-5">{{ $property->city }}
                                        , {{ $property->state }} {{ $property->postal_code }}</h6>
                                    <span class="tag tag-success pull-left">{{ $property->stage }}</span>
                                    <span class="tag tag-default pull-right"> {{ $property->status }}</span>
                                </div>

                                <div class="card-body collapse in">
                                    <a href="{{ route('deals.show', $property->id) }}" class="image-popup" title="">
                                        @if($property->images->count())
                                            <div class="card-img-top img-fluid mb-1"
                                                 style="background: url('{{  $property->images->first()->path }}') center; background-size: cover; height: 225px !important;">
                                            </div>
                                        @else
                                            {{-- Need placeholder image here --}}
                                            <img src="/assets/images/no-propertyfound.png"
                                                 class="card-img-top mb-1 img-fluid" alt="work-thumbnail">
                                        @endif
                                    </a>
                                    @if (!$is_admin)
                                        <p>
                                            <!-- BEGIN HOUSINGALERTS WIDGET INSTANCE -->
                                        <div class="ha-widget-embed-f1-v1"
                                             data-ha-embed-mc="{{!empty($property->postal_code) ? $property->postal_code : 34473}}"
                                             data-ha-embed-ids="a70_ptnr-zcskey6pct"></div>
                                        <script src="https://www.housingalerts.com/ha_widgets/partners/embed.js"></script>
                                        <!-- END HOUSINGALERTS WIDGET INSTANCE -->
                                        </p>
                                    @endif
                                    <div class="card-block compact-card">

                                        <ul class="info-inline p-t-20">
                                            <li>
                                                <h3 class="m-b-0">{{ $property->bedrooms }}</h3>
                                                <p class="text-muted m-t-0 font-small-2">Bedroom(s)</p>
                                            </li>
                                            <li>
                                                <h3 class="m-b-0">{{ $property->bathrooms }}</h3>
                                                <p class="text-muted m-t-0 font-small-2">Bathroom(s)</p>
                                            </li>
                                            <li>
                                                <h3 class="m-b-0">{{ $property->square_feet}}</h3>
                                                <p class="text-muted m-t-0 font-small-2">Square Feet</p>
                                            </li>
                                        </ul>

                                        <div class="clearfix m-b-0"></div>

                                        <ul class="list-financials p-t-10">
                                            <li class="stats">
                                                <h6 class="m-b-0 m-l-0 p-l-0">{{ formatPrice($property->offer_price) }}</h6>
                                                <p class="text-muted m-t-0 font-small-2">Offer Price</p>
                                            </li>
                                            <li class="stats">
                                                <h6 class="m-b-0 m-l-0 p-l-0">{{ formatPrice($property->estimated_rehab) }}</h6>
                                                <p class="text-muted m-t-0 font-small-2">Rehab</p>
                                            </li>
                                            <li class="stats">
                                                <h6 class="m-b-0 m-l-0  p-l-0">{{ formatPrice($property->after_repair_value) }}</h6>
                                                <p class="text-muted m-t-0 font-small-2">ARV</p>
                                            </li>
                                        </ul>

                                        <!-- Equity percentage should be shown here -->
                                            <?php
                                            $grossProfit = 0;

                                            if ($property->offer_price > 0) {
                                                $tmpPrice = $property->offer_price > 0 ? $property->offer_price : 0;
                                            } else {
                                                $tmpPrice = $property->asking_price > 0 ? $property->asking_price : 0;
                                            }

                                            if ($property->after_repair_value > 0) {
                                                $grossProfit = $property->after_repair_value - ($tmpPrice + $property->rehab_estimate);
                                            }

                                            $equity = 0;

                                            $divideBy = $property->offer_price + $property->rehab_estimate;

                                            if ($divideBy > 0) {
                                                $equity = ($grossProfit / $divideBy) * 100;
                                            }
                                            ?>
                                        <p class="m-b-0 m-t-0 font-small-2">RETURN ON INVESTMENT: <span
                                                    class="text-success font-main-color pull-right">{{ round($equity, 2) }}%</span>
                                        </p>

                                        <progress class="progress progress-sm progress-primary mt-1 mb-0"
                                                  value="{{ round($equity, 2) }}" max="100"></progress>


                                        <p class="font-small-2 m-t-20"><strong>Deal
                                                Owner:</strong> {{ $property->user->name }} @if($property->finder)
                                                <span class="pull-right font-medium-1 text-primary"><i
                                                            class="fa fa-cloud-download m-t-10n"
                                                            data-toggle="tooltip" data-placement="bottom" title=""
                                                            data-original-title="Submitted by: {{ $property->finder->name }}"></i></span>
                                            @endif
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
</div>