<div class="border border-gray-700 p-5 mt-5 text-sm space-y-3">
    <h4 class="text-xl font-extrabold text-center">REAL ESTATE PURCHASE AND SALE AGREEMENT</h4>

    <p>In consideration of the agreements and conditions herein contained, the Buyer and Seller enter into this agreement for the sale of the below described Real Estate:</p>

    <table>
        <tr>
            <td style="width: 20%"><strong>PROPERTY ADDRESS:</strong></td>
            <td>{{ $property->address }}</td>
        </tr>
        <tr>
            <td></td>
            <td>{{ $property->city }}, {{ $property->state }} {{ $property->postal_code }}</td>
        </tr>
        
        <tr>
            <td><strong>SALE PRICE:</strong></td>
            <td>
                ${{ $purchase_price }}, {{ numberToWords($purchase_price) }}
            </td>
        </tr>
        <tr>
            <td><strong>Property Tax ID:</strong></td>
            <td>
                {{ $data['property_tax_id'] ?? 'N/A' }}
            </td>
        </tr>

        <tr>
            <td><strong>Addendums:</strong></td>
            <td>
                {{ !empty($data['addendums']) ? implode(',', $data['addendums']) : 'N/A' }}
            </td>
        </tr>

        <tr>
            <td><strong>Broker name:</strong></td>
            <td>
                {{ $data['broker_name'] ?? 'N/A' }}
            </td>
        </tr>

        <tr>
            <td><strong>Broker Phone:</strong></td>
            <td>
                {{ $data['broker_phone'] ?? 'N/A' }}
            </td>
        </tr>

        <tr>
            <td><strong>Time to accept:</strong></td>
            <td>
                {{ !empty($data['time_to_accept']) ? date('m/d/Y', strtotime($data['time_to_accept'])): 'N/A' }}
            </td>
        </tr>
    </table>

    <p>
        <span class="font-bold">TERMS:</span> Cash at closing paid in U.S. Funds. Property to be purchased AS-IS.
    </p>
    <p>
        <span class="font-bold">DEPOSIT:</span> A deposit of ${{ $initial_deposit ?: '_________' }} will be held in escrow. If title fails to transfer in accordance to this agreement, the deposit will be returned to the Buyer.
    </p>
    <p>
        <span class="font-bold">CLOSING:</span> To be conducted by a closer of the Buyer's choosing within 30 days of contract acceptance. Subject to a 14 day period to remedy any title issues.
    </p>
    <p>
        <span class="font-bold">EXISTING FINANCING:</span> If an existing mortgage is in effect, the Seller will grant a limited power of attorney to the Buyer to coordinate with the Seller's lender(s) and insurance provider(s).
    </p>
    <p>
        <span class="font-bold">PRORATIONS:</span> Property taxes will be prorated based on the current year’s tax without allowance for discounts including homestead or other exemptions. Rents will be current and prorated as of the date title transfers.
    </p>
    <p>
        <span class="font-bold">NO JUDGMENTS:</span> Seller warrants that there are no judgments against the subject property, and that there is no bankruptcy pending or contemplated by any titleholder.
    </p>
    <p>
        <span class="font-bold">CLOSING COSTS:</span> Will be paid by the Buyer.
    </p>
    <p>
        <span class="font-bold">INSPECTION:</span> Seller agrees to cooperate in making the property accessible for inspection(s) prior to the transfer of title. This agreement is contingent upon the Buyer’s approval of such inspection(s).
    </p>
    <p>
        <span class="font-bold">EASE OF ACCESS:</span> A lockbox may be installed to facilitate convenient interior access prior to transfer of title.
    </p>
    <p>
        <span class="font-bold">FIXTURES:</span> This sale shall include any and all fixtures to the property including but not limited to: HVAC equipment, ceiling fans, appliances, carpeting, mirrors, lights, storm doors and windows, garage door openers, TV reception equipment, outbuildings, and exterior plants and trees EXCEPT as otherwise noted.
    </p>
    <p>
        <span class="font-bold">ACCEPTANCE:</span> This agreement is void if modified from the originally offered terms and conditions.
    </p>
    <p>
        <span class="font-bold">SUCCESSORS:</span> This agreement and all provisions hereof shall be binding upon the inure to the benefit of all parties hereto and their respective heirs, executors, permitted assigns, representatives, and successors.
    </p>

    <table style="margin-top:40px;">
        <tr>
            <td style="width:45%;height:40px;border-bottom: 1px solid;">{{ $seller_name }}</td>
            <td></td>
            <td style="width:45%;height:40px;border-bottom: 1px solid">{{ $seller_email }}</td>
        </tr>
        <tr>
            <td><strong>Seller Name</strong></td>
            <td></td>
            <td><strong>Seller Email</strong></td>
        </tr>
        <tr>
            <td style="width:45%;height:40px;border-bottom: 1px solid">{{ $seller_phone }}</td>
            <td></td>
            <td style="width:45%;height:40px;border-bottom: 1px solid"></td>
        </tr>
        <tr>
            <td><strong>Seller Phone</strong></td>
            <td></td>
            <td><strong>Seller Signature & Date</strong></td>
        </tr>
        <tr>
            <td style="width:45%;height:40px;border-bottom: 1px solid">{{ $buyer_name }}</td>
            <td></td>
            <td style="width:45%;height:40px;border-bottom: 1px solid"></td>
        </tr>
        <tr>
            <td><strong>Buyer Name</strong></td>
            <td></td>
            <td><strong>Buyer Signature</strong></td>
        </tr>
    </table>
</div>
