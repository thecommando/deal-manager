<div class="description hidden-print m-t-40">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title"> Deal Activities</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        
                        <div class="card-box table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Activity</th>
                                <th>Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($activities as $activity)
                                <tr>
                                    <td>{!! $activity['message'] !!}</td>
                                    <td>{{ $activity['time']->diffForHumans() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>
