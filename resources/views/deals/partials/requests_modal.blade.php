<div id="requestsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="requestsModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="requestsModalLabel">Request</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<div style="text-align: center">
							<h3><strong>1</strong></h3>
							<h6><strong>Seller Locator Request</strong></h6>
							<p><small>Submit this request if you would like to use a skip tracing service to locate the seller for you.</small></p>
							<p><small>(Payment Required)</small></p>
                            <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#sellerLocatorRequestModal">Submit</button>
							{{--<a href="{{ route('deals.seller-locator-request', $property->id) }}" class="btn btn-success btn-lg">Submit</a>--}}
						</div>
					</div>
					<div class="col-md-4">
						<div style="text-align: center">
							<h3><strong>2</strong></h3>
							<h6><strong>Proof of Funds Request</strong></h6>
							<p><small>Submit this request if you need a proof of funds letter for an offer that you would like to make. This is not a request for funding.</small></p>
							<a href="{{ route('deals.proof-of-funds-request', $property->id) }}" class="btn btn-success btn-lg">Submit</a>
						</div>
					</div>
					<div class="col-md-4">
						<div style="text-align: center">
							<h3><strong>3</strong></h3>
							<h6><strong>Deal Funding Request</strong></h6>
							<p><small>Submit this request if you need a transactional funding for a deal that you have under contract and you have lined up a buyer.</small></p>
							<a href="{{ route('deals.deal-funding-request', $property->id) }}" class="btn btn-success btn-lg">Submit</a>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@include('deals.partials.seller_locator_request_modal')