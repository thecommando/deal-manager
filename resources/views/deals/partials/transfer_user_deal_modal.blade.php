<div id="transferUserDealModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Transfer Deal</h4>
			</div>
			{!! Form::open(['route' => ['deals.user-transfer', $property->id]]) !!}
			<div class="modal-body">
				<!--- Contact Field --->
				<div class="form-group">
				    {!! Form::label('api_key', 'API Key:') !!}
				    {!! Form::text('api_key', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('transfer_note', 'Internal Notes:') !!}
				    {!! Form::text('transfer_note', null, ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary waves-effect waves-light">Transfer</button>
			</div>
			{!! Form::close() !!}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->