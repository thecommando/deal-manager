<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-body">
				<div class="clearfix">
					<div class="pull-left col-md-8">
						<i class="fa fa-map-marker fa-4x pull-left"> </i> <h3 class="font-600 m-t-0 m-b-0 pull-left">{{ $property->address }}
						<BR><h5 >{{ $property->city }}, {{ $property->state }} {{ $property->postal_code }}</h5>
                            </h3>
					</div>
					<div class="pull-right">
						<span class="font-light m-b-0 m-t-0 p-t-0">SALES PRICE:</span>
							<h3 class="m-b-0 m-t-0 p-t-0"><strong>{{ formatPrice($property->wholesale_value) }}</strong>
						</h3>
						
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="pull-left m-t-10 col-md-9">
						<address>
							<ul class="list-inline">
								<li>
									<h4 class="m-b-0"><i class="fa fa-bed"></i> {{ $property->bedrooms }} </h4>
									<h6 class="text-muted m-t-0">Bedrooms</h6>
								</li>
								<li>
									<h4 class="m-b-0"><i class="fa fa-bath"></i> {{ $property->bathrooms }} </h4>
									<h6 class="text-muted m-t-0">Bathrooms</h6>
								</li>
								<li>
									<h4 class="m-b-0"><i class="fa fa-arrows-alt"></i> {{ $property->square_feet }} </h4>
									<h6 class="text-muted m-t-0">Square Feet</h6>
								</li>
								<li>
									<h4 class="m-b-0"><i class="fa fa-home"></i> {{ $property->year_built }} </h4>
									<h6 class="text-muted m-t-0">Year Built</h6>
								</li>
							</ul>
						</address>
						<div>
							<div class="clearfix m-t-5 m-b-30">
								<h5 class="small text-inverse font-600">QUICK DEAL OVERVIEW</h5>

								<p class="font-light">
									This deal is a {{ $property->type }} that was originally a {{ $property->status }} property.<br>
									<h5 class="small text-inverse font-600 m-t-20">DESCRIPTION</h5>
                                    {!! (nl2br($property->description)) !!}
                                    <h5>&nbsp;</h5>
									The <span class="text-highlight">comparable price per square foot is {{ formatPrice($property->comparable_price_per_sqft) }}</span>
									and has an <span class="text-highlight">estimated <br> {{ $property->equity_position }}% equity built in</span>. <BR><BR>The estimated ARV on this property is <span class="text-highlight">{{ formatPrice($property->after_repair_value) }}</span> and the rehab will be <BR>around <span class="text-highlight">{{ formatPrice($property->estimated_rehab) }}</span>. For the right person, this is a great deal.
								</p>
							</div>
						</div>
					<!--
					  <address>
						  <h5 class="m-b-0 p-b-0">CONTACT DETAILS</h5><br>
						  {{ $property->user->name }}<br>
						  <i class="fa fa-phone m-r-5"></i> {{ $property->user->mobile_phone }}
						  <i class="fa fa-envelope m-l-10"></i> {{ $property->user->email }}<br>
					  </address>
					  -->
						<div>
							@if($property->images()->count())
								<!-- <img src="{{ $property->defaultImage()->getIconPath() }}" > -->
							@else
								{{-- Need placeholder image here --}}
								<img src="{{ url('assets/images/no-propertyfound.png') }}" width="100px">
							@endif

							<div class="clearfix"></div>

							<div class="m-t-0">
								<div id="lightgallery">
									@foreach($property->images as $image)
										<a href="{{ $image->getPath() }}">
											<img src="{{ $image->getIconPath() }}" style="margin-top: 3px" />
										</a>
									@endforeach
								</div>
							</div>
						</div>
					</div>

                    <!-- Hide Property Details
					<div class="pull-right m-t-10 col-md-3">
						<h5 class=""><strong class="m-r-5">ARV Estimate: </strong> <span class="pull-right font-light">{{ formatPrice($property->after_repair_value) }}</span></h5>
						<h5 class=""><strong class="m-r-5">Offer Price: </strong> <span class="pull-right font-light">{{ formatPrice($property->offer_price) }}</span></h5>
						<h5 class=""><strong class="m-r-5">Wholesale Fee: </strong> <span class="pull-right font-light">{{ formatPrice($property->wholesale_fee) }}</span></h5>
						<h5 class=""><strong class="m-r-5">Comparable $/SQFT: </strong> <span class="pull-right font-light">{{ formatPrice($property->comparable_price_per_sqft) }}</span></h5>
						<h5 class="m-t-10"><strong class="m-r-5">Estimated Rehab: </strong> <span class="pull-right font-light">{{ formatPrice($property->estimated_rehab) }}</span></h5>
						<h5 class="m-t-10"><strong class="m-r-5">Estimated Equity: </strong> <span class="pull-right badge label-primary">{{ $property->equity_position }}%</span></h5>
						<div class="progress progress-bar-success-alt progress-sm m-b-5 m-t-10">
								<div class="progress-bar progress-bar-success progress-animated wow animated animated"
									 role="progressbar" aria-valuenow="{{ $property->equity_position }}" aria-valuemin="0" aria-valuemax="100"
									 style="width: {{ $property->equity_position }}%; visibility: visible; animation-name: animationProgress;">
								</div>
						</div>
					</div>
                    -->
				</div>
				<!-- end row -->

				<div class="m-h-40"></div>
				<hr>

				<div class="row">

					<div class="col-md-7 col-sm-7 col-xs-7">

						<h5 class="small text-inverse font-600">PROPERTY MAP</h5>

						<iframe height="200" width="350" border="0" marginwidth="0" marginheight="0" src="https://www.mapquest.com/embed/search/results?query={{ $property->getFullAddress() }} &page=0&zoom=11&maptype=map"></iframe>

					</div>

					<div class="col-md-5 col-sm-5 col-xs-5 p-l-20">
						<div class="clearfix m-t-0">
							<h5 class="small text-inverse font-600">SUBMITTING AN OFFER </h5>

							<small>
								This property will not be available for a long time, so if you are
								interested contact us using the information below.
							</small>
							<address>

								<h4 class="m-t-20 p-t-0 m-b-0"><strong>{{ $property->user->name }}</strong><br></h4>
								<h5>
							  <i class="fa fa-phone-square m-r-5"></i> {{ $property->user->mobile_phone }}<br>
							  <i class="fa fa-envelope m-r-5"></i> {{ $property->user->email }}</h5>
							</address>

						</div>
					</div>

			</div>

		</div>
		</div>
	</div>
</div>
<!-- end row -->