<div id="contractModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Contract Information</h4>
			</div>
			{!! Form::open(['method' => 'GET', 'route' => ['deals.show.contract', $property->id]]) !!}
			<div class="modal-body">

				{{-- Section 1 --}}
				<div class="row">
					<div class="col-md-6">
                        <h5 class="page-title">Seller Information</h5>
						<!--- Seller Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('seller_name', 'Seller:') !!}
							{!! Form::text('seller_name', $property->seller_name, ['class' => 'form-control']) !!}
						</div>

						<!--- Seller Email Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('seller_email', 'Seller Email:') !!}
							{!! Form::email('seller_email', $property->seller_email, ['class' => 'form-control']) !!}
						</div>

						<!--- Seller Phone Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('seller_phone', 'Seller Phone:') !!}
							{!! Form::text('seller_phone', $property->seller_phone, ['class' => 'form-control']) !!}
						</div>

						<!--- Seller address Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('seller_address', 'Seller Address:') !!}
							{!! Form::text('seller_address', $property->seller_address, ['class' => 'form-control']) !!}
						</div>

						<!--- Seller city Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('seller_city', 'Seller City:') !!}
							{!! Form::text('seller_city', $property->seller_city, ['class' => 'form-control']) !!}
						</div>

						<!--- Seller state Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('seller_state', 'Seller State:') !!}
							{!! Form::text('seller_state', $property->seller_state, ['class' => 'form-control']) !!}
						</div>

						<!--- Seller zip Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('seller_zip', 'Seller Zip:') !!}
							{!! Form::text('seller_zip', $property->seller_postal_code, ['class' => 'form-control']) !!}
						</div>
					</div>

					<div class="col-md-6">
                        <h5 class="page-title">Buyer Information</h5>
						<!--- Buyer name Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('buyer_name', 'Buyer Name:') !!}
							{!! Form::text('buyer_name', $user->name, ['class' => 'form-control']) !!}
						</div>

						<!--- Buyer address Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('buyer_address', 'Buyer Address:') !!}
							{!! Form::text('buyer_address', $user->address, ['class' => 'form-control']) !!}
						</div>

						<!--- Buyer city Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('buyer_city', 'Buyer City:') !!}
							{!! Form::text('buyer_city', $user->city, ['class' => 'form-control']) !!}
						</div>

						<!--- Buyer state Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('buyer_state', 'Buyer State:') !!}
							{!! Form::text('buyer_state', $user->state, ['class' => 'form-control']) !!}
						</div>

						<!--- Buyer zip Field --->
						<div class="form-group input-group-sm">
							{!! Form::label('buyer_zip', 'Buyer Zip:') !!}
							{!! Form::text('buyer_zip', $user->postal_code, ['class' => 'form-control']) !!}
						</div>
					</div>
				</div>

				<hr/>

				{{-- Section 2 --}}
                
                <div class="row">
                        <!--- Street address Field --->
                        <div class="form-group input-group-sm col-sm-12">
                            {!! Form::label('street_address', 'Street Address:') !!}
                            {!! Form::text('street_address', $property->address, ['class' => 'form-control']) !!}
                        </div>

                        <!--- City Field --->
                        <div class="form-group input-group-sm col-sm-4">
                            {!! Form::label('city', 'City:') !!}
                            {!! Form::text('city', $property->city, ['class' => 'form-control']) !!}
                        </div>

                        <!--- State Field --->
                        <div class="form-group input-group-sm col-sm-4">
                            {!! Form::label('state', 'State:') !!}
                            {!! Form::text('state', $property->state, ['class' => 'form-control']) !!}
                        </div>

                        <!--- Zip Field --->
                        <div class="form-group input-group-sm col-sm-4">
                            {!! Form::label('zip', 'Zip:') !!}
                            {!! Form::text('zip', $property->postal_code, ['class' => 'form-control']) !!}
                        </div>

                        <!--- Described as Field --->
                        <div class="form-group input-group-sm col-sm-12">
                            {!! Form::label('property_tax_id', 'Property Tax ID:') !!}
                            {!! Form::text('property_tax_id', null, ['class' => 'form-control']) !!}
                        </div>
                </div>

				{{-- Section 3 --}}

				<!--- Additional personal property Field --->
				<div class="form-group input-group-sm">
					{{--{!! Form::label('additional_personal_property', 'Additional personal property:') !!}--}}
				    {!! Form::hidden('additional_personal_property', null, ['class' => 'form-control']) !!}
				</div>


				{{-- Section 4 --}}
                <div class="row">
                    
                    <!--- Purchase price Field --->
                    <div class="form-group input-group-sm col-sm-4">
                        {!! Form::label('purchase_price', 'Purchase price:') !!}
                        {!! Form::text('purchase_price', $property->offer_price, ['class' => 'form-control']) !!}
                    </div>

                    <!--- Initial deposit Field --->
                    <div class="form-group input-group-sm col-sm-4">
                        {!! Form::label('initial_deposit', 'Initial deposit:') !!}
                        {!! Form::text('initial_deposit', null, ['class' => 'form-control']) !!}
                    </div>

                    <!--- Time to accept Field --->
                    <div class="form-group input-group-sm col-sm-4">
                        {!! Form::label('time_to_accept', 'Time to accept:') !!}
                        {!! Form::text('time_to_accept', null, ['class' => 'form-control datepicker', 'placeholder' => 'mm/dd/yyyy']) !!}
                    </div>
                </div>

				<!--- Additional deposit Field --->
				<div class="form-group input-group-sm">
					{{--{!! Form::label('additional_deposit', 'Additional deposit:') !!}--}}
				    {!! Form::hidden('additional_deposit', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Proceeds of financing Field --->
				<div class="form-group input-group-sm">
				 	{{--{!! Form::label('proceeds_of_financing', 'Proceeds of financing:') !!}--}}
				    {!! Form::hidden('proceeds_of_financing', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Other option Field --->
				<div class="form-group input-group-sm">
				 	{{--{!! Form::label('other_option', 'Other option:') !!}--}}
				    {!! Form::hidden('other_option', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Balance amount Field --->
				<div class="form-group input-group-sm">
					{{--{!! Form::label('balance_amount', 'Balance amount:') !!}--}}
				    {!! Form::hidden('balance_amount', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Total price Field --->
				<div class="form-group input-group-sm">
					{{--{!! Form::label('total_price', 'Total price:') !!}--}}
				    {!! Form::hidden('total_price', null, ['class' => 'form-control']) !!}
				</div>


				{{-- Section 6 --}}

				<!--- Contingency amount Field --->
				<div class="form-group input-group-sm">
					{{--{!! Form::label('contingency_amount', 'Contingency amount:') !!}--}}
				    {!! Form::hidden('contingency_amount', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Maximum interest rate Field --->
				<div class="form-group input-group-sm">
					{{--{!! Form::label('maximum_interest_rate', 'Maximum interest rate:') !!}--}}
				    {!! Form::hidden('maximum_interest_rate', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Contingency years Field --->
				<div class="form-group input-group-sm">
				 	{{--{!! Form::label('contingency_years', 'Contingency years:') !!}--}}
				    {!! Form::hidden('contingency_years', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Commitment date Field --->
				<div class="form-group input-group-sm">
					{{--{!! Form::hidden('commitment_date', 'Commitment date:') !!}--}}
					{!! Form::hidden('commitment_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'mm/dd/yyyy']) !!}
				</div>

				<!--- Contingency type Field --->
				{{--<div class="form-group input-group-sm">
				 	{!! Form::label('contingency_type', 'Contingency type:') !!}
				    {!! Form::select('contingency_type', [
						'Conventional Fixed' => 'Conventional Fixed',
						'Variable' => 'Variable',
						'FHA' => 'FHA',
						'VA' => 'VA',
						'CHFA' => 'CHFA',
						'Other' => 'Other',
				    ], null, ['class' => 'form-control']) !!}
				</div>--}}


				{{-- Section 8 --}}

				<!--- Inspection completion date Field --->
				<div class="form-group input-group-sm">
				{{--{!! Form::label('inspection_completion_date', 'Inspection completion date:') !!}--}}
				    {!! Form::hidden('inspection_completion_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'mm/dd/yyyy']) !!}
				</div>

				<!--- Buyer election date Field --->
				<div class="form-group input-group-sm">
				    {{--{!! Form::label('buyer_election_date', 'Buyer election date:') !!}--}}
				    {!! Form::hidden('buyer_election_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'mm/dd/yyyy']) !!}
				</div>


				{{-- Section 17 --}}

				<div class="row">
					<!--- Broker name Field --->
					<div class="form-group input-group-sm col-sm-6">
						{!! Form::label('broker_name', 'Broker name:') !!}
						{!! Form::text('broker_name', null, ['class' => 'form-control']) !!}
					</div>

					<!--- Broker phone Field --->
					<div class="form-group input-group-sm col-sm-6">
						{!! Form::label('broker_phone', 'Broker phone:') !!}
						{!! Form::text('broker_phone', null, ['class' => 'form-control']) !!}
					</div>
				</div>


				{{-- Section 20 --}}

				<!--- Addendums Field --->
				<div class="form-group input-group-sm">
				    {!! Form::label('addendums', 'Addendums:') !!}
				    <label>{!! Form::checkbox('addendums[]', 'Seller’s Property Condition Disclosure') !!} Seller’s Property Condition Disclosure</label>
				    <label>{!! Form::checkbox('addendums[]', 'Agency Disclosure') !!} Agency Disclosure</label>
				    <label>{!! Form::checkbox('addendums[]', 'Title') !!} Title</label>
				    <label>{!! Form::checkbox('addendums[]', 'Lead hazards') !!} Lead hazards</label>
				    <label>{!! Form::checkbox('addendums[]', 'Dual Agency Consent') !!} Dual Agency Consent</label>
				    <label>{!! Form::checkbox('addendums[]', 'Multi-family or Commercial Property Rider') !!} Multi-family or Commercial Property Rider</label>
				    <label>{!! Form::checkbox('addendums[]', 'Other') !!} Other</label>
				</div>


				{{-- Section 26 --}}

				


				{{-- Section 27 --}}

				<hr/>
				<div class="form-group">
					{!! Form::label('send_to', 'Send to?') !!}
					{!! Form::select('send_to', ['' => '-- Select contact --'] + get_contacts(), null, ['class' => 'form-control select2']) !!}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary waves-effect waves-light">Continue</button>
			</div>
			{!! Form::close() !!}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->