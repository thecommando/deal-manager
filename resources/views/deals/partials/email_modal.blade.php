@inject('emailTemplate', 'App\Services\EmailTemplate')

<deal-email inline-template>
<div id="emailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Email Deal Summary</h4>
			</div>
			{!! Form::open(['route' => ['deals.email', $property->id]]) !!}
			<div class="modal-body">

				<div class="form-group">
					<p><label>Send To?</label></p>
					<input type="radio" v-model="to" name="to" id="contacts" value="contacts">
					<label for="contacts">Contacts</label>

					<input type="radio" v-model="to" name="to" id="group" value="group">
					<label for="group">Contact Group</label>

					<input type="radio" v-model="to" name="to" id="contact_category" value="contact_category">
					<label for="contact_category">Contact Category</label>

					<input type="radio" v-model="to" name="to" id="other" value="other">
					<label for="other">Other Contact</label>
				</div>

				<!--- Contact Field --->
				<div class="form-group" v-show="to === 'contacts'">
				    {!! Form::label('contact', 'Contact:') !!}
				    {!! Form::select('contact[]', get_contacts(), null, ['class' => 'form-control select2 select2-multiple', 'multiple']) !!}
				</div>

				{{-- Contact Group --}}
				<div class="form-group" v-show="to === 'group'">
					{!! Form::label('group', 'Contact Group:') !!}
					{!! Form::select('group', [null => '-- Select --'] + \App\ContactGroup::where('user_id', auth()->id())->pluck('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
				</div>

				{{-- Contact Category --}}
				<div class="form-group" v-show="to === 'contact_category'">
					{!! Form::label('contact_category', 'Contact Category:') !!}
					{!! Form::select('contact_category', [null => '-- Select --'] + \App\ContactCategory::pluck('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
				</div>

				{{-- Other --}}
				<div class="form-group" v-show="to === 'other'">
					{!! Form::label('other', 'Email:') !!}
					{!! Form::email('other', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Subject Field --->
				<div class="form-group">
				    {!! Form::label('subject', 'Subject:') !!}
				    {!! Form::text('subject', null, ['class' => 'form-control']) !!}
				</div>

				<!--- Message Field --->
				<div class="form-group">
				    {!! Form::label('message', 'Message:') !!}
					<select v-model="selectedTemplate" class="form-control" @change="selectTemplate">
						<option value="">-- Select message template --</option>
						@foreach($emailTemplate->all($property) as $title => $template)
							<option value="{{ $template }}">{{ $title }}</option>
						@endforeach
						{{--<option v-for="template in templates" :value="template">@{{ template }}</option>--}}
					</select>
				    {!! Form::textarea('message', null, ['class' => 'form-control', 'id' => 'mail-message', 'v-model' => 'message']) !!}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary waves-effect waves-light">Send</button>
			</div>
			{!! Form::close() !!}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</deal-email>