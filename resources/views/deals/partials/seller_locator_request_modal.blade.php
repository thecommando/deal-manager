<div id="sellerLocatorRequestModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sellerLocatorRequestModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="sellerLocatorRequestModalLabel">Seller Locator Request</h4>
			</div>
			<div class="modal-body">
				<p>The fee for this request is listed below. A confirmation will be sent to your email address once you complete the payment below. The report usually takes 48-72 hours.</p>
				<p>Price: <strong>$10</strong></p>
				<form method="POST" action="{!! route('deals.seller-locator-request', $property) !!}" id="payment-form">

					{!! csrf_field() !!}

					<!-- Credit Card Number -->
					<div class="form-group">
						<label for="cc-number">Credit Card Number</label>
						<input type="text" id="cc-number" class="form-control" data-stripe="number" placeholder="**** **** **** {!! $user->card_last_four ?:'****' !!}" required>
					</div>

					<!-- Expiration Date -->
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label>Expiration Month</label>
								<select class="form-control" data-stripe="exp-month">
									<option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option>
								</select>
							</div>
							<div class="col-md-3">
								<label>Expiration Year</label>
								<select class="form-control" data-stripe="exp-year">
									@for ($year = \Carbon\Carbon::today()->year; $year <= \Carbon\Carbon::today()->addYears(15)->year; $year++)
										<option value="{{ $year }}">{{ $year }}</option>
									@endfor
								</select>
							</div>
							<div class="col-md-3">
								<label for="cvv">CVV Number</label>
								<input type="text" id="cvv" class="form-control" data-stripe="cvc" required>
							</div>
						</div>
					</div>

					<div class="form-group text-right">
						<button type="submit" class="btn btn-success">Pay & Request</button> <span class="payment-errors margin-left-10 font-size-16" style="color: red"></span>
						<button type="button" data-dismiss="modal" aria-hidden="true" class="btn btn-default waves-effect">Cancel</button>
					</div>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

