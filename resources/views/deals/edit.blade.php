@extends('layouts.default')

@section('styles')
	<link href="/assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

<!-- Page-Title -->
<div class="content-header row">
      <div class="content-header-left col-md-6 col-xs-12 mb-2">
        <h3 class="content-header-title mb-0">Edit Existing Deal</h3>
        <div class="row breadcrumbs-top">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
              </li>
              <li class="breadcrumb-item"><a href="{{ route('deals.index') }}">Deal Management</a>
              </li>
              <li class="breadcrumb-item active">Edit Existing Deal
              </li>
            </ol>
          </div>
        </div>
      </div>    
</div>


	{!! Form::model($property, ['route' => ['deals.update', $property->id], 'method' => 'put', 'files' => true, 'id' => 'deal-form']) !!}
<div class="content-detached content-left">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card">
           
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                    
                        <div class="col-md-12">
                            @include('partials.errors')
                        </div>

                        <div class="col-lg-12">
                            <div class="card-box">
                                @include('deals.form')
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>

<!-- Sidebar Content Area -->
<div class="sidebar-detached sidebar-right">
    <div class="sidebar">
        <div class="sidebar-content card">
            <div class="card-block">
                <div class="category-title pb-1">
                    <h6>INSTRUCTIONS</h6>
                </div>

                <p class="card-text">Complete the necessary information so that the DMP "deal simulator" can tell you the best strategy for your real estate deal.</p>

            </div>
        </div>

    </div>
</div>

{!! Form::close() !!}

@endsection

@section('scripts')
	@include('partials.location-script', ['country' => $property->country, 'state' => $property->state])
	@include('partials.location-script', ['country' => $property->seller_country, 'state' => $property->seller_state, 'country_id' => 'seller_country', 'state_id' => 'seller_state'])

	<script src="/assets/plugins/fileuploads/js/dropify.min.js"></script>
	<script src="/assets/js/jquery.maskMoney.min.js"></script>
	<script src="/assets/js/jquery.mask.min.js"></script>
	{{--<script src="/assets/app/js/deal-calculations.js"></script>--}}

	<script type="text/javascript">
        $(function() {
            $('#seller_mobile_formatted').mask('(000) 000-0000');
            $('#seller_phone_formatted').mask('(000) 000-0000');
        });

		$('.dropify').dropify({
			messages: {
				'default': 'Click or drag & drop a photo.',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
			},
			error: {
				'fileSize': 'The file size is too big (1M max).'
			}
		});

		var drEvent = $('.dropify').dropify();

		drEvent.on('dropify.beforeClear', function (event, element) {
			let isConfirmed = confirm("Do you really want to delete?");

			let imageId = $(this).data("id");
			let imageControl = $("#" + $(this).data("image-id"));

			if (!isConfirmed) return false;

			$.ajax({
				url: "/api/properties/image/remove/" + imageId,
				success: function (result) {
					imageControl.dropify().data('dropify').destroy();
					imageControl.parent().remove();
					imageControl.remove();
				}
			});
		});
	</script>
@endsection