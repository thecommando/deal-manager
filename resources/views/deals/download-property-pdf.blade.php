<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print</title>
    <style>
        * {
            box-sizing: border-box;
        }

        @media print {

            .no-print,
            .no-print * {
                display: none !important;
            }

            .print-m-0 {
                margin: 0 !important;
            }
        }

        .btn {
            padding: 10px 17px;
            border-radius: 3px;
            background: #f4b71a;
            border: none;
            font-size: 12px;
            margin: 10px 5px;
        }

        .toolbar {
            background: #333;
            width: 100vw;
            position: fixed;
            left: 0;
            top: 0;
            text-align: center;
        }

        .cert-container {
            margin: 0px 0 10px 0;
            width: 100%;
            display: flex;
            justify-content: center;
        }

        .cert {
            width: 700px;
            height: 800px;
            padding: 15px 20px;
            position: relative;
            z-index: -1;
        }

        .cert-bg {
            position: absolute;
            left: 0px;
            top: 0;
            z-index: -1;
            width: 100%;
        }

        .cert-content {
            width: 100%;
            /* height: 470px; */
            padding: 0 20px 0px 20px;
            font-family: Arial, Helvetica, sans-serif;
        }


        small {
            font-size: 14px;
            line-height: 12px;
        }

        .bottom-txt {
            padding: 12px 5px;
            display: flex;
            justify-content: space-between;
            font-size: 16px;
        }

        .bottom-txt * {
            white-space: nowrap !important;
        }

        .other-font {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 0;
            border-bottom: 2px solid #cccc;
            margin-bottom: 15px;
            padding-bottom: 10px;
        }

        .table-full .table-inner {
            background-color: #f1f1f1;
            padding: 15px;

        }

        .ml-215 {
            margin-left: 215px;
        }

        .top-header h2,
        .top-header p {
            margin: 0;
        }

        .bottom-txt.top-header {
            border-bottom: 2px solid #12a2b9;
        }

        .top-header h2 {
            font-weight: bold;
            font-size: 20px;
            margin-bottom: 5px;
        }

        .top-header p {
            font-size: 16px;
            margin-bottom: 5px;
        }

        .data-table {
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }

        .data-table td,
        .data-table th {
            padding: 8px;
        }

        .padding-x-0.data-table td {
            padding-left: 0;
            padding-right: 0;
        }

        .gab-20 {
            margin-bottom: 20px;
        }
        .bottom-text-right p {
    margin-top: 0;
    margin-bottom: 5px;
    text-align: right;
}
.bottom-text-right h3 {
    margin-bottom: 5px;
    margin-top: 0;
    text-align: right;
}
    </style>

</head>

<body>




    
    <div class="cert-container print-m-0">
        <div id="content2" class="cert">
            <!-- <img
          src="https://edgarsrepo.github.io/certificate.png"
          class="cert-bg"
          alt=""
        /> -->

            <div class="cert-content">
                <div class="bottom-txt top-header gab-20">
                    <div>
                        <h1>Property Report</h1>
                        <p>{{ $property->address }}</p>
                        <p>{{ $property->city }}, {{ $property->state }} {{ $property->postal_code }}</p>
                    </div>
                </div>
                <table class="data-table" border="1">
                    <thead>
                        <tr>
                            <th>Bedrooms</th>
                            <th>Bathrooms</th>
                            <th>Square Feet </th>
                            <th>Year Built</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $property->bedrooms }}</td>
                            <td>{{ $property->bathrooms }}</td>
                            <td>{{ $property->square_feet }}</td>
                            <td>{{ $property->year_built }}</td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <div class="table-full" style="margin-bottom: 20px;">
                        <h1 class="other-font">Property Status</h1>
                        <div class="table-inner">
                            <table class="data-table padding-x-0" border="0">
                                <tbody>
                                    <tr>
                                        <td>What stage are you in closing this deal?</td>
                                        <td style="text-align: right;"><strong>{{$property->stage}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>What type of deal do you anticipate this being? </td>
                                        <td style="text-align: right;"><strong>{{$property->deal_type}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>How would you best categorize this type of property?</td>
                                        <td style="text-align: right;"><strong>{{$property->status}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Is the property currently under contract?</td>
                                        <td style="text-align: right;"><strong>{{ (!empty($property->contract_status) && $property->contract_status== 1) ? 'Yes' : 'No'}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Have you had an inspection done on the property?</td>
                                        <td style="text-align: right;"><strong>{{ (!empty($property->inspection_status) && $property->inspection_status== 1) ? 'Yes' : 'No'}}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-full">
                        <h1 class="other-font">Financial Information</h1>
                        <div class="table-inner">
                        <table class="data-table padding-x-0" border="0">
                            <tbody>
                                <tr>
                                    <td>What is the asking price of the property (if available)?</td>
                                    <td style="text-align: right;"><strong>{{ formatPrice($property->asking_price) }}</strong></td>
                                </tr>
                                <tr>
                                    <td>What is the Mortgage Delinquent amount (if applicable)? </td>
                                    <td style="text-align: right;"><strong>{{ formatPrice($property->delinquent_mortgage_amount) }}</strong></td>
                                </tr>
                                <tr>
                                    <td>What is the documented appraised value of this property? </td>
                                    <td style="text-align: right;"><strong>{{ formatPrice($property->appraised_value) }}</strong></td>
                                </tr>
                                <tr>
                                    <td>What is the Fair Market Rent? </td>
                                    <td style="text-align: right;"><strong>{{ formatPrice($property->fair_market_rent) }}</strong></td>
                                </tr>
                                <tr>
                                    <td>What is the Actual Monthly Rent (if available)? </td>
                                    <td style="text-align: right;"><strong>{{ formatPrice($property->actual_monthly_rent) }}</strong></td>
                                </tr>
                                <tr>
                                    <td>What are the Annual Taxes?</td>
                                    <td style="text-align: right;"><strong>{{ formatPrice($property->annual_taxes) }}</strong></td>
                                </tr>
                                <tr>
                                    <td>What is the Annual Insurance amount?</td>
                                    <td style="text-align: right;"><strong>{{ formatPrice($property->annual_insurance) }}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        
                    </div>
                    <div class="gab-20">
                        <h1 class="other-font">After Repair Value Comparables</h1>
                        <table class="data-table" border="1">
                            <thead>
                                <tr>
                                    <th>ARV</th>
									<th>Street Address</th>
									<th>BR</th>
									<th>BA</th>
									<th>Sqft</th>
									<th>Sold Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($property->comparable_properties as $key => $comparable)
                                <tr>
                                    <td>Property - {{ $key + 1}}</td>
                                    <td>{{ $comparable->address }}</td>
                                    <td>{{ $comparable->br }}</td>
                                    <td>{{ $comparable->ba }}</td>
                                    <td>{{ $comparable->sqft }}</td>
                                    <td>{{ formatPrice($comparable->sold_price) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="gab-20">
                        <h1 class="other-font">Deal Calculation Info</h1>
                        <table class="data-table" border="1">
                            <thead>
                                <tr>
                                    <th>AFTER REPAIR VALUE</th>
									<th>PURCHASE PRICE</th>
									<th>REHAB ESTIMATE</th>
									<th>GROSS PROFIT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ formatPrice($property->after_repair_value) }}</td>
                                    <td>
                                    	@if ($property->offer_price > 0)
										{{ formatPrice(($property->offer_price)) }}
										@else
											$0
										@endif
                                    </td>
                                    <td>{{ formatPrice($property->rehab_estimate) }}</td>
                                    <td>{{ formatPrice($property->after_repair_value - (($property->offer_price > 0 ? $property->offer_price : 0 || $property->asking_price > 0 ? $property->asking_price :0) + $property->rehab_estimate)) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="gab-20">
                    <h1 class="other-font">Quick Deal Overview</h1>
                    <small>This deal is a {{ $property->type }} that was originally a {{ $property->status }} property.</small>
                </div>
                <div class="gab-20">
                    <h1 class="other-font">Description</h1>
                    <small>{!! (nl2br($property->description)) !!}</small>                    
                </div>
                <div class="gab-10">
                @foreach($property->images as $image)
                    <img src="{{ $image->getIconPath() }}" style="margin-top: 3px;" alt="image" />
                @endforeach
                </div>
                <div class="bottom-txt" style="background-color: #fff;">
                    <span></span>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
    <script>
        $("#downloadPDF").click(function () {
            // $("#content2").addClass('ml-215'); // JS solution for smaller screen but better to add media queries to tackle the issue
            getScreenshotOfElement(
                $("div#content2").get(0),
                0,
                0,
                $("#content2").width() +
                45, // added 45 because the container's (content2) width is smaller than the image, if it's not added then the content from right side will get cut off
                $("#content2").height() +
                30, // same issue as above. if the container width / height is changed (currently they are fixed) then these values might need to be changed as well.
                function (data) {
                    var pdf = new jsPDF("l", "pt", [
                        $("#content2").width(),
                        $("#content2").height(),
                    ]);

                    pdf.addImage(
                        "data:image/png;base64," + data,
                        "PNG",
                        0,
                        0,
                        $("#content2").width(),
                        $("#content2").height()
                    );
                    pdf.save("azimuth-certificte.pdf");
                }
            );
        });

        // this function is the configuration of the html2cavas library (https://html2canvas.hertzen.com/)
        // $("#content2").removeClass('ml-215'); is the only custom line here, the rest comes from the library.
        function getScreenshotOfElement(element, posX, posY, width, height, callback) {
            html2canvas(element, {
                onrendered: function (canvas) {
                    // $("#content2").removeClass('ml-215');  // uncomment this if resorting to ml-125 to resolve the issue
                    var context = canvas.getContext("2d");
                    var imageData = context.getImageData(posX, posY, width, height).data;
                    var outputCanvas = document.createElement("canvas");
                    var outputContext = outputCanvas.getContext("2d");
                    outputCanvas.width = width;
                    outputCanvas.height = height;

                    var idata = outputContext.createImageData(width, height);
                    idata.data.set(imageData);
                    outputContext.putImageData(idata, 0, 0);
                    callback(outputCanvas.toDataURL().replace("data:image/png;base64,", ""));
                },
                width: width,
                height: height,
                useCORS: true,
                taintTest: false,
                allowTaint: false,
            });
        }
    </script>
    {{-- PDF Pagination Script --}}
	<script type="text/php">
		if (isset($pdf)) {
			$text = "{PAGE_NUM} / {PAGE_COUNT}";
			$size = 10;
			$font = $fontMetrics->getFont("Verdana");
			$width = $fontMetrics->get_text_width($text, $font, $size) / 2;
			$x = ($pdf->get_width() - $width) / 2;
			$y = $pdf->get_height() - 35;
			$pdf->page_text($x, $y, $text, $font, $size);
		}
	</script>
</body>

</html>