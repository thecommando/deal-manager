@extends('layouts.default')

@section('content')

	<!-- Page-Title -->

    <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">User Deals</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item">User Deal Summary
                  </li>
                    <li class="breadcrumb-item active">{{ $user->first_name }} {{ $user->last_name }}
                  </li>
                </ol>
              </div>
            </div>
          </div>
          <div class="content-header-right col-md-6 col-xs-12">
            
          </div>
    </div>

	<div class="content-body"><!-- Open main content section -->
        <section id="number-tabs">
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Current Deals </h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="card-body collapse in ">
                            <div class="card-block">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Address</th>
                                                    <th>Type</th>
                                                    <th>Category</th>
                                                    <th>Stage</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($properties as $property)
                                                <tr>
                                                    <td><i class="fa fa-home"> &nbsp; </i> {{ $property->getFullAddress() }}</td>
                                                    <td>{{ $property->type }}</td>
                                                    <td>{{ $property->status }}</td>
                                                    <td>{{ $property->stage }}</td>
                                                    <td>
                                                        <a href="{{ route('deals.show', $property->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End main content section -->
    </div>


@endsection