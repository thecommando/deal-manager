@extends('layouts.pdf')

@section('content')

	@include('deals.partials.contract')

	<a href="{{ route('deals.download.contract', $property->id) }}" class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="position: fixed; right: 20px; bottom: 120px">
		<i class="fa fa-download"></i> @if($send_to) Download & Send Contract @else Download Contract @endif
	</a>

@endsection