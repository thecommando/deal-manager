@extends('layouts.default')

@section('content')
	@include('deals.partials.email_modal')
	@include('deals.partials.transfer_deal_modal')
	@include('deals.partials.transfer_user_deal_modal')
	@include('deals.partials.contract_modal')
	@include('deals.partials.requests_modal')
    @include('deals.partials.request_deal_funding')
    <div id="rehabTemplatesModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rehabTemplatesModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="rehabTemplatesModalLabel">Please Select Rehab Template</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                            <select id="select-template-rehab" class="form-control" name="apports">
                                <option value="">Please Select</option>
                                @foreach($rehabTemplates as $templateRehab)
                                    <option value="{{ route('rehab.download.pdf', $templateRehab->id) }}">{{ $templateRehab->template_name }}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<!-- Page-Title -->
<div class="content-header row hidden-print">
          <div class="content-header-left col-md-3 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Current Deals</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('deals.index') }}">Deal Managment</a>
                  </li>
                  <li class="breadcrumb-item active">Current Deals
                  </li>
                </ol>
              </div>
            </div>
          </div>
          <div class="content-header-right col-md-9 col-xs-12">
            <div class="media float-xs-right">
              <div class="media-body media-right text-xs-right">
                
                 <div class="project-sort pull-right btn-group pull-right m-t-15">
                <div class="project-sort-item">
                    <div class="">
                        <!-- Download REHAB PDF Button -->
                        <!-- <span data-toggle="modal" data-target="#rehabTemplatesModal"> -->
                            <a href="{{ route('rehab.download.pdf', $property->id) }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Rehab Calcs PDF"><span class=""><i class="fa fa-file-pdf-o"></i></span></a>
                        <!-- </span> -->
                        <!-- Download PDF Button -->
                        <a href="{{ route('property.download.pdf', $property->id) }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Property Report PDF"><span class=""><i class="fa fa-file-pdf-o"></i></span></a>

                        <!-- Edit Button -->
                        <a href="{{ route('deals.edit', $property->id) }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit Deal"><span class=""><i class="fa fa-pencil"></i></span></a>

                        <!-- Request Deal Funding -->
                        <span data-toggle="modal" data-target="#RequestDealModel">
                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Request Deal Funding"><i class="fa fa-usd"></i></button>
                        </span>

                        {{-- Seller locator request --}}
                        <!-- <span data-toggle="modal" data-target="#requestsModal">
                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Requests"><i class="fa fa-bell"></i></button>
                        </span> -->
                        {{--<a href="{{ route('deals.seller-locator-request', $property->id) }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Seller Locator Request"><span class=""><i class="fa fa-map-marker"></i></span></a>--}}

                        
                        <!-- Contract Button -->
                        <span data-toggle="modal" data-target="#contractModal">
                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Send Contract"><i class="fa fa-file"></i></button>
                        </span>
                        <!-- View PDF Button -->
                        <!--<a href="{{ route('deals.show.pdf', $property->id) }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View PDF"><span class=""><i class="fa fa-file-pdf-o"></i></span></a> -->

                        <!-- Download PDF Button -->
                        <a href="{{ route('deals.download.pdf', $property->id) }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Download PDF"><span class=""><i class="fa fa-file-pdf-o"></i></span></a>

                        <!-- Print Button -->
                        <a href="javascript:window.print()" class="btn btn-success btnPrint waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Print Deal"><span class=""><i class="fa fa-print"></i></span></a>

                        <!-- Email PDF to Buyers List Button -->
                        <span data-toggle="modal" data-target="#emailModal">
                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Email Deal"><i class="fa fa-envelope-o"></i></button>
                        </span>

                        <a href="{{ route('login.dbm') }}" target="_blank" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Marketing Center"><i class="fa fa-sign-in"></i></a>

                        <!-- Transfer Deal to another user -->
                        @role(['admin', 'super-user'])
                        <span data-toggle="modal" data-target="#transferDealModal">
                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Transfer Deal"><i class="fa fa-refresh"></i></button>
                        </span>
                        @endrole

                        @role(['user', 'bird-dog-deal-finder'])
                        <span data-toggle="modal" data-target="#transferUserDealModal">
                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Transfer Deal"><i class="fa fa-refresh"></i></button>
                        </span>
                        @endrole

                        <!-- Delete Deal -->
                        <a href="{{ route('deals.delete', $property->id) }}" class="btn btn-danger waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete Deal" onclick="if(!confirm('Are you sure to delete this deal?')){return false;};"><i class="fa fa-close"></i></a>
                    </div>
                </div>
            </div>
                  
              </div>
            </div>
          </div>
    </div>

	<!-- Page-Title -->

@include('partials.errors')

<div class="deal-summary">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title"><span class="font-small-2"><i class="fa fa-map-marker"></i> Property Address:</span><BR><strong>{{ $property->address }}</strong><BR> {{ $property->city }}, {{ $property->state }} {{ $property->postal_code }}</h4>
                
                <div class="heading-elements">
                    SALES PRICE:
				<h3 class="m-b-0 m-t-0 p-t-0">
                    <strong>{{ formatPrice($property->wholesale_value) }}</strong>
				</h3>
                    <span class="tag tag-success pull-right hidden-print">{{ $property->stage }}</span>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                     
                        <div class="col-md-12">

							<div class="pull-left m-t-10 p-t-20">
								<address>
									<ul class="info-inline">
										<li>
											<h4 class="m-b-0"><i class="fa fa-bed"></i> {{ $property->bedrooms }} </h4>
											<h6 class="text-muted m-t-0 font-small-2">Bedrooms</h6>
										</li>
										<li>
											<h4 class="m-b-0"><i class="fa fa-bath"></i> {{ $property->bathrooms }} </h4>
											<h6 class="text-muted m-t-0 font-small-2">Bathrooms</h6>
										</li>
										<li>
											<h4 class="m-b-0"><i class="fa fa-arrows-alt"></i> {{ $property->square_feet }} </h4>
											<h6 class="text-muted m-t-0 font-small-2">Square Feet</h6>
										</li>
										<li>
											<h4 class="m-b-0"><i class="fa fa-home"></i> {{ $property->year_built }} </h4>
											<h6 class="text-muted m-t-0 font-small-2">Year Built</h6>
										</li>
									</ul>
								</address>
								<div>
									<div class="clearfix m-t-5 m-b-30">
										<h5 class="small text-inverse font-600 m-t-20">QUICK DEAL OVERVIEW</h5>

										<p class="font-light">
											This deal is a {{ $property->type }} that was originally a {{ $property->status }} property.<br>
                                            <h5 class="small text-inverse font-600 m-t-20">DESCRIPTION</h5>
                                            {!! (nl2br($property->description)) !!}
                                            <h5>&nbsp;</h5>
                                            The <span class="text-highlight">comparable price per square foot is {{ formatPrice($property->comparable_price_per_sqft) }}</span>
                                            @if($property->equity_position)
                                                and has an <span class="text-highlight">estimated <br> {{ $property->equity_position }}% equity built in</span>. <BR><BR>The estimated ARV on this property is <span class="text-highlight">{{ formatPrice($property->after_repair_value) }}</span> and the rehab will be <BR>around <span class="text-highlight">{{ formatPrice($property->estimated_rehab) }}</span>
                                            @endif
                                            . For the right person, this is a great deal.
										</p>
                                            
                                        <h5 class="small text-inverse font-600 m-t-20">ADDITIONAL DETAILS</h5>
                                        <p class="font-light">
                                            Fair Market Rent: {{ formatPrice($property->fair_market_rent) }}<BR>
                                            Annual Taxes: {{ formatPrice($property->annual_taxes) }}<BR>
                                            Annual Insurance: {{ formatPrice($property->annual_insurance) }}<BR>
                                            <h5 class="small text-inverse font-600 m-t-20">AFTER REPAIR VALUE COMPARABLES</h5>
                                            <table class="table table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"><small>ARV</small></th>
                                                        <th scope="col"><small>Street Address</small></th>
                                                        <th scope="col"><small>BR</small></th>
                                                        <th scope="col"><small>BA</small></th>
                                                        <th scope="col"><small>Sqft</small></th>
                                                        <th scope="col"><small>Sold Price</small></th>
                                                    </tr>
                                                </thead>
                                              <tbody>
                                                @foreach($property->comparable_properties as $key => $comparable)
                                                <tr>
                                                    <th><small>Property - {{ $key + 1}}</small></th>
                                                    <th><small>{{ $comparable->address }}</small></th>
                                                    <td><small>{{ $comparable->br }}</small></td>
                                                    <td><small>{{ $comparable->ba }}</small></td>
                                                    <td><small>{{ $comparable->sqft }}</small></td>
                                                    <td><small>{{ formatPrice($comparable->sold_price) }}</small></td>
                                                </tr>
                                                @endforeach                                               
                                              </tbody>
                                            </table>
                                        </p>
									</div>
								</div>
							<!--
							  <address>
								  <h5 class="m-b-0 p-b-0">CONTACT DETAILS</h5><br>
								  {{ $property->user->name }}<br>
								  <i class="fa fa-phone m-r-5"></i> {{ $property->user->mobile_phone }}
								  <i class="fa fa-envelope m-l-10"></i> {{ $property->user->email }}<br>
							  </address>
							  -->
							 	<div>
									@if($property->images()->count())
										<!-- <img src="{{ $property->defaultImage()->getIconPath() }}" > -->
									@else
										{{-- Need placeholder image here --}}
										<img src="/assets/images/no-propertyfound.png" width="100px">
									@endif

									<div class="clearfix"></div>

									<div class="m-t-40">
										<div id="lightgallery">
											@foreach($property->images as $image)
												<a href="{{ $image->getPath() }}">
													<img src="{{ $image->getIconPath() }}" style="margin-top: 3px" />
												</a>
											@endforeach
										</div>
									</div>
							 	</div>
							</div>
							
                            <div class="pull-right m-t-10 col-md-3 hidden-print">
								<h5 class=""><strong class="m-r-5">ARV Estimate: </strong> <span class="pull-right font-light">{{ formatPrice($property->after_repair_value) }}</span></h5>
                                <h5 class=""><strong class="m-r-5">Offer Price: </strong> <span class="pull-right font-light">{{ formatPrice($property->offer_price) }}</span></h5>
                                <h5 class=""><strong class="m-r-5">Wholesale Fee: </strong> <span class="pull-right font-light">{{ formatPrice($property->wholesale_fee) }}</span></h5>
								<h5 class=""><strong class="m-r-5">Comparable $/SQFT: </strong> <span class="pull-right font-light">{{ formatPrice($property->comparable_price_per_sqft) }}</span></h5>
								<h5 class="m-t-10"><strong class="m-r-5">Estimated Rehab: </strong> <span class="pull-right font-light">{{ formatPrice($property->estimated_rehab) }}</span></h5>
								<h5 class="m-t-10"><strong class="m-r-5">Return on Investment: </strong> <span class="pull-right badge label-primary">
                                    <?php                                        
                                        $grossProfit = 0;

                                        if ($property->offer_price > 0 ) {
                                            $tmpPrice = $property->offer_price > 0 ? $property->offer_price : 0;
                                        } else {
                                            $tmpPrice = $property->asking_price > 0 ? $property->asking_price :0;
                                        }

                                        if ($property->after_repair_value > 0) {
                                            $grossProfit = $property->after_repair_value - ($tmpPrice + $property->rehab_estimate);
                                        }

                                        $equity = 0;

                                        $divideBy = $property->offer_price + $property->rehab_estimate;

                                        if ( $divideBy > 0) {
                                            $equity = ($grossProfit / $divideBy) * 100;
                                        }
                                    ?>
                                    {{ number_format($equity, 2) }}%
                                </span></h5>
                                <div class="progress progress-sm m-b-5 m-t-10">
                                    <div class="progress-bar" role="progressbar" 
                                    aria-valuenow="{{ round($equity, 2) }}"
                                    aria-valuemin="0" 
                                    aria-valuemax="100" 
                                    style="width:{{ round($equity, 2) }}% !important; background:#16D39A;">
                                    -
                                    </div>
                                </div>
                                <deal-blacklist inline-template>
                                    <p class="font-light m-t-40 hidden-print">
                                        Read-Only Display Toggle:
                                        <input type="checkbox" data-size="small" v-on:change="toggle({!! $property->id !!})" data-plugin="switchery" data-color="#ff0000" {!! $property->isBlackListed() ? 'checked' : '' !!}>
                                        <br>
                                        <span class="font-small-2">When selected, this property will NOT be displayed on any external sites.</span>
                                    </p>
                                </deal-blacklist>
                                <p>  
                                    <!-- BEGIN HOUSINGALERTS WIDGET INSTANCE -->
                                    <div class="ha-widget-embed-f1-v1" data-ha-embed-mc="{{!empty($property->postal_code) ? $property->postal_code : 34473}}" data-ha-embed-ids="a70_ptnr-zcskey6pct"></div>
                                    <script src="https://www.housingalerts.com/ha_widgets/partners/embed.js" type="application/javascript"></script>
                                    <!-- END HOUSINGALERTS WIDGET INSTANCE -->
                                </p>
                                <address>
									<h6 class="m-t-20 m-b-10">SELLER CONTACT DETAILS:</h6>
										
											<div class="col-md-12 font-small-3">
												<strong>{{ $property->seller_name }}</strong><BR>
												{{ $property->seller_address }} <BR>
												{{ $property->seller_city }}, {{ $property->seller_state }} {{ $property->seller_postal_code }}<BR>
												{{ $property->seller_country }}                                                
                                                <span class="clearfix"></span>
												<i class="fa fa-envelope fa-fw  m-t-20"></i> {{ $property->seller_email }}<BR>
												<i class="fa fa-phone-square fa-fw"></i> {{ $property->seller_mobile }} (mobile)<BR>
												<i class="fa fa-phone-square fa-fw"></i> {{ $property->seller_phone }} (phone)<BR>
                                                <br>
                                                <br>
                                                @php
                                                $notes = \App\Note::where(['subject_id' => $property->id])->select('text')->get();                                                
                                                @endphp

                                                <strong> 
                                                    @if(count($notes) > 0)
                                                        Notes:
                                                        @foreach($notes as $note)
                                                                {{ $note->text }}
                                                        @endforeach
                                                        
                                                    @endif
                                                </strong>

                                                <address>
                                                    <h6 class="m-t-20 m-b-10"><strong>Transfer From:</strong></h6>
                                                    @if (!empty($property->transferredBy))
                                                        <strong>Name: </strong>{{ $property->transferredBy->name }}<br>
                                                        <strong>Email: </strong>{{ $property->transferredBy->email }}
                                                    @endif
                                                </address>
											</div>
										
								</address>
							</div>
                                   
						</div><!-- end col -->
                     
                        <div class="clearfix m-b-20"></div>
                                
                        <hr class="m-t-40">
                                
                        <div class="row m-t-40">
					
                            <div class="col-md-7 col-sm-7 col-xs-7">

                                <h5 class="small text-inverse font-600">PROPERTY MAP</h5>
                                <iframe
                                        width="450"
                                        height="250"
                                        frameborder="0" style="border:0"
                                        referrerpolicy="no-referrer-when-downgrade"
                                        src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAnV96rN0iDZCoSf5UTQPXXV43J5Hzb9HE&q={{ $property->getFullAddress() }}">
                                </iframe>
                            </div>

{{--                            <div class="col-md-5 col-sm-5 col-xs-5 p-l-20">--}}
{{--                                <div class="clearfix m-t-0">--}}
{{--                                    <h5 class="small text-inverse font-600">SUBMITTING AN OFFER </h5>--}}

{{--                                    <small>--}}
{{--                                        This property will not be available for a long time, so if you are --}}
{{--                                        interested contact us using the information below. --}}
{{--                                    </small>--}}
{{--                                    <address>--}}
{{--                                        <h4 class="m-t-20 p-t-0 m-b-0"><strong>{{ $property->user->name }}</strong></h4>--}}
{{--                                        @if($property->finder)--}}
{{--                                            <h5 class="m-t-20 p-t-0 m-b-0">Finder: {{ $property->finder->first_name }}</h5>--}}
{{--                                        @endif--}}
{{--                                        <h5>--}}
{{--                                            @if($property->user->mobile_phone)--}}
{{--                                                <i class="fa fa-phone-square m-r-5"></i> {{ $property->user->mobile_phone }}--}}
{{--                                                <br>--}}
{{--                                            @endif--}}
{{--                                            <i class="fa fa-envelope m-r-5"></i> {{ $property->user->email }}--}}
{{--                                        </h5>--}}
{{--                                    </address>--}}
{{--                                </div>--}}
{{--                            </div>--}}
				        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>
<!-- end row -->

@include('deals.partials.activities')
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#lightgallery").lightGallery();
		});
        $(document).on('change', '#select-template-rehab', function (e) {
            var link = $("option:selected", this).val();
            if (link) {
                location.href = link;
            }
        });
	</script>
@endsection