@extends('layouts.pdf')

@section('content')

	@include('deals.partials.pdf')

	<a href="{{ route('deals.download.pdf', $property->id) }}" class="btn btn-success hidden-print" style="position: fixed; right: 20px; bottom: 120px"><i class="fa fa-download"></i> Download PDF</a>
	<a href="javascript:window.print()" class="btn btn-success hidden-print" style="position: fixed; right: 170px; bottom: 120px"><i class="fa fa-print"></i> Print</a>

@endsection