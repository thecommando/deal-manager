@extends('layouts.default')

@section('styles')
    <!-- <link href="/assets/css/core.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="/assets/css/pages.css" rel="stylesheet" type="text/css" /> -->
    <!--<link href="/assets/css/components.css" rel="stylesheet" type="text/css" /> // Equitity Position Line-->

    <style>
        .ajax-loading-content {
            height: 40px;
        }

        .ajax-loading-content .ajax-loading {
            display: none;
            width: 28px;
        }

        #compact-style {
            position: relative;
        }

        .filter-ajax-loading-content {
            position: absolute;
            z-index: 99;
            bottom: 0;
            background: #ffffffa6;
            top: 0;
            left: 0;
            right: 0;
            justify-content: center;
            display: none;
            align-items: center;
        }

        .filter-ajax-loading-content img {
            width: 25px;
            height: 25px;
        }
    </style>

@endsection

@section('content')
    <div class="filter-ajax-loading-content">
        <img class="filter-ajax-loading" src="{{ asset('app-assets/images/icons/loading.gif') }}"/>
    </div>
    <div class="content-header row">

        <div class="content-header-left col-md-3 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Current Deals</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('deals.index') }}">Deal Managment</a>
                        </li>
                        <li class="breadcrumb-item active">Current Deals
                        </li>
                    </ol>
                </div>
            </div>
            <a href="{{ route('deals.export') }}" class="btn btn-primary mt-1">Export</a>
        </div>
        <div class="content-header-right col-md-9 col-xs-12">
            <div class="media float-xs-right">
                <div class="media-body media-right text-xs-right">
                    <div class="project-sort pull-right">
                        <div class="project-sort-item">
                            <form id="filterForm" class="form-inline">
                                <div class="form-group input-group-sm">
                                    <label class="font-light">Deal Stage:</label>
                                    {!! Form::select('property_stage', ['' => '-- All Deals --'] + property_stages(), Request::get('property_stage'), ['class' => 'form-control property_stage'/*, 'onchange'=>'this.form.submit()'*/]) !!}
                                </div>
                                <div class="form-group input-group-sm">
                                    <label class="font-light">Deal Type:</label>
                                    {!! Form::select('deal_type', ['' => '-- All Deals --'] + deal_types(), Request::get('deal_type'), ['class' => 'form-control'/*, 'onchange'=>'this.form.submit()'*/]) !!}
                                </div>
                                <div class="form-group input-group-sm">
                                    <label class="font-light">Property Status:</label>
                                    {!! Form::select('property_status', ['' => '-- All Deals --'] + property_statuses(), Request::get('property_status'), ['class' => 'form-control'/*, 'onchange'=>'this.form.submit()'*/]) !!}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="compact-style">
        <div id="deal-results">
            @include('deals.partials.deal-content')
        </div>
        @if(count($properties) && count($properties) >= $paginateCount)
            <div class="mt-2" style="text-align: center">
                <div class="ajax-loading-content">
                    <img class="ajax-loading" src="{{ asset('app-assets/images/icons/loading.gif') }}"/>
                </div>
                <button id="load-more" class="btn btn-primary">Load More</button>
            </div>
        @endif
    </section>

    @if($transferredProperties->count())
        <a name="transferreddeals"></a>

        <div class="pending-deals m-t-40">
            <div class="content-body">
                <!-- Main Content Area -->
                <section id="description" class="card">
                    <div class="card-header">
                        <h4 class="card-title">Transferred Deals</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="card-text">

                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Address</th>
                                        <th>Deal Owner</th>
                                        <th>Date Added</th>
                                        <th>Bedroom(s)</th>
                                        <th>Bathroom(s)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transferredProperties as $property)
                                        <tr>
                                            <td>{{ $property->address }} {{ $property->city }}
                                                , {{ $property->state }} {{ $property->postal_code }}</td>
                                            <td>{{ $property->user->name }}</td>
                                            <td>{{ $property->created_at->format('M d, Y h:i a') }}</td>
                                            <td>{{ $property->bedrooms }}</td>
                                            <td>{{ $property->bathrooms }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

    @endif

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            var site_url = "{{ url('/deals') }}";
            var page = 1;

            $('#filterForm select').change(function (e) {
                e.preventDefault();

                page = 1;
                let get_forms_filter_url = $('#filterForm').serialize();
                let url = site_url + '?' + get_forms_filter_url;
                window.history.pushState({path: url}, '', url);
                filter('?' + get_forms_filter_url);
            });

            $('#load-more').click(function () {
                let get_filter_url = window.location.search;
                page++;
                load_more(page, get_filter_url);
            });

            function load_more(page, url_params = '') {
                $.ajax({
                    url: site_url + url_params + (url_params ? '&' : '?') + "properties_page=" + page,
                    type: "get",
                    datatype: "html",
                    beforeSend: function () {
                        $('.ajax-loading').show();
                    }
                })
                    .done(function (data) {
                        if (data.hideShowMoreButton === true) {
                            $('#load-more').hide();
                            $('.ajax-loading').hide();
                            $("#deal-results").append(data.html);
                            return false;
                        }

                        $('.ajax-loading').hide();
                        $("#deal-results").append(data.html);
                    })
                    .fail(function (jqXHR, ajaxOptions, thrownError) {
                        $('.ajax-loading').hide();
                    });
            }

            function filter(url_params = '') {
                $.ajax({
                    url: site_url + url_params,
                    type: "get",
                    datatype: "html",
                    beforeSend: function () {
                        $('.filter-ajax-loading-content').css('display', 'flex')
                    }
                })
                    .done(function (data) {
                        $("#deal-results").html(data.html);
                        $('.filter-ajax-loading-content').hide();

                        if (data.hideShowMoreButton === true) {
                            $('#load-more').hide();
                            return false;
                        } else {
                            $('#load-more').show();
                        }
                    })
                    .fail(function (jqXHR, ajaxOptions, thrownError) {
                        $('.filter-ajax-loading-content').hide();
                    });
            }
        });
    </script>
@endsection