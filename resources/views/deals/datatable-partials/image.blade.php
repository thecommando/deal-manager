@if($property->images->count())
    <div class="card-img-top img-fluid mb-1"
         style="background: url('{{ $property->images->first()->path }}') center; background-size: cover; object-fit: contain;height: 110px !important;">
    </div>
@else
    <img src="/assets/images/no-propertyfound.png" class="card-img-top mb-1 img-fluid " alt="work-thumbnail" style="height: auto">
@endif