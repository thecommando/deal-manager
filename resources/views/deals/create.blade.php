@extends('layouts.default')

@section('styles')
    <link href="/assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- Page-Title -->
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Add New Deal</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{ route('deals.index') }}">Deal Management</a>
                        </li>
                        <li class="breadcrumb-item active">Add New Deal
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => 'deals.store', 'files' => true, 'id' => 'deal-form']) !!}



    <div class="content-detached content-left">
        <div class="content-body">

            <!-- Main Content Area -->
            <section id="description" class="card">

                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="card-text">

                            <div class="col-md-12">
                                @include('partials.errors')
                            </div>

                            <div class="col-lg-12">
                                <div class="card-box">
                                    @include('deals.form')
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
            <!--/ Main Content Area -->

        </div>
    </div>

    <!-- Sidebar Content Area -->
    <div class="sidebar-detached sidebar-right">
        <div class="sidebar">
            <div class="sidebar-content card">
                <div class="card-block">
                    <div class="category-title pb-1">
                        <h6>INSTRUCTIONS</h6>
                    </div>

                    <p class="card-text">Take a look at the video below for a quick overview of how to add a deal.</p>
                    <p class="card-text">

                    </p>

                </div>
            </div>

        </div>
    </div>


    {!! Form::close() !!}

@endsection

@section('scripts')
    @include('partials.location-script', ['country' => null, 'state' => null])
    @include('partials.location-script', ['country' => null, 'state' => null, 'country_id' => 'seller_country', 'state_id' => 'seller_state'])

    <script src="/assets/plugins/fileuploads/js/dropify.min.js"></script>
    <script src="/assets/js/jquery.maskMoney.min.js"></script>
    <script src="/assets/js/jquery.mask.min.js"></script>
    {{--<script src="/assets/app/js/deal-calculations.js"></script>--}}

{{--    <script async--}}
{{--            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnV96rN0iDZCoSf5UTQPXXV43J5Hzb9HE&loading=async&libraries=places&callback=initMap">--}}
{{--    </script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnV96rN0iDZCoSf5UTQPXXV43J5Hzb9HE&libraries=places" async defer></script>

    <script type="text/javascript">
        $(function () {
            $('#seller_mobile_formatted').mask('(000) 000-0000');
            $('#seller_phone_formatted').mask('(000) 000-0000');
        });

		$('.dropify').dropify({
			messages: {
				'default': 'Upload A Photo Here',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
			},
			error: {
				'fileSize': 'The file size is too big (1M max).'
			}
		});

        function initialize() {
            // Create the autocomplete object for the address input field.
            var input = document.getElementById('address');

            input.addEventListener('keydown', function (e) {
                if (e.key === 'Enter') {
                    e.preventDefault();
                }
            });

            // Initialize autocomplete for addresses only, restricted to USA, UK, and Canada
            var autocomplete = new google.maps.places.Autocomplete(input, {
                types: ['address'],
                componentRestrictions: { country: ['us', 'gb', 'ca'] }
            });

            // Add a listener to capture the selected place data.
            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();

                if (!place.geometry) {
                    console.log("No details available for input: '" + place.name + "'");
                    return;
                }

                var streetAddress = '';

                // Extract street address (premise, street_number, route)
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];

                    // Check if it's street_number or route (street name)
                    if (addressType === 'street_number') {
                        streetAddress += place.address_components[i].long_name + ' ';
                    } else if (addressType === 'route') {
                        streetAddress += place.address_components[i].long_name;
                    }
                }

                // Only fill the address field with the street address (no city, state, etc.)
                document.getElementById('address').value = streetAddress.trim();

                // Extract city, state, country, postal code and fill other inputs
                var city = '';
                var state = '';
                var country = '';
                var postal_code = '';

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (addressType === 'locality') {
                        city = place.address_components[i].long_name;
                    } else if (addressType === 'administrative_area_level_1') {
                        state = place.address_components[i].long_name;
                    } else if (addressType === 'country') {
                        country = place.address_components[i].long_name;
                    } else if (addressType === 'postal_code') {
                        postal_code = place.address_components[i].long_name;
                    }
                }

                document.getElementById('city').value = city;
                document.getElementById('state').value = state;
                document.getElementById('country').value = country;
                document.getElementById('postal_code').value = postal_code;

                // Get the latitude and longitude from the selected place.
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();

                console.log("Selected place: " + place.formatted_address);
                console.log("Lat: " + lat + ", Lng: " + lng);

                // Generate the static map URL based on the selected location.
                var mapImageUrl = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng +
                    "&zoom=15&size=600x300&maptype=roadmap&markers=color:red%7C" + lat + "," + lng +
                    "&key=AIzaSyAnV96rN0iDZCoSf5UTQPXXV43J5Hzb9HE";

                uploadImageToDropify(mapImageUrl);
            });
        }

        function uploadImageToDropify(imageUrl) {
                fetch(imageUrl)
                    .then(response => response.blob())
                    .then(blob => {
                        var file = new File([blob], 'map-image.png', { type: blob.type });

                        // Simulate file upload to Dropify
                        var dataTransfer = new DataTransfer();
                        dataTransfer.items.add(file);

                        var input = document.querySelector('#address_map_image_input');
                        input.files = dataTransfer.files;

                        //also show address_map_image
                        var img = document.querySelector('#address_map_image');
                        img.src = imageUrl;

                        $('#address_map_image_block').show();
                    })
                    .catch(error => {
                        $('#address_map_image_block').hide();
                        console.error('Error fetching the image:', error);
                    });
        }

        // Initialize autocomplete on window load
        window.onload = function () {
            initialize();
        };
    </script>
@endsection