<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Print</title>
	<style>
		* {
			box-sizing: border-box;
		}

		@media print {

			.no-print,
			.no-print * {
				display: none !important;
			}

			.print-m-0 {
				margin: 0 !important;
			}
		}

		.btn {
			padding: 10px 17px;
			border-radius: 3px;
			background: #f4b71a;
			border: none;
			font-size: 12px;
			margin: 10px 5px;
		}

		.toolbar {
			background: #333;
			width: 100vw;
			position: fixed;
			left: 0;
			top: 0;
			text-align: center;
		}

		.cert-container {
			margin: 0 0 10px 0;
			width: 100%;
			display: flex;
			justify-content: center;
		}

		.cert {
			width: 700px;
			height: 600px;
			padding: 15px 20px;
			position: relative;
			z-index: -1;
		}

		.cert-bg {
			position: absolute;
			left: 0px;
			top: 0;
			z-index: -1;
			width: 100%;
		}

		.cert-content {
			width: 100%;
			/* height: 470px; */
			padding: 0 20px 0px 20px;
			font-family: Arial, Helvetica, sans-serif;
		}


		small {
			font-size: 14px;
			line-height: 12px;
		}

		.bottom-txt {
			padding: 12px 5px;
			display: flex;
			justify-content: space-between;
			font-size: 16px;
		}

		.bottom-txt * {
			white-space: nowrap !important;
		}

		.other-font {
			font-family: Arial, Helvetica, sans-serif;
			font-size: 15px;
			font-weight: bold;
			margin-bottom: 0;
			border-bottom: 2px solid #cccc;
			margin-bottom: 15px;
			padding-bottom: 6px;
		}

		.table-full .table-inner {
			background-color: #f1f1f1;
			padding: 15px;

		}

		.ml-215 {
			margin-left: 215px;
		}

		.top-header h2,
		.top-header p {
			margin: 0;
		}

		.bottom-txt.top-header {
			border-bottom: 2px solid #12a2b9;
		}

		.top-header h2 {
			font-weight: bold;
			font-size: 20px;
			margin-bottom: 5px;
		}

		.top-header p {
			font-size: 16px;
			margin-bottom: 5px;
		}

		.data-table {
			width: 100%;
			text-align: left;
			border-collapse: collapse;
		}

		.data-table td,
		.data-table th {
			padding: 8px;
			font-size: 13px;
		}

		.padding-x-0.data-table td {
			padding-left: 0;
			padding-right: 0;
		}

		.gab-20 {
			margin-bottom: 20px;
		}
		.bottom-text-right p {
	margin-top: 0;
	margin-bottom: 5px;
	text-align: right;
}
.bottom-text-right h3 {
	margin-bottom: 5px;
	margin-top: 0;
	text-align: right;
}

.total-amount {
	text-align: center;
	background-color: #80cef3;
	padding: 10px 10px;
	font-size: 24px;
	font-weight: 600;
	box-shadow: 0 5px 0 1px #c7c7c7;
	border: 1px solid #12a2b9;
	border-radius: 2px;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}

.nobreak
{
  page-break-inside: avoid;
  break-inside: avoid
}
</style>

</head>

<body>
	<div class="cert-container print-m-0">
		<div id="content2" class="cert">
			<div class="cert-content">
				<div class="bottom-txt top-header gab-20">
					<p>
						<img src="https://app.dealmanagerpro.com/assets/images/dmp-logo.png" alt="Logo" width="250px" />
					</p>
					<div>
						<h2>Rehab Calc Report - {{ $property->address }}</h2>
						<p>
							{{ $property->city }}, {{ $property->state }} {{ $property->postal_code }}
						</p>
					</div>
				</div>
				<p>
					<strong>Estimated Rehab</strong>: <span class="pull-right font-light">{{ formatPrice($property->estimated_rehab) }}</span>
				</p>
				@php
					$finalCost = 0;
				@endphp
				@if (count($items) > 0 )
				<div>
					<h1 class="other-font">{{ $template->template_name ?? 'Rehab Template' }}</h1>
					<div class="gab-20 nobreak">
						<h1 class="other-font">ROOFING</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=0 && $key <=2)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{getUnit($item['unit'])}}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
							</tbody>
						</table>
					</div>
					<div class="gab-20 nobreak">
						<h1 class="other-font">WINDOWS</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=3 && $key <=5)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="gab-20 nobreak">
						<h1 class="other-font">LANDSCAPING</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=6 && $key <=12)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">EXTERIOR</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=13 && $key <=17)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">INTERIOR</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=18 && $key <=23)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">FLOORING</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=24 && $key <=28)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">DOORS</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=29 && $key <=37)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">ELECTRICAL</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=38 && $key <=45)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">PLUMBING</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=46 && $key <=48)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">HVAC</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=49 && $key <=55)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">KITCHEN</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=56 && $key <=58)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">BATHROOMS</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=59 && $key <=68)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="gab-20 nobreak">
						<h1 class="other-font">MISCELLANEOUS</h1>
						<table class="data-table" border="1">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th>Unit of Measure</th>
									<th>Unit Cost</th>
									<th>Total Cost</th>
									<th>Product SKU</th>
									<th>Notes</th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $key => $item)
								@if ($key >=69 && $key <=75)
								@php
									$qty = $item['qty'];
									$price = $item['price'] ?? 0;
									$totalCost = $qty * $price;
									$finalCost += $totalCost;
								@endphp
								<tr>
									<td>{{$item['name'] ?? ''}}</td>
									<td>{{$qty}}</td>
									<td>{{ getUnit($item['unit']) }}</td>
									<td>{{formatPrice($price)}}</td>
									<td>{{formatPrice($totalCost)}}</td>
									<td>{{$item['sku'] ?? ''}}</td>
									<td>{{$item['notes'] ?? ''}}</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="gab-20">
						<div class="total-amount">
						   Total Amount: {{formatPrice($finalCost)}}
						</div>
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>

	
</body>

</html>