@extends('layouts.default')

@section('content')

<!-- Page-Title -->
<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-2">
        <h3 class="content-header-title mb-0">Create Template</h3>
        <div class="row breadcrumbs-top">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
              </li>
                <li class="breadcrumb-item"><a href="{{ route('calculator.template.index') }}">Template</a>
              </li>
              <li class="breadcrumb-item active">Create Tempalte
              </li>
            </ol>
          </div>
            <div class="col-xs-12">@include('partials.errors')</div>
        </div>
     </div>
     <div class="content-header-right col-md-6 col-xs-12">
        <div class="media width-250 float-xs-right">
            <div class="media-left media-middle">
            </div>
            <div class="media-body media-right text-xs-right">
                <div class="btn-group pull-right m-t-15 hidden-print">
                    <!-- Add Templates Button -->
                    <a href="{{ route('calculator.template.index') }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="Templates" data-original-title="Tempates"><span class="">Templates</span></a>                 
                </div>
            </div>
        </div>
    </div>         
</div>

<div class="content-detached">
    <div class="content-body">

        <!-- Main Content Area -->
        <section id="description" class="card">
            <div class="card-header">
                <h4 class="card-title">Add New Template</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <!-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li> -->
                        <!-- <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li> -->
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">

                        <div class="col-md-12">
                            @include('partials.errors')
                        </div>

                        <div class="col-lg-12">
                            <div class="card-box">
                                <cost-calculator template-id="{{ isset($template) ? $template->id : 0 }}" property-id="{{ !empty($propertyId) ? $propertyId : 0 }}"></cost-calculator>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
	
@endsection