<table border="1">
    <thead>
    <tr>
        <th>Product</th>
        <th>Quantity</th>
        <th>Unit of Measure</th>
        <th>Unit Cost</th>
        <th>Total Cost</th>
        <th>Product SKU</th>
        <th>Notes</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total = 0;
    @endphp
    @foreach($data as $key => $value)
    @php
        $total += ($value['qty'] * $value['price']);
    @endphp
    @if(!empty($value['title']))
    <tr>
        <td colspan="7">{{ $value['title'] }}</td>        
    </tr>
    @endif
    <tr>
        <td>{{ $value['name'] }}</td>
        <td>{{ $value['qty'] ?? 1 }}</td>
        <td>
            @if($value['unit'] == 1)
                unit
            @elseif($value['unit'] == 2)
                linear feet
            @elseif($value['unit'] == 3)
                blind
            @elseif($value['unit'] == 4)
                per foot
            @elseif($value['unit'] == 5)
                knob
            @elseif($value['unit'] == 6)
                handle
            @elseif($value['unit'] == 7)
                square feet
            @elseif($value['unit'] == 8)
                panel
            @elseif($value['unit'] == 9)
                door
            @elseif($value['unit'] == 10)
                panel
            @elseif($value['unit'] == 11)
                window
            @else
            @endif
            
        </td>
        <td>
            @if($value['price'] > 0)
                ${{$value['price']}}
            @else
            @endif
        </td>
        <td>${{ $value['qty'] * $value['price'] }}</td>
        <td>{{ $value['sku'] }}</td>
        <td>{{ $value['notes'] }}</td>        
    </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>TOTAL REHAB EXPENSES:</td>
        <td>${{ $total }}</td>        
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>