@extends('layouts.default')
@section('content')
@include('contacts.partials.email_modal')
@include('contacts.partials.message_modal')
@include('contacts.partials.transfer_contact_modal')
<!-- Page-Title -->
<div class="content-header row">
	<div class="content-header-left col-md-6 col-xs-12 mb-2">
		<h3 class="content-header-title mb-0">Repair Cost Templates</h3>
		<div class="row breadcrumbs-top">
			<div class="breadcrumb-wrapper col-xs-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ route('calculator.template.index') }}">Templates</a>
					</li>
					<li class="breadcrumb-item active">My Templates
					</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="content-header-right col-md-6 col-xs-12">
		<div class="media width-250 float-xs-right">
			<div class="media-left media-middle">
			</div>
			<div class="media-body media-right text-xs-right">
				<div class="btn-group pull-right m-t-15 hidden-print">
					<!-- Add Templates Button -->
					<a href="{{ route('calculator.template.create') }}" class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip" data-placement="bottom" title="Add New" data-original-title="Add New Tempate"><span class=""><i class="fa fa-plus"></i></span></a>					
				</div>
			</div>
		</div>
	</div>
</div>
<section id="mobile-support">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport">
							<thead>
								<tr>
									<th>S.No</th>									
									<th>Template Name</th>									
									<th>Total Cost</th>									
									<th>Created At</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($templates as $key => $template)
								<tr>
									<td>{{ $key+1 }}</td>
									<td>{{ $template->template_name ?? 'N/A' }}</td>
									<td>{{ $template->total_cost }}</td>
									<td>{{ $template->created_at->format('M d, Y h:i a') }}</td>
									<td>
										<a href="{{ route('calculator.template.edit', [$template->id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
										<a href="{{ route('calculator.template.delete', $template->id) }}" class="btn btn-sm btn-danger" onclick="if(!confirm('Are you sure to delete this template?')){return false;};"><i class="fa fa-close"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>ID</th>									
									<th>Total Cost</th>									
									<th>Created At</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection