@extends('layouts.default')

@section('content')
	<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Documents</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Resources</a>
                  </li>
                  <li class="breadcrumb-item active">Documents
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>

<div class="description">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card p-b-80">
            <div class="card-header">
                <h4 class="card-title">Uploaded Documents</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                        
                        @if($documents->count())
				<table class="table table-bordered table-striped">
					<thead>
					<tr>
						<th>Title</th>
						<th>Description</th>
						<th>File</th>
					</tr>
					</thead>
					<tbody>
					@foreach($documents as $document)
						<tr>
							<td>{{ $document->title }}</td>
							<td>{{ $document->description }}</td>
							<td>
								@if($document->hasFile())
									<a href="{{ $document->getUrl() }}" target="_blank"><i class="fa fa-download"></i></a>
								@else
									-
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				@else
					<p class="text-center">No documents found.</p>
				@endif

                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>

@endsection