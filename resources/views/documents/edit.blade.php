@extends('layouts.default')

@section('content')
	<!-- Page-Title -->
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Documents</h4>
		</div>
	</div>

	{!! Form::model($document, ['route' => ['documents.update', $document->id], 'method' => 'PUT', 'files' => true]) !!}
		<div class="row m-b-30">
			<div class="col-md-12">
				@include('partials.errors')
			</div>

			<div class="col-md-3">
				<div class="card-box">
					<h4 class="m-t-0 m-b-20 font-300">Edit Document</h4>
					<!--- Title Field --->
					<div class="form-group">
					    {!! Form::label('title', 'Title:') !!} <span class="red">*</span>
					    {!! Form::text('title', null, ['class' => 'form-control']) !!}
					</div>

					<!--- Description Field --->
					<div class="form-group">
					    {!! Form::label('description', 'Description:') !!} <span class="red">*</span>
					    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
					</div>

					{{-- Current file --}}
					<div class="form-group">
						<a href="{{ $document->getUrl() }}" target="_blank" class="btn btn-success center-block"><i class="fa fa-download"></i> Current Document</a>
					</div>

					<!--- File Field --->
					<div class="form-group">
						{!! Form::label('file', 'Replace with new:') !!} <span class="red">*</span>
						{!! Form::file('file') !!}
					</div>

					<div class="form-group">
						<label>
							{!! Form::checkbox('is_global') !!} Is Global Document?
						</label>
					</div>

					<!--- Users Field --->
					<div class="form-group">
						{!! Form::label('users', 'Users:') !!}

						<select name="users[]" id="users" class="form-control select2 select2-multiple" multiple data-placeholder="Users ...">
							@foreach(\App\User::orderBy('name')->pluck('name', 'id') as $id => $name)
								<option value="{{ $id }}" {{ $documentUsers->contains($id) ? 'selected' : '' }}>{{ $name }}</option>
							@endforeach
						</select>
					</div>

					<!--- Submit Field --->
					<div class="form-group input-group-sm m-t-30 text-right">
						{!! Form::submit('Upload', ['class' => 'btn btn-success']) !!}
						<button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
					</div>

				</div>
			</div>
			
		</div>
		<!-- end row -->
	{!! Form::close() !!}
@endsection