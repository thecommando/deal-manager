@extends('layouts.default')

@section('content')

<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Documents</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Resources</a>
                  </li>
                  <li class="breadcrumb-item active">All Documents
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>

	{!! Form::open(['route' => 'documents.store', 'files' => true]) !!}

<div class="content-detached content-left">
    <div class="content-body">

<!-- Main Content Area -->
        <section id="description" class="card p-b-80">
            <div class="card-header">
                <h4 class="card-title">Uploaded Documents</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="card-text">
                     
                    <div class="col-md-12">
				    @include('partials.errors')
			         </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Users</th>
                                    <th>File</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($documents as $document)
                                <tr>
                                    <td><i class="fa fa-file-text-o"></i>   {{ $document->title }}</td>
                                    <td class="font-light">
                                        @if($document->is_global)
                                            Global Document
                                        @else
                                            {{ $document->users->count() ? $document->users->implode('name', ', ') : '-' }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($document->hasFile())
                                            <a href="{{ $document->getUrl() }}" target="_blank"><i class="fa fa-download"></i></a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td style="width: 120px">
                                        <a href="{{ route('documents.edit', $document->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{ $document->title }}'s Details"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('documents.delete', $document->id) }}" class="btn btn-sm btn-danger" onclick="if(!confirm('Are you sure to delete this document?')){return false;};"><i class="fa fa-close"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>
        <!--/ Main Content Area -->

    </div>
</div>

<create-document inline-template>
<!-- Sidebar Content Area -->
<div class="sidebar-detached sidebar-right">
    <div class="sidebar"><div class="sidebar-content card">
        <div class="card-block">
            <h4 class="card-title">qwsAdd New Document</h4>
                <!--- Title Field --->
                <div class="form-group">
                    {!! Form::label('title', 'Title:') !!} <span class="red">*</span>
                     {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>

                <!--- Description Field --->
                <div class="form-group">
                    {!! Form::label('description', 'Description:') !!} <span class="red">*</span>
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
                </div>

                <!--- File Field --->
                <div class="form-group">
                    {!! Form::label('file', 'Upload File:') !!} <span class="red">*</span>
                    {!! Form::file('file') !!}
                </div>

                <div class="form-group">
                    <label>
                        {!! Form::checkbox('is_global', 1, null, ['v-model' => 'is_global']) !!} Is Global Document?
                    </label>
                </div>

                <div v-if="!is_global">
                    <div class="form-group">
                        <p><label>Accessible To?</label></p>

                        <input type="radio" v-model="to" name="to" id="users" value="users">
                        <label for="users">Users</label>

                        <input type="radio" v-model="to" name="to" id="groups" value="groups">
                        <label for="groups">Teams</label>
                    </div>

                    <!--- Users Field --->
                    <div class="form-group" v-show="to === 'users'">
                        {!! Form::label('users', 'Users:') !!}

                        <select name="users[]" id="users" class="form-control select2 select2-multiple" multiple data-placeholder="Users ..." :disabled="selection == 'all'">
                            @foreach(\App\User::orderBy('name')->pluck('name', 'id') as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!--- Teams Field --->
                    <div class="form-group" v-show="to === 'groups'">
                        {!! Form::label('groups', 'Teams:') !!}

                        <select name="groups[]" id="groups" class="form-control select2 select2-multiple" multiple data-placeholder="Teams ..." :disabled="selection == 'all'">
                            @foreach(\App\Team::pluck('name', 'id') as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{-- Selection --}}
                    <div class="form-group">
                        <input type="radio" v-model="selection" name="selection" id="selected" value="selected">
                        <label for="selected">Selected</label>

                        <input type="radio" v-model="selection" name="selection" id="all" value="all">
                        <label for="all">All</label>
                    </div>
                </div>

                <!--- Submit Field --->
                <div class="form-group input-group-sm m-t-30 text-right">
                    {!! Form::submit('Upload', ['class' => 'btn btn-success']) !!}
                    <button type="button" onclick="goBack()" class="btn btn-default waves-effect">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
</create-document>
<!--/ Sidebar Content Area -->

<!-- end row -->
{!! Form::close() !!}
@endsection