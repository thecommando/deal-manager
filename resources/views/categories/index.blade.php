@extends('layouts.default')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">

@endsection
@section('content')
    <!-- Page-Title -->
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Categories</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Categories</a>
                        </li>
                        <li class="breadcrumb-item active">My Categories
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-xs-12">
            <div class="media width-250 float-xs-right">
                <div class="media-left media-middle">
                </div>
                <div class="media-body media-right text-xs-right">
                    <div class="btn-group pull-right m-t-15 hidden-print">
                        <!-- Add categories Button -->
                        <a href="{{ route('categories.create') }}"
                           class="btn btn-success waves-effect waves-light m-r-5" data-toggle="tooltip"
                           data-placement="bottom" title="" data-original-title="Add New Contact"><span class=""><i
                                        class="fa fa-plus"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="mobile-support">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">

                            <table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport"
                                   id="dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contactCategories as $contactCategory)
                                    <tr>
                                        <td>{{ $contactCategory->name }}</td>
                                        <td>
                                            <a href="{{ route('categories.edit', $contactCategory->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{ $contactCategory->name }}'s Details"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ route('categories.delete', $contactCategory->id) }}" class="btn btn-sm btn-danger" onclick="if(!confirm('Are you sure to delete this contact category?')){return false;};"><i class="fa fa-close"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

