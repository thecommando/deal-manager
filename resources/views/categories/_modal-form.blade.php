<div class="card-box">
	<h5 class="m-t-0 m-t-20 mb-3 subtitle-text">CREATE CATEGORY</h5>
	<div class="row">
		<!--- Name Field --->
		<div class="form-group input-group-sm col-sm-6">
			{!! Form::label('name', 'Name:') !!} <span class="red">*</span>
			{!! Form::text('name', null, ['class' => 'form-control p-1', 'required' => 'required']) !!}
		</div>
	</div>

	<!--- Save Changes Field --->
	<div class="form-group input-group-sm input-group-sm m-t-30 text-right">
		{!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
		<button id="cancelCategory" type="button" class="btn btn-default">Cancel</button>
	</div>
</div>