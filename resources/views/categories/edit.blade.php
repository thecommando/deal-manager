@extends('layouts.default')

@section('content')

	<!-- Page-Title -->
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Edit Category</h4>
		</div>
	</div>

	{!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PUT']) !!}
	<div class="row">

		<div class="col-md-12">
			@include('partials.errors')
		</div>

		@include('categories._form', ['submitButtonText' => 'Update'])

	</div>
	<!-- end row -->
	{!! Form::close() !!}
@endsection