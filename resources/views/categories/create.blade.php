@extends('layouts.default')

@section('content')

    <!-- Page-Title -->
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Create Category</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('dashboard') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ route('categories.index') }}">Categories></a>
                        </li>
                        <li class="breadcrumb-item active">Create Category</li>
                    </ol>
                </div>
                <div class="col-xs-12">@include('partials.errors')</div>
            </div>
        </div>
    </div>

    <div class="content-detached">
        <div class="content-body">

            <!-- Main Content Area -->
            <section id="description" class="card">
                <div class="card-header">
                    <h4 class="card-title">Add New Category</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                {!! Form::open(['route' => 'categories.store']) !!}
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="card-text">
                            @include('categories._form', ['submitButtonText' => 'Save'])
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </section>
            <!--/ Main Content Area -->

        </div>
    </div>

@endsection