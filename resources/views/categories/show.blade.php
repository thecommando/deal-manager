@extends('layouts.default')

@section('content')
    
    <!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Contact</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('contacts.index') }}">Contacts</a></li>
                  <li class="breadcrumb-item active">My Contacts</li>
                </ol>
              </div>
            </div>
          </div>
    </div>

    <section id="mobile-support">
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-body collapse in">
						<div class="card-block card-dashboard">
							<h3>{{ $contact->full_name }}</h3>
							<br>
							<p><strong>Category</strong>: {{ $contact->category->name }}</p>
							<p><strong>Email</strong>: {{ $contact->email }}</p>
							<p><strong>Mobile</strong>: {{ $contact->mobile_phone }}</p>
							<p><strong>Created At</strong>: {{ $contact->created_at->format('M d, Y h:i a') }}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection