@extends('layouts.integration')

@section('content')

	{!! Form::open(['route' => 'integration.contacts.store']) !!}
	<div class="modern-forms">
        	<div class="modern-container mc2">
                <span class="font-small-3">
                Enter your information below to join our Buyer's List. 
                </span>
                <div class="clearfix m-b-20"></div>
                
			@include('partials.errors')
		    @include('integrations.contacts._form', ['submitButtonText' => 'Submit'])
        </div>
	</div>
	<!-- end row -->
	{!! Form::close() !!}
@endsection

@section('scripts')
	@include('partials.location-script', ['country' => null, 'state' => null])
@endsection