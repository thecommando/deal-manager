{!! Form::hidden('api_key', Request::get('api_key')) !!}

<!--- Category id Field --->
<div class="form-group input-group-sm col-sm-12">
    {!! Form::select('category_id', \App\ContactCategory::pluck('name', 'id'), 6, ['class' => 'form-control hidden']) !!}
</div>

<!--- First Name Field --->
<div class="form-row">
    <div class="col col-6">
        <div class="field-group prepend-icon">
            {!! Form::label('first_name', 'First Name:', ['class' => 'mdn-label']) !!}
            {!! Form::text('first_name', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your first name']) !!}
            <span class="mdn-icon"><i class="fa fa-male"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>

    <!--- Last name Field --->
    <div class="col col-6">
        <div class="field-group prepend-icon">
            {!! Form::label('last_name', 'Last Name:', ['class' => 'mdn-label']) !!}
            {!! Form::text('last_name', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your last name']) !!}
            <span class="mdn-icon"><i class="fa fa-male"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
</div>

    <!--- Email Field --->
    <div class="field-group prepend-icon">
        {!! Form::label('email', 'Email:', ['class' => 'mdn-label']) !!}
        {!! Form::email('email', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your primary email ']) !!}
        <span class="mdn-icon"><i class="fa fa-envelope"></i></span>
        <span class="mdn-bar"></span>
    </div>

<div class="form-row">
    <div class="col col-6">
        <!--- Mobile phone Field --->
        <div class="field-group prepend-icon">
            {!! Form::label('mobile_phone', 'Mobile Phone:', ['class' => 'mdn-label']) !!}
            {!! Form::text('mobile_phone', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your mobile number']) !!}
            <span class="mdn-icon"><i class="fa fa-phone"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-6">
        <!--- Alternate phone Field --->
        <div class="field-group prepend-icon">
            {!! Form::label('alternate_phone', 'Alternate Phone:', ['class' => 'mdn-label']) !!}
            {!! Form::text('alternate_phone', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter an alternate number']) !!}
            <span class="mdn-icon"><i class="fa fa-phone"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col col-6">
        <!--- Minimum Price Range --->
        <div class="field-group prepend-icon">
            {!! Form::label('price_range_min', 'Minimum Price:', ['class' => 'mdn-label']) !!}
            {!! Form::text('price_range_min', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your low end price']) !!}
            <span class="mdn-icon"><i class="fa fa-dollar"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-6">
        <!--- Maximum Price Range --->
        <div class="field-group prepend-icon">
            {!! Form::label('price_range_max', 'Maximum Price:', ['class' => 'mdn-label']) !!}
            {!! Form::text('price_range_max', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your high end price']) !!}
            <span class="mdn-icon"><i class="fa fa-dollar"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
</div>
<!--- Location Field --->
    <div class="field-group  prepend-icon">
        {!! Form::label('location', 'Location:', ['class' => 'mdn-label']) !!}
        {!! Form::text('location', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter the specific area where you are looking for deals']) !!}
        <span class="mdn-icon"><i class="fa fa-map-marker"></i></span>
        <span class="mdn-bar"></span>
    </div>

<!--- Notes Field --->
    <div class="field-group">
        {!! Form::label('note', 'Notes:', ['class' => 'mdn-label']) !!}
        {!! Form::textarea('note', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter any additional notes about the types of properties you are looking for here']) !!}
        <span class="mdn-msg right">400 characters maximum </span>
        <span class="mdn-bar"></span>
    </div>


<!--- Save Changes Field --->
<div class="mdn-footer">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-success btn-flat']) !!}
    <button type="reset" class=" btn btn-default btn-flat">Cancel</button>
</div>

               
	