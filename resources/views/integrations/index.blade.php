@extends('layouts.default')

@section('styles')	
	<link rel="stylesheet" href="/app-assets/vendors/css/ui/prism.min.css">
@endsection

@section('content')

<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Integrations</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item active">Integrations
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>

<div class="row match-height">
    
    <div class="col-xl-8 col-lg-8 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">
                        1. Direct Integration Links
                        <small class="pull-right text-info">Click on text to copy</small>
                    </h4>

                    <p class="card-text font-medium-1">Private API KEY:
                        <div class="btn-copy" title="copied" data-placement="top" data-trigger="click" data-toggle="tooltip" data-clipboard-text="{{ $user->api_key}}">
                            <pre><code class="language-none clipper">{{ $user->api_key}}</code></pre>
                        </div>
                    </p>

                    <p class="card-text font-medium-1">Basic Property Deals Form:
                        <div class="btn-copy" title="copied" data-placement="top" data-trigger="click" data-toggle="tooltip" data-clipboard-text="{{ route('integration.basic-deals.create', ['api_key' => $user->api_key]) }}">
                            <pre><code class="language-none clipper">{{ route('integration.basic-deals.create', ['api_key' => $user->api_key]) }}</code></pre>
                        </div>
                    </p>

                    <p class="card-text font-medium-1">Property Deals Form:
                        <div class="btn-copy" title="copied" data-placement="top" data-trigger="click" data-toggle="tooltip" data-clipboard-text="{{ route('integration.deals.create', ['api_key' => $user->api_key]) }}">
                            <pre><code class="language-none clipper">{{ route('integration.deals.create', ['api_key' => $user->api_key]) }}</code></pre>
                        </div>
                    </p>

                    <p class="card-text font-medium-1">Cash Buyer Leads Form:
                        <div class="btn-copy" title="copied" data-placement="top" data-trigger="click" data-toggle="tooltip" data-clipboard-text="{{ route('integration.contacts.create', ['api_key' => $user->api_key]) }}">
                            <pre><code class="language-none clipper">{{ route('integration.contacts.create', ['api_key' => $user->api_key]) }}</code></pre>
                        </div>
                    </p>

                    <p class="card-text font-medium-1">Deal Finder Form:
                        <div class="btn-copy" title="copied" data-placement="top" data-trigger="click" data-toggle="tooltip" data-clipboard-text="{{ route('integration.deal-finders.create', ['api_key' => $user->api_key]) }}">
                            <pre><code class="language-none clipper">{{ route('integration.deal-finders.create', ['api_key' => $user->api_key]) }}</code></pre>
                        </div>
                    </p>
                </div>
            </div>
        </div>
    </div>  
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">Integrations</h4>
                    
                    
                    <p class="card-text">There are several ways you can integrate your Deal Manager Pro account with external websites: </p>
                    <h4 class="card-text">1. Direct Links</h4>
                    <p class="card-text font-small-2">This option is used when you are working with someone like a Deal Finder (Bird Dog) directly and want them to send leads directly to you. The link opens a web form where they can add information that comes directly to you in Deal Manager Pro.  You can also recruit new Deal Finders and build your Buyer’s list with these.</p>
                    <p class="font-small-2 alert alert-warning">Please do not post this link on any public websites. It would be better to use one of the embedding methods.</p>
                    <h4 class="card-text m-t-20">2. Basic Embed Codes </h4>
                    <p class="card-text font-small-2">Use these embed codes to add web forms directly to your website(s). Motivated Seller forms, Cash Buyer forms, and forms for signing up Deal Finders.</p>
                    <h4 class="card-text">3. Advanced Custom Embed Codes </h4>
                    <p class="card-text font-small-2">These are the same as the Basic Embed Codes above, but you’re able to customize them (requires dealing with form code) to the look and feel you choose. This is an advanced option! Only use if you have knowledge of coding.</p>
                    
                    
                </div>
                
                
            </div>
        </div>
    </div> 
</div>
<div class="row match-height">
<div class="col-xl-8 col-lg-8 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">2. Basic Embed Codes</h4>
                    
                    <p class="card-text font-medium-1">
                        <strong>Primary Lead Form To Embed On Your Website: </strong>
                        <div class="clearfix"></div>
                        <span class="font-small-3">1. External Tab</span>
                        <pre><code class="language-markup">&lt;a href="{{ route('integration.basic-deals.create', ['api_key' => $user->api_key]) }}" target="_blank"&gt;Get Cash Offer&lt;/a&gt;</code></pre>

                        <span class="font-small-3">2. IFrame (On Page)</span>
                        <pre><code class="language-markup">&lt;iframe src="{{ route('integration.basic-deals.create', ['api_key' => $user->api_key]) }}" frameborder="0" style="width: 100%;border: none;height: 500px;"&gt;&lt;/iframe&gt;</code></pre>

                        <span class="font-small-3">3. IFrame (Popup)</span>
                        <pre>
                            <code class="language-markup">&lt;button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"&gt;Get Cash Offer&lt;/button&gt;

                            &lt;div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"&gt;
                              &lt;div class="modal-dialog modal-lg" role="document"&gt;
                                &lt;div class="modal-content"&gt;
                                  &lt;iframe src="{{ route('integration.basic-deals.create', ['api_key' => $user->api_key]) }}" frameborder="0" style="width: 100%;border: none;height: 500px;"&gt;&lt;/iframe&gt;
                                &lt;/div&gt;
                              &lt;/div&gt;
                            &lt;/div&gt;
                            </code>
                        </pre>
                    </p>

                    <p class="card-text font-medium-1">
                        <strong>Collect Property Leads: </strong>
                    <div class="clearfix"></div>
                    <span class="font-small-3">1. External Tab</span>
                    <pre><code class="language-markup">&lt;a href="{{ route('integration.deals.create', ['api_key' => $user->api_key]) }}" target="_blank"&gt;Get Cash Offer&lt;/a&gt;</code></pre>

                    <span class="font-small-3">2. IFrame (On Page)</span>
                    <pre><code class="language-markup">&lt;iframe src="{{ route('integration.deals.create', ['api_key' => $user->api_key]) }}" frameborder="0" style="width: 100%;border: none;height: 500px;"&gt;&lt;/iframe&gt;</code></pre>

                    <span class="font-small-3">3. IFrame (Popup)</span>
                    <pre>
                                    <code class="language-markup">&lt;button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"&gt;Get Cash Offer&lt;/button&gt;

                                    &lt;div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"&gt;
                                      &lt;div class="modal-dialog modal-lg" role="document"&gt;
                                        &lt;div class="modal-content"&gt;
                                          &lt;iframe src="{{ route('integration.deals.create', ['api_key' => $user->api_key]) }}" frameborder="0" style="width: 100%;border: none;height: 500px;"&gt;&lt;/iframe&gt;
                                        &lt;/div&gt;
                                      &lt;/div&gt;
                                    &lt;/div&gt;
                                    </code>
                                </pre>
                    </p>

                    <p class="card-text font-medium-1">
                        <strong>Collect Cash Buyer Leads: </strong>
                        <div class="clearfix"></div>
                        <span class="font-small-3">1. External Tab</span>
                            <pre><code class="language-markup">&lt;a href="{{ route('integration.contacts.create', ['api_key' => $user->api_key]) }}" target="_blank"&gt;Join Buyer's List&lt;/a&gt;</code></pre>

                            <span class="font-small-3">2. IFrame (On Page)</span>
                            <pre><code class="language-markup">&lt;iframe src="{{ route('integration.contacts.create', ['api_key' => $user->api_key]) }}" frameborder="0" style="width: 100%;border: none;height: 500px;"&gt;&lt;/iframe&gt;</code></pre>

                            <span class="font-small-3">3. IFrame (Popup)</span>
                                <pre>
                                    <code class="language-markup">&lt;button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"&gt;Join Buyer's List&lt;/button&gt;

                                    &lt;div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"&gt;
                                      &lt;div class="modal-dialog modal-lg" role="document"&gt;
                                        &lt;div class="modal-content"&gt;
                                          &lt;iframe src="{{ route('integration.contacts.create', ['api_key' => $user->api_key]) }}" frameborder="0" style="width: 100%;border: none;height: 500px;"&gt;&lt;/iframe&gt;
                                        &lt;/div&gt;
                                      &lt;/div&gt;
                                    &lt;/div&gt;
                                    </code>
                                </pre>
                    </p>
                    
                    <p class="card-text font-medium-1">
                        <strong>Collect Deal Finders: </strong>
                        <div class="clearfix"></div>
                        <span class="font-small-3">1. External Tab</span>
                            <pre><code class="language-markup">&lt;a href="{{ route('integration.deal-finders.create', ['api_key' => $user->api_key]) }}" target="_blank"&gt;Become a Deal Finder&lt;/a&gt;</code></pre>

                            <span class="font-small-3">2. IFrame (On Page)</span>
                            <pre><code class="language-markup">&lt;iframe src="{{ route('integration.deal-finders.create', ['api_key' => $user->api_key]) }}" frameborder="0" style="width: 100%;border: none;height: 500px;"&gt;&lt;/iframe&gt;</code></pre>

                            <span class="font-small-3">3. IFrame (Popup)</span>
                                <pre>
                                    <code class="language-markup">&lt;button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"&gt;Become a Deal Finder&lt;/button&gt;

                                    &lt;div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"&gt;
                                      &lt;div class="modal-dialog modal-lg" role="document"&gt;
                                        &lt;div class="modal-content"&gt;
                                          &lt;iframe src="{{ route('integration.deal-finders.create', ['api_key' => $user->api_key]) }}" frameborder="0" style="width: 100%;border: none;height: 500px;"&gt;&lt;/iframe&gt;
                                        &lt;/div&gt;
                                      &lt;/div&gt;
                                    &lt;/div&gt;
                                    </code>
                                </pre>
                    </p> 
                    
                </div>
                
            </div>
        </div>
    </div> 
</div>
<div class="row match-height">
<div class="col-xl-8 col-lg-8 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <h4 class="card-title">3. Advanced Custom Embed Codes</h4>
                
                    <p class="card-text font-medium-1">Property Deals Form: </p> 
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".deal-create-form-example">See Example</button>

		            @include('integrations.partials.deal_form')
                
                    <p class="card-text font-medium-1 m-t-20">Cash Buyer Leads Form: </p>
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".contact-create-form-example">See Example</button>

                    @include('integrations.partials.contact_form')
                    
            
                    <p class="card-text font-medium-1 m-t-20">Deal Finder Form:</p>
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".deal-finder-create-form-example">See Example</button>

		            @include('integrations.partials.deal_finder_form')
                    
                </div>
                
            </div>
        </div>
    </div> 
</div>

@endsection

@section('scripts')	
	<script src="/app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>
    <script src="/app-assets/js/scripts/clipboard.min.js" type="text/javascript"></script>

    <!--
    <script type="text/javascript">
        var clipboard = new Clipboard('.btn-copy', {
            target: function() {
                return document.querySelector('.clipper');
            }
        });

        clipboard.on('success', function(e) {
            console.log(e);
        });

        clipboard.on('error', function(e) {
            console.log(e);
        });
    </script>
    -->

    <script type="text/javascript">
        var clipboard = new Clipboard('.btn-copy');

        clipboard.on('success', function(e) {
        });

        clipboard.on('error', function(e) {
        });
    </script>

@endsection