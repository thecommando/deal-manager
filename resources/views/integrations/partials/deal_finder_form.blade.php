<div class="modal fade deal-finder-create-form-example" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Create Deal Finder Form Example</h4>
            </div>
            <div class="modal-body">
<pre><xmp>
&lt;!doctype html&gt;
&lt;html lang=&quot;en&quot;&gt;
&lt;head&gt;
 &lt;meta charset=&quot;UTF-8&quot;&gt;
 &lt;title&gt;Join Our Buyer's List&lt;/title&gt;

 &lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;

&lt;/head&gt;
&lt;body&gt;
 &lt;div class=&quot;container-fluid&quot;&gt;
  &lt;form method=&quot;POST&quot; action=&quot;{{ url('integration/form/deal-finders') }}&quot; accept-charset=&quot;UTF-8&quot;&gt;
   &lt;div class=&quot;row&quot;&gt;
    &lt;input name=&quot;api_key&quot; type=&quot;hidden&quot; value=&quot;{{ auth()->user()->api_key }}&quot;&gt;
    &lt;input name=&quot;redirect_to&quot; type=&quot;hidden&quot; value=&quot;http://example.com/contacts&quot;&gt;

    &lt;h3&gt;Personal Information&lt;/h3&gt;

    &lt;!--- Name Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;name&quot;&gt;Name:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;name&quot; type=&quot;text&quot; id=&quot;name&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Username Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;username&quot;&gt;Username:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;username&quot; type=&quot;text&quot; id=&quot;username&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Email Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;email&quot;&gt;Email:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;email&quot; type=&quot;email&quot; id=&quot;email&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Mobile phone Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;mobile_phone&quot;&gt;Mobile Phone:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;mobile_phone&quot; type=&quot;text&quot; id=&quot;mobile_phone&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Alternate phone Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;office_phone&quot;&gt;Alternate Phone:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;office_phone&quot; type=&quot;text&quot; id=&quot;office_phone&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Website Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;website&quot;&gt;Website:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;website&quot; type=&quot;text&quot; id=&quot;website&quot;&gt;
    &lt;/div&gt;

    &lt;h3&gt;Location Information&lt;/h3&gt;

    &lt;!--- Address Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;address&quot;&gt;Address:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;address&quot; type=&quot;text&quot; id=&quot;address&quot;&gt;
    &lt;/div&gt;

    &lt;!--- City Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;city&quot;&gt;City:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;city&quot; type=&quot;text&quot; id=&quot;city&quot;&gt;
    &lt;/div&gt;

    &lt;!--- State Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;state&quot;&gt;State:&lt;/label&gt;
        &lt;select class=&quot;form-control&quot; id=&quot;state&quot; name=&quot;state&quot;&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Country Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;country&quot;&gt;Country:&lt;/label&gt;
        &lt;select class=&quot;form-control&quot; id=&quot;country&quot; name=&quot;country&quot;&gt;&lt;option value=&quot;&quot; selected=&quot;selected&quot;&gt;-- Select --&lt;/option&gt;&lt;option value=&quot;Canada&quot;&gt;Canada&lt;/option&gt;&lt;option value=&quot;United Kingdom&quot;&gt;United Kingdom&lt;/option&gt;&lt;option value=&quot;United States&quot;&gt;United States&lt;/option&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Postal code Field ---&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
        &lt;label for=&quot;postal_code&quot;&gt;Postal Code:&lt;/label&gt;
        &lt;input class=&quot;form-control&quot; name=&quot;postal_code&quot; type=&quot;text&quot; id=&quot;postal_code&quot;&gt;
    &lt;/div&gt;

   &lt;/div&gt;

   &lt;!--- Save Changes Field --&gt;
   &lt;div class=&quot;form-group input-group-sm input-group-sm m-t-20 text-right&quot;&gt;
    &lt;input class=&quot;btn btn-success&quot; type=&quot;submit&quot; value=&quot;Submit&quot;&gt;
   &lt;/div&gt;

  &lt;/form&gt;
 &lt;/div&gt;
&lt;/body&gt;
&lt;/html&gt;
</xmp></pre>
            </div>
        </div>
    </div>
</div>