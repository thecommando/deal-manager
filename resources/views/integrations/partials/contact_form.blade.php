<div class="modal fade contact-create-form-example" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Create Contact Form Example</h4>
            </div>
            <div class="modal-body">
<pre><xmp>
&lt;!doctype html&gt;
&lt;html lang=&quot;en&quot;&gt;
&lt;head&gt;
 &lt;meta charset=&quot;UTF-8&quot;&gt;
 &lt;title&gt;Join Our Buyer's List&lt;/title&gt;

 &lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;

&lt;/head&gt;
&lt;body&gt;
 &lt;div class=&quot;container-fluid&quot;&gt;
  &lt;form method=&quot;POST&quot; action=&quot;{{ url('integration/form/contacts') }}&quot; accept-charset=&quot;UTF-8&quot;&gt;
   &lt;div class=&quot;row&quot;&gt;
    &lt;input name=&quot;api_key&quot; type=&quot;hidden&quot; value=&quot;{{ auth()->user()->api_key }}&quot;&gt;
    &lt;input name=&quot;redirect_to&quot; type=&quot;hidden&quot; value=&quot;http://example.com/contacts&quot;&gt;

    &lt;!--- Category id Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-12&quot;&gt;
     &lt;select class=&quot;form-control hidden&quot; name=&quot;category_id&quot;&gt;&lt;option value=&quot;1&quot;&gt;Attorney&lt;/option&gt;&lt;option value=&quot;2&quot;&gt;Broker&lt;/option&gt;&lt;option value=&quot;3&quot;&gt;Bird Dog&lt;/option&gt;&lt;option value=&quot;4&quot;&gt;Builder&lt;/option&gt;&lt;option value=&quot;5&quot;&gt;Buyer Lead&lt;/option&gt;&lt;option value=&quot;6&quot; selected=&quot;selected&quot;&gt;Cash Buyer&lt;/option&gt;&lt;option value=&quot;7&quot;&gt;Contractor&lt;/option&gt;&lt;option value=&quot;8&quot;&gt;Hard Money Lender&lt;/option&gt;&lt;option value=&quot;9&quot;&gt;Inspector&lt;/option&gt;&lt;option value=&quot;10&quot;&gt;Insurance&lt;/option&gt;&lt;option value=&quot;11&quot;&gt;Mentor&lt;/option&gt;&lt;option value=&quot;12&quot;&gt;Private Money Investor&lt;/option&gt;&lt;option value=&quot;13&quot;&gt;Property Manager&lt;/option&gt;&lt;option value=&quot;14&quot;&gt;Real Estate Agent&lt;/option&gt;&lt;option value=&quot;15&quot;&gt;REI Club Member&lt;/option&gt;&lt;option value=&quot;16&quot;&gt;Seller&lt;/option&gt;&lt;option value=&quot;17&quot;&gt;Title Agent&lt;/option&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- First Name Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
     &lt;label for=&quot;first_name&quot;&gt;First Name:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;first_name&quot; type=&quot;text&quot; id=&quot;first_name&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Last name Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
     &lt;label for=&quot;last_name&quot;&gt;Last Name:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;last_name&quot; type=&quot;text&quot; id=&quot;last_name&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Email Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
     &lt;label for=&quot;email&quot;&gt;Email:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;email&quot; type=&quot;email&quot; id=&quot;email&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Mobile phone Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-6&quot;&gt;
     &lt;label for=&quot;mobile_phone&quot;&gt;Mobile Phone:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;mobile_phone&quot; type=&quot;text&quot; id=&quot;mobile_phone&quot;&gt;
    &lt;/div&gt;

   &lt;/div&gt;

    &lt;!--- Save Changes Field --&gt;
   &lt;div class=&quot;form-group input-group-sm input-group-sm m-t-20 text-right&quot;&gt;
    &lt;input class=&quot;btn btn-success&quot; type=&quot;submit&quot; value=&quot;Submit&quot;&gt;
   &lt;/div&gt;

  &lt;/form&gt;
 &lt;/div&gt;
&lt;/body&gt;
&lt;/html&gt;
</xmp></pre>
            </div>
        </div>
    </div>
</div>