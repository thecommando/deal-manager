<div class="modal fade deal-create-form-example" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
         <div class="modal-header">
            <h4>Create Deal Form Example</h4>
         </div>
         <div class="modal-body">
<pre><xmp>
&lt;!doctype html&gt;
&lt;html lang=&quot;en&quot;&gt;
&lt;head&gt;
	&lt;meta charset=&quot;UTF-8&quot;&gt;
	&lt;title&gt;Motivated Seller Form&lt;/title&gt;
	&lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;
&lt;/head&gt;
&lt;body&gt;
 &lt;div class=&quot;container-fluid&quot;&gt;
  &lt;form method=&quot;POST&quot; action=&quot;{{ url('integration/form/deals') }}&quot; accept-charset=&quot;UTF-8&quot; id=&quot;deal-form&quot;&gt;
   &lt;div class=&quot;row&quot;&gt;
    &lt;h5 class=&quot;m-t-0 m-b-20 font-light&quot;&gt;Submit Your Property Today&lt;/h5&gt;

    &lt;input name=&quot;api_key&quot; type=&quot;hidden&quot; value=&quot;{{ auth()->user()->api_key }}&quot;&gt;

    &lt;input name=&quot;redirect_to&quot; type=&quot;hidden&quot; value=&quot;http://example.com/properties&quot;&gt;

    &lt;!--- Address Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-12&quot;&gt;
     &lt;label for=&quot;address&quot;&gt;Address:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;address&quot; type=&quot;text&quot; id=&quot;address&quot;&gt;
    &lt;/div&gt;

    &lt;!--- City Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-4&quot;&gt;
     &lt;label for=&quot;city&quot;&gt;City:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;city&quot; type=&quot;text&quot; id=&quot;city&quot;&gt;
    &lt;/div&gt;

    &lt;!--- State Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-4&quot;&gt;
     &lt;label for=&quot;state&quot;&gt;State:&lt;/label&gt;
     &lt;select class=&quot;form-control&quot; id=&quot;state&quot; name=&quot;state&quot;&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Country Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-4&quot;&gt;
     &lt;label for=&quot;country&quot;&gt;Country:&lt;/label&gt;
     &lt;select class=&quot;form-control&quot; id=&quot;country&quot; name=&quot;country&quot;&gt;&lt;option value=&quot;&quot; selected=&quot;selected&quot;&gt;-- Select --&lt;/option&gt;&lt;option value=&quot;Canada&quot;&gt;Canada&lt;/option&gt;&lt;option value=&quot;United Kingdom&quot;&gt;United Kingdom&lt;/option&gt;&lt;option value=&quot;United States&quot;&gt;United States&lt;/option&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Postal code Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-4&quot;&gt;
     &lt;label for=&quot;postal_code&quot;&gt;Postal Code:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;postal_code&quot; type=&quot;text&quot; id=&quot;postal_code&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Bedrooms Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-2&quot;&gt;
     &lt;label for=&quot;bedrooms&quot;&gt;Bedrooms:&lt;/label&gt;
     &lt;select class=&quot;form-control&quot; id=&quot;bedrooms&quot; name=&quot;bedrooms&quot;&gt;&lt;option value=&quot;1&quot;&gt;1&lt;/option&gt;&lt;option value=&quot;2&quot;&gt;2&lt;/option&gt;&lt;option value=&quot;3&quot;&gt;3&lt;/option&gt;&lt;option value=&quot;4&quot;&gt;4&lt;/option&gt;&lt;option value=&quot;5&quot;&gt;5&lt;/option&gt;&lt;option value=&quot;6&quot;&gt;6&lt;/option&gt;&lt;option value=&quot;7&quot;&gt;7+&lt;/option&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Bathrooms Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-2&quot;&gt;
     &lt;label for=&quot;bathrooms&quot;&gt;Bathrooms:&lt;/label&gt;
     &lt;select class=&quot;form-control&quot; id=&quot;bathrooms&quot; name=&quot;bathrooms&quot;&gt;&lt;option value=&quot;1&quot;&gt;1&lt;/option&gt;&lt;option value=&quot;2&quot;&gt;1.5&lt;/option&gt;&lt;option value=&quot;3&quot;&gt;2&lt;/option&gt;&lt;option value=&quot;4&quot;&gt;2.5&lt;/option&gt;&lt;option value=&quot;5&quot;&gt;3&lt;/option&gt;&lt;option value=&quot;6&quot;&gt;3.5&lt;/option&gt;&lt;option value=&quot;7&quot;&gt;4&lt;/option&gt;&lt;option value=&quot;8&quot;&gt;4.5&lt;/option&gt;&lt;option value=&quot;9&quot;&gt;5+&lt;/option&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Type Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-2&quot;&gt;
     &lt;label for=&quot;type&quot;&gt;Type:&lt;/label&gt;
     &lt;select class=&quot;form-control&quot; id=&quot;type&quot; name=&quot;type&quot;&gt;&lt;option value=&quot;1&quot;&gt;Single Family&lt;/option&gt;&lt;option value=&quot;2&quot;&gt;Townhouse&lt;/option&gt;&lt;option value=&quot;3&quot;&gt;Condominium&lt;/option&gt;&lt;option value=&quot;4&quot;&gt;Commercial&lt;/option&gt;&lt;option value=&quot;5&quot;&gt;Land&lt;/option&gt;&lt;option value=&quot;6&quot;&gt;MultiUnit&lt;/option&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Year built Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-2&quot;&gt;
     &lt;label for=&quot;year_built&quot;&gt;Year Built:&lt;/label&gt;
     &lt;select class=&quot;form-control&quot; id=&quot;year_built&quot; name=&quot;year_built&quot;&gt;&lt;option value=&quot;1920&quot;&gt;1920&lt;/option&gt;&lt;option value=&quot;1921&quot;&gt;1921&lt;/option&gt;&lt;option value=&quot;1922&quot;&gt;1922&lt;/option&gt;&lt;option value=&quot;1923&quot;&gt;1923&lt;/option&gt;&lt;option value=&quot;1924&quot;&gt;1924&lt;/option&gt;&lt;option value=&quot;1925&quot;&gt;1925&lt;/option&gt;&lt;option value=&quot;1926&quot;&gt;1926&lt;/option&gt;&lt;option value=&quot;1927&quot;&gt;1927&lt;/option&gt;&lt;option value=&quot;1928&quot;&gt;1928&lt;/option&gt;&lt;option value=&quot;1929&quot;&gt;1929&lt;/option&gt;&lt;option value=&quot;1930&quot;&gt;1930&lt;/option&gt;&lt;option value=&quot;1931&quot;&gt;1931&lt;/option&gt;&lt;option value=&quot;1932&quot;&gt;1932&lt;/option&gt;&lt;option value=&quot;1933&quot;&gt;1933&lt;/option&gt;&lt;option value=&quot;1934&quot;&gt;1934&lt;/option&gt;&lt;option value=&quot;1935&quot;&gt;1935&lt;/option&gt;&lt;option value=&quot;1936&quot;&gt;1936&lt;/option&gt;&lt;option value=&quot;1937&quot;&gt;1937&lt;/option&gt;&lt;option value=&quot;1938&quot;&gt;1938&lt;/option&gt;&lt;option value=&quot;1939&quot;&gt;1939&lt;/option&gt;&lt;option value=&quot;1940&quot;&gt;1940&lt;/option&gt;&lt;option value=&quot;1941&quot;&gt;1941&lt;/option&gt;&lt;option value=&quot;1942&quot;&gt;1942&lt;/option&gt;&lt;option value=&quot;1943&quot;&gt;1943&lt;/option&gt;&lt;option value=&quot;1944&quot;&gt;1944&lt;/option&gt;&lt;option value=&quot;1945&quot;&gt;1945&lt;/option&gt;&lt;option value=&quot;1946&quot;&gt;1946&lt;/option&gt;&lt;option value=&quot;1947&quot;&gt;1947&lt;/option&gt;&lt;option value=&quot;1948&quot;&gt;1948&lt;/option&gt;&lt;option value=&quot;1949&quot;&gt;1949&lt;/option&gt;&lt;option value=&quot;1950&quot;&gt;1950&lt;/option&gt;&lt;option value=&quot;1951&quot;&gt;1951&lt;/option&gt;&lt;option value=&quot;1952&quot;&gt;1952&lt;/option&gt;&lt;option value=&quot;1953&quot;&gt;1953&lt;/option&gt;&lt;option value=&quot;1954&quot;&gt;1954&lt;/option&gt;&lt;option value=&quot;1955&quot;&gt;1955&lt;/option&gt;&lt;option value=&quot;1956&quot;&gt;1956&lt;/option&gt;&lt;option value=&quot;1957&quot;&gt;1957&lt;/option&gt;&lt;option value=&quot;1958&quot;&gt;1958&lt;/option&gt;&lt;option value=&quot;1959&quot;&gt;1959&lt;/option&gt;&lt;option value=&quot;1960&quot;&gt;1960&lt;/option&gt;&lt;option value=&quot;1961&quot;&gt;1961&lt;/option&gt;&lt;option value=&quot;1962&quot;&gt;1962&lt;/option&gt;&lt;option value=&quot;1963&quot;&gt;1963&lt;/option&gt;&lt;option value=&quot;1964&quot;&gt;1964&lt;/option&gt;&lt;option value=&quot;1965&quot;&gt;1965&lt;/option&gt;&lt;option value=&quot;1966&quot;&gt;1966&lt;/option&gt;&lt;option value=&quot;1967&quot;&gt;1967&lt;/option&gt;&lt;option value=&quot;1968&quot;&gt;1968&lt;/option&gt;&lt;option value=&quot;1969&quot;&gt;1969&lt;/option&gt;&lt;option value=&quot;1970&quot;&gt;1970&lt;/option&gt;&lt;option value=&quot;1971&quot;&gt;1971&lt;/option&gt;&lt;option value=&quot;1972&quot;&gt;1972&lt;/option&gt;&lt;option value=&quot;1973&quot;&gt;1973&lt;/option&gt;&lt;option value=&quot;1974&quot;&gt;1974&lt;/option&gt;&lt;option value=&quot;1975&quot;&gt;1975&lt;/option&gt;&lt;option value=&quot;1976&quot;&gt;1976&lt;/option&gt;&lt;option value=&quot;1977&quot;&gt;1977&lt;/option&gt;&lt;option value=&quot;1978&quot;&gt;1978&lt;/option&gt;&lt;option value=&quot;1979&quot;&gt;1979&lt;/option&gt;&lt;option value=&quot;1980&quot;&gt;1980&lt;/option&gt;&lt;option value=&quot;1981&quot;&gt;1981&lt;/option&gt;&lt;option value=&quot;1982&quot;&gt;1982&lt;/option&gt;&lt;option value=&quot;1983&quot;&gt;1983&lt;/option&gt;&lt;option value=&quot;1984&quot;&gt;1984&lt;/option&gt;&lt;option value=&quot;1985&quot;&gt;1985&lt;/option&gt;&lt;option value=&quot;1986&quot;&gt;1986&lt;/option&gt;&lt;option value=&quot;1987&quot;&gt;1987&lt;/option&gt;&lt;option value=&quot;1988&quot;&gt;1988&lt;/option&gt;&lt;option value=&quot;1989&quot;&gt;1989&lt;/option&gt;&lt;option value=&quot;1990&quot;&gt;1990&lt;/option&gt;&lt;option value=&quot;1991&quot;&gt;1991&lt;/option&gt;&lt;option value=&quot;1992&quot;&gt;1992&lt;/option&gt;&lt;option value=&quot;1993&quot;&gt;1993&lt;/option&gt;&lt;option value=&quot;1994&quot;&gt;1994&lt;/option&gt;&lt;option value=&quot;1995&quot;&gt;1995&lt;/option&gt;&lt;option value=&quot;1996&quot;&gt;1996&lt;/option&gt;&lt;option value=&quot;1997&quot;&gt;1997&lt;/option&gt;&lt;option value=&quot;1998&quot;&gt;1998&lt;/option&gt;&lt;option value=&quot;1999&quot;&gt;1999&lt;/option&gt;&lt;option value=&quot;2000&quot;&gt;2000&lt;/option&gt;&lt;option value=&quot;2001&quot;&gt;2001&lt;/option&gt;&lt;option value=&quot;2002&quot;&gt;2002&lt;/option&gt;&lt;option value=&quot;2003&quot;&gt;2003&lt;/option&gt;&lt;option value=&quot;2004&quot;&gt;2004&lt;/option&gt;&lt;option value=&quot;2005&quot;&gt;2005&lt;/option&gt;&lt;option value=&quot;2006&quot;&gt;2006&lt;/option&gt;&lt;option value=&quot;2007&quot;&gt;2007&lt;/option&gt;&lt;option value=&quot;2008&quot;&gt;2008&lt;/option&gt;&lt;option value=&quot;2009&quot;&gt;2009&lt;/option&gt;&lt;option value=&quot;2010&quot;&gt;2010&lt;/option&gt;&lt;option value=&quot;2011&quot;&gt;2011&lt;/option&gt;&lt;option value=&quot;2012&quot;&gt;2012&lt;/option&gt;&lt;option value=&quot;2013&quot;&gt;2013&lt;/option&gt;&lt;option value=&quot;2014&quot;&gt;2014&lt;/option&gt;&lt;option value=&quot;2015&quot;&gt;2015&lt;/option&gt;&lt;option value=&quot;2016&quot;&gt;2016&lt;/option&gt;&lt;option value=&quot;2017&quot;&gt;2017&lt;/option&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Square feet Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-2&quot;&gt;
     &lt;label for=&quot;square_feet&quot;&gt;Square Feet:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;square_feet&quot; type=&quot;text&quot; id=&quot;square_feet&quot;&gt;
    &lt;/div&gt;
   &lt;/div&gt;

   &lt;div class=&quot;row&quot;&gt;
    &lt;!--- Seller Information --&gt;
    &lt;h4 class=&quot;m-t-0 m-b-20 font-600&quot;&gt;&lt;b&gt;Seller Information&lt;/b&gt;&lt;/h4&gt;

    &lt;!--- Seller name Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-4&quot;&gt;
     &lt;label for=&quot;seller_name&quot;&gt;Name:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;seller_name&quot; type=&quot;text&quot; id=&quot;seller_name&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Seller email Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-4&quot;&gt;
     &lt;label for=&quot;seller_email&quot;&gt;Email:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;seller_email&quot; type=&quot;email&quot; id=&quot;seller_email&quot;&gt;
    &lt;/div&gt;

    &lt;!--- Seller mobile Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-4&quot;&gt;
     &lt;label for=&quot;seller_mobile&quot;&gt;Mobile Phone:&lt;/label&gt;
     &lt;input class=&quot;form-control&quot; name=&quot;seller_mobile&quot; type=&quot;text&quot; id=&quot;seller_mobile&quot;&gt;
    &lt;/div&gt;
   &lt;/div&gt;

   &lt;div class=&quot;row&quot;&gt;
    &lt;!--- Condition Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-4&quot;&gt;
     &lt;label for=&quot;condition&quot;&gt;Condition of property?&lt;/label&gt;
     &lt;select class=&quot;form-control&quot; id=&quot;condition&quot; name=&quot;condition&quot;&gt;&lt;option value=&quot;&quot; selected=&quot;selected&quot;&gt;-- Select --&lt;/option&gt;&lt;option value=&quot;1&quot;&gt;Good&lt;/option&gt;&lt;option value=&quot;2&quot;&gt;Fair&lt;/option&gt;&lt;option value=&quot;3&quot;&gt;Poor&lt;/option&gt;&lt;/select&gt;
    &lt;/div&gt;

    &lt;!--- Reason for selling Field --&gt;
    &lt;div class=&quot;form-group input-group-sm col-sm-8&quot;&gt;
     &lt;label for=&quot;reason_for_selling&quot;&gt;Reason for selling?&lt;/label&gt;
     &lt;textarea class=&quot;form-control&quot; name=&quot;reason_for_selling&quot; cols=&quot;50&quot; rows=&quot;10&quot; id=&quot;reason_for_selling&quot;&gt;&lt;/textarea&gt;
    &lt;/div&gt;
   &lt;/div&gt;

   &lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;col-md-12 m-t-20&quot;&gt;
    &lt;!--- Submit Field --&gt;
     &lt;div class=&quot;text-right m-t-30 form-group&quot;&gt;
      &lt;input class=&quot;btn btn-success waves-light&quot; type=&quot;submit&quot; value=&quot;Submit&quot;&gt;
     &lt;/div&gt;
    &lt;/div&gt;
   &lt;/div&gt;

  &lt;/form&gt;
 &lt;/div&gt;

 &lt;script src=&quot;https://code.jquery.com/jquery-3.2.1.min.js&quot; integrity=&quot;sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=&quot; crossorigin=&quot;anonymous&quot;&gt;&lt;/script&gt;
 &lt;script src=&quot;{{ url('assets/app/js/location-script.js') }}&quot;&gt;&lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;
</xmp></pre>
         </div>
        </div>
    </div>
</div>