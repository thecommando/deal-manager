{!! Form::hidden('api_key', Request::get('api_key')) !!}
{!! Form::hidden('finder_key', Request::get('finder_key')) !!}

<!--- Address Field --->
<div class="form-row">
    <div class="col col-12">
        <div class="field-group prepend-icon">
            {!! Form::label('seller_name', 'Your Name', ['class' => 'mdn-label']) !!}
            {!! Form::text('seller_name', null, ['class' => 'form-control mdn-input']) !!}
        </div>

        <!--- Email Field --->
        <div class="field-group prepend-icon m-t-10">
            {!! Form::label('seller_email', 'Primary Email', ['class' => 'mdn-label']) !!}
            {!! Form::email('seller_email', null, ['class' => 'form-control mdn-input']) !!}
        </div>

        <!--- Mobile phone Field --->
        <div class="field-group prepend-icon m-t-10">
            {!! Form::label('seller_mobile', 'Best Phone #', ['class' => 'mdn-label']) !!}
            {!! Form::text('seller_mobile', null, ['class' => 'form-control mdn-input']) !!}
        </div>

        <div class="field-group prepend-icon m-t-10">
            {!! Form::label('address', 'Property Address', ['class' => 'mdn-label']) !!}
            {!! Form::text('address', null, ['class' => 'form-control mdn-input']) !!}
        </div>

        <div class="field-group prepend-icon m-t-10">
            {!! Form::label('notes', 'Why Are You Selling?', ['class' => 'mdn-label']) !!}
            {!! Form::textarea('notes[]', null, ['class' => 'form-control mdn-input', 'rows' => 3]) !!}
        </div>
    </div>
</div>

<div class="row">
	<!--- Submit Field --->
    <div class="text-center m-t-20 form-group">
        {!! Form::submit('Yes, I\'d Like Help With My Property.', ['class' => 'btn waves-light text-white', 'v-on:click' => 'onSubmit', 'style' => 'background-color:#2e61db']) !!}
    </div>
</div>

<div class="row m-t-2">
    By submitting this form, I consent to be contacted by you via phone, email, or text as provided. I understand I can opt out anytime. Standard message and data rates may apply.
</div>