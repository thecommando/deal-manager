@extends('layouts.integration')

@section('content')

{!! Form::open(['route' => 'integration.basic-deals.store', 'id' => 'deal-form']) !!}
	<div class="modern-container mc2" style="background-color: #f0f0f0; margin: 0 auto">
		@if(session('alert'))
			<div class="alert alert-success">
				<i class="fa fa-check-circle"></i>
				{{ session('alert') }}
			</div>
		@endif

		@include('partials.errors')
		@include('integrations.basic-deals.form')
	</div>
{!! Form::close() !!}

@endsection