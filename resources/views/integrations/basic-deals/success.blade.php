@extends('layouts.integration')

@section('content')

	<div class="modern-container mc2 m-t-0" style="background-color: #f0f0f0">
		<div class="alert alert-success">
			<i class="fa fa-check-circle"></i>
			Property submitted successfully.
		</div>
	</div>

@endsection