@extends('layouts.default')

@section('content')

<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Housing Alerts</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item active">Alerts
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>
    <div class="col-sm-12">
                
        <!-- BEGIN HOUSINGALERTS WIDGET INSTANCE -->
        <div class="ha-widget-embed-f1-v1" data-ha-embed-mc="34473" data-ha-embed-ids="a70_ptnr-zcskey5rw"></div>
        <script src="https://www.housingalerts.com/ha_widgets/partners/embed.js"></script>
        <!-- END HOUSINGALERTS WIDGET INSTANCE -->

        <!-- BEGIN HOUSINGALERTS WIDGET INSTANCE -->
        <div class="ha-widget-embed-f1-v1" data-ha-embed-mc="34473" data-ha-embed-ids="a70_ptnr-zcskey6pct"></div>
        <script src="https://www.housingalerts.com/ha_widgets/partners/embed.js"></script>
        <!-- END HOUSINGALERTS WIDGET INSTANCE -->


        <!-- BEGIN HOUSINGALERTS WIDGET INSTANCE -->
        <div class="ha-widget-embed-f1-v1" data-ha-embed-mc="34473" data-ha-embed-ids="a70_ptnr-fzkey5rw"></div>
        <script src="https://www.housingalerts.com/ha_widgets/partners/embed.js"></script>
        <!-- END HOUSINGALERTS WIDGET INSTANCE -->

        <!-- BEGIN HOUSINGALERTS WIDGET INSTANCE -->
        <div class="ha-widget-embed-f1-v1" data-ha-embed-mc="34473" data-ha-embed-ids="a70_ptnr-fzkey6pct"></div>
        <script src="https://www.housingalerts.com/ha_widgets/partners/embed.js"></script>
        <!-- END HOUSINGALERTS WIDGET INSTANCE -->

        <!-- BEGIN HOUSINGALERTS WIDGET INSTANCE -->
        <div class="ha-widget-embed-f1-v1" data-ha-embed-mc="34473" data-ha-embed-ids="a70_ptnr-fzcpkey5rw"></div>
        <script src="https://www.housingalerts.com/ha_widgets/partners/embed.js"></script>
        <!-- END HOUSINGALERTS WIDGET INSTANCE -->

        <!-- BEGIN HOUSINGALERTS WIDGET INSTANCE -->
        <div class="ha-widget-embed-f1-v1" data-ha-embed-mc="34473" data-ha-embed-ids="a70_ptnr-fzcpkey6pct"></div>
        <script src="https://www.housingalerts.com/ha_widgets/partners/embed.js"></script>
        <!-- END HOUSINGALERTS WIDGET INSTANCE -->

    </div>
@endsection