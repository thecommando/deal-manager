@extends('layouts.integration')

@section('content')

	<div class="modern-container">
		<h1 class="text-center display-1 text-success"><i class="fa fa-check-circle" style="font-size: 70px"></i></h1>
		<h4 class="text-center m-t-20">
			You account has been created successfully.
		</h4>
		<h4 class="text-center">Please check your mailbox to get your password and click below to login.</h4>
		<div class="text-center m-t-30">
			<a class="btn btn-primary" href="{{ route('login') }}">Click to Login</a>
		</div>
	</div>

@endsection