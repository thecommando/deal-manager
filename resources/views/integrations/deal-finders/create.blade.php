@extends('layouts.integration')

@section('content')

	{!! Form::open(['route' => 'integration.deal-finders.store']) !!}
	<div class="modern-forms">
        <div class="modern-container mc3">
            <span class="font-small-3">
            Enter your information below to be considered for our Deal Finders Program where you get the ability to locate and submit properties that fit certain criteria to our team. If we end up purchasing any of the properties you submit, you will receive a Finder's Fee which is either a flat fee or a percentage of the deal.
            </span>
        <div class="clearfix m-b-20"></div>
            
            @include('partials.errors')
		  @include('integrations.deal-finders._form', ['submitButtonText' => 'Submit'])
        </div>
	</div>
	<!-- end row -->
	{!! Form::close() !!}
@endsection

@section('scripts')
	@include('partials.location-script', ['country' => null, 'state' => null])
@endsection