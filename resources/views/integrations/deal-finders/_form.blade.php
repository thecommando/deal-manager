{!! Form::hidden('api_key', Request::get('api_key')) !!}

<!--- Name Field --->
<div class="form-row m-t-40">
    <div class="col col-12">
        <div class="field-group prepend-icon">
            {!! Form::label('name', 'Name:', ['class' => 'mdn-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your full name']) !!}
            <span class="mdn-icon"><i class="fa fa-male"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>

    <!--- Username Field --->

</div>    

<!--- Email Field --->
<div class="field-group prepend-icon">
    {!! Form::label('email', 'Email:', ['class' => 'mdn-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your primary email ']) !!}
    <span class="mdn-icon"><i class="fa fa-envelope"></i></span>
    <span class="mdn-bar"></span>
</div>
    
<div class="form-row">
    <div class="col col-12">
        <!--- Mobile phone Field --->
        <div class="field-group prepend-icon">
            {!! Form::label('mobile_phone', 'Mobile Phone:', ['class' => 'mdn-label']) !!}
            {!! Form::text('mobile_phone', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your mobile number']) !!}
            <span class="mdn-icon"><i class="fa fa-phone"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>

</div>
    
<!--- Website Field --->

    
<!--- Address Field --->


<!--- State Field --->
    
<!--- Save Changes Field --->
<div class="mdn-footer">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-success btn-flat']) !!}
    <button type="reset" class=" btn btn-default btn-flat">Cancel</button>
</div>

	