@extends('layouts.integration')

@section('styles')
	<link href="/assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')

{!! Form::open(['route' => 'integration.deals.store', 'files' => true, 'id' => 'deal-form']) !!}
<div class="modern-forms">
    <div class="modern-container mc3">
        
        <h5><strong>PROPERTY INFORMATION</strong></h5>
        <hr class="m-t-0">

        <div class="clearfix m-b-20"></div>
            @include('partials.errors')
            @include('integrations.deals.form')
        </div>
    </div>


{!! Form::close() !!}

@endsection

@section('scripts')
	@include('partials.location-script', ['country' => null, 'state' => null])
	@include('partials.location-script', ['country' => null, 'state' => null, 'country_id' => 'seller_country', 'state_id' => 'seller_state'])

	<script src="/assets/plugins/fileuploads/js/dropify.min.js"></script>
	<script src="/assets/js/jquery.maskMoney.min.js"></script>
	<script src="/assets/js/jquery.mask.min.js"></script>
	<script src="/assets/app/js/deal-calculations.js"></script>

	<script type="text/javascript">
		$('.dropify').dropify({
			messages: {
				'default': 'Click or drag & drop a photo.',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
			},
			error: {
				'fileSize': 'The file size is too big (1M max).'
			}
		});
	</script>
@endsection