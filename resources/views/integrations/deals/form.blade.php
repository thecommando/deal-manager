{!! Form::hidden('api_key', Request::get('api_key')) !!}
{!! Form::hidden('finder_key', Request::get('finder_key')) !!}

<!--- Address Field --->
<div class="form-row">
    <div class="col col-8">
        <div class="field-group prepend-icon">
            {!! Form::label('address', 'Street Address:', ['class' => 'mdn-label']) !!}
            {!! Form::text('address', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your street address ']) !!}
            <span class="mdn-icon"><i class="fa fa-map-o"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-4">
        <!--- City Field --->
        <div class="field-group prepend-icon">
            {!! Form::label('city', 'City:', ['class' => 'mdn-label']) !!}
            {!! Form::text('city', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your city']) !!}
            <span class="mdn-icon"><i class="fa fa-map-marker"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
</div> 


<!--- State Field --->
<div class="form-row">
    <div class="col col-4">
        <div class="field-group mdn-select">
            {!! Form::label('state', 'State:', ['class' => 'mdn-label']) !!}
            {!! Form::select('state', [], null, ['class' => 'form-control mdn-input', 'placeholder' => 'Select your state ']) !!}
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-4">
        <!--- Country Field --->
        <div class="field-group mdn-select">
            {!! Form::label('country', 'Country:', ['class' => 'mdn-label']) !!}
            {!! Form::select('country', country_options(), null, ['class' => 'form-control mdn-input', 'placeholder' => 'Select your country']) !!}
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-4">
        <!--- Postcal Code Field --->
        <div class="field-group prepend-icon">
            {!! Form::label('postal_code', 'Postal Code:', ['class' => 'mdn-label']) !!}
            {!! Form::text('postal_code', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your postal code']) !!}
            <span class="mdn-icon"><i class="fa fa-barcode"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
</div>


<!--- Bedroom Field --->
<div class="form-row">
    <div class="col col-2">
        <div class="field-group mdn-select prepend-icon">
            {!! Form::label('bedrooms', 'Bedroom(s):', ['class' => 'mdn-label']) !!}
            {!! Form::select('bedrooms', bedrooms(), null, ['class' => 'form-control mdn-input', 'placeholder' => '']) !!}
            <span class="mdn-icon"><i class="fa fa-bed"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-2">
        <!--- Bathroom Field --->
        <div class="field-group mdn-select prepend-icon">
            {!! Form::label('bathrooms', 'Bathroom(s):', ['class' => 'mdn-label']) !!}
            {!! Form::select('bathrooms', bathrooms(), null, ['class' => 'form-control mdn-input', 'placeholder' => '']) !!}
            <span class="mdn-icon"><i class="fa fa-bath"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-4">
        <!--- Property Type Field --->
        <div class="field-group mdn-select prepend-icon">
            {!! Form::label('type', 'Property Type:', ['class' => 'mdn-label']) !!}
            {!! Form::select('type', property_types(), null, ['class' => 'form-control mdn-input', 'placeholder' => '']) !!}
            <span class="mdn-icon"><i class="fa fa-home"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-2">
        <!--- Year Built Type Field --->
        <div class="field-group mdn-select prepend-icon">
            {!! Form::label('year_built', 'Year Built:', ['class' => 'mdn-label']) !!}
            {!! Form::select('year_built', year_built(), null, ['class' => 'form-control mdn-input', 'placeholder' => '']) !!}
            <span class="mdn-icon"><i class="fa fa-clock-o"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-2">
        <!--- Square Feet Field --->
        <div class="field-group prepend-icon">
            {!! Form::label('square_feet', 'SQFT:', ['class' => 'mdn-label']) !!}
            {!! Form::text('square_feet', null, ['class' => 'form-control mdn-input', 'placeholder' => '']) !!}
            <span class="mdn-icon"><i class="fa fa-arrows-alt"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
</div>

<!--- Description Field --->
    <div class="field-group prepend-icon">
        {!! Form::label('description', 'Description:', ['class' => 'mdn-label']) !!}
        {!! Form::textarea('description', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter any additional information about the your property here that you think would be helpful.']) !!}
        <span class="mdn-icon"><i class="fa fa-pencil"></i></span>
        <span class="mdn-msg right">700 characters maximum </span>
        <span class="mdn-bar"></span>
    </div>

<!--- Asking Price Field --->
<div class="field-group prepend-icon">
    {!! Form::label('asking_price', 'What is the asking price of the property?', ['class' => 'mdn-label']) !!}
    {!! Form::email('asking_price', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter the lowest you would take for this property.']) !!}
    <span class="mdn-icon"><i class="fa fa-dollar"></i></span>
    <span class="mdn-bar"></span>
</div>

<h5><strong>SELLER INFORMATION</strong></h5>
<hr class="m-t-0">


<!--- Name Field --->
<div class="form-row m-t-40">
    <div class="col col-6">
        <div class="field-group prepend-icon">
            {!! Form::label('name', 'Name:', ['class' => 'mdn-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your full name']) !!}
            <span class="mdn-icon"><i class="fa fa-male"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
    <div class="col col-6">
        <!--- Mobile phone Field --->
        <div class="field-group prepend-icon">
            {!! Form::label('mobile_phone', 'Mobile Phone:', ['class' => 'mdn-label']) !!}
            {!! Form::text('mobile_phone', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your mobile number']) !!}
            <span class="mdn-icon"><i class="fa fa-phone"></i></span>
            <span class="mdn-bar"></span>
        </div>
    </div>
</div>    

<!--- Email Field --->
<div class="field-group prepend-icon">
    {!! Form::label('email', 'Email:', ['class' => 'mdn-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control mdn-input', 'placeholder' => 'Enter your primary email ']) !!}
    <span class="mdn-icon"><i class="fa fa-envelope"></i></span>
    <span class="mdn-bar"></span>
</div>


<div class="row">	
	
    <!--- Asking price Field  --->
    <div class="form-group input-group-sm col-sm-8">
        {!! Form::hidden('asking_price', 'What is the asking price of the property (if available)?') !!}
    </div>
    <div class="form-group input-group-sm col-sm-4 small">
        {!! Form::hidden('asking_price', null, ['class' => 'form-control', 'v-model' => 'asking_price']) !!}
    </div>

    <div class="form-group input-group-sm col-sm-8">
        {!! Form::hidden('rehab_estimate', 'What is the estimated rehab amount for this property?') !!}
    </div>
    <div class="form-group input-group-sm col-sm-4 small">
        {!! Form::hidden('rehab_estimate', null, ['class' => 'form-control', 'data-toggle' => 'tooltip', 'v-model' => 'rehab_estimate']) !!}
    </div>

    <div class="form-group input-group-sm col-sm-4 small">
        <label>API Key (If you wanna transfer this deal to another user)</label>
        {!! Form::text('transfer_key', null, ['class' => 'form-control', 'placeholder' => 'Optional']) !!}
    </div>

	<!--- Submit Field --->
    <div class="text-right m-t-30 form-group">
        {!! Form::submit('Submit', ['class' => 'btn btn-success waves-light', 'v-on:click' => 'onSubmit']) !!}
        <a href="{{ route('dashboard') }}" class="btn btn-default waves-effect">Cancel</a>
    </div>
	
</div>