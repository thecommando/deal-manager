@extends('layouts.default')

@section('styles')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/select.dataTables.min.css">

@endsection


@section('content')

<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Manage Teams</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">User Management</a>
                  </li>
                  <li class="breadcrumb-item active">Manage Teams
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>


    <section id="compact-style">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Current Active Users</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						</ul>
					</div>
				</div>
                
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<p></p>
						<table class="table table-striped table-bordered compact data-table">
							<thead>
                            <tr>
                                <th>Team Name</th>
                                <th>Team Admin</th>
                                <th>Open Deals</th>
                                <th>Members</th>
                                <th>Registration Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->teamName() }}</td>
                                <td><a href="{{ route('users.edit', $user->id) }}">{{ $user->name }}</a></td>
                                <td><p class="font-light">
                                    <span class="tag tag-md btn-warning">
                                        {{ $user->teamOpenDeals() }}
                                    </span>
                                </td>
                                <td><p class="font-light">
                                    <span class="tag tag-md btn-success">
                                        {{ $user->sub_users()->count() }}
                                    </span></p>
                                </td>
                                <td>
                                    <a href="{{ url("register?suid=$user->id") }}">{{ url("register?suid=$user->id") }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('teams.edit', $user->id) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Add/Edit Team Users"><i class="fa fa-user"></i></a>
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit {{ $user->name }}'s Profile"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
											
						</table>				
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Base -->

@endsection