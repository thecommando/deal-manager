@if($contacts->count())
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Contacts</h3>
        </div>
    </div>

    <section id="mobile-support">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div class="list-group">
                                @foreach($contacts as $contact)
                                    <a href="{{ route('contacts.show', $contact->id) }}" class="list-group-item">
                                        {{ $contact->full_name }}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif