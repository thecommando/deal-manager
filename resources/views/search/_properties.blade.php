@if($properties->count())
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Properties</h3>
        </div>
    </div>

    <section id="mobile-support">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div class="list-group">
                                @foreach($properties as $property)
                                    <a href="{{ route('deals.show', $property->id) }}" class="list-group-item">
                                        {{ $property->full_address }}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif