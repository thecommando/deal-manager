<nav class="header-navbar navbar navbar-with-menu navbar-static-top navbar-dark bg-gradient-x-grey-blue navbar-border navbar-brand-left">
  <div class="navbar-wrapper">
    <div class="navbar-header" style="width: auto">
      <ul class="nav navbar-nav">
        <li class="nav-item float-xs-left">
            <a href="{{ route('dashboard') }}" class="navbar-brand" style="margin-top: 2px">
                <img src="{{ url('assets/images/dmp-logo-light.png') }}" alt="Deal Manager Pro" class="img img-responsive" width="300px">
            </a>
        </li>
        <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>

        <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
      </ul>
    </div>
    <div class="navbar-container content container-fluid">
      <div id="navbar-mobile" class="collapse navbar-toggleable-sm">

        <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item nav-search">
                <a class="nav-link nav-link-search" href="#"><i class="ficon ft-search"></i></a>
                <div class="search-input {{ request()->routeIs('search') ? 'open' : '' }}">
                    <form action="{{ route('search') }}" method="GET">
                        <input class="input" type="text" name="q" placeholder="Search..." value="{{ request('q') }}">
                    </form>
                </div>
            </li>
        </ul>

        <ul class="nav navbar-nav float-xs-right">            
            <user-notifications></user-notifications>

            <li class="dropdown dropdown-user nav-item">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                    <span class="avatar avatar-online">
                        <img src="{{ auth()->user()->photoURL() }}" alt="{{ auth()->user()->first_name }}" class="img-circle user-img">
                    </span>
                    <span class="user-name">{{ auth()->user()->name }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="{{ route('profile') }}" class="dropdown-item"><i class="ft-user"></i> Edit Profile</a>
                  <a href="{{ route('activities') }}" class="dropdown-item"><i class="ft-mail"></i> Activity Log</a>
                  @role(['admin', 'super-user', 'user', 'bird-dog-deal-finder'])
                      <a href="{{ route('dashboard.onboarding') }}" class="dropdown-item"><i class="ft-cog"></i> Onboarding</a>
                  
                      <a href="{{ route('integrations') }}" class="dropdown-item"><i class="ft-check-square"></i> Integrations</a>
                      <a href="{{ route('broadcasts.index') }}" class="dropdown-item"><i class="fa fa-bullhorn"></i> Broadcasts</a>
                      <a href="{{ route('settings.index') }}" class="dropdown-item"><i class="ft-comment-square"></i> Settings</a>
                  @endrole
                  <div class="dropdown-divider"></div>

                  <a href="{{ route('logout') }}" class="dropdown-item"
                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="ft-power"></i> Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                </div>
            </li>
        </ul>
      </div>
    </div>
  </div>
</nav>

@include('layouts.partials.menubar')