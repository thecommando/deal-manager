<!-- Footer -->
<footer class="footer footer-static footer-light m-t-40">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
          <span class="float-md-left d-xs-block d-md-inline-block">© {{ \Carbon\Carbon::today()->year }} All Rights Reserved. Deal Manager Pro.</span>
          <span class="float-md-right d-xs-block d-md-inline-block">
              
						<a class="breadcrumb-item" href="{{ route('dashboard') }}">Dashboard</a>
				
						<a class="breadcrumb-item" href="{{ route('resources.support') }}">Support</a>
				  
						<a class="breadcrumb-item" href="/legal">Privacy & Terms</a> <!-- Proper routing needs to be updated -->
				  
          </span>
       </p>
</footer>
<!-- End Footer -->