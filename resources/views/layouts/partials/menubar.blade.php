
<div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-bordered navbar-shadow menu-border">
    <!-- Horizontal menu content-->

    <div data-menu="menu-container" class="navbar-container main-menu-content">
        @role(['finder'])
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
          <li class="nav-item"><a href="{{ route('dashboard') }}" class="dropdown-toggle nav-link"><i class="fa fa-area-chart"></i><span>DASHBOARD</span></a>
          </li>
        </ul>
        @endrole

        @role(['admin', 'super-user', 'user', 'bird-dog-deal-finder'])
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
          <li class="nav-item"><a href="{{ route('dashboard') }}" class="dropdown-toggle nav-link"><i class="fa fa-area-chart"></i><span>DASHBOARD</span></a>
          </li>

         <li data-menu="dropdown" class="dropdown nav-item nav-wide"><a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-briefcase"></i><span>DEAL MANAGEMENT</span></a>
            <ul class="dropdown-menu">
              <li data-menu=""><a href="{{ route('deals.index') }}" data-toggle="dropdown" class="dropdown-item">View My Deals</a>
              </li>
              <li data-menu=""><a href="{{ route('deals.pending-properties') }}" data-toggle="dropdown" class="dropdown-item">View My Pending Deals</a>
              </li>
              <li data-menu=""><a href="{{ route('deals.create') }}" data-toggle="dropdown" class="dropdown-item">Add New Deal</a>
              </li>
              <li data-menu=""><a href="{{ route('deals.upload') }}" data-toggle="dropdown" class="dropdown-item">Upload Deals</a>
              </li>
            </ul>
          </li>

          @role(['super-user', 'admin', 'coach'])
          <li data-menu="dropdown" class="dropdown nav-item nav-wide"><a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-user"></i><span>USER MANAGEMENT</span></a>
            <ul class="dropdown-menu">
              @role('admin')
              <li data-menu=""><a href="{{ route('users.index') }}" data-toggle="dropdown" class="dropdown-item">Manage Users</a>
              </li>
              <li data-menu=""><a href="{{ route('users.index',['archived' => 'true']) }}" data-toggle="dropdown" class="dropdown-item">Archive Users</a>
              </li>
              <li data-menu=""><a href="{{ route('users.trash') }}" data-toggle="dropdown" class="dropdown-item">Restore Users</a>
              </li>
              <li data-menu=""><a href="{{ route('teams.index') }}" data-toggle="dropdown" class="dropdown-item">Manage Teams</a>
              </li>
              <li data-menu=""><a href="{{ route('users.create') }}" data-toggle="dropdown" class="dropdown-item">Add New User</a>
              </li>
              @endrole

              @role(['super-user', 'coach'])
              <li data-menu=""><a href="{{ route('team.users') }}" data-toggle="dropdown" class="dropdown-item">Manage Team</a>
              </li>
              @endrole
            </ul>
          </li>
          @endrole

          <li data-menu="dropdown" class="dropdown nav-item nav-wide"><a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-envelope"></i><span>CONTACTS</span></a>
            <ul class="dropdown-menu">
                <li data-menu=""><a href="{{ route('categories.index', ['all' => 'true']) }}" data-toggle="dropdown" class="dropdown-item">View All Categories</a></li>
                <li data-menu=""><a href="{{ route('categories.create') }}" data-toggle="dropdown" class="dropdown-item">Add New Category</a></li>
                <li data-menu=""><a href="{{ route('contacts.index') }}" data-toggle="dropdown" class="dropdown-item">View My Contacts</a></li>
                <li data-menu=""><a href="{{ route('contacts.create') }}" data-toggle="dropdown" class="dropdown-item">Add New Contact</a></li>
                <li data-menu=""><a href="{{ route('contacts.upload') }}" data-toggle="dropdown" class="dropdown-item">Upload Contacts</a></li>
                <li data-menu=""><a href="{{ route('contact-groups.index') }}" data-toggle="dropdown" class="dropdown-item">Contact Groups</a></li>

              @role('admin')
                  <div class="dropdown-divider"></div>
                  <li data-menu=""><a href="{{ route('contacts.index', ['all' => 'true']) }}" data-toggle="dropdown" class="dropdown-item">View All Contacts</a></li>
              @endrole

            </ul>
          </li>

          <li data-menu="dropdown" class="dropdown nav-item nav-wide"><a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-cogs"></i><span>RESOURCES</span></a>
            <ul class="dropdown-menu">
              <li data-menu=""><a href="{{ route('calendar.index') }}" data-toggle="dropdown" class="dropdown-item">Calendar</a>
              </li>
              <li data-menu=""><a href="{{ route('print-genie-login.index') }}"  target="_blank" data-toggle="dropdown" class="dropdown-item">Print Genie</a>
              <li data-menu=""><a href="{{ route('crei-ms-login.index') }}"  target="_blank" data-toggle="dropdown" class="dropdown-item">My Website</a>
                <li data-menu=""><a href="{{ route('documents.index') }}" data-toggle="dropdown" class="dropdown-item">My Documents</a>
              </li>
              <li data-menu=""><a href="{{ route('manage-templates.index') }}" data-toggle="dropdown" class="dropdown-item">Manage Templates</a>
              </li>
              <li data-menu=""><a href="{{ route('dashboard.onboarding') }}" data-toggle="dropdown" class="dropdown-item">Onboarding</a>
              </li>
              <li data-menu=""><a href="{{ route('resources.training') }}" data-toggle="dropdown" class="dropdown-item">Training Videos</a>
              </li>
              <li data-menu=""><a href="{{ route('resources.support') }}" data-toggle="dropdown" class="dropdown-item">Support</a>
              </li>
              @role(['admin', 'super-user', 'bird-dog-deal-finder'])
              <div class="dropdown-divider"></div>
                <li data-menu=""><a href="{{ route('documents.create') }}" data-toggle="dropdown" class="dropdown-item">View All Documents</a></li>
              @endrole

              <li data-menu=""><a href="{{ route('my-team.index') }}" data-toggle="dropdown" class="dropdown-item">Build My Team</a></li>
            </ul>

          </li>

          <li class="nav-item nav-wide"><a href="{{ route('login.dbm') }}" target="_blank" class="dropdown-toggle nav-link"><i class="fa fa-line-chart"></i><span>MARKETING CENTER</span></a></li>
        </ul>@endrole
      </div>
    <!-- /horizontal menu content-->
</div>
