<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="A fully featured real estate deal management application">
	<meta name="author" content="Deal Manager Pro">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="shortcut icon" href="/assets/images/favicon.ico">

	<title>Deal Manager Pro™ - Real Estate Software</title>


	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

	<!-- BEGIN VENDOR CSS-->
	{{--    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap.min.css">--}}
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/bootstrap.min.css') }}">

	{{--    <link rel="stylesheet" type="text/css" href="/app-assets/fonts/feather/style.min.css">--}}
	{{--    <link rel="stylesheet" type="text/css" href="/app-assets/fonts/font-awesome/css/font-awesome.min.css">--}}
	{{--    <link rel="stylesheet" type="text/css" href="/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">--}}
	{{--    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/pace.css">--}}
	{{--    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/pickers/daterange/daterangepicker.css">--}}

	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/fonts/feather/style.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/fonts/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/fonts/flag-icon-css/css/flag-icon.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/extensions/pace.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/pickers/daterange/daterangepicker.css') }}">

	<!-- END VENDOR CSS-->

	<!-- BEGIN STACK CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/bootstrap-extended.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/colors.css') }}">
	<!-- END STACK CSS-->

	<!-- BEGIN Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/core/menu/menu-types/horizontal-menu.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/core/colors/palette-gradient.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/plugins/forms/wizard.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/css/plugins/pickers/daterange/daterange.css')}}">
	<!-- END Page Level CSS-->

	<!-- BEGIN Custom CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/custom-style.css')}}">
	<!-- END Custom CSS-->



	{{-- Plugins --}}
	<link href="{{ asset('/assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
	<!--<link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" /> -->
	<link href="{{ asset('/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
	<link href="{{ asset('/assets/plugins/select2/dist/css/select2.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('/assets/plugins/select2/dist/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('/assets/plugins/multiselect/css/multi-select.css')}}"  rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/plugins/light-gallery/css/lightgallery.css')}}" type="text/css" rel="stylesheet" />
	<link href="{{ asset('/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">




	<!--
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/responsive.css" rel="stylesheet" type="text/css" />
    -->

	<script src="{{ asset('/assets/js/modernizr.min.js')}}"></script>

	<style>
		[v-cloak] {
			display: none;
		}
	</style>
	<script>
		let inactivityTime = function() {
			let time;
			window.onload = resetTimer;
			document.onmousemove = resetTimer;
			document.onkeypress = resetTimer;
			function logout() {
				$('#inactiveModal').modal({
					show:true,
					keyboard: false,
					backdrop:'static',
					focus:true,
				});
			}
			function resetTimer() {
				clearTimeout(time);
				time = setTimeout(logout, 1000*60*20)
			}
		};
		window.onload = function() {
			inactivityTime();
		}
	</script>
	<script>
		function goBack() {
			window.history.back();
		}

		window.Application = <?php echo json_encode(array_merge(
				Application::scriptVariables(), []
		)); ?>
	</script>

	@yield('styles')

</head>
<body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns">
	<div id="app" v-cloak>
		@include('layouts.partials.navbar')

		<div class="app-content content container-fluid">
			<div class="content-wrapper">

				@yield('content')

			</div>

		</div>

		@include('layouts.partials.footer')
	</div>

	<script src="{{ mix('js/app.js') }}"></script>

	<!-- NEW THEME JS-->

	<!-- BEGIN VENDOR JS-->
	<script src="{{ asset('/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
	<!-- BEGIN VENDOR JS-->

	<!-- BEGIN PAGE VENDOR JS-->
	<script type="text/javascript" src="{{ asset('/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
	<script type="text/javascript" src="{{ asset('/app-assets/vendors/js/charts/jquery.sparkline.min.js')}}"></script>
	<script src="{{ asset('/app-assets/vendors/js/extensions/jquery.steps.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('/app-assets/vendors/js/pickers/daterange/daterangepicker.js')}}" type="text/javascript"></script>
	<script src="{{ asset('/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}" type="text/javascript"></script>
	<!-- END PAGE VENDOR JS-->

	<!-- BEGIN STACK JS-->
	<script src="{{ asset('/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/app-assets/js/core/app.js') }}" type="text/javascript"></script>
	<!-- END STACK JS-->

	<!-- BEGIN PAGE LEVEL JS-->
	<script type="text/javascript" src="{{ asset('/app-assets/js/scripts/ui/breadcrumbs-with-stats.js') }}"></script>
	<script src="{{ asset('/app-assets/js/scripts/forms/wizard-steps.js') }}" type="text/javascript"></script>
	<!-- END PAGE LEVEL JS-->

	<!-- END NEW THEME JS-->


	<!-- jQuery  -->
	<!-- <script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/detect.js"></script>
	-->
	<script src="{{ asset('/assets/js/fastclick.js') }}"></script>

	<script src="{{ asset('/assets/js/accounting.js') }}"></script>
	<script src="{{ URL::asset('assets/js/numeral.js') }}"></script>

	<script src="{{ asset('/assets/js/jquery.slimscroll.js') }}"></script>
	<script src="{{ asset('/assets/js/jquery.blockUI.js') }}"></script>
	<script src="{{ asset('/assets/js/waves.js') }}"></script>
	<script src="{{ asset('/assets/js/wow.min.js') }}"></script>
	<script src="{{ asset('/assets/js/jquery.nicescroll.js') }}"></script>
	<script src="{{ asset('/assets/js/jquery.scrollTo.min.js') }}"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>


	<!-- KNOB JS -->
	<!--[if IE]
	<script type="text/javascript" src="/assets/plugins/jquery-knob/excanvas.js"></script>
	[endif]-->

	{{-- Plugins --}}
	<script src="{{ asset('/assets/plugins/jquery-knob/jquery.knob.js') }}"></script>
	<script src="{{ asset('/assets/plugins/raphael/raphael-min.js') }}"></script>
	<script src="{{ asset('/assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
	<script src="{{ asset('/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
	<script src="{{ asset('/assets/plugins/switchery/switchery.min.js') }}"></script>
	<script src="{{ asset('/assets/plugins/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
	<script src="{{ asset('/assets/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
	<script src="{{ asset('/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
	<script src="{{ asset('/assets/plugins/vue/vue.js') }}"></script>
	<script src="{{ asset('/assets/plugins/vue/vue-resource.js') }}"></script>

	{{-- Light Gallery --}}
	<script src="{{ asset('/assets/plugins/light-gallery/js/lightgallery.min.js') }}"></script>
	<!-- A jQuery plugin that adds cross-browser mouse wheel support. (Optional) -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
	<!-- lightgallery plugins -->
	<script src="{{ asset('/assets/plugins/light-gallery/js/lg-thumbnail.min.js') }}"></script>
	<script src="{{ asset('/assets/plugins/light-gallery/js/lg-fullscreen.min.js') }}"></script>

	<!-- Counter Up  -->
	<script src="{{ asset('/assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
	<script src="{{ asset('/assets/plugins/counterup/jquery.counterup.min.js') }}"></script>


	<!-- App js -->
	<script src="{{ asset('/assets/js/jquery.core.js') }}"></script>
	<script src="{{ asset('/assets/js/jquery.app.js') }}"></script>

    @if(request()->root() != 'https://app.dealmanagerpro.com')
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    @endif

	@include('partials.flash')

	<!-- Function for switching between Grid View and List View on certain pages. -->
	<script>
		$(document).ready(function() {
			$(".select2").select2();
			$(".data-table").dataTable();
			$("#grid").hide();
			$('.datepicker').datepicker({
				autoclose: true,
				todayHighlight: true
			});

			$("#btnToggleView").click(function(){
				$("#list").toggle(500);
				$("#grid").toggle(500);
			});
		});
	</script>
	<!-- Script used to mask currency fields: doesn't work well because of calculations
	<script>
	  $(function() {
		$('.currencyVal').maskMoney({precision:0});
	  })
	</script>
	-->

	@yield('scripts')
	@yield('footer')
	<!-- Modal -->
	<div class="modal fade" id="inactiveModal" tabindex="-1" role="dialog" aria-labelledby="inactiveModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<form id="L-frm" action="{{ route('logout') }}" method="POST">
				{{ csrf_field() }}
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Deal Manager Pro™</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						Are you still there?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Yes. Keep me logged in.</button>
						<button type="submit" class="btn btn-primary">I’m done. Please log me out.</button>
					</div>
				</div>
		</div>
	</div>
</body>
</html>