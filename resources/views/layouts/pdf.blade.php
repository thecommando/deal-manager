<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

	<title>@yield('title', __('Pdf'))</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
	<style>
		html {
			font-family: sans-serif;
		}

		body {
			background: #fff;
			background-image: none;
			font-size: 12px;
		}

		address {
			margin-top: 15px;
		}

		hr {
			border: 1px dotted lightgray;
		}

		h2 {
			font-size: 28px;
			color: #cccccc;
		}

		.container {
			padding-top: 30px;
		}

		.invoice-head td {
			padding: 0 8px;
		}

		.invoice-body {
			background-color: transparent;
		}

		.logo {
			padding-bottom: 10px;
		}

		.well {
			margin-top: 15px;
		}

		table {
			width: 100%;
			border: 0;
			border-collapse: collapse;
		}
		table tr,
		table td,
		table th {
			border: 0;
		}

		/*.row {*/
		/*overflow: hidden;*/
		/*margin-left: -15px;*/
		/*margin-right: -15px;*/
		/*}*/

		.pdf-table {
			display: table;
			width: 100%;
		}

		.pdf-table .row {
			display: table-row;
			width: 100%;
		}

		.pdf-table .col-md-6 {
			display: table-cell;
			width: 50%;
			padding-right: 20px;
		}

		.pdf-table .col-md-4 {
			display: table-cell;
			width: 33%;
			padding-right: 20px;
		}

		.pdf-table .col-md-8 {
			display: table-cell;
			width: 66%;
			padding-right: 20px;
		}

		.sign-here {
			/*padding-top: 50px;*/
		}

		.fill-out {
			padding-top: 30px;
		}

		.text-right {
			text-align: right;
		}

		.text-left {
			text-align: left;
		}

		.text-center {
			text-align: center;
		}

		.page-break {
			page-break-after: always;
		}

		/*.col-md-6 {*/
		/*width: calc(50% - 30px);*/
		/*float: left;*/
		/*padding: 15px;*/
		/*}*/
	</style>

	@yield('head')
</head>
<body>
	<div class="max-w-4xl mx-auto">
		@yield('content')
	</div>
	{{-- PDF Pagination Script --}}
	<script type="text/php">
		if (isset($pdf)) {
			$text = "{PAGE_NUM} / {PAGE_COUNT}";
			$size = 10;
			$font = $fontMetrics->getFont("Verdana");
			$width = $fontMetrics->get_text_width($text, $font, $size) / 2;
			$x = ($pdf->get_width() - $width) / 2;
			$y = $pdf->get_height() - 35;
			$pdf->page_text($x, $y, $text, $font, $size);
		}
	</script>
</body>
</html>