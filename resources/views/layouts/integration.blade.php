<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Deal Manager Pro">

	<link rel="shortcut icon" href="/assets/images/favicon.ico">

	<title>Deal Manager Pro™ - Real Estate Software</title>

	{{-- Plugins --}}
	<link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
	<link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
	<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
	<link href="/assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
	<link href="/assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />

	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/core.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/components.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/pages.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/menu.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/modernforms.css" rel="stylesheet" type="text/css" >
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/custom-style.css">
    <!-- END Custom CSS-->
    
    

	<script src="/assets/js/modernizr.min.js"></script>
	
	<script>
		function goBack() {
			window.history.back();
		}
	</script>

	@yield('styles')
	
</head>
<body style="padding-bottom: 0">
	@yield('content')

	<!-- jQuery  -->
	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/detect.js"></script>
	<script src="/assets/js/fastclick.js"></script>

	<script src="/assets/js/jquery.slimscroll.js"></script>
	<script src="/assets/js/jquery.blockUI.js"></script>
	<script src="/assets/js/waves.js"></script>
	<script src="/assets/js/wow.min.js"></script>
	<script src="/assets/js/jquery.nicescroll.js"></script>
	<script src="/assets/js/jquery.scrollTo.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>

	<!-- KNOB JS -->
	<!--[if IE]>
	<script type="text/javascript" src="/assets/plugins/jquery-knob/excanvas.js"></script>
	<![endif]-->

	{{-- Plugins --}}
	<script src="/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="/assets/plugins/raphael/raphael-min.js"></script>
	<script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables/dataTables.bootstrap.js"></script>
	<script src="/assets/plugins/switchery/switchery.min.js"></script>
	<script src="/assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
	<script src="/assets/plugins/multiselect/js/jquery.multi-select.js"></script>
	<script src="/assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
	<script src="/assets/plugins/vue/vue.js"></script>
	<script src="/assets/plugins/vue/vue-resource.js"></script>

	<!-- Counter Up  -->
	<script src="/assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
	<script src="/assets/plugins/counterup/jquery.counterup.min.js"></script>


	<!-- App js -->
	<script src="/assets/js/jquery.core.js"></script>
	<script src="/assets/js/jquery.app.js"></script>

	@include('partials.flash')

	@yield('scripts')
	
</body>
</html>