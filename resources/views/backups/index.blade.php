@extends('layouts.default')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Backups</h1>
            </div>
            <div class="col-md-6">
                <a href="{{ route('backups.create') }}" class="btn btn-primary pull-right">Create Backup</a>
            </div>
        </div>

        <br>

        <div class="panel panel-default">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>File Name</th>
                        <th>Size</th>
                        <th>Last Modified</th>
                        <th>Download</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($files as $file)
                        <tr>
                            <td>
                                {{ $file['name'] }}
                            </td>
                            <td>{{ $file['size'] }}</td>
                            <td>{{ $file['lastModified'] }}</td>
                            <td>
                                <a href="{{ route('backups.show', [$file['name']]) }}">
                                    <i class="fa fa-download"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection