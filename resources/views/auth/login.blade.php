@extends('layouts.empty')

@section('content')

<div class="side-cover-wrapper full-screen visible-lg visible-md">
		<div class="fslider" data-speed="3000" data-pause="5000" data-animation="fade" data-arrows="false" data-pagi="false" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; background-color: #333;">
			<div class="flexslider" style="height: 100% !important;">
				<div class="slider-wrap" style="height: inherit !important;">
					<div class="slide full-screen force-full-screen" style="background: url('assets/images/todd-kent-178j8tJrNlc-unsplash-1.jpg') center right; background-size: cover; height: 100% !important;"></div>
					<div class="slide full-screen force-full-screen" style="background: url('assets/images/vu-anh-TiVPTYCG_3E-unsplash-e1623334221153.jpg') center left; background-size: cover; height: 100% !important;"></div>
                    <div class="slide full-screen force-full-screen" style="background: url('assets/images/trinity-nguyen-xQhqS7OWEqE-unsplash-1.jpg') center; background-size: cover; height: 100% !important;"></div>
                    <div class="slide full-screen force-full-screen" style="background: url('assets/images/home-lower-scaled-1.jpeg') center right; background-size: cover; height: 100% !important;"></div>
                    <div class="slide full-screen force-full-screen" style="background: url('assets/images/feature1.jpeg') center left; background-size: cover; height: 100% !important;"></div>
                    <div class="slide full-screen force-full-screen" style="background: url('assets/images/home.jpeg') center; background-size: cover; height: 100% !important;"></div>
				</div>
			</div>
		</div>
</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap m-t-40">

				<div id="section-about" class="container-fluid">
                    
                    <div class="m-t-20">&nbsp;</div>
                    
                    <div id="narrow-box" class="m-t-40 card-box">
                        
                        <div class="text-center m-t-20">
                            <a href="{{ url('/') }}" style="margin-top: 13px">
                                <img src="{{ url('assets/images/dmp-logo.png') }}" alt="Deal Manager Pro" class="img center-block img-responsive" width="300px">
                            </a>
                            
                            
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('/login') }}">

                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-xs-12">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-xs-12">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                @if($errors->has('access_denied'))
                                <div>
                                    <span class="text-danger help-block">
                                        <strong>{!! $errors->first('access_denied') !!}</strong>
                                    </span>
                                </div>
                                @endif

                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <div class="checkbox checkbox-custom">
                                            <input id="checkbox-remember" type="checkbox" name="remember">
                                            <label for="checkbox-remember">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group text-center m-t-30">
                                    <div class="col-xs-12">
                                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Log In</button>
                                    </div>
                                </div>

                                <div class="form-group m-t-30 m-b-0">
                                    <div class="col-sm-12">
                                        <a href="{{ url('/password/reset') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                    <!-- end card-box-->

                    <div class="m-t-40 text-center">
                        <h3>Deal Manager Pro</h3>
                        <h4>Built BY Real Estate Investors, FOR Real Estate Investors</h4>
                    </div>

                </div>
                
            </div>
        </section>
    </div>

@endsection