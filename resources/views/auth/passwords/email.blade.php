@extends('layouts.empty')

@section('content')

    <div class="side-cover-wrapper full-screen visible-lg visible-md">
        <div class="fslider" data-speed="3000" data-pause="5000" data-animation="fade" data-arrows="false" data-pagi="false" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; background-color: #333;">
            <div class="flexslider" style="height: 100% !important;">
                <div class="slider-wrap" style="height: inherit !important;">
                    <div class="slide full-screen force-full-screen" style="background: url('/assets/images/4.jpg') center right; background-size: cover; height: 100% !important;"></div>
                    <div class="slide full-screen force-full-screen" style="background: url('/assets/images/5.jpg') center left; background-size: cover; height: 100% !important;"></div>
                    <div class="slide full-screen force-full-screen" style="background: url('/assets/images/7.jpg') center; background-size: cover; height: 100% !important;"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap m-t-40">

                <div id="section-about" class="container-fluid">

                    <div class="m-t-20">&nbsp;</div>

                    <div id="narrow-box" class="m-t-40 card-box">

                        <div class="text-center m-t-20">
                            <a href="{{ url('/') }}" style="margin-top: 13px">
                                <img src="{{ url('/assets/images/dmp-logo.png') }}" alt="Deal Manager Pro" class="img center-block img-responsive" width="300px">
                            </a>


                        </div>
                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('/password/email') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="EMail Address">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{!! $errors->first('email') !!}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group text-center m-t-30">
                                    <div class="col-xs-12">
                                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit"><i class="fa fa-btn fa-envelope"></i> Send Password Reset Link</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                    <!-- end card-box-->

                </div>

            </div>
        </section>
    </div>

@endsection