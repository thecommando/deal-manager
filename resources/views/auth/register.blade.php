@extends('layouts.empty')

@section('content')

<div class="side-cover-wrapper full-screen visible-lg visible-md">
		<div class="fslider" data-speed="3000" data-pause="5000" data-animation="fade" data-arrows="false" data-pagi="false" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; background-color: #333;">
			<div class="flexslider" style="height: 100% !important;">
				<div class="slider-wrap" style="height: inherit !important;">
					<div class="slide full-screen force-full-screen" style="background: url('assets/images/4.jpg') center right; background-size: cover; height: 100% !important;"></div>
					<div class="slide full-screen force-full-screen" style="background: url('assets/images/5.jpg') center left; background-size: cover; height: 100% !important;"></div>
                    <div class="slide full-screen force-full-screen" style="background: url('assets/images/7.jpg') center; background-size: cover; height: 100% !important;"></div>
				</div>
			</div>
		</div>
</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div id="section-about" class="container-fluid">
                    
                    
                    
                    <div id="" class="m-t-40 card-box">
                        
                        <div class="text-center m-t-20">
                            <a href="{{ url('/') }}" style="margin-top: 13px">
                                <img src="{{ url('assets/images/dmp-logo.png') }}" alt="Deal Manager Pro" class="img center-block img-responsive" width="300px">
                            </a>
                            
                            
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal m-t-20" id="payment-form" role="form" method="POST" action="{{ url('/register') }}">

                                    {{ csrf_field() }}

                                    <input type="hidden" value="{{ request('suid') }}" name="suid">

                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required>
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username" required>
                                                @if ($errors->has('username'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 m-b-20">
                                    <HR>
                                    <div class="col-xs-6">
                                    <h4 class="m-t-0 m-b-20 font-300">Deal Manager Pro Plan:</h4>

                                    </div>
                                    <!--- Plan Field --->
                                        <div class="form-group{{ $errors->has('plan') ? ' has-error' : '' }}">
                                            <div class="col-xs-6">
                                                    {!! Form::select('plan', [

                                                        'nfe_access'       => '12 Month Free Trial, $49/mo',

                                                    ], Request::has('plan') ? Request::get('plan') : null, ['class' => 'form-control']) !!}
                                                    @if ($errors->has('plan'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('plan') }}</strong>
                                                        </span>
                                                    @endif
                                            </div>
                                        </div>

                                    <HR>
                                    </div>

                                    <div class="col-md-6">
                                        <!--- Card number Field --->
                                        <div class="form-group{{ $errors->has('card_number') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                {!! Form::text('card_number', null, ['class' => 'form-control', 'data-stripe' => 'number', 'placeholder' => 'Card Number']) !!}
                                                @if ($errors->has('card_number'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('card_number') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <!--- Cvc Field --->
                                        <div class="form-group{{ $errors->has('cvc') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                {!! Form::text('cvc', null, ['class' => 'form-control', 'data-stripe' => 'cvc', 'placeholder' => 'CVC']) !!}
                                                @if ($errors->has('cvc'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('cvc') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <!--- Month Field --->
                                        <div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                {!! Form::text('month', null, ['class' => 'form-control', 'data-stripe' => 'exp-month', 'placeholder' => 'Month (2 Digit Month)']) !!}
                                                @if ($errors->has('month'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('month') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <!--- Year Field --->
                                        <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                                            <div class="col-xs-12">
                                                {!! Form::text('year', null, ['class' => 'form-control', 'data-stripe' => 'exp-year', 'placeholder' => 'Year (4 Digit Year)']) !!}
                                                @if ($errors->has('year'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('year') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">


                                        <p class="payment-errors text-center"></p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="checkbox checkbox-custom pull-right">
                                                    <input id="checkbox-terms" type="checkbox" name="terms" checked="checked">
                                                    <label for="checkbox-terms">I accept <a href="#">Terms and Conditions</a></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="form-group text-center m-t-40">
                                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">
                                                    Register
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <!-- end card-box-->

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p class="text-muted">Already have account?<a href="{{ url('login') }}" class="text-primary m-l-5"><b>Sign In</b></a></p>
                        </div>
                    </div>
                    
                    
                </div>
                
            </div>
        </section>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script type="text/javascript">
        // This identifies your website in the createToken call below
        Stripe.setPublishableKey('{!! env('STRIPE_PUBLIC') !!}');

        jQuery(function($) {
            $('#payment-form').submit(function(event) {
                var $form = $(this);

                // Disable the submit button to prevent repeated clicks
                $form.find('button').prop('disabled', true);

                Stripe.card.createToken($form, stripeResponseHandler);

                // Prevent the form from submitting with the default action
                return false;
            });
        });

        function stripeResponseHandler(status, response) {
            var $form = $('#payment-form');

            if (response.error) {
                // Show the errors on the form
                $form.find('.payment-errors').text(response.error.message);
                $form.find('button').prop('disabled', false);
            } else {
                // response contains id and card, which contains additional card details
                var token = response.id;
                // Insert the token into the form so it gets submitted to the server
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                // and submit
                $form.get(0).submit();
            }
        }
    </script>
@stop