@foreach($activities as $activity)
    <tr>
        <td>{!! $activity['message'] !!}</td>
        <td>{{ $activity['time']->diffForHumans() }}</td>
    </tr>
@endforeach