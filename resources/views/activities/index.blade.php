@extends('layouts.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/select.dataTables.min.css">

@endsection
@section('content')

<!-- Page-Title -->
	<div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Activity Log</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item active">Activity Log
                  </li>
                </ol>
              </div>
            </div>
          </div>    
    </div>


<section id="compact-style">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">User Activity Log</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
						</ul>
					</div>
				</div>
                
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						<table class="table table-striped table-bordered responsive dataex-rowre-mobilesupport">
							<thead>
								<tr>
									<th>Activity</th>
                                    <th>Time</th>
								</tr>
							</thead>
							<tbody id="activityTableData">
								@include('activities.index-paginate-data')
                            </tbody>

						</table>
						<div id="customAjaxPagination" style="display: flex; justify-content: center;">
							{{ $activitiesPaginator->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection


@section('scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$(document).on("click","#customAjaxPagination .pagination .page-item",function(e) {
				e.preventDefault();

				let url = $(this).find('a').attr('href');
				window.history.pushState({ path: url }, '', url);
				$.ajax({
					url: url,
					type: 'GET',
					success: function(response) {
						$('#activityTableData').html(response.activitiesHtml);
						$('#customAjaxPagination').html(response.paginationHtml);
					}
				});
			});
		});
	</script>
@endsection
