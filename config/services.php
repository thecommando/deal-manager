<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'zillow' => [
        'key' => env('ZILLOW_API_KEY'),
    ],

    'facebook_poster' => [
        'app_id'       => env('FACEBOOK_APP_ID'),
        'app_secret'   => env('FACEBOOK_APP_SECRET'),
        'access_token' => env('FACEBOOK_ACCESS_TOKEN'),
    ],

    'slybroadcast' => [
        'email' => env('SLYBROADCAST_EMAIL'),
        'password' => env('SLYBROADCAST_PASSWORD'),
        'caller_id' => env('SLYBROADCAST_CALLER_ID'),
    ],

    'infusionsoft' => [
        'client_id' => env('INFUSIONSOFT_CLIENT_ID'),
        'client_secret' => env('INFUSIONSOFT_CLIENT_SECRET'),
        'redirect_uri' => env('INFUSIONSOFT_REDIRECT_URI'),
        'access_tag' => env('INFUSIONSOFT_ACCESS_TAG'),
        'deny_tag' => env('INFUSIONSOFT_DENY_TAG'),
        'payment_unsuccessful_tag' => env('INFUSIONSOFT_PAYMENT_UNSUCCESSFUL_TAG'),
    ],

    'twilio' => [
        'account_sid' => '',
        'auth_token' => '',
        'from' => '',
    ],
];
