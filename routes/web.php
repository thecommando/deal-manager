<?php

use App\Services\Infusionsoft\Infusionsoft;

// Authentication Routes
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes
//Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('/register', 'Auth\RegisterController@register');

// Password Reset Routes
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

//Route::get('test', function() {

//    $event = \App\Event::latest()->first();
//    $event->user->notify(new \App\Notifications\EventReminder($event));

//    $contact = \App\Contact::latest()->first();
//    $user = $contact->user->setTwilioCredentials();

//    return $contact->notify(new \App\Notifications\SMSNotification('this is test'));
//    $service = new App\Services\Infusionsoft\Infusionsoft();

//    return $service->updateContactPassword('davidcorbaley@gmail.com', '1234');
//    dd(cache('infusionsoft.expires_in'));
//    dd($service->contactHasTag('davidcorbaley@gmail.com', 'M-CDP-ACCESS'));
//    dd($service->contactHasTag('david@marketingprollc.com', 'M-CDP-ACCESS'));
//    return $service->getContactTagsByEmail('david@marketingprollc.com');
//    return $service->getContactTags(3);
//    return $service->getContactByEmail('david@marketingprollc.com');
//    dd($service->getToken());
//    var_dump($service->requestAccessToken('pGtpFp0X'));
//    dd($service->refreshAccessToken());
//});
//Route::resource('test', 'TestController');

Route::get('legal', 'PageController@legal')->name('page.legal');

Route::get('countries/{country}/states', 'CountryController@getStates')->middleware('cors');
Route::get('deals/{property}/pdf/download', 'DealController@downloadPdf')->name('deals.download.pdf');
Route::get('property/{property}/pdf/download', 'DealController@downloadPropertyInfoPdf')->name('property.download.pdf');
Route::get('property/{property}/view-pdf', 'DealController@viewPdf');
//Rehab
Route::get('rehab/{property}/pdf/download', 'CostCalculatorController@downloadRehabPdf')->name('rehab.download.pdf');
Route::get('rehab/{template}/view-pdf', 'CostCalculatorController@viewRehabPdf');
Route::get('/housing-alert', function() {
    return view('integrations.housing-alert');
});

/**
 * Authenticated routes
 */
Route::group(['middleware' => 'auth'], function () {
    Route::post('upload-image', 'ContactController@uploadImage')->name('image.upload');
    Route::get('google-api-login', 'GcalendarController@googleLogin')->name('google.login');
    Route::get('sync-events', 'GcalendarController@create')->name('sync-gcalendar.create');

    Route::post('/save-tempalte', [
        'uses'=>'ContactController@saveTemplate',
        'as'=>'saveTemplate'
    ]);

    Route::get('/copy-template/{id}', [
        'uses'=>'CopyTemplateController@copyTemplate',
        'as'=>'copyTemplate'
    ]);
    Route::resource('manage-templates', 'TemplateController');

    Route::resource('my-team', 'MyTeamController');

    Route::get('print-genie-login', 'PrintGenieLoginController@index')->name('print-genie-login.index');

    Route::get('crei-ms-login', 'CreimsLoginController@index')->name('crei-ms-login.index');

    Route::get('creims-create-user', 'CreateCreiUsersController@create')->name('creims.create');

    Route::get('search', 'SearchController@index')->name('search');

    // Settings...
    Route::get('settings', 'SettingController@index')->name('settings.index');
    Route::post('settings/{group}', 'SettingController@store')->name('settings.store');

    // Subscription...
    Route::get('settings/billing', 'SettingController@billing')->name('settings.billing');
//	Route::get('settings/card', 'SettingController@getCard')->name('settings.card');
    Route::post('settings/card', 'SettingController@postCard')->name('settings.card');
//	Route::get('settings/subscription', 'SettingController@getSubscription')->name('settings.subscription');
    Route::post('settings/subscription', 'SettingController@postSubscription')->name('settings.subscription');
//	Route::get('settings/payments', 'SettingController@getPayments')->name('settings.payments');
    Route::get('settings/invoice/{invoice}', 'SettingController@downloadInvoice')->name('settings.invoice.download');
//	Route::get('settings/subscription/cancel', 'SettingController@getSubscriptionCancel')->name('settings.subscription.cancel');
    Route::post('settings/subscription/cancel', 'SettingController@postSubscriptionCancel')->name('settings.subscription.cancel');

    /**
     * Common routes
     */
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('onboarding', 'DashboardController@onboarding')->name('dashboard.onboarding');

    // Profile...
    Route::get('profile', 'ProfileController@getProfile')->name('profile');
    Route::post('profile', 'ProfileController@postProfile')->name('profile');
    Route::post('profile/picture', 'ProfileController@profilePicture')->name('profile.picture');
    Route::post('profile/password', 'ProfileController@updatePassword')->name('profile.password');

    // Integration...
    Route::get('integrations', 'IntegrationController@index')->name('integrations');
    // Activities...
    Route::get('activities', 'ActivityController@index')->name('activities');

    // DBM Integration
    Route::get('login/dbm', 'DBMController@index')->name('login.dbm');

    // user, super-user, admin routes...
    Route::group(['middleware' => ['role:user|super-user|admin|bird-dog-deal-finder']], function () {

        // Deals...
        Route::get('deals/upload', 'DealController@getUpload')->name('deals.upload');
        Route::post('deals/upload', 'DealController@postUpload')->name('deals.upload');
        Route::get('deals/watchlist', 'DealController@watchlist')->name('deals.watchlist');

        Route::get('deals', 'DealController@index')->name('deals.index');
        Route::get('deals/export', 'DealController@export')->name('deals.export');
        Route::get('deals/pending-properties', 'DealController@pendingProperties')->name('deals.pending-properties');
        Route::get('deals/create', 'DealController@create')->name('deals.create');
        Route::post('deals', 'DealController@store')->name('deals.store');
        Route::get('deals/{property}/edit', 'DealController@edit')->name('deals.edit');
        Route::get('deals/{property}', 'DealController@show')->name('deals.show');
        Route::put('deals/{property}', 'DealController@update')->name('deals.update');
        Route::get('deals/{property}/pdf', 'DealController@showPdf')->name('deals.show.pdf');
        Route::post('deals/{property}/email', 'DealController@sendEmail')->name('deals.email');
        Route::post('deals/{property}/transfer', 'DealController@transferDeal')->name('deals.transfer');
        Route::post('deals/{property}/user-transfer', 'DealController@transferUserDeal')->name('deals.user-transfer');
        Route::get('deals/{deal}/delete', 'DealController@destroy')->name('deals.delete');
        Route::get('user/{user}/deals', 'DealController@userDeals')->name('user.deals');
        Route::get('deals/blacklist/toggle/{property}', 'BlacklistController@dealToggle')->name('deals.blacklist.toggle');
        Route::get('deals/{property}/contract', 'DealController@showContract')->name('deals.show.contract');
        Route::post('deals/{property}/seller-locator-request', 'DealController@sendSellerLocatorRequest')->name('deals.seller-locator-request');
        Route::get('deals/{property}/proof-of-funds-request', 'DealController@sendProofOfFundsRequest')->name('deals.proof-of-funds-request');
        Route::get('deals/{property}/deal-funding-request', 'DealController@sendDealFundingRequest')->name('deals.deal-funding-request');
        Route::get('deals/{property}/contract/download', 'DealController@downloadContract')->name('deals.download.contract');

        // Contacts...
        Route::get('contacts/upload', 'ContactController@getUpload')->name('contacts.upload');
        Route::post('contacts/upload', 'ContactController@postUpload')->name('contacts.upload');
        Route::post('categories/file-upload', 'ContactController@fileUpload')->name('contacts.upload-ajax');
        Route::post('contacts/transfer', 'ContactController@transferContact')->name('contacts.transfer');
        Route::resource('contacts', 'ContactController', ['parameters' => 'singular']);
        Route::get('contacts/{contact}/delete', 'ContactController@destroy')->name('contacts.delete');
        Route::post('contacts/email', 'ContactController@sendEmail')->name('contacts.email');
        Route::post('contacts/message', 'ContactController@sendMessage')->name('contacts.message');
        Route::delete('contactsDeleteAll', 'ContactController@deleteAll')->name('delete-all-contacts');

        //Categories
        Route::get('categories/{category}/delete', 'ContactCategoryController@destroy')->name('categories.delete');
        Route::resource('categories', 'ContactCategoryController');


        // Contact groups...
        Route::resource('contact-groups', 'ContactGroupController', ['parameters' => 'singular']);
        Route::get('contact-groups/{contactGroup}/delete', 'ContactGroupController@destroy')->name('contact-groups.delete');
        Route::get('contact-groups/{contactGroup}/edit-members', 'ContactGroupController@editMembers')->name('contact-groups.edit-members');
        Route::put('contact-groups/{contactGroup}/update-members', 'ContactGroupController@updateMembers')->name('contact-groups.update-members');

        // Documents...
        Route::get('documents', 'DocumentController@index')->name('documents.index');
        Route::get('documents/create', 'DocumentController@create')->name('documents.create');
        Route::post('documents', 'DocumentController@store')->name('documents.store');
        Route::get('documents/{document}/edit', 'DocumentController@edit')->name('documents.edit');
        Route::put('documents/{document}', 'DocumentController@update')->name('documents.update');
        Route::get('documents/{document}/delete', 'DocumentController@destroy')->name('documents.delete');

        // Settings...
        Route::get('settings/emails', 'SettingController@getEmails')->name('settings.emails');
        Route::get('settings/message/templates', 'SettingController@getMessageTemplates')->name('settings.message-templates');

        // Calendar...
        Route::get('calendar', 'CalendarController@index')->name('calendar.index');
        Route::apiResource('events', 'EventController');

        // Resources...
        Route::get('resources/training', 'ResourceController@getTraining')->name('resources.training');
        Route::get('resources/faq', 'ResourceController@getFaq')->name('resources.faq');
        Route::get('resources/support', 'ResourceController@getSupport')->name('resources.support');

        Route::resource('broadcasts', 'BroadcastController');

        //DMP repair cost calculator
        Route::get('repair-cost-calculator/create/{property_id?}', 'CostCalculatorController@create')->name('calculator.template.create');
        Route::get('repair-cost-calculator/{template}/edit', 'CostCalculatorController@edit')->name('calculator.template.edit');
        Route::get('repair-cost-templates/list', 'CostCalculatorController@index')->name('calculator.template.index');
        Route::post('rehab/templates/save', 'CostCalculatorController@saveTemplate');
        Route::get('{templateId}/fetch-template-json', 'CostCalculatorController@fetchTemplateJson');
        Route::get('/my-templates/list', 'CostCalculatorController@myTemplates');

        Route::get('repair-cost-calculator/{template}/delete', 'CostCalculatorController@destroy')->name('calculator.template.delete');

        //Request Deal funding
        Route::post('deals/{property}/request-deal-funding', 'DealFundingController@sendRequestDealFunding')->name('deals.request.funding');

        Route::get('export-rehab-calcuator-data/{template}', 'ExportCalcultorController@export')->name('export-rehab');
        Route::get('rehab-export-view-html/{template}', 'ExportCalcultorController@show');

    });

    /**
     * User routes
     */
    Route::group(['middleware' => ['role:user']], function () {

    });


    /**
     * Super user routes
     */
    Route::group(['middleware' => ['role:super-user|coach']], function () {
//        dd(465);
        Route::post('profile/team', 'ProfileController@postTeam')->name('profile.team');
        Route::get('teams/users', 'ProfileController@teamUsers')->name('team.users');
    });

    Route::get('watch-training-video/{name}', 'VideoController@show')->name('watch-training-video');
    /**
     * Admin routes
     */
    Route::group(['middleware' => ['role:admin|user']], function () {
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::resource('backups', 'DatabaseBackupController', ['only' => ['index', 'show', 'create']]);

        Route::post('users/{user}', 'UserController@updateMapLink')->name('users.update-map-link');
        Route::get('export-users', 'ExportUsersController@export')->name('export-users');
        Route::resource('users', 'UserController', ['parameters' => 'singular']);
        Route::get('users/{user}/restore', 'UserController@restore')->name('users.restore');
        Route::get('users/{user}/delete', 'UserController@destroy')->name('users.delete');
        Route::get('users/status/toggle/{user}', 'UserController@statusToggle')->name('users.status.toggle');
        Route::get('users/bypassPayment/toggle/{user}', 'UserController@bypassPaymentToggle')->name('users.bypassPayment.toggle');
        Route::put('users/{user}/roles', 'UserController@updateRoles')->name('users.roles.update');
        Route::put('users/{user}/deal-finders', 'UserController@updateDealFinders')->name('users.deal-finders.update');
        Route::put('users/{user}/coaches', 'UserController@updateCoaches')->name('users.coaches.update');
        Route::post('users/{user}/team', 'UserController@postTeam')->name('users.team');

        Route::get('teams', 'TeamController@index')->name('teams.index');
        Route::get('teams/{manager}/edit', 'TeamController@edit')->name('teams.edit');
        Route::put('teams/{manager}', 'TeamController@update')->name('teams.update');

        Route::get('trash-users', 'TrashedUserController@trash')->name('users.trash');
        Route::get('trash-users-restore/{id}', 'TrashedUserController@restore')->name('users.trash.restore');

        // Admin pages...
        Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
            Route::get('contacts', 'ContactController@index')->name('admin.contacts.index');
        });

    });
});

Route::post('funnel_webhooks/test', 'Webhook\ClickFunnelsController@test');

Route::group(['prefix' => 'webhooks', 'namespace' => 'Webhook', 'middleware' => 'throttle:5,1'], function() {
    Route::post('clickfunnels/register', 'ClickFunnelsController@store');
    Route::post('slybroadcast', 'SlybroadcastController@store');
    Route::post('infusionsoft', 'InfusionSoftController@store');
});

Route::group(['prefix' => 'api', 'namespace' => 'API'], function() {
    Route::get('properties/comparables', 'PropertyController@getComparables');
    Route::post('properties/save-comparable', 'PropertyController@saveComparable');
    Route::get('properties/image/remove/{image}', 'PropertyController@removeImage');

    // Create user...
    Route::get('createUser', 'UserController@store');

    Route::get('notifications', 'UserNotificationController@index');
    Route::delete('notifications/{id}', 'UserNotificationController@destroy');
    Route::post('notifications/mark-all-read', 'UserNotificationController@markAllRead');

    Route::get('deals/{deal}', 'DealController@show');

    Route::get('deal-addresses/{userId?}', 'DealController@addresses');

    Route::get('broadcasts/{broadcast}/statuses', 'BroadcastStatusController@index');
});


/**
 * Integration routes
 */
Route::group(['prefix' => 'integration', 'middleware' => 'integration-check', 'namespace' => 'Integration'], function () {

    // Contacts...
    Route::get('contacts/create', 'ContactController@create')->name('integration.contacts.create');
    Route::post('contacts', 'ContactController@store')->name('integration.contacts.store');

    // Deals...
    Route::get('deals', 'DealController@index')->name('integration.deals.index');
    Route::get('deals/create', 'DealController@create')->name('integration.deals.create');
    Route::post('deals', 'DealController@store')->name('integration.deals.store');

    // Basic Deal...
    Route::get('basic-deals/create', 'BasicDealController@create')->name('integration.basic-deals.create');
    Route::get('basic-deals/success', 'BasicDealController@success')->name('integration.basic-deals.success');
    Route::post('basic-deals', 'BasicDealController@store')->name('integration.basic-deals.store');

    // Deal Finders...
    Route::get('deal-finders/create', 'DealFinderController@create')->name('integration.deal-finders.create');
    Route::get('deal-finders/success', 'DealFinderController@success')->name('integration.deal-finders.success');
    Route::post('deal-finders', 'DealFinderController@store')->name('integration.deal-finders.store');

    Route::group(['prefix' => 'form'], function() {
        Route::post('deals', 'DealController@postForm');
        Route::post('contacts', 'ContactController@postForm');
        Route::post('deal-finders', 'DealFinderController@postForm');
    });
});

Route::any('{any}', function () {
    return redirect()->back();
})->where('any', '.*');