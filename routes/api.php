<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'API\V1'], function() {
    Route::post('login', 'LoginController@login');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('user', 'UserController@get');

        Route::get('properties', 'PropertyController@index');
        Route::post('properties', 'PropertyController@store');
        Route::post('properties/{property}', 'PropertyController@update');
        Route::delete('properties/{property}/images/{image}', 'PropertyController@removeImage');
    });
});