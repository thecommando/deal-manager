<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactEmailTemplate extends Model
{
	protected $fillable = [
		'user_id',
		'name',
		'subject',
		'message',
		'is_global'
	];
}
