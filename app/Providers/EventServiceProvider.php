<?php

namespace App\Providers;

use App\Events\DealFinderWasCreated;
use App\Listeners\HandleFailedNotification;
use App\Listeners\SendPasswordToDealFinderEmail;
use Illuminate\Notifications\Events\NotificationFailed;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserWasCreated' => [
//            'App\Listeners\SendPasswordEmail',
        ],
        DealFinderWasCreated::class => [
            SendPasswordToDealFinderEmail::class,
        ],
        NotificationFailed::class => [
            HandleFailedNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
