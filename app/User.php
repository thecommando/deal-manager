<?php

namespace App;

use Image;
use Carbon\Carbon;
use Plank\Metable\Metable;
use App\Traits\HasSettings;
use Laravel\Cashier\Billable;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Billable, Metable, HasSettings;
    use EntrustUserTrait { restore as private restoreA; }
    use SoftDeletes { restore as private restoreB; }

    public $profileDir = '/uploads/profile';

    protected $placeholderImage = '/assets/images/user-placeholder.png';

    protected $fillable = [
        'name', 'email', 'username', 'password', 'address', 'city', 'state', 'country', 'postal_code',
        'mobile_phone', 'office_phone', 'photo', 'is_active', 'last_login', 'api_key', 'bypass_payment',
        'website', 'api_token', 'google_map_link', 'is_crei_created', 'crei_uuid', 'g_token', 'g_client_id', 'g_secret'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'last_login',
    ];

    protected $appends = [
        'first_name', 'last_name',
    ];

    /**************************************************************************************
     *                                  Relationships
     *************************************************************************************/

    public function manager()
    {
        return $this->belongsTo(User::class);
    }

    public function team()
    {
        return $this->hasOne(Team::class, 'owner_id');
    }

    public function sub_users()
    {
        return $this->hasMany(User::class, 'manager_id');
    }

    public function deal_finders()
    {
        return $this->belongsToMany(User::class, 'deal_finders', 'user_id', 'deal_finder_id');
    }

    public function coaches()
    {
        return $this->belongsToMany(User::class, 'coach_users', 'user_id', 'coach_id');
    }

    public function coach_users()
    {
        return $this->belongsToMany(User::class, 'coach_users', 'coach_id', 'user_id');
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function transferred_properties()
    {
        return $this->hasMany(Property::class, 'transferred_by');
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function templates()
    {
        return $this->hasMany(ContactEmailTemplate::class);
    }

    public function contact_groups()
    {
        return $this->hasMany(ContactGroup::class);
    }

    public function documents()
    {
        return $this->belongsToMany(Document::class);
    }

    /**************************************************************************************
     *                                  Query Scopes
     *************************************************************************************/

    public function scopeActive($query)
    {
        return $query->whereIsActive(true);
    }

    public function scopeInActive($query)
    {
        return $query->whereIsActive(false);
    }

    public function scopeHavingRole($query, $role)
    {
        $role = (array) $role;

        return $query->whereHas('roles', function ($q) use ($role) {
            $q->whereIn('name', $role);
        });
    }

    public function scopeUnallocated($query)
    {
        return $query->whereNull('manager_id');
    }


    /**************************************************************************************
     *                                  Helpers
     *************************************************************************************/

    /**
     * Get users for manager team
     *
     * @return mixed
     */
    public function teamUsers()
    {
        return static::havingRole('user')
            ->where('id', '<>', $this->id)
            ->where(function ($query) {
                $query->unallocated()->OrWhere('manager_id', $this->id);
            })
            ->get();
    }

    /**
     * Check if user is active or not
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active;
    }

    /**
     * Sync roles of user
     *
     * @param array $roles
     */
    public function rolesSync(array $roles)
    {
        $this->roles()->sync($roles);
    }

    /**
     * Sync deal finders for user
     *
     * @param array $finders
     */
    public function dealFindersSync(array $finders)
    {
        $this->deal_finders()->sync($finders);
    }

    /**
     * Sync coaches for user
     *
     * @param array $coaches
     */
    public function coachesSync(array $coaches)
    {
        $this->coaches()->sync($coaches);
    }

    /**
     * Set manager for current user
     *
     * @param User $manager
     * @return mixed
     */
    public function setManager(User $manager)
    {
        $this->manager_id = $manager->id;

        return $this->save();
    }

    /**
     * User status toggle
     *
     * @return mixed
     */
    public function statusToggle()
    {
        $this->is_active = !$this->is_active;

        return $this->save();
    }

    /**
     * Upload profile picture
     *
     * @param UploadedFile $image
     * @return mixed
     */
    public function uploadProfilePicture(UploadedFile $image)
    {
        $filename = sprintf("%s-%s", generateUUID(), $image->getClientOriginalName());

        $path = sprintf("%s/%s", $this->profileDir, $filename);

        Image::make($image->getRealPath())->resize(200, 200)->save(public_path($path));

        $this->photo = $path;

        return $this->save();
    }

    /**
     * Update team information
     *
     * @param $teamName
     * @return Team
     */
    public function updateTeam($teamName)
    {
        $team = $this->team()->firstOrCreate(['owner_id' => $this->id]);

        $team->name = $teamName;

        $team->save();

        return $team;
    }

    /**
     * Get photo url
     *
     * @return string
     */
    public function photoURL()
    {
        return $this->photo ? url($this->photo) : url($this->placeholderImage);
    }

    /**
     * Get team name
     *
     * @return string
     */
    public function teamName()
    {
        return $this->team ? $this->team->name : $this->name;
    }

    /**
     * Determine if given user is member of the team manager
     *
     * @param User $user
     * @return bool
     */
    public function hasMember(User $user)
    {
        return $this->id == $user->manager_id;
    }

    /**
     * Update team members for manager
     *
     * @param array $members
     */
    public function updateTeamMembers(array $members)
    {
        static::where('manager_id', $this->id)->update(['manager_id' => null]);

        static::whereIn('id', $members)->update(['manager_id' => $this->id]);
    }

    public function updateTeamIfSuperUser()
    {
        if ($this->hasRole('super-user') && !$this->team()->exists()) {
            $this->team()->create(['name' => $this->name]);
        } else {
            $this->team()->delete();
        }
    }

    public function openDeals()
    {
//        return $this->properties()->open()->count();
        return $this->properties->where('stage', '<>', 8)->count();
    }

    public function closedDeals()
    {
//        return $this->properties()->closed()->count();
        return $this->properties->where('stage', 8)->count();
    }

    public function teamOpenDeals()
    {
        return $this->sub_users->sum(function ($user) {
            return $user->openDeals();
        });
    }

    public function canBypassPayment()
    {
        return $this->bypass_payment;
    }

    public function bypassPayment()
    {
        $this->bypass_payment = !$this->bypass_payment;

        return $this->save();
    }

    public function setLastLoginTime()
    {
        $this->last_login = Carbon::now();

        $this->save();
    }

    public function getFirstNameAttribute()
    {
        return explode(' ', $this->name)[0];
    }

    public function getLastNameAttribute()
    {
        $array = explode(' ', $this->name);

        return count($array) > 1 ? $array[1] : '';
    }

    public static function guessUsername($name, $count = 0)
    {
        $userName = $count ? str_slug($name, '') . $count : str_slug($name, '');

        if(self::where('username', $userName)->exists()) {
            return self::guessUsername($userName, ++$count);
        }

        return $userName;
    }

    public function routeNotificationForTwilio()
    {
        return $this->mobile_phone;
    }

    /**
     * Restore method collision fixed for entrust and softdelete traits.
     */
    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }
}
