<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComparableProperty extends Model
{
	protected $fillable = [
		'property_id', 'address', 'br', 'ba', 'sqft', 'sold_price',
	];

	public function property()
	{
		return $this->belongsTo(Property::class);
	}
}
