<?php

namespace App;

use Image as InterventionImage;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'name',
        'path',
        'thumbnail_path',
        'icon_path',
        'title',
        'description',
        'is_default',
    ];

    protected $hidden = [
        'imageable_type',
        'imageable_id',
    ];

    protected $casts = [
        'is_default' => 'boolean',
    ];

    protected $baseDir = 'uploads';

    protected $thumbSize = 300;

    protected $iconSize = 100;

    public function imageable()
    {
        return $this->morphTo();
    }

    /**
     * Is default image
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeDefault($query)
    {
        return $query->whereIsDefault(true);
    }

    /**
     * Make new image object
     *
     * @param $name
     *
     * @return mixed
     */
    public static function named($name)
    {
        return (new static)->saveAs($name);
    }

    /**
     * Save file attributes
     *
     * @param $name
     *
     * @return $this
     */
    protected function saveAs($name)
    {
        $this->name = sprintf("%s-%s", generateUUID(), $name);
        $this->path = sprintf("%s/%s", $this->baseDir, $this->name);
        $this->thumbnail_path = sprintf("%s/th-%s", $this->baseDir, $this->name);
        $this->icon_path = sprintf("%s/ic-%s", $this->baseDir, $this->name);

        return $this;
    }

    /**
     * Move file to directory
     *
     * @param UploadedFile $file
     * @return $this
     */
    public function move(UploadedFile $file)
    {
        $file->move($this->baseDir, $this->name);

        $this->makeThumbnail();

        $this->makeIcon();

        return $this;
    }

    /**
     * Make image thumbnail
     */
    private function makeThumbnail()
    {
        InterventionImage::make($this->path)
            ->orientate()
            ->fit($this->thumbSize)
            ->save($this->thumbnail_path);
    }

    /**
     * Make image icon
     */
    private function makeIcon()
    {
        InterventionImage::make($this->path)
            ->orientate()
            ->fit($this->iconSize)
            ->save($this->icon_path);
    }

    /**
     * Remove images
     */
    public function removeImages()
    {
        unlink(public_path($this->path));
        unlink(public_path($this->thumbnail_path));
        unlink(public_path($this->icon_path));
    }

    public function getThumbnailPath()
    {
        return url($this->thumbnail_path);
    }

    public function getIconPath()
    {
        return url($this->icon_path);
    }

    public function getPath()
    {
        return url($this->path);
    }
}
