<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];

    protected $dates = [
        'date', 'remind_at',
    ];

    protected $casts = [
        'is_global' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function scopeToday($query)
    {
        return $query->where('date', today());
    }

    public function scopeForUser($query, User $user = null)
    {
        if (!$user) {
            $user = auth()->user();
        }

        return $query->where(function ($q) use ($user) {
            $q->where('user_id', $user->id)
                ->orWhere('is_global', true);
        });
    }

    public function getLinkAttribute($value)
    {
        if ($this->property_id) {
            return route('deals.show', $this->property_id);
        }

        return $value;
    }
}
