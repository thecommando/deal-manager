<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BroadcastStatus extends Model
{
    protected $fillable = [
        'session_id', 'phone', 'status', 'failure_reason', 'delivery_time', 'carrier',
    ];

    public static function saveStatus($statusString)
    {
//        $statusString = '"2324444455"|"6173999980"|"OK"|""|"2018-01-10 10:00:00"|"landline"';

        $statusString = str_replace('"', '', $statusString);

        list($session_id, $phone, $status, $failure_reason, $delivery_time, $carrier) = explode("|", $statusString, 6);

        return self::create(compact('session_id', 'phone', 'status', 'failure_reason', 'delivery_time', 'carrier'));
    }
}
