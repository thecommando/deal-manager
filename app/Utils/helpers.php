<?php

/**
 * Returns UUID of 32 characters
 *
 * @return string
 */
function generateUUID()
{
    $currentTime = (string)microtime(true);

    $randNumber = (string)rand(10000, 1000000);

    $shuffledString = str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");

    return md5($currentTime . $randNumber . $shuffledString);
}

/**
 * Alert Helper
 *
 * @param null $title
 * @param null $text
 * @return \Illuminate\Foundation\Application|mixed
 */
function alert($title = null, $text = null)
{
    $flash = app('App\Classes\Flash');

    if (func_num_args() == 0) {
        return $flash;
    }

    return $flash->info($title, $text);
}

/**
 * Delete form link
 *
 * @param $body
 * @param $path
 *
 * @return string
 */
function delete_form($body, $path)
{
    $csrf = csrf_field();

    if (is_object($path)) {
        $action = '/' . $path->getTable() . '/' . $path->getKey();
    } else {
        $action = $path;
    }

    return <<<EOT
        <form method="POST" action="{$action}">
            <input type="hidden" name="_method" value='DELETE'>
            $csrf
            <button type="submit" class="btn btn-link">{$body}</button>
        </form>
EOT;
}

function property_types()
{
    return \App\PropertyType::pluck('name', 'id')->toArray();
}

function property_signals()
{
    return \App\PropertySignal::pluck('name', 'id')->toArray();
}

function property_stages()
{
    return \App\PropertyStage::pluck('name', 'id')->toArray();
}

function property_statuses()
{
    return \App\PropertyStatus::orderBy('name')->pluck('name', 'id')->toArray();
}

function contract_periods()
{
    return [
        '15' => '15 Days',
        '30' => '30 Days',
        '45' => '45 Days',
        '60' => '60 Days',
        '90' => '90 Days',
    ];
}

function property_conditions()
{
    return \App\PropertyCondition::pluck('name', 'id')->toArray();
}

function deal_types()
{
    return \App\DealType::orderBy('name')->pluck('name', 'id')->toArray();
}

function bedrooms()
{
    return \App\Bedroom::pluck('name', 'id')->toArray();
}

function bathrooms()
{
    return \App\Bathroom::pluck('name', 'id')->toArray();
}

function year_built()
{
    $result = [];

    for ($i = 1920; $i <= \Carbon\Carbon::today()->year; $i++) {
        $result[$i] = $i;
    }

    return $result;
}

function country_options()
{
    return ['' => '-- Select --'] + \App\Country::active()->orderBy('name')->pluck('name', 'name')->toArray();
}

function get_contacts()
{
    $contacts = auth()->user()->contacts;

    $result = [];

    foreach ($contacts as $contact) {
        $result[$contact->id] = $contact->full_name;
    }

    return $result;
}

function formatPrice($price)
{
    return "$" . number_format(sprintf('%01.2f', $price));
    //return numeral($price).format('$0,0.00');


    //setlocale(LC_MONETARY, 'en_US');
    //return money_format('%(10.0n', $price) . "\n";

    //<script type="text/javascript">

    //accounting.formatMoney($price);

    //</stript>

}

function formatDecimal($price)
{
    setlocale(LC_MONETARY, 'en_US');
    return money_format('%(10.2n', $price) . "\n";
}


/**
 * Get short name of class
 *
 * @param $class
 * @return string
 */
function get_class_short_name($class)
{
    return (new ReflectionClass($class))->getShortName();
}

/**
 * Get link
 *
 * @param $url
 * @param $placeholder
 * @return string
 */
function get_link($url, $placeholder)
{
    return sprintf("<a href='%s' class='name'>%s</a>", url($url), $placeholder);
}

function checkAddendum($key, $values)
{
    return in_array($key, $values) ? '<i class="fa fa-check custom-input"></i>' : '____';
}

function checkContingencyType($key, $value)
{
    return $key == $value ? '<i class="fa fa-check custom-input"></i>' : '____';
}

function getMapAddresses()
{
    $user = auth()->user();

    if ($user->hasRole('admin') || ($user->hasRole('user') && $user->manager_id > 0)) {
        return \App\Property::
        where('user_id', '!=', 0)
            ->with('user')
            ->presentation()->get();
    }

    return \App\Property::where('user_id', $user->id)->with('user')->presentation()->get();
}

function dollars($amount)
{
    return '$' . number_format($amount, 2);
}

function today()
{
    return \Carbon\Carbon::today();
}

function now()
{
    return \Carbon\Carbon::now();
}

function getUnit($unit)
{
    if ($unit == 1) {
        return 'unit';
    }
    if ($unit == 2) {
        return 'linear feet';
    }
    if ($unit == 3) {
        return 'blind';
    }
    if ($unit == 4) {
        return 'per foot';
    }
    if ($unit == 5) {
        return 'knob';
    }
    if ($unit == 6) {
        return 'handle';
    }
    if ($unit == 7) {
        return 'square feet';
    }
    if ($unit == 8) {
        return 'panel';
    }
    if ($unit == 9) {
        return 'door';
    }
    if ($unit == 10) {
        return 'panel';
    }
    if ($unit == 11) {
        return 'window';
    }

    return '';
}

function numberToWords($amount)
{
    $ones = array(
        1 => "one",
        2 => "two",
        3 => "three",
        4 => "four",
        5 => "five",
        6 => "six",
        7 => "seven",
        8 => "eight",
        9 => "nine",
        10 => "ten",
        11 => "eleven",
        12 => "twelve",
        13 => "thirteen",
        14 => "fourteen",
        15 => "fifteen",
        16 => "sixteen",
        17 => "seventeen",
        18 => "eighteen",
        19 => "nineteen"
    );

    $tens = array(
        1 => "ten",
        2 => "twenty",
        3 => "thirty",
        4 => "forty",
        5 => "fifty",
        6 => "sixty",
        7 => "seventy",
        8 => "eighty",
        9 => "ninety"
    );

    $hundreds = array(
        "hundred",
        "thousand",
        "million",
        "billion",
        "trillion",
        "quadrillion"
    );

    $amount = number_format($amount, 2, ".", ",");
    $num_arr = explode(".", $amount);
    $wholenum = $num_arr[0];
    $decnum = $num_arr[1];
    $whole_arr = array_reverse(explode(",", $wholenum));
    krsort($whole_arr);
    $words = "";

    foreach ($whole_arr as $key => $i) {
        if ($i == 0) {
            continue;
        }
        if ($i < 20) {
            $words .= $ones[intval($i)];
        } elseif ($i < 100) {
            if (substr($i, 0, 1) == 0 && strlen($i) == 3) {
                $words .= $tens[substr($i, 1, 1)];
                if (substr($i, 2, 1) != 0) {
                    $words .= " " . $ones[substr($i, 2, 1)];
                }
            } else {
                $words .= $tens[substr($i, 0, 1)];
                if (substr($i, 1, 1) != 0) {
                    $words .= " " . $ones[substr($i, 1, 1)];
                }
            }
        } else {
            // $words .= $ones[substr($i,0,1)]." ".$hundreds[0].' and ';
            if (substr($i, 1, 1) != 0 || substr($i, 2, 1) != 0) {
                $words .= $ones[substr($i, 0, 1)] . " " . $hundreds[0] . ' and ';
            } else {
                $words .= $ones[substr($i, 0, 1)] . " " . $hundreds[0];
            }
            if (substr($i, 1, 2) < 20 && substr($i, 1, 1) != 0) {
                $words .= " " . $ones[(substr($i, 1, 2))];
            } else {
                if (substr($i, 1, 1) != 0) {
                    $words .= " " . $tens[substr($i, 1, 1)];
                }
                if (substr($i, 2, 1) != 0) {
                    $words .= " " . $ones[substr($i, 2, 1)];
                }
            }
        }
        if ($key > 0) {
            $words .= " " . $hundreds[$key] . " ";
        }
    }

    $words .= $unit ?? ' dollars';

    if ($decnum > 0) {
        $words .= " and ";
        if ($decnum < 20) {
            $words .= $ones[intval($decnum)];
        } elseif ($decnum < 100) {
            $words .= $tens[substr($decnum, 0, 1)];
            if (substr($decnum, 1, 1) != 0) {
                $words .= " " . $ones[substr($decnum, 1, 1)];
            }
        }
        $words .= $subunit ?? ' cents';
    }

    return $words;
}