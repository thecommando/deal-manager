<?php

namespace App\Console;

use App\Console\Commands\InfusionsoftRefreshToken;
use App\Console\Commands\RemindEvents;
use App\Console\Commands\TestEmail;
use App\Console\Commands\RemoveOnHoldProperties;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        RemoveOnHoldProperties::class,
        InfusionsoftRefreshToken::class,
        RemindEvents::class,
        TestEmail::class,
    ];

    protected $timezone = 'America/Phoenix';

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('backup:clean')->daily()->at('01:00');
        $schedule->command('backup:run --only-db')->daily()->at('02:00');

        $schedule->command('infusionsoft:refresh-token')->daily()->at('03:00');
        $schedule->command('infusionsoft:refresh-token')->daily()->at('15:00');

        $schedule->command('remind:events')->timezone($this->timezone)->daily()->at('09:00');

//        $schedule->command('remove:on-hold-properties')->dailyAt('02:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
