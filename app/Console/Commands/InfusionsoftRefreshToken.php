<?php

namespace App\Console\Commands;

use App\Services\Infusionsoft\Infusionsoft;
use Illuminate\Console\Command;

class InfusionsoftRefreshToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'infusionsoft:refresh-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch new refresh token for infusionsoft APIs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new Infusionsoft)->refreshAccessToken();
        //var_dump((new Infusionsoft)->requestAccessToken('drYEztLG'));
    }
}
