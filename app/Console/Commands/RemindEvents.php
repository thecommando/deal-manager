<?php

namespace App\Console\Commands;

use App\Event;
use App\Notifications\EventReminder;
use Illuminate\Console\Command;

class RemindEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remind:events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind events';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = Event::whereBetween('remind_at', [today(), today()->endOfDay()])
            ->with('user')
            ->get();

        foreach ($events as $event) {
            $event->user->notify(new EventReminder($event));
        }
    }
}
