<?php

namespace App\Console\Commands;

use App\Property;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveOnHoldProperties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:on-hold-properties';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove on hold properties after 60 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Property::pending()->whereDate('created_at', '<', Carbon::today()->subDays(60))->delete();
    }
}
