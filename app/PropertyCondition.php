<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyCondition extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
