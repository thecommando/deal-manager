<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Broadcast extends Model
{
    protected $fillable = [
        'user_id', 'phone', 'file', 'type', 'campaign_name', 'status', 'session_id',
    ];

    protected $appends = [
        'file_url',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function statuses()
    {
        return $this->hasMany(BroadcastStatus::class, 'session_id', 'session_id');
    }

    public function getFileUrlAttribute()
    {
        return Storage::url($this->file);
    }

    public function updateSessionId()
    {
        $status = $this->status;

        if (str_contains($status, 'session_id')) {
            $sessionId = substr(str_after($status, 'session_id='), 0, 10);

            $this->update(['session_id' => $sessionId]);
        }
    }
}
