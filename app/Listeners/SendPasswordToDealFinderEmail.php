<?php

namespace App\Listeners;

use Mail;
use App\Mail\SendPassword;
use App\Events\DealFinderWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPasswordToDealFinderEmail
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DealFinderWasCreated  $event
     * @return void
     */
    public function handle(DealFinderWasCreated $event)
    {
        $user = $event->user;
        $password = $event->password;

        Mail::to($user)->send(new SendPassword($user, $password));
    }
}
