<?php

namespace App\Listeners;

use Exception;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Events\NotificationFailed;

class HandleFailedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NotificationFailed $event
     * @return void
     */
    public function handle(NotificationFailed $event)
    {
        logger()->error(get_class($event->notification) . ' failed', $event->data);

        throw new Exception(array_get($event->data, 'message'));
    }
}
