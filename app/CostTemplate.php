<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostTemplate extends Model
{
	protected $fillable = [
		'user_id',
		// 'property_id',
		'items',
		'total_cost',
		'template_name',
		'path'
	];

	public function getTotalCostAttribute($value)
    {
        return number_format($value);
    }
}
