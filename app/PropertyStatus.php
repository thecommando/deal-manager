<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyStatus extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'name',
	];
}
