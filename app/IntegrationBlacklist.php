<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegrationBlacklist extends Model
{
	public function subject()
	{
		return $this->morphTo();
    }
}
