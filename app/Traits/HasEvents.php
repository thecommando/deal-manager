<?php

namespace App\Traits;

use App\Event;
use App\Services\Calendar\EventManager;

trait HasEvents
{
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function syncPropertyEvents()
    {
        switch ($this->stage) {
            case 2: // offer submitted
                EventManager::make('Offer submitted', today())->property($this)->create();
                break;

            case 3: // under contract
                if($this->contract_period) {
                    EventManager::make('Under contract', $this->contract_period)
                        ->remindAt($this->contract_period->copy()->subDay()->setTimeFromTimeString('09:00'))
                        ->color('darkred')
                        ->property($this)
                        ->create();
                }
                break;

            case 8: // closed
                $this->events()->update(['color' => 'darkgreen']);
                break;

            case 9: // no deal
                $this->events()->update(['color' => 'red']);
                break;
        }
    }
}