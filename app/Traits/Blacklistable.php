<?php

namespace App\Traits;

use App\IntegrationBlacklist;

trait Blacklistable
{
	public function blacklist()
	{
		return $this->morphOne(IntegrationBlacklist::class, 'subject');
	}

	public function scopeWhitelisted($query)
	{
		return $query->doesntHave('blacklist');
	}

	public function isBlackListed()
	{
		return $this->blacklist()->exists();
	}

	public function toggleBlacklist()
	{
		if (!$this->isBlackListed()) {
			$this->addBlacklist();
		} else {
			$this->removeBlacklist();
		}
	}

	public function addBlacklist()
	{
		return $this->blacklist()->create([]);
	}

	public function removeBlacklist()
	{
		return $this->blacklist()->delete();
	}
}