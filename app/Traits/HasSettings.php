<?php

namespace App\Traits;

use App\UserSetting;
use Config;

trait HasSettings
{
    public function settings()
    {
        return $this->hasMany(UserSetting::class);
    }

    public function getSetting($group, $key, $default = null)
    {
        $setting = $this->settings()->where('group', $group)->where('key', $key)->first();

        return $setting ? $setting->value : $default;
    }

    public function setTwilioCredentials()
    {
        Config::set('services.twilio.account_sid', $this->getSetting('twilio', 'account_sid', 'test'));
        Config::set('services.twilio.auth_token', $this->getSetting('twilio', 'auth_token', 'test'));
        Config::set('services.twilio.from', $this->getSetting('twilio', 'from', 'test'));
    }

    public function setSendInBlueCredentials()
    {
        Config::set('mail.host', 'smtp-relay.sendinblue.com');
        Config::set('mail.from.address', $this->getSetting('sendinblue', 'username'));
        Config::set('mail.from.name', $this->name);
        Config::set('mail.username', $this->getSetting('sendinblue', 'username'));
        Config::set('mail.password', $this->getSetting('sendinblue', 'password'));
    }

    public function hasTwilioCredentials()
    {
        $sid = Config::get('services.twilio.account_sid');

        return !empty($sid) && $sid != 'test';
    }

    public function hasSendInBlueCredentials()
    {
        $username = Config::get('mail.username');

        return !empty($username);
    }
}