<?php

namespace App\Traits;

use App\Note;

trait HasNotes
{
    public function notes()
    {
        return $this->morphMany(Note::class, 'subject');
    }

    public function saveNotes($notes)
    {
        $this->notes()->delete();

        foreach (array_filter($notes) as $note) {
            $this->notes()->create([
                'text' => $note,
            ]);
        }
    }
}