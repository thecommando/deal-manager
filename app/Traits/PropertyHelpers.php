<?php

namespace App\Traits;

trait PropertyHelpers
{
    /*public function getAfterRepairValueAttribute()
    {
        if ($this->sqft) {
            return $this->sqft * $this->comparable_price_per_sqft;
        }

        return 0;
    }*/

    public function getComparablePricePerSqftAttribute()
    {
        $totalSqftPrice = 0;
        $totalComparables = 0;

        foreach ($this->comparable_properties as $comparable_property) {
            if ($comparable_property->sold_price && $comparable_property->sqft) {
                $totalSqftPrice += $comparable_property->sold_price / $comparable_property->sqft;
                $totalComparables++;
            }
        }

        if(!$totalComparables) {
            return 0;
        }
        
        return $totalSqftPrice / $totalComparables;
    }

    public function getAnnualGrossIncomeAttribute()
    {
        if ($this->actual_monthly_rent) {
            return $this->actual_monthly_rent * 12;
        }

        return ($this->fair_market_rent ?: 0) * 12;
    }

    public function getAnnualExpensesAttribute()
    {
        $value = 0;

        $value += $this->annual_taxes ?: 0;
        $value += $this->annual_insurance ?: 0;

        return $value;
    }

    public function getAnnualNetOperatingIncomeAttribute()
    {
        return $this->annual_gross_income - $this->annual_expenses;
    }

    public function getSuggestedMaximumOfferAttribute()
    {
        if ($this->after_repair_value) {
            return ($this->after_repair_value * ($this->offer_scale_percentage / 100)) - $this->estimated_rehab;
        }

        return 0;
    }

    public function getWholesaleValueAttribute()
    {
        return ($this->offer_price ?: 0) + ($this->wholesale_fee ?: 0);
    }
}