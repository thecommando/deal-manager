<?php

namespace App\Services\API;

use Zillow\ZillowClient;

class Zillow
{
    protected $client;

    protected $address;

    protected $citystatezip;

    protected $compCount;

    public function __construct($address, $citystatezip, $compCount = 5)
    {
        $this->client = new ZillowClient(config('services.zillow.key'));

        $this->address = $address;
        $this->citystatezip = $citystatezip;
        $this->compCount = $compCount;
    }

    public function getComparables()
    {
        $property = $this->searchProperty();

        if (!$property) {
            return [];
        }

        $response = $this->client->GetDeepComps([
            'zpid' => $property['zpid'],
            'count' => $this->compCount
        ]);

        if (!array_get($response, 'properties.comparables.comp')) {
            return [];
        }

        return array_get($response, 'properties.comparables.comp');
    }

    public function searchProperty()
    {
        $response = $this->client->GetSearchResults([
            'address' => $this->address,
            'citystatezip' => $this->citystatezip,
        ]);

        if (!array_get($response, 'results.result')) {
            return false;
        }

        return array_get($response, 'results.result');
    }
}