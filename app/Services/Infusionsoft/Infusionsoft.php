<?php

namespace App\Services\Infusionsoft;

use Cache;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;

class Infusionsoft
{
    /**
     * @var string URL all XML-RPC requests are sent to
     */
    protected $url = 'https://api.infusionsoft.com/crm/xmlrpc/v1';

    /**
     * @var string URL a user visits to authorize an access token
     */
    protected $auth = 'https://signin.infusionsoft.com/app/oauth/authorize';

    /**
     * @var string Base URL of all API requests
     */
    protected $baseUri = 'https://api.infusionsoft.com/crm';

    /**
     * @var string URL used to request an access token
     */
    protected $tokenUri = 'https://api.infusionsoft.com/token';

    /**
     * @var string URL used to request an contacts
     */
    protected $contactsUri = 'https://api.infusionsoft.com/crm/rest/v1/contacts';

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $clientSecret;

    /**
     * @var string
     */
    protected $redirectUri;

    /**
     * @var array Cache for services so they aren't created multiple times
     */
    protected $apis = [];

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var Token
     */
    protected $token;

    public function __construct()
    {
        $this->clientId = config('services.infusionsoft.client_id');
        $this->clientSecret = config('services.infusionsoft.client_secret');
        $this->redirectUri = config('services.infusionsoft.redirect_uri');

        $this->setToken(new Token([
            'access_token' => Cache::store('file')->get('infusionsoft.access_token'),
            'refresh_token' => Cache::store('file')->get('infusionsoft.refresh_token'),
            'expires_in' => Cache::store('file')->get('infusionsoft.expires_in'),
        ]));

        $this->httpClient = new Client();
    }

    /**
     * @param string $code
     *
     * @return array
     * @throws Exception
     */
    public function requestAccessToken($code)
    {
        $params = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'code' => $code,
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->redirectUri,
        ];

        $tokenInfo = $this->httpClient->post($this->tokenUri, [
            'body' => http_build_query($params),
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
        ]);

        $data = json_decode($tokenInfo->getBody(), true);
        $data['expires_in'] += time();

        $this->setToken(new Token($data));

        return $this->getToken();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function refreshAccessToken()
    {
        $headers = [
            'Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret),
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];

        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->getToken()->getRefreshToken(),
        ];

        $tokenInfo = $this->httpClient->post($this->tokenUri,
            ['body' => http_build_query($params), 'headers' => $headers]);

        $data = json_decode($tokenInfo->getBody(), true);
        $data['expires_in'] += time();

        $this->setToken(new Token($data));

        return $this->getToken();
    }

    public function getContactByEmail($email)
    {
        $response = $this->httpClient->get($this->contactsUri, [
            'query' => [
                'email' => $email,
                'access_token' => $this->getToken()->accessToken,
            ],
        ]);
        info($response->getBody());
        return array_get(
            json_decode($response->getBody(), true), 'contacts.0'
        );
    }

    public function getContactTags($contactId)
    {
        $uri = sprintf("%s/%s/tags", $this->contactsUri, $contactId);

        $response = $this->httpClient->get($uri, [
            'query' => [
                'access_token' => $this->getToken()->accessToken,
            ],
        ]);

        info($response->getBody());

        return array_get(
            json_decode($response->getBody(), true), 'tags', []
        );
    }

    public function getContactTagsByEmail($email)
    {
        $contact = $this->getContactByEmail($email);

        if (!$contact) {
            return [];
        }

        return $this->getContactTags(array_get($contact, 'id'));
    }

    public function contactHasTag($email, $tag)
    {
        return collect($this->getContactTagsByEmail($email))
            ->pluck('tag.name')
            ->contains($tag);
    }

    public function updateContact($contactId, $values)
    {
        $response = $this->httpClient->patch("https://api.infusionsoft.com/crm/rest/v2/contacts/$contactId", [
            'query' => [
                'access_token' => $this->getToken()->accessToken,
            ],
            'json' => $values,
        ]);

        info('---------updateContact----------------');
        info(json_encode($values));
        info($response->getBody());
        info($response->getStatusCode());

        return json_decode($response->getBody(), true);
    }

    public function createContact($values)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.infusionsoft.com/crm/rest/v2/contacts?access_token='. $this->getToken()->accessToken,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{  
            "given_name": "'.$values['given_name'].'",            
            "custom_fields": [
                {
                    "content": "'.$values['password'].'",
                    "id": 7
                }
            ],
            "email_addresses": [
                {
                    "email": "'.$values['email'].'",
                    "field": "EMAIL1"
                }
            ]
        }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',            
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function createTags($contactID)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.infusionsoft.com/crm/rest/contacts/'.$contactID.'/tags?access_token='. $this->getToken()->accessToken,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{  
            "tagIds": [1007, 1013]
        }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',            
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function updateContactPassword($email, $password)
    {
        $contact = $this->getContactByEmail($email);

        if(!$contact) {
            return;
        }

        return $this->updateContact(array_get($contact, 'id'), [
            'custom_fields' => [
                ['id' => 7, 'content' => $password]
            ]
        ]);
    }

    public function getCreiContactPassword($email)
    {
        $contact = $this->getContactByEmail($email);

        if(!$contact) {
            return;
        }

        $response = $this->httpClient->get($this->contactsUri . '/' . array_get($contact, 'id'), [
            'query' => [
                'optional_properties' =>'custom_fields',
                'access_token' => $this->getToken()->accessToken,
            ],
        ]);

        if (!$response) {
            return false;
        }

        $customFieldsArr = array_get(json_decode($response->getBody(), true), 'custom_fields');

        $creiPwd = NULL;

        foreach ($customFieldsArr as $k => $v) {
            if ($v['id'] == 7) {
                $creiPwd = $v['content'];
            }
        }

        return $creiPwd;
    }

    public function getInfusionContactID($email)
    {
        $contact = $this->getContactByEmail($email);

        if(!$contact) {
            return;
        }

        $response = $this->httpClient->get($this->contactsUri . '/' . array_get($contact, 'id'), [
            'query' => [
                'optional_properties' =>'custom_fields',
                'access_token' => $this->getToken()->accessToken,
            ],
        ]);

        if (!$response) {
            return false;
        }

        $res = json_decode($response->getBody(), true);        

        return $res['id'];
    }

    /**
     * @return Token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param Token $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Checks if the current token is null or expired
     *
     * @return boolean
     */
    public function isTokenExpired()
    {
        $token = $this->getToken();

        if (!is_object($token)) {
            return true;
        }

        return $token->isExpired();
    }
}
