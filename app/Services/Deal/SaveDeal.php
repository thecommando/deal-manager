<?php

namespace App\Services\Deal;

use App\Notifications\DealCreated;
use App\Notifications\DealCreatedByFinder;
use App\Notifications\DealWasTransferred;
use App\Services\Calendar\EventManager;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SaveDeal
{
    public function handle(Request $request, $user)
    {
        $input = $request->all();
        $input['signal'] = 1;

        if (!$request->bedrooms) {
            $input['bedrooms'] = 1;
        }

        if (!$request->bathrooms) {
            $input['bathrooms'] = 1;
        }

        if (!$request->type) {
            $input['type'] = 1;
        }

        if ($request->has('contract_period_days')) {
            $input['contract_period'] = Carbon::today()->addDays($request->contract_period_days);
        }

        // if ($request->has('street_number') && $request->has('street_name')) {
        //     $input['address'] = $request->street_number . ', ' . $request->street_name;
        //     $input['seller_address'] = $request->street_number . ', ' . $request->street_name;
        // }

        // if ($request->has('seller_city')) {
        //     $input['city'] = $request->seller_city;
        // }
        // if ($request->has('seller_state')) {
        //     $input['state'] = $request->seller_state;
        // }

        // if ($request->has('seller_postal_code')) {
        //     $input['postal_code'] = $request->seller_postal_code;
        // }

        if (!$request->has(['stage', 'deal_type', 'status', 'type', 'bedrooms', 'bathrooms'])) {
            $input['stage'] = 7;
        }

        if (is_string($request->get('notes'))) {
            $input['notes'] = $request->get('notes');
        }

        $property = $user->properties()->create($input);

        if ($apiKey = $request->transfer_key) {
            $owner = User::where('api_key', $apiKey)->first();

            if ($owner && $owner->id != $user->id) {
                $property->transferred_by = $user->id;
                $property->user_id = $owner->id;

                $property->save();

                activity()->causedBy($user)->performedOn($property)->log('transferred');

                $property->user->notify(new DealWasTransferred($property));
            }
        }

        if ($request->finder_key) {
            $finder = User::where('api_key', $request->finder_key)->firstOrFail();

            $property->finder_id = $finder->id;

            $property->save();

            $finder->notify(new DealCreated($property));
            $user->notify(new DealCreatedByFinder($property, $finder));
        }

        if (!$request->has(['stage', 'deal_type', 'status', 'type', 'bedrooms', 'bathrooms'])) {
            $property->setPending();
        }

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                if (!is_null($image)) {
                    $property->uploadImage($image);
                }
            }
        }

        if (is_array($request->get('notes', []))) {
            $property->saveNotes($request->get('notes', []));
        }

        $property->saveComparables();

        activity()->on($property)->by($user)->log('created');

        EventManager::make('New deal added', today())->property($property)->create();

        return $property;
    }
}