<?php

namespace App\Services\Contact;

class SaveContact
{
	public function handle($request, $user)
	{
		$contact = $user->contacts()->create($request->all());

		activity()->on($contact)->by($user)->log('created');

		return $contact;
	}
}