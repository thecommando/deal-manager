<?php

namespace App\Services\Contact;

use App\Contact;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class GetDatatable
{
    public function getDatatable(Request $request, $getAll = false)
    {
        $contactModel = Contact::query();

        $user = auth()->user();
        $is_admin = $user->hasRole('admin');

        if ($is_admin && $getAll) {

        }
         else if ($is_admin) {
            $contactModel->where('user_id', auth()->user()->id);
        } else if (($user->hasRole('user') && $user->manager_id > 0)) {
            $contactModel->where('user_id', $user->manager_id)
                ->orWhere('user_id', auth()->user()->id);
        } else {
            $teamMembersIDs = [];

            if ($user->sub_users->count()) {
                $teamMembersIDs = $user->sub_users->pluck('id')->toArray();
            }

            $ownersIds = array_merge($teamMembersIDs, [auth()->user()->id]);

            $contactModel->whereIn('user_id', $ownersIds);
        }

        return Datatables::of($contactModel->with('category', 'contact_groups', 'user')->orderBy('created_at', 'desc'))
            ->addColumn('assigned_group', function ($contact) {
                $groupassigned = [];
                foreach ($contact->contact_groups->toArray() as $group) {
                    $groupassigned[] = $group['name'];
                }
                return implode(', ', $groupassigned);
            })
            ->editColumn('created_at', function ($contact) {
                return $contact->created_at->format('M d, Y h:i a');
            })
            ->addColumn('action', function ($contact) use ($is_admin) {
                return view('contacts.action', compact('contact', 'is_admin'))->render();
            })
            ->make(true);
    }
}