<?php

namespace App\Services\Contact;

class SaveTemplate
{
	public function handle($request, $user)
	{
		$template = $user->templates()->create($request->all());

		activity()->on($template)->by($user)->log('created');

		return $template;
	}
}