<?php

namespace App\Services;

use App\User;
use App\Team;
use App\Contact;
use App\Property;
use App\ContactCategory;

class AdminDashboard
{
	public $totalUsers;

	public $totalTeams;

	public $latestContacts;

	public $latestDeals;
	
	public $listDeals;

	public $totalOpenDeals;

	public $totalClosedDeals;

	/**
	 * Dashboard constructor.
	 */
	public function __construct()
	{
		$this->totalUsers = $this->totalUsers();
		$this->totalTeams = $this->totalTeams();
		$this->latestContacts = $this->latestContacts();
		$this->latestDeals = $this->latestDeals();
		$this->listDeals = $this->listDeals();
		$this->totalOpenDeals = $this->totalOpenDeals();
		$this->totalClosedDeals = $this->totalClosedDeals();
	}

	public function totalUsers()
	{
		return User::count();
	}

	public function totalTeams()
	{
		return Team::count();
	}

	public function latestContacts()
	{
		return Contact::latest()->take(5)->get();
	}

	public function latestDeals()
	{
		return Property::latest()->presentation()->take(25)->get();
	}
	
	public function listDeals()
	{
		return Property::latest()->presentation()->take(5000)->get();
	}

	public function totalOpenDeals()
	{
		return Property::open()->count();
	}

    public function totalDealsOnContract()
    {
        return Property::open()->where('contract_status', true)->count();
    }

	public function totalClosedDeals()
	{
		return Property::closed()->count();
	}

	public function totalCashBuyers()
	{
		return ContactCategory::find(6)->contacts()->count();
	}

	public function totalContacts()
	{
		return Contact::count();
	}

	public function totalWholeSaleFeesCollected()
	{
		
	}
	
}