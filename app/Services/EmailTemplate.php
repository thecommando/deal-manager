<?php

namespace App\Services;

class EmailTemplate
{
    public function all($property)
    {
        return [
            'Cash Buyer 1'                    => $this->getTemplate('cash-buyer-1', $property),
            'Motivated Seller 1'              => $this->getTemplate('motivated-seller-1', $property),
            'Motivated Seller 2'              => $this->getTemplate('motivated-seller-2', $property),
            'Motivated Seller Vacation/Short' => $this->getTemplate('motivated-seller-vacation-short', $property),
            'Bird Dog'                        => $this->getTemplate('bird-dog-1', $property),
            'Rehabber'                        => $this->getTemplate('rehabber-1', $property),
        ];
    }

    private function getTemplate($name, $property)
    {
        return view("emails.templates.$name", compact('property'));
    }
}