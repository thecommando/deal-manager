<?php

namespace App\Services;

use App\Contact;
use App\Property;
use App\ContactCategory;
use Kordy\Ticketit\Models\Ticket;

class CoachDashboard
{
	public $totalUsers;

	public $latestContacts;

	public $latestDeals;
	
	public $latestDealsAll;

	public $totalOpenDeals;

	public $totalClosedDeals;

	public $adminActiveTickets;

    public $totalWholesaleDeals;

    public $adminCompletedTickets;

    public $userActiveTickets;

    public $userCompletedTickets;

    public $coach;

    public $coachUsers;

    public $coachUserIds;

    /**
	 * Dashboard constructor.
	 */
	public function __construct()
	{
        $this->coach = auth()->user();
        $this->coachUsers = $this->coach->coach_users;
        $this->coachUserIds = $this->coachUsers->pluck('id')->toArray();

		$this->totalUsers = $this->totalUsers();
		$this->latestContacts = $this->latestContacts();
		$this->latestDeals = $this->latestDeals();
		$this->latestDealsAll = $this->latestDealsAll();
		$this->totalOpenDeals = $this->totalOpenDeals();
        $this->totalClosedDeals = $this->totalClosedDeals();
        $this->totalNewDealsAll = $this->totalNewDealsAll();
        $this->totalOfferSubmittedDealsAll = $this->totalOfferSubmittedDealsAll();
        $this->totalOfferAcceptedDealsAll = $this->totalOfferAcceptedDealsAll();
        $this->totalContactAssignedDealsAll = $this->totalContactAssignedDealsAll();
        $this->totalFundingRequestedDealsAll = $this->totalFundingRequestedDealsAll();
        $this->totalFundedDealsAll = $this->totalFundedDealsAll();
        $this->totalPendingDealsAll = $this->totalPendingDealsAll();
        $this->totalNoDealDealsAll = $this->totalNoDealDealsAll();
        $this->totalWholesaleDeals = $this->totalWholesaleDeals();

        $this->adminActiveTickets = $this->adminActiveTickets();
		$this->adminCompletedTickets = $this->adminCompletedTickets();
		$this->userActiveTickets = $this->userActiveTickets();
		$this->userCompletedTickets = $this->userCompletedTickets();
	}

	public function totalUsers()
	{
		return $this->coachUsers->count();
	}

	public function latestContacts()
	{
		return Contact::whereIn('user_id', $this->coachUserIds)->latest()->take(5)->get();
	}
	
	public function latestDeals()
	{
		return Property::whereIn('user_id', $this->coachUserIds)->latest()->presentation()->take(25)->get();
	}
	
	public function latestDealsAll()
	{
		return Property::whereIn('user_id', $this->coachUserIds)->latest()->presentation()->take(25)->get();
	}

	public function totalOpenDeals()
	{
		return Property::whereIn('user_id', $this->coachUserIds)->open()->count();
	}

    public function totalDealsOnContract()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->open()->where('contract_status', true)->count();
    }

	public function totalWholesaleDeals()
	{
		return Property::whereIn('user_id', $this->coachUserIds)->wholesale()->count();
	}

	public function totalClosedDeals()
	{
		return Property::where('user_id', $this->coach->id)->closed()->count();
	}

	public function totalClosedDealsAll()
	{
		return Property::whereIn('user_id', $this->coachUserIds)->closed()->count();
	}

    public function totalNewDealsAll()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->new()->count();
    }

    public function totalOfferSubmittedDealsAll()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->offerSubmitted()->count();
    }

    public function totalOfferAcceptedDealsAll()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->offerAccepted()->count();
    }

    public function totalContactAssignedDealsAll()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->contactAssigned()->count();
    }

    public function totalFundingRequestedDealsAll()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->fundingRequested()->count();
    }

    public function totalFundedDealsAll()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->funded()->count();
    }

    public function totalPendingDealsAll()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->pending()->count();
    }

    public function totalNoDealDealsAll()
    {
        return Property::whereIn('user_id', $this->coachUserIds)->noDeal()->count();
    }

	public function totalCashBuyers()
	{
		return ContactCategory::find(6)->contacts()->where('user_id', $this->coach->id)->count();
	}

	public function totalCashBuyersAll()
	{
		return ContactCategory::find(6)->contacts()->whereIn('user_id', $this->coachUserIds)->count();
	}

	public function totalContacts()
	{
		return Contact::where('user_id', $this->coach->id)->count();
	}

	public function totalContactsAll()
	{
		return Contact::whereIn('user_id', $this->coachUserIds)->count();
	}

	public function totalWholeSaleFeesCollected()
	{
		return Property::where('user_id', $this->coach->id)->closed()->sum('wholesale_fee');
	}

	public function totalWholeSaleFeesCollectedAll()
	{
		return Property::whereIn('user_id', $this->coachUserIds)->closed()->sum('wholesale_fee');
	}

	public function adminActiveTickets()
	{
		return Ticket::whereIn('user_id', $this->coachUserIds)->active()->count();
	}

	public function adminCompletedTickets()
	{
		return Ticket::whereIn('user_id', $this->coachUserIds)->complete()->count();
	}

	public function userActiveTickets()
	{
		return Ticket::whereIn('user_id', $this->coachUserIds)->active()->count();
	}

	public function userCompletedTickets()
	{
		return Ticket::whereIn('user_id', $this->coachUserIds)->complete()->count();
	}
}