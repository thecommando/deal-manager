<?php

namespace App\Services\Calendar;

use App\Event;
use App\Property;
use App\Traits\Makeable;
use Carbon\Carbon;

class EventManager
{
    use Makeable;

    protected $title;
    protected $date;
    protected $property_id;
    protected $link;
    protected $color = 'grey';
    protected $is_global = false;
    protected $remind_at;

    public function __construct($title, $date)
    {
        $this->title = $title;
        $this->date = $date;
    }

    public function property(Property $property)
    {
        $this->property_id = $property->id;

        return $this;
    }

    public function color($color)
    {
        $this->color = $color;

        return $this;
    }

    public function link($link)
    {
        $this->link = $link;

        return $this;
    }

    public function isGlobal()
    {
        $this->is_global = true;

        return $this;
    }

    public function remindAt(Carbon $dateTime)
    {
        $this->remind_at = $dateTime;

        return $this;
    }

    public function create()
    {
        return Event::firstOrCreate([
            'user_id' => auth()->id(),
            'property_id' => $this->property_id,
            'title' => $this->title,
            'date' => $this->date,
            'remind_at' => $this->remind_at,
            'color' => $this->color,
            'link' => $this->link,
            'is_global' => $this->is_global,
        ]);
    }
}