<?php

namespace App\Services\Activity;

use App\Contact;
use App\Document;
use App\Property;
use App\Services\Activity\Parser\ContactParser;
use App\Services\Activity\Parser\DocumentParser;
use App\Services\Activity\Parser\PropertyParser;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Database\Eloquent\Collection;
use App\Services\Activity\Parser\CommonParser;

class Parser
{
	/**
	 * Activity parser registration.
	 *
	 * @var array
	 */
	protected $parsers = [
		Property::class => PropertyParser::class,
		Contact::class  => ContactParser::class,
		Document::class => DocumentParser::class,
	];

	/**
	 * Process activity feed.
	 *
	 * @param Collection|Activity $activity
	 * @return \Illuminate\Support\Collection|array
	 */
	public static function process($activity)
	{
		$parser = new static;

		if ($activity instanceof Collection) {
			return $parser->parseMultiple($activity);
		}

		if (is_array($activity)) {
			return $parser->parseMultiple(collect($activity));
		}

		return $parser->parse($activity);
	}

	/**
	 * Parse activity.
	 *
	 * @param Activity $activity
	 * @return array
	 */
	public function parse(Activity $activity)
	{
		return $this->getParser($activity)->parse();
	}

	/**
	 * Parse multiple activities.
	 *
	 * @param Collection $activities
	 * @return \Illuminate\Support\Collection
	 */
	public function parseMultiple(Collection $activities)
	{
		return $activities->map(function (Activity $activity) {
			return $this->parse($activity);
		});
	}

	/**
	 * Get parser for an activity, If parser not registered for model, common parser would get used.
	 *
	 * @param Activity $activity
	 * @return mixed
	 */
	private function getParser(Activity $activity)
	{
		$subject = $activity->subject;

		foreach ($this->parsers as $class => $parser) {
			if ($subject instanceof $class) {
				return new $parser($activity);
			}
		}

		return new CommonParser($activity);
	}
}