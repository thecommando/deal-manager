<?php

namespace App\Services\Activity\Parser;

class ContactParser extends BaseParser
{
	protected function getCreatedMessage()
	{
		return sprintf("%s added new contact %s.", $this->getCauser()->name, $this->getContactName());
	}

	protected function getUpdatedMessage()
	{
		return sprintf("%s %s contact %s.", $this->getCauser()->name, $this->getEvent(), $this->getContactName());
	}

	protected function getDeletedMessage()
	{
		return sprintf("%s %s contact %s.", $this->getCauser()->name, $this->getEvent(), $this->getContactName());
	}

	private function getContactName()
	{
		return $this->getSubject()->full_name;
	}
}