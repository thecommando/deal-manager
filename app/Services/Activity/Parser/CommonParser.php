<?php

namespace App\Services\Activity\Parser;

class CommonParser extends BaseParser
{
	protected function getCreatedMessage()
	{
		return sprintf("%s %s %s.", $this->getCauser()->name, $this->getEvent(), $this->getSubjectName());
	}

	protected function getUpdatedMessage()
	{
		return sprintf("%s %s %s.", $this->getCauser()->name, $this->getEvent(), $this->getSubjectName());
	}

	protected function getDeletedMessage()
	{
		return sprintf("%s %s %s.", $this->getCauser()->name, $this->getEvent(), $this->getSubjectName());
	}

	private function getSubjectName()
	{
		return strtolower(get_class_short_name($this->getSubject()));
	}
}