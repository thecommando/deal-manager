<?php

namespace App\Services\Activity\Parser;

use Spatie\Activitylog\Models\Activity;

abstract class BaseParser
{
	/**
	 * Activity to parse.
	 *
	 * @var Activity
	 */
	protected $activity;

	/**
	 * Parsed activity.
	 *
	 * @var array
	 */
	protected $parsedActivity = [];

	/**
	 * Base directory for content views.
	 *
	 * @var string
	 */
	protected $contentBaseDir = 'activity-contents';

	/**
	 * Content directory for event.
	 * i.e. comment, company etc.
	 *
	 * @var string
	 */
	protected $contentDir;

	/**
	 * Base parser constructor.
	 *
	 * @param Activity $activity
	 */
	public function __construct(Activity $activity)
	{
		$this->activity = $activity;

		$this->setContentDir();
	}

	/**
	 * Parse activity.
	 *
	 * @return $this
	 */
	public function parse()
	{
		$this->setCommonAttributes();

		$this->setMessage();

		$this->setContent();

		return $this->getParsedActivity();
	}


	/******************************************************************
	 *                        Abstract Methods
	 *****************************************************************/

	/**
	 * Message for create event.
	 *
	 * @return string
	 */
	protected abstract function getCreatedMessage();

	/**
	 * Message for update event.
	 *
	 * @return string
	 */
	protected abstract function getUpdatedMessage();

	/**
	 * Message for delete event.
	 *
	 * @return string
	 */
	protected abstract function getDeletedMessage();


	/******************************************************************
	 *                          Getters
	 *****************************************************************/

	/**
	 * Get causer object.
	 *
	 * @return mixed
	 */
	protected function getCauser()
	{
		return $this->activity->causer;
	}

	/**
	 * Get subject object.
	 *
	 * @return mixed
	 */
	protected function getSubject()
	{
		return $this->activity->subject;
	}

	/**
	 * Get event.
	 *
	 * @return mixed
	 */
	protected function getEvent()
	{
		return $this->activity->description;
	}

	/**
	 * Get activity time.
	 *
	 * @return \Carbon\Carbon
	 */
	protected function getActivityTime()
	{
		return $this->activity->created_at;
	}

	/**
	 * Get formatted activity time.
	 *
	 * @return string
	 */
	protected function getFormattedActivityTime()
	{
		return $this->getActivityTime()->toDateTimeString();
	}

	/**
	 * Get parsed activity.
	 *
	 * @return array
	 */
	public function getParsedActivity()
	{
		return $this->parsedActivity;
	}

	public function __toString()
	{
		return $this->getParsedActivity();
	}


	/******************************************************************
	 *                          Setters
	 *****************************************************************/

	/**
	 * Set common attributes to parsed activity.
	 */
	private function setCommonAttributes()
	{
		$this->parsedActivity['id'] = $this->activity->id;
		$this->parsedActivity['time'] = $this->getActivityTime();
	}

	/**
	 * Set activity message.
	 */
	private function setMessage()
	{
		$this->parsedActivity['message'] = $this->getMessage();
	}

	/**
	 * Set activity content.
	 */
	private function setContent()
	{
		$this->parsedActivity['content'] = $this->getContent();
	}

	/**
	 * Determine content directory, If not specified.
	 */
	private function setContentDir()
	{
		if (!isset($this->contentDir)) {
			$this->contentDir = $this->getDefaultSubject();
		}
	}


	/******************************************************************
	 *                          Helpers
	 *****************************************************************/

	/**
	 * Get activity message from event.
	 *
	 * @return string
	 */
	private function getMessage()
	{
		$method = sprintf("get%sMessage", camel_case($this->getEvent()));

		if (!method_exists($this, $method)) {
			return $this->getDefaultMessage();
		}

		return $this->$method();
	}

	/**
	 * Get activity content.
	 *
	 * @return null
	 */
	protected function getContent()
	{
		$view = $this->getContentView();

		return $view ? view($view, ['activity' => $this->activity])->render() : null;
	}

	/**
	 * Get default message for activity.
	 *
	 * @return string
	 */
	protected function getDefaultMessage()
	{
		return sprintf("%s %s %s", $this->getCauserLink(), $this->getEvent(), strtolower(get_class_short_name($this->getSubject())));
	}

	/**
	 * Get default subject name.
	 *
	 * @return string
	 */
	protected function getDefaultSubject()
	{
		return strtolower(get_class_short_name($this->getSubject()));
	}

	/**
	 * Get content view name.
	 *
	 * @return mixed
	 */
	private function getContentView()
	{
		if (!isset($this->contentDir)) {
			return null;
		}

		$view = sprintf("%s.%s.%s", $this->contentBaseDir, $this->contentDir, $this->getEvent());

		return view()->exists($view) ? $view : null;
	}

	/**
	 * Get link for causer.
	 *
	 * @return string
	 */
	protected function getCauserLink()
	{
		$causer = $this->getCauser();

		return $this->getLink("{$causer->username}/profile", $causer->name);
	}

	/**
	 * Create link.
	 *
	 * @param $url
	 * @param $placeholder
	 *
	 * @return string
	 */
	protected function getLink($url, $placeholder)
	{
		return sprintf("<a href='%s' class='name'>%s</a>", url($url), $placeholder);
	}
}