<?php

namespace App\Services\Activity\Parser;

class DocumentParser extends BaseParser
{
	protected function getCreatedMessage()
	{
		return sprintf("%s uploaded new document %s.", $this->getCauser()->name, $this->getDocumentTitle());
	}

	protected function getUpdatedMessage()
	{
		return sprintf("%s %s document %s.", $this->getCauser()->name, $this->getEvent(), $this->getDocumentTitle());
	}

	protected function getDeletedMessage()
	{
		return sprintf("%s %s document %s.", $this->getCauser()->name, $this->getEvent(), $this->getDocumentTitle());
	}

	private function getDocumentTitle()
	{
		return $this->getSubject()->title;
	}
}