<?php

namespace App\Services\Activity\Parser;

class PropertyParser extends BaseParser
{
	protected function getCreatedMessage()
	{
		return sprintf("%s added new deal for %s.", $this->getCauser()->name, $this->getPropertyName());
	}

	protected function getUpdatedMessage()
	{
		return sprintf("%s %s property %s.", $this->getCauser()->name, $this->getEvent(), $this->getPropertyName());
	}

	protected function getDeletedMessage()
	{
		return sprintf("%s %s property %s.", $this->getCauser()->name, $this->getEvent(), $this->getPropertyName());
	}

	protected function getTransferredMessage()
	{
		$transferNote = !empty($this->activity->getExtraProperty('transfer_note')) ? '| Notes: ' . $this->activity->getExtraProperty('transfer_note') : '';

		return sprintf("%s %s property %s to %s %s.", $this->getCauser()->name, $this->getEvent(), $this->getPropertyName(), $this->getSubject()->user->name, $transferNote);
	}

	protected function getContractSentMessage()
	{
		return sprintf("%s sent contract to %s for property %s", $this->getCauser()->name, $this->activity->getExtraProperty('recipient'), $this->getPropertyName());
	}

	protected function getEmailedMessage()
    {
        return sprintf("%s emailed deal to %s.", $this->getCauser()->name, $this->activity->getExtraProperty('recipient'));
    }

	private function getPropertyName()
	{
		return $this->getSubject()->getFullAddress();
	}
}