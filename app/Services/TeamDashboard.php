<?php

namespace App\Services;

use App\DealFinder;
use App\User;
use App\Contact;
use App\Property;
use App\ContactCategory;
use Kordy\Ticketit\Models\Ticket;

class TeamDashboard
{
	public $totalUsers;

	public $latestContacts;

	public $latestDeals;
	
	public $latestDealsAll;

	public $totalOpenDeals;

	public $totalClosedDeals;

	public $adminActiveTickets;

    public $totalWholesaleDeals;

    public $adminCompletedTickets;

    public $userActiveTickets;

    public $userCompletedTickets;

    public $teamManager;

    public $teamUsers;

    public $teamUserIds;

    /**
	 * Dashboard constructor.
	 */
	public function __construct()
	{
        $this->teamManager = auth()->user();
        $this->teamUsers = $this->teamManager->sub_users;
        $this->teamUserIds = $this->teamUsers->pluck('id')->toArray();

		$this->totalUsers = $this->totalUsers();
		$this->latestContacts = $this->latestContacts();
		$this->latestDeals = $this->latestDeals();
		$this->latestDealsAll = $this->latestDealsAll();
		$this->totalOpenDeals = $this->totalOpenDeals();
        $this->totalClosedDeals = $this->totalClosedDeals();
        $this->totalNewDealsAll = $this->totalNewDealsAll();
        $this->totalOfferSubmittedDealsAll = $this->totalOfferSubmittedDealsAll();
        $this->totalOfferAcceptedDealsAll = $this->totalOfferAcceptedDealsAll();
        $this->totalContactAssignedDealsAll = $this->totalContactAssignedDealsAll();
        $this->totalFundingRequestedDealsAll = $this->totalFundingRequestedDealsAll();
        $this->totalFundedDealsAll = $this->totalFundedDealsAll();
        $this->totalPendingDealsAll = $this->totalPendingDealsAll();
        $this->totalNoDealDealsAll = $this->totalNoDealDealsAll();
        $this->totalWholesaleDeals = $this->totalWholesaleDeals();

        $this->adminActiveTickets = $this->adminActiveTickets();
		$this->adminCompletedTickets = $this->adminCompletedTickets();
		$this->userActiveTickets = $this->userActiveTickets();
		$this->userCompletedTickets = $this->userCompletedTickets();
	}

	public function totalUsers()
	{
		return $this->teamUsers->count();
	}

	public function latestContacts()
	{
		return Contact::whereIn('user_id', $this->teamUserIds)->latest()->take(5)->get();
	}
	
	public function latestDeals()
	{
		return Property::where('user_id', $this->teamManager->id)->latest()->presentation()->take(25)->get();
	}
	
	public function latestDealsAll()
	{
		return Property::whereIn('user_id', $this->teamUserIds)->latest()->presentation()->take(25)->get();
	}

	public function totalOpenDeals()
	{
		return Property::whereIn('user_id', $this->teamUserIds)->open()->count();
	}

    public function totalDealsOnContract()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->open()->where('contract_status', true)->count();
    }

	public function totalWholesaleDeals()
	{
		return Property::whereIn('user_id', $this->teamUserIds)->wholesale()->count();
	}

	public function totalClosedDeals()
	{
		return Property::where('user_id', $this->teamManager->id)->closed()->count();
	}

	public function totalClosedDealsAll()
	{
		return Property::whereIn('user_id', $this->teamUserIds)->closed()->count();
	}

    public function totalNewDealsAll()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->new()->count();
    }

    public function totalOfferSubmittedDealsAll()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->offerSubmitted()->count();
    }

    public function totalOfferAcceptedDealsAll()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->offerAccepted()->count();
    }

    public function totalContactAssignedDealsAll()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->contactAssigned()->count();
    }

    public function totalFundingRequestedDealsAll()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->fundingRequested()->count();
    }

    public function totalFundedDealsAll()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->funded()->count();
    }

    public function totalPendingDealsAll()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->pending()->count();
    }

    public function totalNoDealDealsAll()
    {
        return Property::whereIn('user_id', $this->teamUserIds)->noDeal()->count();
    }

	public function totalCashBuyers()
	{
		return ContactCategory::find(6)->contacts()->where('user_id', $this->teamManager->id)->count();
	}

	public function totalCashBuyersAll()
	{
		return ContactCategory::find(6)->contacts()->whereIn('user_id', $this->teamUserIds)->count();
	}

	public function totalContacts()
	{
		return Contact::where('user_id', $this->teamManager->id)->count();
	}

	public function totalContactsAll()
	{
		return Contact::whereIn('user_id', $this->teamUserIds)->count();
	}

	public function totalWholeSaleFeesCollected()
	{
		return Property::where('user_id', $this->teamManager->id)->closed()->sum('wholesale_fee');
	}

	public function totalWholeSaleFeesCollectedAll()
	{
		return Property::whereIn('user_id', $this->teamUserIds)->closed()->sum('wholesale_fee');
	}

	public function adminActiveTickets()
	{
		return Ticket::whereIn('user_id', $this->teamUserIds)->active()->count();
	}

	public function adminCompletedTickets()
	{
		return Ticket::whereIn('user_id', $this->teamUserIds)->complete()->count();
	}

	public function userActiveTickets()
	{
		return Ticket::whereIn('user_id', $this->teamUserIds)->active()->count();
	}

	public function userCompletedTickets()
	{
		return Ticket::whereIn('user_id', $this->teamUserIds)->complete()->count();
	}

    public function dealFinders()
    {
        $userIds = DealFinder::whereIn('user_id', $this->teamUserIds)->pluck('deal_finder_id')->toArray();

        return User::whereIn('id', $userIds)->get();
    }

    public function dealsCountByFinder($finderId)
    {
        return Property::whereIn('user_id', $this->teamUserIds)->where('finder_id', $finderId)->count();
    }
}