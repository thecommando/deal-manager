<?php

namespace App\Services;

use App\DealFinder;
use App\Team;
use App\User;
use App\Contact;
use App\Property;
use App\ContactCategory;
use Kordy\Ticketit\Models\Ticket;

class Dashboard
{
	public $totalUsers;

	public $totalTeams;

	public $latestContacts;

	public $latestDeals;
	
	public $latestDealsAll;

	public $totalOpenDeals;

	public $totalClosedDeals;

    public $totalWholesaleDeals;

    public $adminActiveTickets;

    public $adminCompletedTickets;

    public $userActiveTickets;

    public $userCompletedTickets;

    public $user;

    /**
	 * Dashboard constructor.
	 */
	public function __construct()
	{
	    $this->user = auth()->user();

		$this->totalUsers = $this->totalUsers();
		$this->totalTeams = $this->totalTeams();
		$this->latestContacts = $this->latestContacts();
		$this->latestDeals = $this->latestDeals();
		$this->latestDealsAll = $this->latestDealsAll();
		$this->totalOpenDeals = $this->totalOpenDeals();
        $this->totalClosedDeals = $this->totalClosedDeals();
        $this->totalWholesaleDeals = $this->totalWholesaleDeals();

        $this->adminActiveTickets = $this->adminActiveTickets();
		$this->adminCompletedTickets = $this->adminCompletedTickets();
		$this->userActiveTickets = $this->userActiveTickets();
		$this->userCompletedTickets = $this->userCompletedTickets();
	}

	public function totalUsers()
	{
		return User::count();
	}

	public function totalTeams()
	{
		return Team::count();
	}

	public function latestContacts()
	{
		//For My team condition - contact
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Contact::where('user_id', $this->user->manager_id)->with('category')->latest()->take(5)->get();
		}

		return Contact::where('user_id', $this->user->id)->with('category')->latest()->take(5)->get();
	}
	
	public function latestDeals()
	{
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Property::with('user', 'finder')
			->where('user_id', $this->user->manager_id)
            ->latest()
            ->presentation()
            ->take(25)
            ->get();
		}

		return Property::where('user_id', $this->user->id)
            ->with('user', 'finder')
            ->latest()
            ->presentation()
            ->take(25)
            ->get();
	}
	
	public function latestDealsAll()
	{
		return Property::
            where('user_id', '!=' , 0)
            ->latest()->presentation()->take(25)->get();
	}

	public function totalOpenDeals()
	{
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Property::open()->where('user_id', $this->user->manager_id)->count();
		}

		return Property::where('user_id', $this->user->id)->open()->count();
	}

    public function totalDealsOnContract()
    {
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Property::open()->where('user_id', $this->user->manager_id)->where('contract_status', true)->count();
		}
        return Property::where('user_id', $this->user->id)->open()->where('contract_status', true)->count();
    }

	public function totalWholesaleDeals()
	{
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Property::wholesale()->where('user_id', $this->user->manager_id)->count();
		}

		return Property::where('user_id', $this->user->id)->wholesale()->count();
	}

	public function totalClosedDeals()
	{
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Property::closed()->where('user_id', $this->user->manager_id)->count();
		}

		return Property::where('user_id', $this->user->id)->closed()->count();
	}

	public function totalClosedDealsAll()
	{
		return Property::closed()->count();
	}

	public function totalCashBuyers()
	{
		//For My team condition - contact
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return ContactCategory::find(6)->contacts()->where('user_id', $this->user->manager_id)->count();
		}

		return ContactCategory::find(6)->contacts()->where('user_id', $this->user->id)->count();
	}

	public function totalCashBuyersAll()
	{
		return ContactCategory::find(6)->contacts()->count();
	}

	public function totalContacts()
	{
		//For My team condition - contact
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Contact::where('user_id', $this->user->manager_id)->count();
		}

		return Contact::where('user_id', $this->user->id)->count();
	}

	public function totalContactsAll()
	{
		return Contact::count();
	}

	public function totalWholeSaleFeesCollected()
	{
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Property::closed()->where('user_id', $this->user->manager_id)->sum('wholesale_fee');
		}

		return Property::where('user_id', $this->user->id)->closed()->sum('wholesale_fee');
	}

	public function totalWholeSaleFeesCollectedAll()
	{
		return Property::closed()->sum('wholesale_fee');
	}

	public function adminActiveTickets()
	{
		return Ticket::active()->count();
	}

	public function adminCompletedTickets()
	{
		return Ticket::complete()->count();
	}

	public function userActiveTickets()
	{
		return Ticket::userTickets($this->user->id)->active()->count();
	}

	public function userCompletedTickets()
	{
		return Ticket::userTickets($this->user->id)->complete()->count();
	}

    public function totalInvestorPartners()
    {
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return DealFinder::count();
		}

        return DealFinder::where('deal_finder_id', $this->user->id)->count();
	}

    public function dealFinders()
    {
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			$userIds = DealFinder::pluck('deal_finder_id')->toArray();
		} else {
			$userIds = DealFinder::where('user_id', $this->user->id)->pluck('deal_finder_id')->toArray();
		}

        return User::whereIn('id', $userIds)->get();
	}

    public function dealsCountByFinder($finderId)
    {
        return Property::where('user_id', $this->user->id)->where('finder_id', $finderId)->count();
	}

    public function investorPartners()
    {
        $userIds = DealFinder::where('deal_finder_id', $this->user->id)->pluck('user_id')->toArray();

        return User::whereIn('id', $userIds)->get();
	}

    public function finderDealsCount()
    {
        return Property::where('finder_id', $this->user->id)->count();
	}

    public function finderDeals()
    {
        return Property::where('finder_id', $this->user->id)->latest()->presentation()->get();
	}

    public function finderClosedDealsCount()
    {
        return Property::where('finder_id', $this->user->id)->closed()->count();
	}

    public function dealsOnContractPeriod()
    {
		//For My team condition
		if ($this->user->hasRole('user') && $this->user->manager_id > 0) {
			return Property::whereNotNull('contract_period')->where('user_id', $this->user->manager_id)->get();
		}

        return Property::whereNotNull('contract_period')->where('user_id', $this->user->id)->get();
	}
}