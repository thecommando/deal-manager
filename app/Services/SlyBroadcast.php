<?php

namespace App\Services;

use App\Broadcast;
use GuzzleHttp\Client;

class SlyBroadcast
{
    protected $apiUrl = 'https://www.mobile-sphere.com/gateway/vmb.php';

    public function send(Broadcast $broadcast)
    {
        $credentials = $this->getCredentials();

        $data = [
            'c_uid'          => array_get($credentials, 'email'),
            'c_password'     => array_get($credentials, 'password'),
            'c_callerID'     => array_get($credentials, 'caller_id'),
            'c_phone'        => $broadcast->phone,
            'c_url'          => $broadcast->file_url,
            'c_date'         => 'now',
            'c_audio'        => $broadcast->type,
            'c_dispo_url'    => url('webhooks/slybroadcast'),
        ];

        $client = new Client();

        $response = $client->post($this->apiUrl, [
            'form_params' => $data,
        ]);

        return (string)$response->getBody();
    }

    public function credits()
    {
        $credentials = $this->getCredentials();

        if (!$this->hasCredentials($credentials)) {
            return null;
        }

        $data = [
            'c_uid'          => array_get($credentials, 'email'),
            'c_password'     => array_get($credentials, 'password'),
            'remain_message' => '1',
        ];

        $client = new Client();

        $response = $client->post($this->apiUrl, [
            'form_params' => $data,
        ]);

        return (string)$response->getBody();
    }

    public function getCredentials()
    {
        return auth()->user()->getMeta('slybroadcast');
    }

    public function setCredentials($email, $password, $callerId)
    {
        return auth()->user()->setMeta('slybroadcast', [
            'email'     => $email,
            'password'  => $password,
            'caller_id' => $callerId,
        ]);
    }

    public function hasCredentials($credentials)
    {
        return array_get($credentials, 'email') && array_get($credentials, 'password') && array_get($credentials, 'caller_id');
    }
}