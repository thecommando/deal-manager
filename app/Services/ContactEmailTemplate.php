<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;

class ContactEmailTemplate
{
    /**
     * @todo remove the commented code in future
     * @return Global Templates to all user | Local Templates as per user
     */
    public function all()
    {
        if((auth()->user()->hasRole('user') && auth()->user()->manager_id > 0)) {
            $emailTemp = DB::table('contact_email_templates')
                ->select('id', 'name')
                ->where('user_id', auth()->user()->manager_id)
                ->orWhere('user_id', auth()->user()->id)
                ->orWhere('is_global', 1)
				->orderBy('is_global', 'DESC')
                ->get();
        } else {
            $emailTemp = DB::table('contact_email_templates')
                ->select('id', 'name')
                ->where('user_id', auth()->user()->id)
                ->orWhere('is_global', 1)
				->orderBy('is_global', 'DESC')
                ->get();
        }
        
        /*
        Commented for now
        Future use if any
        if ($emailTemp->count() == 0) {
            $emailTemp = DB::table('contact_email_templates')
                ->select('id', 'name')
                ->get();
        }
        */

        $arr = [];
        foreach($emailTemp as $k => $v) {
            $arr[$v->name] = $this->getTemplate($v->id);
        }
        return $arr;
    }

    private function getTemplate($id)
    {
        $res = DB::table('contact_email_templates')->select('message', 'subject')->where('id', $id)->first();
        $message = $res->message;
        return view("contacts.templates.template-1", compact('message'));
    }
}