<?php

namespace App\Services\DealFinder;

use App\Events\DealFinderWasCreated;
use App\Notifications\DealFinderCreated;
use App\Role;
use App\Services\Calendar\EventManager;
use App\User;

class SaveDealFinder
{
    public function handle($request, $user)
    {
        $roleFinder = Role::whereName('finder')->first();

        $dealFinder = new User($request->all());

        $password = $request['password'];//str_random(6);

        $dealFinder->password = bcrypt($password);
        $dealFinder->api_key = generateUUID();

        $dealFinder->save();

        $dealFinder->attachRole($roleFinder);

        $user->deal_finders()->attach($dealFinder->id);

        event(new DealFinderWasCreated($dealFinder, $password));

        $user->notify(new DealFinderCreated($dealFinder));

        EventManager::make('Deal Finder Added', today())->color('orange')->create();

        $this->addAsContact($user, $dealFinder);

        return $dealFinder;
    }

    public function addAsContact(User $user, User $dealFinder)
    {
        return $user->contacts()->create([
            'category_id'     => 3,
            'first_name'      => $dealFinder->first_name,
            'last_name'       => $dealFinder->last_name,
            'email_address'           => $dealFinder->email,
            'phone'    => $dealFinder->mobile_phone,
            'alternate_phone' => $dealFinder->office_phone,
            'site_address'         => $dealFinder->address,
            'site_city'            => $dealFinder->city,
//            'state'           => $dealFinder->state,
//            'country'         => $dealFinder->country,
            'site_zip_code'     => $dealFinder->postal_code,
        ]);
    }
}