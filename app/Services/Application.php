<?php

namespace App\Services;

class Application
{
    public static function scriptVariables()
    {
        return [
            'csrfToken' => csrf_token(),
            'env'       => config('app.env'),
            'user'      => auth()->user(),
            'userId'    => auth()->id(),
            'signedIn'  => auth()->check(),
        ];
    }
}