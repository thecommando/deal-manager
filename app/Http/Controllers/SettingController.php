<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Notifications\TestSendInBlueNotification;
use App\Notifications\TestTwilioNotification;
use App\User;
use Exception;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $settings = $user->settings()->pluck('value', 'key');

        return view('settings.index', compact('user', 'settings'));
    }

    public function store($group, Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        foreach ($request->except('_token') as $key => $value) {
            $user->settings()->updateOrCreate(
                compact('group', 'key'),
                compact('value')
            );
        }

        $this->testNotifications($group, $user);

        return back();
    }

    public function billing()
    {
        $user = auth()->user();

        $invoices = $user->hasStripeId() ? $user->invoices() : [];

        return view('settings.billing', compact('invoices'))->withUser($user);
    }

    public function getEmails()
    {
        return view('settings.emails');
    }

    public function getMessageTemplates()
    {
        return view('settings.message-templates');
    }

    public function getCard()
    {
        return view('settings.card')->withUser(auth()->user());
    }

    public function postCard(Request $request)
    {
        auth()->user()->createAsStripeCustomer($request->stripeToken);

        alert()->success('Success!', 'Credit card details updated successfully.');

        return redirect()->back();
    }

    public function getSubscription()
    {
        return view('settings.subscription')->withUser(auth()->user());
    }

    public function postSubscription(Request $request)
    {
        $user = $request->user();

        $plan = $request->plan;

        if ($user->subscribedToPlan($plan)) {
            alert()->info('Oops!', 'You are already on this plan.');

            return back();
        }

        if (!$user->subscribed()) {
            $user->newSubscription('default', $plan)->create();
        } else {
            $user->subscription()->swap($plan);
        }

        alert()->success('Success!', 'Plan subscribed successfully.');

        return back();
    }

    public function getPayments()
    {
        $invoices = auth()->user()->invoices();

        return view('settings.payments', compact('invoices'));
    }

    public function downloadInvoice($invoiceId)
    {
        return auth()->user()->downloadInvoice($invoiceId, [
            'vendor' => 'Deal Manager Pro',
            'product' => 'Your Product',
        ]);
    }

    public function getSubscriptionCancel()
    {
        return view('settings.subscription-cancel');
    }

    public function postSubscriptionCancel()
    {
        auth()->user()->subscription('main')->cancel();

        alert()->success('Success!', 'Your subscription cancelled successfully.');

        return back();
    }

    private function testNotifications($group, User $user)
    {
        try {
            switch ($group) {
                case 'sendinblue':
                    $user->setSendInBlueCredentials();
                    $user->notify(new TestSendInBlueNotification);
                    break;

                case 'twilio':
                    $user->setTwilioCredentials();
                    $user->notify(new TestTwilioNotification);
                    break;
                case 'google_calendar_api':
                    break;
            }

            alert()->success('Success!', 'Settings updated successfully.');
            session()->forget('error_settings');
        } catch (Exception $exception) {
            $text = $exception->getMessage();
            $text = str_replace("&quot;"," ",$text);
            $text = str_replace("&lt;"," ",$text);
            $text = str_replace("&gt;"," ",$text);
            // alert()->error('Error!!', 'There is a problem with your credentials or setup.'. $text, 'error');
            session()->put('error_settings', 'Error! There is a problem with your credentials or setup.'. $text);
        }
    }
}
