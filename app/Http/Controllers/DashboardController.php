<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{
	public function index()
	{
	    $user = auth()->user();

		if ($user && is_null($user->last_login)) {
			return redirect()->route('dashboard.onboarding');
		}

		if ($user->hasRole('super-user')) {
            return view('dashboard.team');
        }

        if ($user->hasRole('coach')) {
            return view('dashboard.coach');
        }

        return view('dashboard.index', compact('user'));
    }

	public function onboarding()
	{
		if (auth()->check() && is_null(auth()->user()->last_login)) {
			auth()->user()->setLastLoginTime();
		}

		return view('dashboard.onboarding');
	}
}
