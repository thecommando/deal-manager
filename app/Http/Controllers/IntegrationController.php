<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class IntegrationController extends Controller
{
	public function index()
	{
		return view('integrations.index')->withUser(auth()->user());
    }
}
