<?php

namespace App\Http\Controllers\Integration;

use App\User;
use App\Property;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Deal\SaveDeal;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePropertyRequest;

class DealController extends Controller
{
	public function index(Request $request)
	{
		$user = User::where('api_key', $request->api_key)->firstOrFail();

		if ($user->hasRole('admin')) {
			$query = Property::query();
		} else {
			$query = $user->properties();
		}

		if ($request->has('property_stage')) {
			$query->where('stage', $request->property_stage);
		}

		if ($request->has('deal_type')) {
			$query->where('deal_type', $request->deal_type);
		}

		if ($request->has('property_status')) {
			$query->where('status', $request->property_status);
		}

		$properties = $query->where('stage', '<>', 7)
            ->where('stage', '<>', 8)
            ->where('stage', '<>', 9)
            ->where('contract_status', true)
            ->whitelisted()
            ->presentation()
            ->with('images', 'user')
            ->latest()->get();

		return $properties;
	}

	public function create()
	{
		return view('integrations.deals.create');
	}

	public function store(CreatePropertyRequest $request)
	{
		$user = User::where('api_key', $request->api_key)->firstOrFail();

		(new SaveDeal())->handle($request, $user);

		alert()->success('Success!', 'Property published successfully.');

		return back();
	}

    public function postForm(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'address'       => 'required',
            'city'          => 'required',
            'state'         => 'required',
            'postal_code'   => 'required',
            'type'          => 'required|exists:property_types,id',

            // optional fields
            'seller_phone'  => 'phone:LENIENT,US',
            'seller_mobile' => 'phone:LENIENT,US',
            'seller_email'  => 'email',
        ]);

        if($validator->fails()) {
            $url = sprintf('%s&error=%s', $request->redirect_to, $validator->errors()->first());

            return redirect()->away($url);
        }

        $user = User::where('api_key', $request->api_key)->firstOrFail();

        (new SaveDeal())->handle($request, $user);

        return redirect()->away($request->redirect_to);
	}
}
