<?php

namespace App\Http\Controllers\Integration;

use App\Notifications\NewSellerLead;
use App\User;
use App\Property;
use App\State;
use App\Http\Requests;
use Exception;
use Illuminate\Http\Request;
use App\Services\Deal\SaveDeal;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBasicPropertyRequest;

class BasicDealController extends Controller
{
    public function create()
    {
        $statesList = State::pluck('name', 'name')->all();
        return view('integrations.basic-deals.create', compact('statesList'));
    }

    public function store(CreateBasicPropertyRequest $request)
    {
        $user = User::where('api_key', $request->api_key)->firstOrFail();

        $property = (new SaveDeal())->handle($request, $user);

        try {
            $user->notify(new NewSellerLead($property));
        } catch (Exception $exception) {
            logger()->error('Notification failed: ', [
                'user_id' => $user->id,
                'error' => $exception->getMessage(),
            ]);
        }

        return redirect()->route('integration.basic-deals.success', ['api_key' => $request->api_key]);
    }

    public function success()
    {
        return view('integrations.basic-deals.success');
    }
}
