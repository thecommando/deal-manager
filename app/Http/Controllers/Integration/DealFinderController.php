<?php

namespace App\Http\Controllers\Integration;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\DealFinder\SaveDealFinder;

class DealFinderController extends Controller
{
    public function create()
    {
        return view('integrations.deal-finders.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            // 'username' => 'required|unique:users,username',
            'email'    => 'required|email|unique:users',
        ]);

        $user = User::where('api_key', $request->api_key)->firstOrFail();

        //infusion api
        $password = str_random(6);

        $request['password'] = $password;
        $request['username'] = $request->email;

        $data = [               
            'password' => $password,
            'email' => $request->email,
            'given_name' => $request->name
        ];

        try {
            $response = (new \App\Services\Infusionsoft\Infusionsoft)->createContact($data);
            
            (new SaveDealFinder())->handle($request, $user);

        } catch (Exception $e) {
             alert()->error('Error!', $e->getMessage());
        }            

        return redirect()->route('integration.deal-finders.success', ['api_key' => $request->api_key]);
    }

    public function postForm(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'     => 'required',
            'username' => 'required|unique:users,username',
            'email'    => 'required|email',
        ]);

        if ($validator->fails()) {
            $url = sprintf('%s&error=%s', $request->redirect_to, $validator->errors()->first());

            return redirect()->away($url);
        }

        $user = User::where('api_key', $request->api_key)->firstOrFail();

        $password = str_random(6);

        $request['password'] = $password;

        $data = [               
            'password' => $password,
            'email' => $request->email,
            'given_name' => $request->name
        ];

        try {
            $response = (new \App\Services\Infusionsoft\Infusionsoft)->createContact($data);
            
            (new SaveDealFinder())->handle($request, $user);
        } catch (Exception $e) {
             alert()->error('Error!', $e->getMessage());
        }

        return redirect()->away($request->redirect_to);
    }

    public function success()
    {
        return view('integrations.deal-finders.success');
    }
}
