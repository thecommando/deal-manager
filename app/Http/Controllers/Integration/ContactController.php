<?php

namespace App\Http\Controllers\Integration;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Contact\SaveContact;
use App\Http\Requests\CreateContactRequest;

class ContactController extends Controller
{
	public function create()
	{
		return view('integrations.contacts.create');
    }

	public function store(CreateContactRequest $request)
	{
		$user = User::where('api_key', $request->api_key)->firstOrFail();

		(new SaveContact())->handle($request, $user);

		alert()->success('Success!', 'Contact saved successfully.');

		return back();
	}

    public function postForm(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'category_id'     => 'required',
            'first_name'      => 'required',
            'last_name'       => 'required',
            'email'           => 'required|email',
            'mobile_phone'    => 'required|phone:LENIENT,US',
            'alternate_phone' => 'numeric|phone:LENIENT,US',
        ]);

        if($validator->fails()) {
            $url = sprintf('%s&error=%s', $request->redirect_to, $validator->errors()->first());

            return redirect()->away($url);
        }

        $user = User::where('api_key', $request->api_key)->firstOrFail();

        (new SaveContact())->handle($request, $user);

        return redirect()->away($request->redirect_to);
    }
}
