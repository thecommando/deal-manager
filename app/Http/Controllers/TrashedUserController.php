<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
// use App\Exports\DataExport;

class TrashedUserController extends Controller
{
    public function trash()
    {
        $users = User::with('roles')->onlyTrashed()->get();

        return view('users.trash', compact('users'));
    }

    public function restore($id)
    {
        $user = User::withTrashed()->find($id);

        $user->deleted_at = NULL;

        $user->save();

        alert()->success('Success!', 'User restored successfully.');

        return back();
    }
}
