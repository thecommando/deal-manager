<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\CostTemplate;
// use App\Exports\DataExport;

class ExportCalcultorController extends Controller
{
    public function export(CostTemplate $template)
    {
        $template = CostTemplate::findOrFail($template)->first();

        $data = json_decode($template->items, TRUE);

        return Excel::create('Rehab_Calculation', function($excel) use($data, $template) {

            $excel->sheet('Rehab_Calculation', function($sheet) use($data) {

                $sheet->loadView('repair-cost-calculator.export', compact('data'))->withKey('data');

            });
        })->export('csv');
    }

    public function show(CostTemplate $template) {

        $template = CostTemplate::findOrFail($template)->first();

        $data = json_decode($template->items, TRUE);

        return view('repair-cost-calculator.export', compact('data'));
    }
}
