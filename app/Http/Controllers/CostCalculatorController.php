<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CostTemplate;
use PDF;
use App\Property;

class CostCalculatorController extends Controller
{
    protected $baseDir = 'uploads/documents/floorplans';

    private function findProperty($id, $secure = true)
    {
        $property = Property::presentation()->findOrFail($id);

        $user = auth()->user();

        if ($secure) {
            abort_unless($property->user_id == $user->id || $user->id == $property->user->manager_id || $user->id == $user->coach_users->contains($property->user) || $user->hasRole('admin') ||  ($user->hasRole('user') && $user->manager_id > 0), 403);
        }

        return $property;
    }

    public function index(Request $request) {
        $templates = CostTemplate::where('user_id', auth()->id())->get();
        return view('repair-cost-calculator.index', compact('templates'));
    }

    public function create(Request $request)
    {
        $propertyId = (int)$request->property_id ?? 0 ;
        return view('repair-cost-calculator.create', compact('propertyId'));
    }

    public function edit(Request $request, $template)
    {
        $template = CostTemplate::where('user_id', auth()->id())->findOrFail($template);
        return view('repair-cost-calculator.edit', compact('template'));
    }

    public function fetchTemplateJson($template)
    {
        $template = CostTemplate::where('user_id', auth()->id())->findOrFail($template);

        return response()->json([
            'id' => $template->id,
            'items' => $template->items,
            'total'=> (float) $template->total_cost,
            'template_name'=> $template->template_name,
            'path'=> url($template->path),
        ]);
    }

    public function saveTemplate(Request $request) {
        $input = $request->all();

        //Upload File
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            
            $fileName = sprintf("%s-%s", generateUUID(), $file->getClientOriginalName());

            $file->move($this->baseDir, $fileName);

            $input['path'] = sprintf("%s/%s", $this->baseDir, $fileName);
        }

        //Input
        if($input['id'] > 0) { 
            $template = CostTemplate::where('user_id', auth()->id())->findOrFail($input['id']);
            
            $dataToUpdate = [                
                'items' => json_encode(json_decode($input['items'])),
                'total_cost' => (float) $input['total_cost'],
                'template_name' => $input['template_name'],
            ];

            if (!empty($input['path'])) {
                $dataToUpdate['path'] = $input['path'];
                if (!empty($template->path)) {
                    unlink($template->path);
                }
            }

            $template->update($dataToUpdate);
        } else {
            $dataToCreate = [
                'user_id' => auth()->id(),
                'items' => json_encode(json_decode($input['items'])),
                'total_cost' => (float) $input['total_cost'],
                'template_name' => $input['template_name'],
            ];
            
            if (!empty($input['path'])) {
                $dataToCreate['path'] = $input['path'];
            }

            $template = CostTemplate::create($dataToCreate);     
            
            if ($input['property_id'] > 0) {
                //Update property rehab estimate
                $propertyData = Property::find((int)$input['property_id']);
                $propertyData->rehab_estimate = $dataToCreate['total_cost'];
                $propertyData->cost_template_id = $template->id;
                $propertyData->save();
            }
        }

       return response()->json([
            'id' => $template->id,
            'items' => $template->items,
            'total'=> (float) $template->total_cost,
            'template_name'=> $template->template_name,
            'floor_plan'=> $template->floorplan,
            'property_id' => (int) $input['property_id'] ?? 0
        ]);
    }


    public function myTemplates(Request $request) {
        $templates = CostTemplate::where('user_id', auth()->id())->get();
         return response()->json($templates);
    }

    public function destroy($id)
    {
        $costTemplate = CostTemplate::where('user_id', auth()->id())->findOrFail($id);
        
        $costTemplate->delete();

        alert()->success('Success!', 'Rehab Template removed successfully.');

        return back();
    }

    public function downloadRehabPdf($id)
    {
        $property = $this->findProperty($id, false);

        if (!empty($property->cost_template_id)) {
            $templateID = $property->cost_template_id;
            //where('user_id', auth()->id())->
            $template = CostTemplate::findOrFail($templateID);

            $items = json_decode($template['items'], TRUE);
        } else {
            $template = [];
            $items = [];
        }
        
        $pdf = PDF::loadView('repair-cost-calculator.download-rehab-pdf', compact('template', 'items', 'property'))->setOrientation('landscape')
        ->setPaper('a4');

        $fileName = sprintf('rehab_calc_%s.pdf', $template->template_name??'');

        return $pdf->download($fileName);
    }

    public function viewRehabPdf($id)
    {
        $template = CostTemplate::where('user_id', auth()->id())->findOrFail($id);

        $items = json_decode($template['items'], TRUE);

        return view('repair-cost-calculator.download-rehab-pdf', compact('template', 'items'));
    }
}
