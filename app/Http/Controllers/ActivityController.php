<?php

namespace App\Http\Controllers;

use ActivityParser;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Database\Eloquent\Collection;

class ActivityController extends Controller
{
    public function index(Request $request)
    {
        $this->clearRemovedModelActivities();

        $query = Activity::query();

        if (!$request->user()->hasRole('admin')) {
            $query->where('causer_id', $request->user()->id);
        }

        $activitiesPaginator = $query->with('subject', 'causer')->paginate(20);
        $activities = $activitiesPaginator->items();

        $activities = new Collection($activities);
        $activities = ActivityParser::process($activities);

        if ($request->ajax()) {
            $activitiesHtml = view('activities.index-paginate-data', compact('activities', 'activitiesPaginator'))->render();
            $paginationHtml = $activitiesPaginator->links()->toHtml();

            return response()->json([
                'activitiesHtml' => $activitiesHtml,
                'paginationHtml' => $paginationHtml,
            ]);
        }

        return view('activities.index', compact('activities', 'activitiesPaginator'));
    }

    private function clearRemovedModelActivities()
    {
        (new \ClearRemovedModelActivitiesSeeder())->run();
    }
}
