<?php

namespace App\Http\Controllers;

use App\ContactGroup;
use App\Http\Requests\SendEmailRequest;
use App\Http\Requests\SendSMSRequest;
use App\Notifications\SMSNotification;
use Exception;
use Mail;
use Excel;
use App\Contact;
use App\ContactEmailTemplate;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Contact\SaveTemplate;
use App\Http\Requests\UpdateTemplateRequest;
use App\Http\Requests\CreateTemplateRequest;
use Notification;
use Illuminate\Support\Facades\DB;

class TemplateController extends Controller
{
	/**
     * @todo remove the commented code in future
     * @return Global Templates to all user | Local Templates as per user
     */
	public function index()
	{
		if((auth()->user()->hasRole('user') && auth()->user()->manager_id > 0)) {
			$templates = ContactEmailTemplate::where('user_id', auth()->user()->manager_id)
				->orWhere('user_id', auth()->user()->id)
				->orWhere('is_global', 1)
				->orderBy('is_global', 'DESC')
				->get();
		} else {
			$templates = ContactEmailTemplate::where('user_id', auth()->user()->id)
			->orWhere('is_global', 1)
			->orderBy('is_global', 'DESC')
			->get();
		}

		/*
			Commented for now
        	Future use if any
			if ($templates->count() == 0) {
				$templates = ContactEmailTemplate::get();
			}
		*/
		
		return view('templates.index', compact('templates'));
	}

	public function create()
	{
		return view('templates.create');
	}

	public function store(CreateTemplateRequest $request)
	{
		$template = (new SaveTemplate())->handle($request, $request->user());

		// $contact->user->notify(new ContactCreated($contact));		

		alert()->success('Success!', 'Template created successfully.');

		return redirect()->route('manage-templates.index');
	}

	public function edit($id)
	{
		$template = ContactEmailTemplate::find($id);
		return view('templates.edit', compact('template'));
	}

	public function update(UpdateTemplateRequest $request, $id)
	{
		$template = ContactEmailTemplate::find($id);

		$template->fill($request->all());

		$template->save();

		alert()->success('Success!', 'Template updated successfully.');

		return redirect()->route('manage-templates.index');
	}

	public function destroy($id)
    {
    	$template = ContactEmailTemplate::findOrFail($id); //where('user_id', auth()->user()->id)

        $template->delete();

        alert()->success('Success!', 'Template removed successfully.');

        return redirect()->route('manage-templates.index');
    }

}
