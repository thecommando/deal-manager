<?php

namespace App\Http\Controllers\API;

use App\Broadcast;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BroadcastStatusController extends Controller
{
    public function index(Broadcast $broadcast)
    {
        return $broadcast->statuses;
    }
}
