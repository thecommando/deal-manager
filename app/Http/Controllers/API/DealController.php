<?php

namespace App\Http\Controllers\API;

use App\Property;
use App\Http\Controllers\Controller;

class DealController extends Controller
{
	public function show(Property $deal)
	{
		return $deal->load('comparable_properties', 'notes');
	}

    public function addresses($userId = null)
    {
        if($userId) {
            $properties = Property::where('user_id', $userId)->get(['address', 'city', 'state', 'country', 'postal_code']);
        } else {
            $properties = Property::all(['address', 'city', 'state', 'country', 'postal_code']);
        }

        return $properties->pluck('map_address');
    }
}
