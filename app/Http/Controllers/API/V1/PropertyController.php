<?php

namespace App\Http\Controllers\API\V1;

use App\DealFinder;
use App\Image;
use App\Notifications\DealCreated;
use App\Property;
use App\Services\Deal\SaveDeal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    public function index()
    {
        $user = request()->user();

        if($user->hasRole('finder')) {
            return Property::where('finder_id', $user->id)->presentation()->with('images')->latest()->get();
        }

        return $user->properties()->presentation()->with('images')->latest()->get();
    }

    public function store()
    {
        $this->validate(request(), [
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postal_code' => 'required',
            'images' => 'array',
            'notes' => 'nullable|string|max:254',
        ]);

        $user = request()->user();

        logger()->info('Create property from API', ['token' => request()->get('api_token'), 'user_email' => $user->email]);

        if($user->hasRole('finder')) {
            $partner = DealFinder::where('deal_finder_id', $user->id)->first();

            if($partner) {
                request()->merge(['finder_key' => $user->api_key]);
                $user = $partner->user;
            }
        }

        $deal = (new SaveDeal())->handle(request(), $user);

        $deal->user->notify(new DealCreated($deal));

        return Property::presentation()->with('images')->find($deal->id);
    }

    public function update(Property $property, Request $request)
    {
        $this->validate(request(), [
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postal_code' => 'required',
            'images' => 'array',
            'notes' => 'nullable|string|max:254',
        ]);

        $property->fill(
            request()->only('address', 'city', 'state', 'country', 'postal_code')
        );

        $property->save();

        if (request()->hasFile('images')) {
            foreach (request()->file('images') as $image) {
                if (!is_null($image)) {
                    $property->uploadImage($image);
                }
            }
        }

        return Property::presentation()->with('images')->find($property->id);
    }

    public function removeImage(Property $property, Image $image)
    {
        $image->removeImages();

        $image->delete();

        return response()->json('success');
    }
}
