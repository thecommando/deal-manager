<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthenticationException;

class LoginController extends Controller
{
    public function login()
    {
        $this->validate(request(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        if(auth()->attempt(request(['email', 'password']))) {
            $user = auth()->user();

            if(!$user->api_token) {
                $user->update(['api_token' => generateUUID()]);
            }

            return $user;
        }

        throw new AuthenticationException();
    }
}