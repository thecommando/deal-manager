<?php

namespace App\Http\Controllers\API;

use App\Events\UserWasCreated;
use App\Http\Requests;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	public function store(Request $request)
	{
		// Creating new user
		$user = new User();

		$password = str_random(6);

		$user->name = $request->name;
		$user->email = $request->email;
		$user->username = $this->getUniqueUsername($request->name);
		$user->password = bcrypt($password);
        $user->api_key = generateUUID();

		$user->save();

		// Attaching role to user
		$roleUser = Role::whereName('user')->first();
		$user->attachRole($roleUser);

		// Subscribing user to plan
//		$user->newSubscription('default', $plan = $request->plan)->create();

		event(new UserWasCreated($user, $password));

		return "success";
	}

	/**
	 * Generate unique username based on name.
	 *
	 * @param $name
	 * @param int $count
	 *
	 * @return string
	 */
	private function getUniqueUsername($name, $count = 0)
	{
		$username = $count ? str_slug($name) . '-' . $count : str_slug($name);

		if (User::where('username', $username)->withTrashed()->exists()) {
			return $this->getUniqueUsername($name, ++ $count);
		}

		return $username;
	}
}
