<?php

namespace App\Http\Controllers\API;

use App\Image;
use App\Http\Requests;
use App\Services\API\Zillow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    public function getComparables(Request $request)
    {
        if (!$request->has('address') || !$request->has('citystatezip')) {
            return [];
        }

        return (new Zillow($request->address, $request->citystatezip))->getComparables();
    }

    public function saveComparable(Request $request)
    {
        $request->session()->put('comparables', $request->comparables);

        return "success";
    }

    public function removeImage(Image $image)
    {
        $image->removeImages();

        $image->delete();

        return "success";
    }
}
