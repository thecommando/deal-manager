<?php

namespace App\Http\Controllers;

class SearchController extends Controller
{
    public function index()
    {
        $contacts = $this->contacts();
        $properties = $this->properties();

        return view('search.index', compact('contacts', 'properties'));
    }

    protected function contacts()
    {
        $search = request('q');

        return auth()->user()->contacts()->where(function ($q) use ($search) {
            $q->where('first_name', 'like', "%$search%")
                ->orWhere('last_name', 'like', "%$search%")
                ->orWhere('email_address', 'like', "%$search%")
                ->orWhere('phone', 'like', "%$search%")
                ->orWhere('alternate_phone', 'like', "%$search%")
                ->orWhere('site_address', 'like', "%$search%")
                ->orWhere('site_city', 'like', "%$search%")
                ->orWhere('site_state', 'like', "%$search%")
                ->orWhere('site_zip_code', 'like', "%$search%");
        })->take(5)
            ->get();
    }

    protected function properties()
    {
        $search = request('q');

        return auth()->user()->properties()->where(function ($q) use ($search) {
            $q->where('address', 'like', "%$search%")
                ->orWhere('city', 'like', "%$search%")
                ->orWhere('state', 'like', "%$search%")
                ->orWhere('country', 'like', "%$search%")
                ->orWhere('postal_code', 'like', "%$search%");
        })->take(5)
            ->get();
    }

}
