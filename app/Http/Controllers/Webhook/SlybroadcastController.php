<?php

namespace App\Http\Controllers\Webhook;

use App\BroadcastStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlybroadcastController extends Controller
{
    public function store(Request $request)
    {
        logger()->info('Slybroadcast', $request->all());

        BroadcastStatus::saveStatus($request->var);

        return 'OK';
    }
}
