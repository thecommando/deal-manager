<?php

namespace App\Http\Controllers\Webhook;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Events\UserWasCreated;
use App\Http\Controllers\Controller;

class InfusionSoftController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            $password = str_random(6);

            $user = User::create([
                'name' => $request->name,
                'username' => User::guessUsername($request->name),
                'email' => $request->email,
                'password' => bcrypt($password),
                'api_key' => generateUUID(),
            ]);

            $user->attachRole(Role::whereName('user')->first());

            (new \App\Services\Infusionsoft\Infusionsoft)->updateContactPassword($user->email, $password);

            event(new UserWasCreated($user, $password));
        }

        $user->is_active = true;
        $user->save();

        return $user;
    }
}
