<?php

namespace App\Http\Controllers\Webhook;

use App\Mail\SubscribedToPlan;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\UserWasCreated;
use App\Http\Controllers\Controller;
use Mail;
use Storage;
use Validator;

class ClickFunnelsController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'purchase.contact.name'          => 'required',
            'purchase.contact.email'         => 'required',
            'purchase.stripe_customer_token' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors()->all());
        }

        $filename = sprintf('%s-%s.json', Carbon::now()->format('YmdHis'), generateUUID());

        Storage::disk('public')->put($filename, json_encode($request->all()), 'public');

        $contact = $request->purchase['contact'];
        $product = $request->purchase['products'][0];

        $productIds = [1086703];

        if(!in_array($product['id'], $productIds)) {
            return response()->json('Product can not be processed.');
        }

        $user = User::where('email', $contact['email'])->first();

        if (!$user) {
            $password = str_random(6);

            $user = User::create([
                'name'     => $contact['name'],
                'username' => User::guessUsername($contact['name']),
                'email'    => $contact['email'],
                'password' => bcrypt($password),
                'api_key'  => generateUUID(),
            ]);

            $user->attachRole(Role::whereName('user')->first());

            event(new UserWasCreated($user, $password));
        }

        $user->is_active = true;
        $user->save();

        $plan = $product['stripe_plan'];
        $token = $request->purchase['stripe_customer_token'];

        $subscription = $user->subscriptions()->first();

        if(!$subscription) {
            $user->subscriptions()->create([
                'name' => 'default',
                'stripe_id' => '',
                'stripe_plan' => $plan,
                'quantity' => 1,
                'trial_ends_at' => null,
                'ends_at' => null,
            ]);
        } else {
            $subscription->update([
                'stripe_plan' => $plan,
            ]);
        }

        Mail::to($user)->send(new SubscribedToPlan($user));

        if (! $user->hasStripeId()) {
            $user->createAsStripeCustomer($token);
        } else {
            $user->updateCardFromStripe();
        }

        return $user;
    }

    public function test(Request $request)
    {

    }
}
