<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function show($name)
    {
       return view('resources.view_video', compact('name'));
    }
}
