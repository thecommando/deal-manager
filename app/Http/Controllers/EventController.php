<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\CreateEventRequest;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index()
    {
        $query = Event::forUser();

        if (request('from') && request('to')) {
            $query->whereBetween('date', [request('from'), request('to')]);
        }

        return $query->oldest('date')
            ->get();
    }

    public function show(Event $event)
    {
        $event->increment('visits_count');

        return redirect()->to($event->link);
    }

    public function store(CreateEventRequest $request)
    {
        $request->persist();

        alert()->success('Success!', 'Event created successfully.');

        return redirect()->route('calendar.index');
    }

    public function destroy(Event $event)
    {
        abort_unless($event->user_id == auth()->id(), 403);

        $event->delete();

        return response()->json('success');
    }
}
