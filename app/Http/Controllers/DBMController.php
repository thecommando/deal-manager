<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class DBMController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $client = new Client(['base_uri' => 'https://www.impactorder.com/api/']);

        $partnerkey = 'CO0541-DEALMGRP33256477';
        $method = 'LOGIN';
        $formatOutput = 'JSON';
        $officeId = 'DEALMGRP';
        $agentFirst = $user->first_name;
        $agentLast = $user->last_name;
        $email = $user->email;
        $uuid = $user->api_key;

        $body = "METHOD=$method&PARTNERKEY=$partnerkey&FORMAT_OUTPUT=$formatOutput&EMAIL=$email&AGENT_FIRST=$agentFirst&AGENT_LAST=$agentLast&OFFICE_ID=$officeId&OFFICE_NAME=DEALMANAGERPRO&UUID=$uuid";

        $response = $client->post('DEALMANAGERPRO', compact('body'));

        $response = json_decode($response->getBody(), true);

        if(array_get($response, 'response.status') === 'ok') {
            return redirect()->away(array_get($response, 'response.redirect_url'));
        }

        alert()->error('Error!', 'Something went wrong while login');

        logger()->error('DBM Login Failed', $response);

        return back();
    }
}
