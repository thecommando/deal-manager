<?php

namespace App\Http\Controllers\Auth;

use App\Services\Infusionsoft\Infusionsoft;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Client;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');

        $credentials['is_active'] = true;

        return $credentials;
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->hasRole('user')) {
            $this->checkInfusionsoftTag($user);
        }

        try {
            if (!$user->is_crei_created) {
                $infusionID = (new Infusionsoft)->getInfusionContactID($user->email);

                if (empty($infusionID)) {
                    $this->accessDenied('Infusionsoft Contact Id does not exist.');
                }

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://creims.ushomebuyingnetwork.com/api/admin/dmp/createuser',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>'{
                    "InfusionsoftId": "' . $infusionID . '",
                    "Role": "User"
                    }',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'Authorization: Bearer '. $this->getJwtToken()
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);

                $responseBody = json_decode($response, true);

                logger('CREI-MS-LOG: ', (array) $responseBody);

                if ($responseBody['status']['code'] == 200) {
                    auth()->user()->is_crei_created = (int) true;
                    auth()->user()->crei_uuid = $responseBody['data']['id'];
                    auth()->user()->save();
                }
            }
        }
        catch (exception $e) {
            return redirect()->intended($this->redirectPath());
        }

        $user->setLastLoginTime();

        return redirect()->intended($this->redirectPath());
    }

    protected function checkInfusionsoftTag($user)
    {
        $infusionsoftService = new Infusionsoft;

        if ($user->hasRole('user')) {
            try {
                $tags = collect($infusionsoftService->getContactTagsByEmail($user->email))->pluck('tag.name');

                if ($tags->contains(config('services.infusionsoft.payment_unsuccessful_tag'))) {
                    $this->accessDenied('Whoops, it looks like your latest subscription payment for Deal Manager Pro was unsuccessful. Please update your billing by visiting <a href="https://go.therealestatecommando.com/update-card">here</a>.');
                }

                if ($tags->contains(config('services.infusionsoft.deny_tag')) || !$tags->contains(config('services.infusionsoft.access_tag'))) {
                    $this->accessDenied('Sorry, it looks like you don’t currently have access. Please reach out to our support team by emailing <a href="mailto:support@dealmanagerpro.com">support@dealmanagerpro.com</a> and we’ll get this resolved for you.');
                }
            } catch (Exception $exception) {
                logger()->error('Infusionsoft API Failed', [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ]);

                $this->accessDenied('API issue with CRM. Please let us know at support and we can get it fixed right away. <a href="mailto:ssupport@dealmanagerpro.com">support@dealmanagerpro.com</a>');
            }
        }
    }

    protected function accessDenied($message)
    {
        auth()->logout();

        $validator = \Validator::make([], []);
        $validator->errors()->add('access_denied', $message);
        throw new ValidationException($validator);
    }

    protected function generate_jwt($headers, $payload, $secret = 'secret') {
        $headers_encoded = $this->base64url_encode(json_encode($headers));

        $payload_encoded = $this->base64url_encode(json_encode($payload));

        $signature = hash_hmac('SHA256', "$headers_encoded.$payload_encoded", $secret, true);
        $signature_encoded = $this->base64url_encode($signature);

        $jwt = "$headers_encoded.$payload_encoded.$signature_encoded";

        return $jwt;
    }

    protected function base64url_encode($str) {
        return rtrim(strtr(base64_encode($str), '+/', '-_'), '=');
    }

    protected function getJwtToken()
    {
        $headers = array('alg'=>'HS256','typ'=>'JWT');
        $payload = array('iat'=> time(),'role'=>'Admin', 'nbf'=>time(), 'exp'=> time() + 60*60*2, 'iss'=>  "https://creims.ushomebuyingnetwork.com/", "aud"=> "https://creims.ushomebuyingnetwork.com/");
        $secret = "dkv02?32920nrhc6dt[03&9r-04@12312-01=20(65fj3-fc9jc30jc_1230-9%#02fb^30967erkn5)}v;r#r:w*";
        $jwt = $this->generate_jwt($headers, $payload, $secret);

        return $jwt;
    }
}
