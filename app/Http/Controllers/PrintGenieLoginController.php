<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class PrintGenieLoginController
{

    public function index()
    {
        if (!auth()->check()) {
            return redirect()->route('login');
        }

        //add key to header
        $headers = array(
            'x-api-key' => env('PRINT_GENIE_API_KEY', 'fc49c411-01c2-4e1a-bb1c-c44b76b65d79'),
        );

        $full_name = auth()->user()->name;
        $data = [
            "email" => auth()->user()->email,
            "uuid" => auth()->user()->id,
            "first_name" =>  isset(explode(' ', $full_name)[0]) ? explode(' ', $full_name)[0] : '',
            "last_name" => isset(explode(' ', $full_name)[1]) ? explode(' ', $full_name)[1] : '',
            "address" => auth()->user()->address,
            "address_2" => "",
            "state" => auth()->user()->state,
            "city" => auth()->user()->city,
            "zip" => auth()->user()->postal_code,
        ];


        // Create a Guzzle HTTP client
        $client = new Client();

        try {
            // Send POST request
            $response = $client->post(env('PRINT_GENIE_URL', 'https://aws.printgenie.io/pg-api-services/sso/login'), [
                'headers' => $headers,
                'json' => $data,
            ]);

            // Get the response body as a string
            $body = $response->getBody()->getContents();
            $body = json_decode($body);

            if (isset($body->redirect_url)) {
                return redirect($body->redirect_url);
            } else {
                info('print genuine redirect url not found');
                info(json_encode($body));
                abort(404);
            }
        } catch (Exception $e) {
            info('error in print genuine login');
            abort(404);
        }
    }
}