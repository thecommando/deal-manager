<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        return view('contacts.index');
    }
}
