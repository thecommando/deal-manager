<?php

namespace App\Http\Controllers;

use App\DealFinder;
use App\Role;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\UserWasCreated;
use App\Http\Requests\UpdateRoleRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateTeamRequest;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
	public function index(Request $request)
	{
        if($request->ajax()) {

            $isArchived = !$request->has('archived');

            return Datatables::of(User::with('roles', 'properties')->where('is_active', $isArchived))
                ->addColumn('image', function ($user) {
                    return view('users.datatable-partials.image', compact('user'))->render();
                })
                ->addColumn('deals', function ($user) {
                    return view('users.datatable-partials.deals', compact('user'))->render();
                })
                ->editColumn('email', function ($user) {
                    return view('users.datatable-partials.email', compact('user'))->render();
                })
                ->addColumn('roles', function ($user) {
                    return view('users.datatable-partials.roles', compact('user'))->render();
                })
                ->addColumn('active', function ($user) {
                    return view('users.datatable-partials.active', compact('user'))->render();
                })
                ->addColumn('bypass', function ($user) {
                    return view('users.datatable-partials.bypass', compact('user'))->render();
                })
                ->addColumn('actions', function ($user) {
                    return view('users.datatable-partials.actions', compact('user'))->render();
                })
                ->make(true);
        }

		return view('users.index');
	}

	public function create()
	{
		$roles = Role::pluck('name', 'id');

		return view('users.create', compact('roles'));
	}

	public function store(CreateUserRequest $request)
	{
		$user = new User($request->all());

		$password = str_random(6);

		$user->password = bcrypt($password);
        $user->api_key = generateUUID();

		$user->save();

		if ($request->hasFile('profile')) {
			$user->uploadProfilePicture($request->file('profile'));
		}

		$user->rolesSync($request->roles);

		$user->updateTeamIfSuperUser();

		event(new UserWasCreated($user, $password));

		alert()->success('Success!', 'User created successfully.');

		return redirect()->route('users.index');
	}

	public function edit(User $user)
	{
		$roles = Role::pluck('name', 'id');

		$userRoles = $user->roles()->pluck('id');

        $dealFinders = Role::where('name', 'finder')->first()->users()->pluck('name', 'id');
        $coaches = Role::where('name', 'coach')->first()->users()->pluck('name', 'id');

		$userDealFinders = $user->deal_finders()->pluck('deal_finder_id');
		$userCoaches = $user->coaches()->pluck('coach_id');

		return view('users.edit', compact('user', 'roles', 'userRoles', 'dealFinders', 'userDealFinders', 'coaches', 'userCoaches'));
	}

	public function update(Request $request, User $user)
	{
		$this->validate($request, [
			'name'     => 'required|max:255',
			'username' => 'required|max:20|unique:users,username,' . $user->id,
			'email'    => 'required|email|max:255|unique:users,email,' . $user->id,
			'password' => 'min:6|confirmed|nullable',
			'profile'  => 'image|mimes:jpg,jpeg,png',
		]);

		$user->fill($request->except('password'));

		if ($request->has('password') && $request->password) {
			$user->password = bcrypt($request->password);
		}

		if ($request->hasFile('profile')) {
			$user->uploadProfilePicture($request->file('profile'));
		}

		$user->save();

		alert()->success('Success!', 'User details updated successfully.');

		return back();
	}

	public function updateRoles(UpdateRoleRequest $request, User $user)
	{
		$user->rolesSync($request->roles);

		$user->updateTeamIfSuperUser();

		alert()->success('Success!', 'User roles updated successfully.');

		return back();
	}

    public function updateDealFinders(Request $request, User $user)
    {
        $user->dealFindersSync($request->finders ?: []);

        alert()->success('Success!', 'Deal finders updated successfully.');

        return back();
	}

    public function updateCoaches(Request $request, User $user)
    {
        $user->coachesSync($request->coaches ?: []);

        alert()->success('Success!', 'Coaches updated successfully.');

        return back();
    }

	public function postTeam(UpdateTeamRequest $request, User $user)
	{
		$user->updateTeam($request->team_name);

		alert()->success('Success!', 'Team details updated successfully.');

		return back();
	}

	public function statusToggle(User $user)
	{
		$user->statusToggle();

		return $user;
	}

	public function bypassPaymentToggle(User $user)
	{
		$user->bypassPayment();

		return $user;
	}

	public function destroy(User $user)
	{
		$user->delete();

		alert()->success('Success!', 'User removed successfully.');

		return back();
	}

    public function restore($userId)
    {
        $user = User::withTrashed()->findOrFail($userId);

        $user->restore();

        alert()->success('Success!', 'User restored successfully.');

        return back();
	}

    public function updateMapLink(User $user, Request $request)
    {
        $user->google_map_link = $request->google_map_link;

        $user->save();

        alert()->success('Success!', 'Google map link saved successfully.');

        return back();
	}

    public function show()
    {
       abort(404);
    }
}
