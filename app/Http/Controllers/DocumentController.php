<?php

namespace App\Http\Controllers;

use App\Team;
use App\User;
use App\Document;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Notifications\DocumentCreated;
use App\Http\Requests\CreateDocumentRequest;

class DocumentController extends Controller
{
	public function index(Request $request)
	{
	    $documents = Document::whereHas('users', function($q) {
	        $q->where('id', auth()->id());
        })->orWhere('is_global', true)->get();

		return view('documents.index', compact('documents'));
	}

	public function create()
	{
		$documents = Document::with('users')->get();

		return view('documents.create', compact('documents'));
	}

	public function store(CreateDocumentRequest $request)
	{
	    $input = $request->all();
	    $input['path'] = '';

		$document = Document::create($input);

		$document->uploadFile($request->file('file'));

		$users = [];

		if ($request->to == 'users') {
			$users = $request->selection == 'all' ? User::pluck('id')->toArray() : $request->users;
		} elseif ($request->to == 'teams') {
			if ($request->selection == 'all') {
				$users = User::whereNotNull('manager_id')->pluck('id')->toArray();
			} else {
				$managers = Team::whereIn('id', $request->teams)->pluck('owner_id')->toArray();

				$users = User::whereIn('manager_id', $managers)->pluck('id')->toArray();
			}
		}

		$document->attachUsers($users);

        $request->user()->notify(new DocumentCreated($document));

		alert()->success('Success!', 'Document uploaded successfully.');

		return back();
	}

	public function edit(Document $document)
	{
		$documentUsers = $document->users()->pluck('id');

		return view('documents.edit', compact('document', 'documentUsers'));
	}

	public function update(Document $document, Request $request)
	{
	    $input = $request->all();
	    $input['is_global'] = $request->is_global == true;

	    $document->fill($input);
		$document->save();

		if ($request->hasFile('file')) {
			$document->removeFile();

			$document->uploadFile($request->file('file'));
		}

		$document->attachUsers($request->users ?: []);

		alert()->success('Success!', 'Document updated successfully.');

		return redirect()->route('documents.create');
	}

	public function destroy(Document $document)
	{
		$document->users()->detach();

		$document->removeFile();

		$document->delete();

		alert()->success('Success!', 'Document removed successfully.');

		return back();
	}
}
