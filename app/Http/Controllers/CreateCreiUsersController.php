<?php

namespace App\Http\Controllers;

use App\Services\Infusionsoft\Infusionsoft;
use Exception;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Client;
// use App\Exports\DataExport;

class CreateCreiUsersController extends Controller
{
    public function create(Request $request)
    {                
        $infusionID = (new Infusionsoft)->getInfusionContactID(auth()->user()->email);        

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://creims.ushomebuyingnetwork.com/api/admin/dmp/createuser',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "InfusionsoftId": "' . $infusionID . '",
        "Role": "User"
        }',
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Bearer '. $this->getJwtToken()
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $responseBody = json_decode($response, true);

        logger('CREI-MS-LOG: ', (array) $responseBody);
        
        if ($responseBody['status']['code'] == 200) {
            auth()->user()->is_crei_created = (int) true;
            auth()->user()->crei_uuid = $responseBody['data']['id'];
            auth()->user()->save();                
            alert()->success('Success!', 'CREI-MS Accont created successfully.');
        } else {
            alert()->error('Error!', $responseBody['status']['code'] . ' ' . implode(',', $responseBody['status']['messages']));
        }
        
        return back();
    }

    protected function generate_jwt($headers, $payload, $secret = 'secret') {
        $headers_encoded = $this->base64url_encode(json_encode($headers));
        
        $payload_encoded = $this->base64url_encode(json_encode($payload));
        
        $signature = hash_hmac('SHA256', "$headers_encoded.$payload_encoded", $secret, true);
        $signature_encoded = $this->base64url_encode($signature);
        
        $jwt = "$headers_encoded.$payload_encoded.$signature_encoded";
        
        return $jwt;
    }

    protected function base64url_encode($str) {
        return rtrim(strtr(base64_encode($str), '+/', '-_'), '=');
    }

    protected function getJwtToken()
    {
        $headers = array('alg'=>'HS256','typ'=>'JWT');
        $payload = array('iat'=> time(),'role'=>'Admin', 'nbf'=>time(), 'exp'=> time() + 60*60*2, 'iss'=>  "https://creims.ushomebuyingnetwork.com/", "aud"=> "https://creims.ushomebuyingnetwork.com/");
        $secret = "dkv02?32920nrhc6dt[03&9r-04@12312-01=20(65fj3-fc9jc30jc_1230-9%#02fb^30967erkn5)}v;r#r:w*";
        $jwt = $this->generate_jwt($headers, $payload, $secret);

        return $jwt;
    }
}
