<?php

namespace App\Http\Controllers;

use App\ContactCategory;
use App\Http\Requests\CreateContactCategoryRequest;
use App\Http\Requests\UpdateContactCategoryRequest;

class ContactCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $contactCategories = ContactCategory::all();

        return view('categories.index', compact('contactCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * @param CreateContactCategoryRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(CreateContactCategoryRequest $request)
    {
        $contactCategory = ContactCategory::create([
            'name' => $request->name,
        ]);

        if ($request->ajax()) {
            return response()->json([
                'data' => [
                    'id' => $contactCategory->id,
                    'name' => $contactCategory->name,
                ],
                'success' => true,
                'message' => 'Category created successfully.'
            ]);
        } else {
            alert()->success('Success!', 'Category created successfully.');
            return redirect()->route('categories.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $category = ContactCategory::findOrFail($id);
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContactCategoryRequest $request, $id)
    {
        ContactCategory::findOrFail($id)->update($request->all());

        alert()->success('Success!', 'Category updated successfully.');

        return redirect()->route('categories.index');
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(ContactCategory $category)
    {
        $category->delete();

        alert()->success('Success!', 'Category removed successfully.');

        return back();
    }
}
