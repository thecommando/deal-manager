<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Event;

class GcalendarController extends Controller
{
	protected $_CLIENT_ID = NULL;
	protected $_CLIENT_REDIRECT_URL = NULL;
	protected $_CLIENT_SECRET = NULL;

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function googleLogin(Request $request)
	{
        $this->_CLIENT_ID = env('GOOGLE_CLIENT_ID');

        $this->_CLIENT_SECRET = env('GOOGLE_CLIENT_SECRET');
        
        $this->_CLIENT_REDIRECT_URL = env('GOOGLE_CLIENT_REDIRECT_URL');

		if(isset($_GET['code'])) {
			try {
				$gapi = (new \App\Services\GoogleCalendarApi);
				
				// Get the access token 
				$data = $gapi->getAccessToken($this->_CLIENT_ID, $this->_CLIENT_REDIRECT_URL, $this->_CLIENT_SECRET, $_GET['code']);

				// Access Token
				$access_token = $data['access_token'];

				// Refresh Token
				if(array_key_exists('refresh_token', $data)) {
					$refresh_token = $data['refresh_token'];
					$request->session()->put('refresh_token', $refresh_token);				
				}
				
				$request->session()->put('access_token_expiry', time() + $data['expires_in']);

				$request->session()->put('access_token', $data['access_token']);				

				// auth()->user()->g_token = $access_token;

				// auth()->user()->save();
				
				return view('calendar.index');
			}
			catch(Exception $e) {
				echo $e->getMessage();
				exit();
			}
		}
	}

	public function create() {
        $this->_CLIENT_ID = env('GOOGLE_CLIENT_ID');

        $this->_CLIENT_SECRET = env('GOOGLE_CLIENT_SECRET');
        
        $this->_CLIENT_REDIRECT_URL = env('GOOGLE_CLIENT_REDIRECT_URL');

        if (empty($this->_CLIENT_ID) && empty($this->_CLIENT_SECRET)) {
        	alert()->overlay('Error!!', 'Please add google API credentials in setting section.');
        	return back();
        }

		$query = Event::forUser()->where('is_sync', 0);

        $localEvents = $query->get();

		$gapi = (new \App\Services\GoogleCalendarApi);
		
		if  (time() > session('access_token_expiry')) {
			// Get a new access token using the refresh token
			$data = $gapi->getRefreshedAccessToken($this->_CLIENT_ID,  $this->_CLIENT_REDIRECT_URL, session('refresh_token'),  $this->_CLIENT_SECRET);
			
			$request->session()->put('access_token_expiry', time() + $data['expires_in']);

			$request->session()->put('access_token', $data['access_token']);			
		}

		// Get the access token 
		$access_token =  session('access_token'); //auth()->user()->g_token;

		// Get user calendar timezone
		// $user_timezone = $gapi->getUserCalendarTimezone($access_token);
	
		if ($localEvents) {
            $i = 1;
            foreach ($localEvents as $key => $event) {                
                $data = [
                    'summary' => $event->title,
                    'location' => !empty($event->property) ? $event->property->full_address : '',
                    'description' => '',
                    'startDateTime' => $event->date->format('Y-m-d').'T09:00:00-07:00',
                    'endDateTime' =>$event->date->format('Y-m-d').'T09:00:00-07:00'
                ];

                try {                   
                	$calendar_id = 'primary';

					$event_title = $data['summary'];

					$location = $data['location'];

					$full_day_event = 1; 
					
					$event_time = [
						'event_date' => $event->date->format('Y-m-d')
					];

					// Create event on primary calendar
					$eventSynedId = $gapi->createCalendarEvent($calendar_id, $event_title, $location, $full_day_event, $event_time, $access_token);

                    if ($eventSynedId) {
                        $event->g_identifier = $eventSynedId;
                        $event->is_sync = 1;
                        $event->save();
                    }
                }  catch (Exception $exception) {
                    alert()->overlay('Error!!', $exception->getMessage());
                }

                $i++;                            
            }
        }

        alert()->success('Success!', 'Event synced successfully.');  

        return back(); 
	}
}
