<?php

namespace App\Http\Controllers;

use App\Broadcast;
use App\Services\SlyBroadcast;
use DB;
use Illuminate\Http\Request;
use Storage;

class BroadcastController extends Controller
{
    public function index()
    {
        $broadcasts = Broadcast::where('user_id', auth()->id())->latest()->get();

        return view('broadcasts.index', compact('broadcasts'));
    }

    public function create()
    {
        $credits = resolve(SlyBroadcast::class)->credits();
        $slybroadcast = resolve(SlyBroadcast::class)->getCredentials();

        return view('broadcasts.create', compact('slybroadcast', 'credits'));
    }

    public function store(Request $request, SlyBroadcast $slyBroadcast)
    {
        $this->validate($request, [
            'campaign_name'          => 'required',
            'phone'                  => 'required',
            'file'                   => 'required|file',
            'slybroadcast_email'     => 'required',
            'slybroadcast_password'  => 'required',
            'slybroadcast_caller_id' => 'required',
        ]);

        $slyBroadcast->setCredentials($request->slybroadcast_email, $request->slybroadcast_password, $request->slybroadcast_caller_id);

        $filename = sprintf('%s-%s', generateUUID(), $request->file->getClientOriginalName());

        $path = $request->file->storeAs('audio', $filename);

        DB::beginTransaction();

        $broadcast = Broadcast::create([
            'user_id'       => auth()->id(),
            'campaign_name' => $request->campaign_name,
            'phone'         => $request->phone,
            'file'          => $path,
            'type'          => $request->file->getClientOriginalExtension(),
        ]);

        $response = $slyBroadcast->send($broadcast);

        if (str_contains($response, 'ERROR')) {
            alert()->overlay('SlyBroadcast', str_replace("'", "", str_replace("\n", ' ', $response)), 'error');

            Storage::delete($path);

            DB::rollback();

            return back();
        } else {
            $broadcast->update(['status' => str_replace("'", "", str_replace("\n", ' ', $response))]);

            $broadcast->updateSessionId();

            DB::commit();
        }

        alert()->overlay('SlyBroadcast', $broadcast->status, 'success');

        return redirect()->route('broadcasts.index');
    }

    public function show()
    {
        abort(404);
    }
}
