<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests;
use Illuminate\Http\Request;

class CountryController extends Controller
{
	public function getStates($countryName)
	{
		$states = Country::whereName($countryName)->first()->states()->orderBy('name')->pluck('name');

		return response()->json($states);
    }
}
