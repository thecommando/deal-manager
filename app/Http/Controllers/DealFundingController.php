<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Property;
use App\Mail\RequestDealFunding;

class DealFundingController extends Controller
{
	public function sendRequestDealFunding(Request $request, Property $property) {

		$this->validate($request, [
			'name' => 'required',
			'email' => 'required|email',
			'phone' => 'required',
			'repair_value' => 'required',
			'estimated_price' => 'required',
			'amount_need' => 'required',
			'note' => 'required'
		],[
			'name.required' => 'Name is required',
			'email.required' => 'Email is required',
			'phone.required' => 'Phone is required',
			'repair_value.required' => 'What is the After Repair Value field is required',
			'estimated_price.required' => 'What is the Estimated Repair Cost field is required',
			'amount_need.required' => 'How much money do you need to do this deal field is required',
			'note.required' => 'Your Opinion field is required'
		]);

		$user = auth()->user();

		$data = $request->all();

		$data['auth_user'] = $user->name;

		$toEmail = 'funding@therealestatecommando.com';

		Mail::to($toEmail)->send(new RequestDealFunding($property, $data));

		alert()->success('Success!', 'A request for Deal funding has been sent successfully.');

		return back();
	}
}
