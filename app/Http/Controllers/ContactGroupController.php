<?php

namespace App\Http\Controllers;

use App\Contact;
use App\ContactGroup;
use App\Http\Requests\UpdateContactGroupMembersRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateContactGroupRequest;
use App\Http\Requests\UpdateContactGroupRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactGroupController extends Controller
{
    public function index()
    {
        $contactGroups = auth()->user()->contact_groups;

        return view('contact-groups.index', compact('contactGroups'));
    }

    public function create()
    {
        return view('contact-groups.create');
    }

    /**
     * @param CreateContactGroupRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(CreateContactGroupRequest $request)
    {
        $contactGroup = ContactGroup::create([
            'name' => $request->name,
            'user_id' => Auth::id(),
        ]);

        if ($request->ajax()) {
            return response()->json([
                'data' => [
                    'id' => $contactGroup->id,
                    'name' => $contactGroup->name,
                ],
                'success' => true,
                'message' => 'Contact group created successfully.'
            ]);
        } else {
            alert()->success('Success!', 'Contact group created successfully.');
            return redirect()->route('contact-groups.index');
        }
    }

    public function edit(ContactGroup $contactGroup)
    {
        return view('contact-groups.edit', compact('contactGroup'));
    }

    public function update(UpdateContactGroupRequest $request, ContactGroup $contactGroup)
    {
        $contactGroup->fill($request->all());

        $contactGroup->save();

        alert()->success('Success!', 'Contact group updated successfully.');

        return redirect()->route('contact-groups.index');
    }

    public function destroy(ContactGroup $contactGroup)
    {
        $contactGroup->delete();

        alert()->success('Success!', 'Contact group removed successfully.');

        return back();
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    public function removeDuplicatesByFirstName(Collection $collection) {
        $seenFirstNames = collect();
        return $collection->filter(function ($item) use ($seenFirstNames) {
            $firstName = $item['first_name'];
            if (!$seenFirstNames->contains($firstName)) {
                $seenFirstNames->push($firstName);
                return true;
            }
            return false;
        });
    }

    public function editMembers(ContactGroup $contactGroup)
    {
        $allContacts = auth()->user()->contacts;

//        $allContacts = ($this->removeDuplicatesByFirstName($allContacts));
//
        return view('contact-groups.edit-members', compact('contactGroup', 'allContacts'));
    }

    public function updateMembers(ContactGroup $contactGroup, UpdateContactGroupMembersRequest $request)
    {
        $contactGroup->contacts()->sync($request->contacts ?: []);

        alert()->success('Success!', 'Group members updated successfully.');

        return back();
    }

    public function show()
    {
        abort(404);
    }
}
