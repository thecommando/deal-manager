<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\User;
// use App\Exports\DataExport;

class ExportUsersController extends Controller
{
    public function export()
    {
        $users = User::with('roles')->withTrashed()->get();

        return Excel::create('Users', function($excel) use($users) {

            $excel->sheet('Users', function($sheet) use($users) {
                $sheet->loadView('users.export_users', compact('users'))->withKey('users');
            });
        })->export('xlsx');
    }
}
