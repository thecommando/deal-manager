<?php

namespace App\Http\Controllers;

use App\DealFinder;
use App\Role;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\UserWasCreated;
use App\Http\Requests\UpdateRoleRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Notifications\WelcomeNewTeam;

class MyTeamController extends Controller
{
	public function index() {

		$user = auth()->user();

		$users = User::where('manager_id', $user->id)->orderBy('name')->get();

		return view('my-teams.index', compact('users'));
	}

	public function create() {
		$roles = Role::pluck('name', 'id');

		return view('my-teams.create', compact('roles'));   
	}

	public function store(CreateUserRequest $request)
	{
		$teamCount = User::where('manager_id', auth()->user()->id)->count();

		if ($teamCount >= 2) {
			alert()->overlay('Sorry!!', 'You are not allowed to add more than 2 account. Please contact support on support@dealmanagerpro.com');

			return redirect()->route('my-team.index');
		}
		try {
			$password = str_random(6);
			
			$data = [				
				'password' => $password,
				'email' => $request->email,
				'given_name' => $request->name,
				'opt_in_reason' => 'DMP - Team Builder'
			];
			
			$response = (new \App\Services\Infusionsoft\Infusionsoft)->createContact($data);

			// $statusCode = $response->getStatusCode();

			if ($response) {

				$responseBody = json_decode($response, true);
            
				$contactID = $responseBody['id'];

				$tagResponse = (new \App\Services\Infusionsoft\Infusionsoft)->createTags((int) $contactID);
				
				logger('TAG-LOG: ', (array) $tagResponse);

				$user = new User($request->all());
				
				$user->password = bcrypt($password);
				
				$user->api_key = generateUUID();

				$user->manager_id = auth()->user()->id;

				$user->is_active = true;

				$user->save();
				
				if ($request->hasFile('profile')) {
					$user->uploadProfilePicture($request->file('profile'));
				}

				$user->rolesSync($request->roles);

				event(new UserWasCreated($user, $password));

				//Send Welcome Email
				$user->notify(new WelcomeNewTeam($user, $password));
			}
		} catch (Exception $e) {
			 alert()->overlay('Error!', $e->getMessage());
		}

		alert()->success('Success!', 'Team member created successfully.');

		return redirect()->route('my-team.index');
	}

	public function destroy(User $user)
	{
		$user->delete();

		alert()->success('Success!', 'User removed successfully.');

		return back();
	}

    public function show()
    {
        abort(404);
    }
}
