<?php

namespace App\Http\Controllers;

use App\ContactGroup;
use App\Http\Requests\SendDealEmailRequest;
use App\Http\Requests\UpdatePropertyRequest;
use App\Mail\DealFundingRequest;
use App\Mail\ProofOfFundsRequest;
use App\Mail\SellerLocatorRequest;
use App\Notifications\DealCreated;
use App\Notifications\DealFundingRequested;
use App\Notifications\DealsUploaded;
use App\Notifications\DealWasTransferred;
use App\Notifications\ProofOfFundsRequested;
use App\Notifications\SellerLocatorRequested;
use App\Services\Calendar\EventManager;
use Carbon\Carbon;
use PDF;
use Mail;
use Excel;
use App\User;
use App\Bedroom;
use App\Contact;
use App\Bathroom;
use App\DealType;
use App\Property;
use ActivityParser;
use App\PropertyType;
use App\PropertyStage;
use App\Http\Requests;
use App\PropertySignal;
use App\PropertyStatus;
use App\CostTemplate;
use Illuminate\Http\Request;
use App\Services\Deal\SaveDeal;
use App\Http\Requests\UploadDealsRequest;
use App\Http\Requests\CreatePropertyRequest;
use Yajra\Datatables\Datatables;

class DealController extends Controller
{
    public function index(Request $request)
    {
//        dd($request->all());
        $user = $request->user();

        $paginateCount = 12;

        if ($user->hasRole('admin')) {
            $query = Property::query();

        } elseif (($user->hasRole('user') && $user->manager_id > 0)) {
            $query = Property::query();

        } else {
            $query = $user->properties();
        }

        $transferredProperties = $user->transferred_properties;

        if ($request->has('property_stage')) {
            $query->where('stage', $request->property_stage);
        }

        if ($request->has('deal_type')) {
            $query->where('deal_type', $request->deal_type);
        }

        if ($request->has('property_status')) {
            $query->where('status', $request->property_status);
        }

        $properties = $query;
        if (($user->hasRole('user') && $user->manager_id > 0)) {
            $properties = $query
                ->where(function ($query) use ($user) {
                    $query->where('user_id', $user->manager_id)
                        ->orWhere('user_id', auth()->user()->id);
                });
        }
        $properties = $query->where('stage', '<>', 7)
            ->with('finder', 'user')
            ->presentation()
            ->latest()
            ->paginate($paginateCount, ['*'], 'properties_page');

        if ($request->ajax()) {
            $date = [
                'html' => view('deals.partials.deal-content', compact('properties', 'transferredProperties'))->render(),
                'hideShowMoreButton' => false
            ];
            if (count($properties) < $paginateCount) {
                $date['hideShowMoreButton'] = true;
            }
            return $date;
        }

        return view('deals.index', compact('properties', 'transferredProperties', 'paginateCount'));
    }

    public function export() {
        if (auth()->user()->hasRole('admin')) {
            $properties = Property::get();

        } else {
            $properties = auth()->user()->properties();
        }

        $propertyStatus = PropertyStatus::get();

        $filename = 'deals.txt'; // Change the file extension to .txt
        $handle = fopen($filename, 'w+');

// Add headline
        $headline = implode("\t", array(
            'type', 'status', 'stage', 'deal_type', 'bedrooms', 'bathrooms', 'address', 'city', 'state', 'country',
            'postal_code', 'description', 'square_feet', 'year_built', 'contract_status', 'inspection_status',
            'asking_price', 'estimated_rehab', 'annual_taxes', 'annual_insurance', 'seller_name', 'seller_mobile',
            'seller_email', 'signal'
        ));

        fwrite($handle, $headline . PHP_EOL);

        foreach ($properties as $property) {
            $row = implode("\t", array(
                $property->type,
                $propertyStatus->where('id', $property->status)->first() ? $propertyStatus->where('id', $property->status)->first()->name : '',
                $property->stage,
                $property->deal_type,
                $property->bedrooms,
                $property->bathrooms,
                str_replace(',', ';', $property->address),
                str_replace(',', ';', $property->city),
                str_replace(',', ';', $property->state),
                str_replace(',', ';', $property->country),
                $property->postal_code,
                str_replace(',', ' ', $property->description),
                $property->square_feet,
                $property->year_built,
                $property->contract_status ? 'Yes' : 'No',
                $property->inspection_status ? 'Yes' : 'No',
                $property->asking_price,
                $property->estimated_rehab,
                $property->annual_taxes,
                $property->annual_insurance,
                str_replace(',', ';', $property->seller_name),
                $property->seller_mobile,
                $property->seller_email,
                $property->signal
            ));

            fwrite($handle, $row . PHP_EOL);
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/plain', // Change the content type to plain text
        );

        return response()->download($filename, 'deals.txt', $headers);
    }

    public function pendingProperties(Request $request)
    {
        if ($request->ajax()) {
            $user = $request->user();

            if ($user->hasRole('admin')) {
                $pendingProperties = Property::query();
            } elseif (($user->hasRole('user') && $user->manager_id > 0)) {
                $pendingProperties = Property::where('user_id', $user->manager_id)->orWhere('user_id', auth()->user()->id);
            } else {
                $pendingPropertiesId = $user->properties()->pending()->select('id')->get()->pluck('id')->toArray();

                $pendingPropertiesMembersId = [];
                if ($user->sub_users->count()) {
                    $teamMembersIDs = $user->sub_users->pluck('id');
                    $pendingPropertiesMembersId = Property::pending()->whereIn('user_id', $teamMembersIDs)->select('id')->get()->pluck('id')->toArray();
                }

                $pendingProperties = array_merge($pendingPropertiesId, $pendingPropertiesMembersId);

                $pendingProperties = Property::whereIn('id', $pendingProperties);
            }

            return Datatables::of($pendingProperties->with('user', 'transferredBy', 'finder'))
                ->addColumn('image', function ($property) {
                    return view('deals.datatable-partials.image', compact('property'))->render();
                })
                ->addColumn('address', function ($property) {
                    return $property->address;
                })
                ->addColumn('owner', function ($property) {
                    return $property->user->name;
                })
                ->addColumn('created_at', function ($property) {
                    return $property->created_at->format('M d, Y h:i a');
                })
                ->addColumn('transfer_from', function ($property) {
                    return view('deals.datatable-partials.transfer-from', compact('property'))->render();
                })
                ->addColumn('bedrooms', function ($property) {
                    return $property->bedrooms;
                })
                ->addColumn('bathrooms', function ($property) {
                    return $property->bathrooms;
                })
                ->addColumn('actions', function ($property) {
                    return view('deals.datatable-partials.actions', compact('property'))->render();
                })
                ->make(true);
        }

        return view('deals.pending-deals');

    }

    public function create()
    {
        return view('deals.create');
    }

    public function store(CreatePropertyRequest $request)
    {
        $deal = (new SaveDeal())->handle($request, $request->user());

        $deal->user->notify(new DealCreated($deal));

        alert()->success('Success!', 'Property published successfully.');

        return redirect()->route('deals.pending-properties');
    }

    public function show($id)
    {
        $property = $this->findProperty($id);

        $activities = $this->getDealActivities($property);

        $user = auth()->user();

        $rehabTemplates = CostTemplate::where('user_id', auth()->id())->get();

        return view('deals.show', compact('property', 'user', 'activities', 'rehabTemplates'));
    }

    public function showPdf($id)
    {
        $property = $this->findProperty($id);

        return view('deals.show-pdf', compact('property'));
    }

    public function downloadPdf($id)
    {
        $property = $this->findProperty($id, false);

        $pdf = PDF::loadView('deals.download-pdf', compact('property'));

        $fileName = sprintf('dealsheet_%s.pdf', $property->address);

        return $pdf->download($fileName);
    }

    public function edit(Property $property)
    {
        $property->load('comparable_properties');

        return view('deals.edit', compact('property'));
    }

    public function update(UpdatePropertyRequest $request, Property $property)
    {
        if ($request->contract_period_days && $request->contract_period_days != $property->contract_period_days) {
            $property->contract_period = Carbon::today()->addDays($request->contract_period_days);
        }

        $property->fill($request->all());

        $property->contract_status = $request->input('contract_status', false);
        $property->inspection_status = $request->input('inspection_status', false);
        $property->asking_price_source = $request->input('asking_price_source', false);

        $property->save();

        $property->saveComparables();
        $property->saveNotes($request->notes);

        if (!$request->has(['stage', 'deal_type', 'status', 'type', 'bedrooms', 'bathrooms'])) {
            $property->setPending();
        }

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                if (!is_null($image)) {
                    $property->uploadImage($image);
                }
            }
        }

        alert()->success('Success!', 'Property updated successfully.');

        return redirect()->route('deals.pending-properties');
    }

    public function watchlist()
    {
        return view('deals.watchlist');
    }

    public function getUpload()
    {
        $stagesList = PropertyStage::get()->pluck('name', 'id')->toArray();

        return view('deals.upload', compact('stagesList'));
    }

    public function postUpload(UploadDealsRequest $request)
    {
        $file = $request->file('file');

        $user = $request->user();

        $propertyTypes = PropertyType::all();
        $propertyStatuses = PropertyStatus::all();
        $dealTypes = DealType::all();
        $propertyStages = PropertyStage::all();
        $propertySignals = PropertySignal::all();
        $bedrooms = Bedroom::all();
        $bathrooms = Bathroom::all();

        Excel::load($file, function ($reader) use ($user, $propertyTypes, $propertyStatuses, $dealTypes, $propertyStages, $propertySignals, $bedrooms, $bathrooms) {
            $i = 0;

            //validate data
            foreach ($reader->get() as $record) {

                $input = $record->toArray();

                if (!$propertyTypes->where('name', $input['type'])->first()) {
                    alert()->overlay('Validation Error!', 'Invalid value: " ' . $input['type'] . ' " in "Type" column.');

                    return redirect()->route('deals.upload');
                }

                if (!$propertyStatuses->where('name', $input['status'])->first()) {
                    alert()->overlay('Validation Error!', 'Invalid value: " ' . $input['status'] . ' " in "Status" column.');

                    return redirect()->route('deals.upload');
                }

                if (!$dealTypes->where('name', $input['deal_type'])->first()) {
                    alert()->overlay('Validation Error!', 'Invalid value: " ' . $input['deal_type'] . ' " in "Deal Type" column.');

                    return redirect()->route('deals.upload');
                }

                if (!$propertyStages->where('name', $input['stage'])->first()) {
                    alert()->overlay('Validation Error!', 'Invalid value: " ' . $input['stage'] . ' " in "Stage" column.');

                    return redirect()->route('deals.upload');
                }

                if (!$propertySignals->where('name', $input['signal'])->first()) {
                    alert()->overlay('Validation Error!', 'Invalid value: " ' . $input['signal'] . ' " in "Signal" column.');

                    return redirect()->route('deals.upload');
                }

                if (!$bedrooms->where('name', $input['bedrooms'])->first()) {
                    alert()->overlay('Validation Error!', 'Invalid value: " ' . $input['bedrooms'] . ' " in "Bedrooms" column.');

                    return redirect()->route('deals.upload');
                }

                if (!$bathrooms->where('name', $input['bathrooms'])->first()) {
                    alert()->overlay('Validation Error!', 'Invalid value: " ' . $input['bathrooms'] . ' " in "Bathrooms" column.');

                    return redirect()->route('deals.upload');
                }

            }

            foreach ($reader->get() as $record) {

                $input = $record->toArray();

                try {
                    $input['type'] = $propertyTypes->where('name', $input['type'])->first()->id;
                    $input['status'] = $propertyStatuses->where('name', $input['status'])->first()->id;
                    $input['stage'] = $propertyStages->where('name', $input['stage'])->first()->id;
                    $input['signal'] = $propertySignals->where('name', $input['signal'])->first()->id;
                    $input['deal_type'] = $dealTypes->where('name', $input['deal_type'])->first()->id;
                    $input['bedrooms'] = $bedrooms->where('name', $input['bedrooms'])->first()->id;
                    $input['bathrooms'] = $bathrooms->where('name', $input['bathrooms'])->first()->id;

                    $input['contract_status'] = strtolower($input['contract_status']) == 'yes';
                    $input['asking_price_source'] = strtolower($input['asking_price_source']) == 'mls';
                    $input['inspection_status'] = strtolower($input['inspection_status']) == 'yes';

                    // $input['signal'] = 1;

                    $user->properties()->create($input);

                    $i++;
                } catch (\Exception $e) {
                    logger('Error: ' . $e->getMessage(), $input);
                }
            }

            $count = $i;

            $user->notify(new DealsUploaded($count));
        });

        EventManager::make('Deals list uploaded', today())->link(route('deals.index'))->create();

        alert()->success('Success!', 'Properties uploaded successfully.');

        return redirect()->route('deals.upload');
    }

    public function sendEmail(SendDealEmailRequest $request, Property $property)
    {
        if (!$request->user()->getSetting('sendinblue', 'username')) {
            alert()->overlay('Error!', 'Please set your sendinblue credentials to use this service.');

            return redirect()->route('settings.index');
        }

        $request->persist($property);

        alert()->success('Success!', 'Mail sent successfully.');

        return back();
    }

    public function transferDeal(Request $request, Property $property)
    {
        $property->user_id = $request->user_id;

        $property->save();

        activity()->causedBy(auth()->user())->performedOn($property)->log('transferred');

        $property->user->notify(new DealWasTransferred($property));

        EventManager::make('New Deal Transferred', today())->property($property)->create();

        alert()->success('Success!', 'Deal transferred successfully.');

        return redirect()->route('deals.index');
    }

    public function transferUserDeal(Request $request, Property $property)
    {
        $user = User::where('api_key', $request->api_key)->where('id', '<>', auth()->id())->first();

        if (!$user) {
            alert()->error('Error!', 'User with provided API key not found.');

            return back();
        }

        $property->transferred_by = auth()->id();
        $property->transfer_note = $request->transfer_note;
        $property->user_id = $user->id;

        $property->save();

        activity()->causedBy(auth()->user())->performedOn($property)->withProperties(['transfer_note' => $request->transfer_note])->log('transferred');

        $property->user->notify(new DealWasTransferred($property));

        alert()->success('Success!', 'Deal transferred successfully.');

        return redirect()->route('deals.index');
    }

    public function userDeals(User $user)
    {
        abort_unless(auth()->user()->hasRole(['admin', 'super-user', 'coach']), 403);

        $properties = $user->properties()->presentation()->latest()->get();

        return view('deals.user_deals', compact('user', 'properties'));
    }

    private function findProperty($id, $secure = true)
    {
        $property = Property::presentation()->findOrFail($id);

        $user = auth()->user();

        if ($secure) {
            abort_unless($property->user_id == $user->id || $user->id == $property->user->manager_id || $user->id == $user->coach_users->contains($property->user) || $user->hasRole('admin') || ($user->hasRole('user') && $user->manager_id > 0), 403);
        }

        return $property;
    }

    public function destroy(Property $deal)
    {
        $deal->removeImages();
        $deal->delete();

        alert()->success('Success!', 'Deal removed successfully.');

        return redirect()->route('deals.index');
    }

    public function showContract($id, Request $request)
    {
        $property = $this->findProperty($id, false);

        $data = $request->all();

        if (!$request->has('contingency_type')) {
            $data['contingency_type'] = null;
        }

        if (!$request->has('addendums')) {
            $data['addendums'] = [];
        }

        if ($request->has('send_to') && $request->send_to) {
            session()->put('requestUrl', url($request->getRequestUri()));
        }

        session()->put('contractData', $data);

        return view('deals.show-contract', compact('property', 'data'), $data);
    }

    public function downloadContract($id)
    {
        /** @var User $user */
        $user = auth()->user();

        if (!$user->getSetting('sendinblue', 'username')) {
            alert()->overlay('Error!', 'Please set your sendinblue credentials to use this service.');

            return redirect()->route('settings.index');
        }

        $user->setSendInBlueCredentials();

        $property = $this->findProperty($id, false);

        $data = session('contractData');

        $pdf = PDF::loadView('deals.download-contract', compact('property'), $data);

        $fileName = sprintf('contract_%s.pdf', $data['street_address']);

        $contractPdf = $pdf->download($fileName);

        if (session()->has('requestUrl')) {
            $user = auth()->user();

            $recipient = Contact::find($data['send_to']);

            Mail::send('emails.deal_contract',
                ['user' => $user, 'property' => $property, 'contractUrl' => session('requestUrl')],
                function ($m) use ($property, $user, $recipient, $contractPdf, $fileName) {
                    // $m->from($user->email, $user->name);

                    $m->to($recipient->email, $recipient->full_name)->subject('Deal Contract');

                    $m->attachData($contractPdf, $fileName, ['mime' => 'application/pdf']);
                }
            );

            activity()->causedBy(auth()->user())->performedOn($property)
                ->withProperties(['recipient' => $recipient->full_name])
                ->log('contract_sent');

            alert()->success('Success!', "Deal contract sent to {$recipient->full_name}.");

            session()->forget('requestUrl');
        }

        return $contractPdf;
    }

    private function getDealActivities($property)
    {
        $activities = $property->activity()->with('subject', 'causer')->latest()->get();

        return ActivityParser::process($activities);
    }

    public function sendSellerLocatorRequest(Property $property, Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $user->createAsStripeCustomer($request->stripeToken);

        try {
            $user->invoiceFor('Seller Locator Request', 1000);

            Mail::to(config('mail.from.address'))->send(new SellerLocatorRequest($property, $user));

            $user->notify(new SellerLocatorRequested($property));

            alert()->success('Success!', 'Seller locator requested successfully.');

            return back();
        } catch (\Exception $e) {
            alert()->error('Failed!', 'Something went wrong with payment, please contact support.');

            return back();
        }
    }

    public function sendProofOfFundsRequest(Property $property)
    {
        $user = auth()->user();

        Mail::to(config('mail.from.address'))->send(new ProofOfFundsRequest($property, $user));

        $user->notify(new ProofOfFundsRequested($property));

        alert()->success('Success!', 'Proof of funds requested successfully.');

        return back();
    }

    public function sendDealFundingRequest(Property $property)
    {
        $user = auth()->user();

        Mail::to(config('mail.from.address'))->send(new DealFundingRequest($property, $user));

        $user->notify(new DealFundingRequested($property));

        alert()->success('Success!', 'Deal funding requested successfully.');

        return back();
    }

    public function downloadPropertyInfoPdf($id)
    {
        $property = $this->findProperty($id, false);

        $pdf = PDF::loadView('deals.download-property-pdf', compact('property'));

        $fileName = sprintf('property_information_%s.pdf', $property->address);

        return $pdf->download($fileName);
    }

    public function viewPdf($id)
    {
        $property = $this->findProperty($id, false);

        return view('deals.download-property-pdf', compact('property'));
    }

}
