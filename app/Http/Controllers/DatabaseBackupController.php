<?php

namespace App\Http\Controllers;

use File;
use Artisan;
use Carbon\Carbon;
use Illuminate\Support\Facades\File as FileFacade;

class DatabaseBackupController extends Controller
{
    public function index()
    {
        $returningFiles = collect();

        $files = File::allFiles(storage_path('app/backups/'));

        foreach ($files as $file) {
            $returningFiles->push([
                'name'         => File::basename($file),
                'size'         => File::size($file),
                'lastModified' => Carbon::createFromTimestamp(File::lastModified($file)),
            ]);
        }

        $returningFiles = $returningFiles->sortByDesc('lastModified');

        return view('backups.index')->with('files', $returningFiles);
    }

    public function create()
    {
        Artisan::call('backup:run', ['--only-db' => true]);

        return back()->with('success', 'Backup created successfully!');
    }

    public function show($fileName)
    {
        $directory = storage_path('app/backups/');

        if(FileFacade::exists($directory . $fileName))
        {
            return response()->download($directory . $fileName);
        } else {
            return redirect()->route('backups.index')->with('error', 'File not found!');
        }

    }
}
