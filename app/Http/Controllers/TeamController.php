<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateTeamMembersRequest;

class TeamController extends Controller
{
	public function index()
	{
		$users = User::havingRole('super-user')->with('team')->orderBy('name')->get();

		return view('teams.index', compact('users'));
	}

	public function edit(User $manager)
	{
		$manager->load('team');

		$allUsers = $manager->teamUsers();

		return view('teams.edit', compact('manager', 'allUsers'));
	}

	public function update(UpdateTeamMembersRequest $request, User $manager)
	{
		$manager->updateTeamMembers($request->users ?: []);

		alert()->success('Success!', 'Team members updated successfully.');

		return back();
	}
}
