<?php

namespace App\Http\Controllers;

use App\Property;
use App\Http\Requests;
use Illuminate\Http\Request;

class BlacklistController extends Controller
{
	public function dealToggle(Property $property)
	{
		$property->toggleBlacklist();
    }
}
