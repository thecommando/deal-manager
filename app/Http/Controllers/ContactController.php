<?php

namespace App\Http\Controllers;

use App\ContactGroup;
use App\Http\Requests\SendEmailRequest;
use App\Http\Requests\SendSMSRequest;
use App\Notifications\SMSNotification;
use App\Rules\FileExcelRule;
use App\Services\Contact\GetDatatable;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Mail;
use Excel;
use App\Contact;
use App\ContactEmailTemplate;
use App\Http\Requests;
use App\ContactCategory;
use Illuminate\Http\Request;
use App\Services\Contact\SaveContact;
use App\Notifications\ContactCreated;
use App\Notifications\WelcomeNewContact;
use App\Notifications\ContactsUploaded;
use App\Http\Requests\UpdateContactRequest;
use App\Http\Requests\CreateContactRequest;
use App\Http\Requests\UploadContactsRequest;
use App\Http\Requests\UploadContactsStoreRequest;
use App\Notifications\ContactWasTransferred;
use Notification;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $contactDatatableService = new GetDatatable();
            return $contactDatatableService->getDatatable($request, $request->has('all'));
        }

        return view('contacts.index');
    }

    public function create()
    {
        return view('contacts.create');
    }

    public function show(Contact $contact)
    {
        return view('contacts.show', compact('contact'));
    }

    /**
     * Generate unique username based on name.
     *
     * @param $name
     * @param int $count
     *
     * @return string
     */
    private function getUniqueUsername($name, $count = 0)
    {
        $username = $count ? str_slug($name) . '-' . $count : str_slug($name);

        if (User::where('username', $username)->withTrashed()->exists()) {
            return $this->getUniqueUsername($name, ++$count);
        }

        return $username;
    }

    public function store(CreateContactRequest $request)
    {
        //Save New User access Level
        $password = str_random(6);

        $request['password'] = $password;

        //Validate Email
        // if (Contact::where('email', $request->email)->withTrashed()->exists()) {
        // 	alert()->overlay('Error!', 'Email is already in use. Please try with another email.');

        // 	return redirect()->route('contacts.index');
        // }

        if ($request->category_id == 3) {
            try {

                // $data = [
                // 	'custom_fields' => [
                // 		'content' => $password,
                // 		'id' => 7
                // 	],
                // 	'email_addresses' => [
                // 		'email' => $request->email,
                // 		'field' => 'EMAIL1'
                // 	],
                // 	'given_name' => "{$request->first_name}",
                // 	'opt_in_reason' => 'DMP - Deal Finder'
                // ];

                $data = [
                    'password' => $password,
                    'email' => $request->email_address,
                    'given_name' => $request->first_name,
                    'opt_in_reason' => 'DMP - Deal Finder'
                ];

                $response = (new \App\Services\Infusionsoft\Infusionsoft)->createContact($data);

                // $statusCode = $response->getStatusCode();

                if ($response) {

                    $user = new User($request->all());

                    $user->name = $request->first_name;

                    $user->username = $this->getUniqueUsername($user->name);

                    $user->password = bcrypt($password);

                    $user->api_key = generateUUID();

                    $user->save();

                    $user->attachRole(Role::whereName('finder')->first());
                }
            } catch (Exception $e) {
                alert()->overlay('Error!', $e->getMessage());
            }
        }

        //Save contact
        $contact = (new SaveContact())->handle($request, $request->user());

        $contact->user->notify(new ContactCreated($contact));

        //Welcome Email
        $contact->notify(new WelcomeNewContact($contact, $request));

        alert()->success('Success!', 'Contact created successfully.');

        return redirect()->route('contacts.index');
    }

    public function edit(Contact $contact)
    {
        return view('contacts.edit', compact('contact'));
    }

    public function update(UpdateContactRequest $request, Contact $contact)
    {
        $contact->fill($request->all());

        $contact->save();

        alert()->success('Success!', 'Contact updated successfully.');

        return redirect()->route('contacts.index');
    }

    public function getUpload()
    {
        $categoryList = ContactCategory::orderBY('id', 'DESC')->get()->pluck('name', 'id')->toArray();
        $contactGroups = ContactGroup::where('user_id', Auth::Id())->orderBY('id', 'DESC')->get();

        return view('contacts.upload', compact('categoryList', 'contactGroups'));
    }

    public function fileUpload(UploadContactsRequest $request)
    {
        $file = $request->file('file');
        $fileFormatShouldBe = ['xls', 'xlsx', 'csv'];
        $fileType = $file->getClientOriginalExtension();

        if (!in_array($fileType, $fileFormatShouldBe)) {
            return response()->json(['error' => 'File format should be xls or xlsx'], 422);
        }

        Excel::load($file, function ($reader) use ($request) {
            foreach ($reader->get() as $key => $record) {
                $input = $record->toArray();
                $arrayFilter = array_diff(array_filter(array_keys($input)), ['category', 'contact_group']);
                session()->put('upload_fields', $arrayFilter);
                break;
            }
        });

        $uploadFields = array_values(session()->get('upload_fields'));

        return [
            "my_fields" => Contact::DEFAULT_UPLOAD_CONTACTS_FIELDS,
            "upload_fields" => $uploadFields
        ];

    }

    /**
     * @param $filterData
     * @param $myField
     * @return bool
     */
    public function checkExcelHasHeader($filterData, $myField)
    {
        $uploadFields = array_keys($filterData[0]);
        $uploadFieldNew = [];
        $myFieldFieldNew = [];

        foreach ($uploadFields as $key => $uploadField) {
            if (isset($myField[$key])) {
                $myFieldFieldNew[] = strtolower($myField[$key]);
            }
            $uploadFieldNew[] = strtolower($uploadField);
        }
        $countSameFields = count(array_intersect($uploadFieldNew, $myFieldFieldNew));
        $fieldCountShouldBeAbout = (int)floor(count($myField) / 2);

        $isExcelHasHeaders = true;
        if ($countSameFields < $fieldCountShouldBeAbout) {
            $isExcelHasHeaders = false;
        }
        return $isExcelHasHeaders;
    }

    /**
     * @param UploadContactsStoreRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function postUpload(UploadContactsStoreRequest $request)
    {
        try {
            $file = $request->file('file');
            $fileFormatShouldBe = ['xls', 'xlsx', 'csv'];
            $fileType = $file->getClientOriginalExtension();

            if (!in_array($fileType, $fileFormatShouldBe)) {
                return response()->json(['error' => 'File format should be xls or xlsx'], 422);
            }
            $user = $request->user();
            $myField = Contact::DEFAULT_UPLOAD_CONTACTS_FIELDS;
            $myFieldKeyAndUploadsValue = array_combine($myField, $request->upload_field_options);

            Excel::load($file, function ($reader) use ($user, $myFieldKeyAndUploadsValue, $request, $myField) {
                $i = 0;
                $filterData = [];

                foreach ($reader->get() as $record) {
                    $input = $record->toArray();
                    if (count(array_filter($input))) {
                         $filterData[] = $input;
                    }
                }

                if (isset($request->heading)) {
                    $isExcelHasHeaders = true;
                } else {
                    $isExcelHasHeaders = false;
                }

                if (!$isExcelHasHeaders && count($filterData)) {

                    $makeFirstLine[] = array_combine(array_keys($filterData[0]), array_keys($filterData[0]));
                    $filterData = array_merge($makeFirstLine, $filterData);
                }

                $onlyOneField = false;
                if (!$filterData) {
                    $onlyOneField = true;
                    $filterData[] = $myFieldKeyAndUploadsValue;
                }

                foreach ($filterData as $fields) {
                    $resultData = [];
                    if (!$onlyOneField) {
                        foreach ($myFieldKeyAndUploadsValue as $myField => $uploadFields) {
                            if(!$uploadFields) {
                                $resultData[$myField] = null;
                                continue;
                            }

                            if ($myField === 'first_name' and !$fields[$uploadFields]) {
                                $resultData[$myField] = $uploadFields;
                            } else {
                                $resultData[$myField] = $fields[$uploadFields];
                            }
                        }
                    } else {
                        $resultData = $myFieldKeyAndUploadsValue;
                    }

                    $contact = array_merge($resultData, ['category_id' => $request->category]);
                    $contactData = $user->contacts()->create($contact);

                    if ($request->contacts_group) {
                        $contactGroupId = $request->contacts_group;

                        $isCount = DB::table('contact_contact_group')
                            ->where('contact_id', $contactData->id)
                            ->where('contact_group_id', $contactGroupId)
                            ->count();

                        if ($isCount > 0) {
                            DB::table('contact_contact_group')
                                ->where('contact_id', $contactData->id)
                                ->where('contact_group_id', $contactGroupId)
                                ->delete();
                        }

                        DB::table('contact_contact_group')->insert([
                            'contact_id' => $contactData->id,
                            'contact_group_id' => $contactGroupId
                        ]);
                    }


                    $i++;
                }
                $count = $i;

                $user->notify(new ContactsUploaded($count));
            })->skip(0);

        } catch (Exception $e) {
            logger('Error: ' . $e->getMessage());
        }

        alert()->success('Success!', 'Contacts uploaded successfully.');

        return redirect()->route('contacts.upload');
    }

    public function transferContact(Request $request)
    {
        $contact = Contact::findOrFail($request->contact_id);

        $contact->user_id = $request->user_id;

        $contact->save();

        $contact->user->notify(new ContactWasTransferred($contact));

        alert()->success('Success!', 'Contact transferred successfully.');

        return redirect()->route('contacts.index');
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();

        alert()->success('Success!', 'Contact removed successfully.');

        return back();
    }

    public function sendEmail(SendEmailRequest $request)
    {
        if (!$request->user()->getSetting('sendinblue', 'username')) {
            alert()->overlay('Error!', 'Please set your sendinblue credentials to use this service.');

            return redirect()->route('settings.index');
        }

        $request->persist();

        return back();
    }

    public function sendMessage(SendSMSRequest $request)
    {
        if (!$request->user()->getSetting('twilio', 'account_sid')) {
            alert()->overlay('Error!', 'Please set your twilio credentials to use this service.');
            return redirect()->route('settings.index');
        }

        $request->persist();

        return back();
    }

    public function saveTemplate(Request $request)
    {
        if (!empty($request->title)) {
            $template = ContactEmailTemplate::where('user_id', auth()->user()->id)->where('name', $request->title)->first();
            if ($template) {
                $template->message = $request->message;

                $template->save();
            } else {
                $template = ContactEmailTemplate::create([
                    'user_id' => auth()->user()->id,
                    'name' => $request->title,
                    'message' => $request->message
                ]);
            }
        } else {
            $template = ContactEmailTemplate::create([
                'user_id' => auth()->user()->id,
                'name' => $request->title,
                'message' => $request->message
            ]);
        }

        alert()->success('Success!', 'Template Saved successfully.');

        return response()->json(["response" => true, "template" => $template]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteAll(Request $request)
    {
        try {
            $ids = explode(",", $request->ids);

            foreach ($ids as $k => $v) {
                $query = DB::table('contacts')->where('id', $v);

                $contact = $query->first();

                if (DB::table('contact_contact_group')->where('contact_id', $contact->id)->count() > 0) {
                    $groups = DB::table('contact_contact_group')->where('contact_id', $contact->id)->delete();
                }

                $query->delete();
            }

            return response()->json(['success' => "Contact(s) Deleted successfully."]);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function uploadImage(Request $request)
    {

        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;

            $request->file('upload')->move(public_path('uploads'), $fileName);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('uploads/' . $fileName);
            return json_encode(array('url' => $url));
        }


    }
}
