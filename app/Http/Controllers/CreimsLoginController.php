<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Services\Infusionsoft\Infusionsoft;
use Exception;

class CreimsLoginController extends Controller
{
	public function index()
	{
		if (auth()->user()->is_crei_created) {
			// (new Infusionsoft)->getToken();
			$email = auth()->user()->email;

			try {
				$pwd = (new Infusionsoft)->getCreiContactPassword($email);
			} catch (Exception $e) {
				abort(404, alert()->error('Error!', 'Something went wrong while login' . $e->getMessage()));
			}
			
			$apiURL = 'https://creims.ushomebuyingnetwork.com/api/account/invite/url';
			
			$postInput = [
				'Email' => $email,
				'Password' => $pwd
			];
					
			$client = new \GuzzleHttp\Client();
		
			try {
				$response = $client->request('POST', $apiURL, [
					'headers' => [ 'Accept' => 'application/json', 'Content-Type' => 'application/json'],
					'body' => json_encode($postInput)          
				]);

				$statusCode = $response->getStatusCode();        

				if ($statusCode == 200) {
					
					$responseBody = json_decode($response->getBody(), true);
					return redirect()->away(array_get($responseBody, 'url'));
				}
			} catch(HTTP_Request2_Exception $e) {
				alert()->error('CREI-MS API Error!', 'Something went wrong while login' . $e->getMessage());
			}

			logger()->error('CREI-MS Login Failed-', $responseBody);

			return back();
		} else {		
			return view('users.creiuser');			
		}
	}

    public function show()
    {
        abort(404);
    }
}