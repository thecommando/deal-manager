<?php

namespace App\Http\Controllers;

use App\ContactGroup;
use App\Http\Requests\SendEmailRequest;
use App\Http\Requests\SendSMSRequest;
use App\Notifications\SMSNotification;
use Exception;
use Mail;
use Excel;
use App\Contact;
use App\ContactEmailTemplate;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Contact\SaveTemplate;
use Notification;
use Illuminate\Support\Facades\DB;

class CopyTemplateController extends Controller
{
	public function copyTemplate($id)
	{
		$template = ContactEmailTemplate::where('user_id', auth()->user()->id)->find($id);
		$newTemplate = $template->replicate();
		$newTemplate->save();

		alert()->success('Success!', 'Template copied successfully.');

		return redirect()->route('manage-templates.index');
	}
}
