<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UpdateTeamRequest;
use App\Http\Requests\UpdatePhotoRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UpdatePasswordRequest;

class ProfileController extends Controller
{
	public function getProfile()
	{
		return view('user.profile')->withUser(auth()->user());
	}

	public function postProfile(UpdateProfileRequest $request)
	{
	    $user = $request->user();

	    $user->fill($request->except('password'));

		$user->save();

		alert()->success('Success!', 'Your profile updated successfully.');

		return back();
	}

	public function profilePicture(UpdatePhotoRequest $request)
	{
		$request->user()->uploadProfilePicture($request->file('profile'));

		alert()->success('Success!', 'Profile picture updated successfully.');

		return back();
	}

	public function updatePassword(UpdatePasswordRequest $request)
	{
        $user = $request->user();

		$user->password = bcrypt($request->password);

		$user->save();

		$apiMessage = NULL;

		try {
		  (new \App\Services\Infusionsoft\Infusionsoft)->updateContactPassword($user->email, $request->password);
		} catch (\Exception $e) {
		   $apiMessage = $e->getMessage();
		}       

		alert()->success('Success!', 'Your Password updated successfully.');

		return back();
	}

	public function postTeam(UpdateTeamRequest $request)
	{
		$request->user()->updateTeam($request->team_name);

		alert()->success('Success!', 'Team details updated successfully.');

		return back();
	}

	public function teamUsers()
	{
	    $user = auth()->user();

	    if($user->hasRole('super-user')) {
	        $users = auth()->user()->sub_users()->orderBy('name')->get();

            return view('user.team_users', compact('users'))->withUsers($users);
        } else {
            $users = auth()->user()->coach_users()->orderBy('name')->get();

            return view('user.coach_users', compact('users'))->withUsers($users);
        }
	}
}
