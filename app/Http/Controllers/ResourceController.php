<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ResourceController extends Controller
{
	public function getTraining()
	{
		return view('resources.training');
    }

	public function getFaq()
	{
		return view('resources.faq');
	}

	public function getSupport()
	{
		return view('resources.support');
	}
}
