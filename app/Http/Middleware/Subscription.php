<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Subscription
{
    /**
     * @var \App\User|null
     */
    protected $user;

    /**
     * Subscription constructor.
     *
     * @param Guard $guard
     */
    public function __construct(Guard $guard)
    {
        $this->user = $guard->user();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->user->hasRole(['admin', 'super-user', 'finder']) || $this->user->canBypassPayment()) {
            return $next($request);
        }

        if (! $this->user->hasStripeId()) {
            alert()->overlay('Oops!', 'Please add your card details to continue.');

            return redirect()->route('settings.index');
        }

        if (! $this->user->subscribed() && ! $this->user->onTrial()) {
            alert()->overlay('Oops!', 'You need to subscribe to plan to continue.');

            return redirect()->route('settings.index');
        }

        return $next($request);
    }
}
