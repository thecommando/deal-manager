<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePropertyRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			// required fields
			'address'       => 'required',
			'city'          => 'required',
			'state'         => 'required',
			'postal_code'   => 'required',
			'type'          => 'required|exists:property_types,id',
            'square_feet'   => 'required',
			'description'   => 'required',

			// optional fields
			'seller_phone'  => 'phone:LENIENT,US|nullable',
			'seller_mobile' => 'phone:LENIENT,US|nullable',
			'seller_email'  => 'email|nullable',
		];
	}
}
