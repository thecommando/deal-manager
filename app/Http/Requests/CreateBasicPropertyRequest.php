<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBasicPropertyRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			// required fields
			'address'       => 'required',
			// 'street_number'       => 'required',
			// 'street_name'       => 'required',
			// 'seller_city'       => 'required',
			// 'seller_state'       => 'required',
			// 'seller_postal_code' => 'required',

			// optional fields
			'seller_phone'  => 'phone:LENIENT,US|nullable',
			'seller_mobile' => 'phone:LENIENT,US|nullable',
			'seller_email'  => 'email|nullable',
		];
	}
}
