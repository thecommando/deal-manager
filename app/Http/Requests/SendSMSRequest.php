<?php

namespace App\Http\Requests;

use App\Contact;
use App\ContactGroup;
use App\Notifications\SMSNotification;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Notification;

class SendSMSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|min:5',
        ];
    }

    public function persist()
    {
        $user = $this->user();

        $message = $this->message;

        $recipients = [];

        if ($this->to == 'contacts') {
            $recipients = Contact::whereIn('id', $this->contact)->where('phone', '!=', null)->where('phone', '!=', '')->get();
        } elseif ($this->to == 'group') {
            $recipients = ContactGroup::find($this->group)->contacts()->where('phone', '!=', null)->where('phone', '!=', '')->get();
        } elseif ($this->to == 'contact_category') {
            $recipients = Contact::where('user_id', $user->id)->where('category_id', $this->contact_category)->where('phone', '!=', null)->where('phone', '!=', '')->get();
        }

        $user->setTwilioCredentials();

        try {
            Notification::send($recipients, new SMSNotification($message));

            alert()->success('Success!', 'Message sent successfully.');
        } catch (Exception $exception) {
            logger()->error($exception->getMessage());

            alert()->error('Error!', $exception->getMessage());
        }
    }
}
