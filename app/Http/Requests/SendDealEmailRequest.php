<?php

namespace App\Http\Requests;

use App\Contact;
use App\ContactGroup;
use App\Services\Calendar\EventManager;
use Illuminate\Foundation\Http\FormRequest;
use Mail;

class SendDealEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required|min:3',
            'message' => 'required|min:3',
            'contact' => 'required_if:to,contacts',
            'group' => 'required_if:to,group',
            'contact_category' => 'required_if:to,contact_category',
            'other' => 'required_if:to,other',
        ];
    }

    public function persist($property)
    {
        $user = $this->user();

        $user->setSendInBlueCredentials();

        $subject = $this->subject;
        $body = nl2br($this->message);

        $recipients = [];

        if ($this->to == 'contacts') {
            $recipients = Contact::whereIn('id', $this->contact)->get();
        } else if ($this->to == 'group') {
            $recipients = ContactGroup::find($this->group)->contacts;
        } else if ($this->to == 'contact_category') {
            $recipients = Contact::where('user_id', $user->id)->where('category_id', $this->contact_category)->get();
        } else if ($this->to == 'other') {
            $recipient = $this->other;

            try {
                Mail::send('emails.deal',
                    ['user' => $user, 'property' => $property, 'body' => $body],
                    function ($m) use ($property, $user, $recipient, $subject) {
                        $m->from($user->email, $user->name);
                        $m->to($recipient, $recipient)->subject($subject);
                    }
                );
            } catch (\Exception $e) {
                info('Email failed to send to ' . $recipient . ' ' . $e->getMessage());
            }
        }

        foreach ($recipients as $recipient) {
            try {
                Mail::send('emails.deal',
                    ['user' => $user, 'recipient' => $recipient, 'property' => $property, 'body' => $body],
                    function ($m) use ($property, $user, $recipient, $subject) {
                        $m->from($user->email, $user->name);
                        $m->to($recipient->email, $recipient->full_name)->subject($subject);
                    }
                );
            } catch (\Exception $e) {
                info('MASS Email failed to send to ' . $recipient->email . ' ' . $e->getMessage());
            }
        }

        $recipient = $this->to == 'other' ? 'individual' : $this->to;

        activity()->causedBy($user)->performedOn($property)->withProperty('recipient', $recipient)->log('emailed');

        EventManager::make('Group email sent', today())->property($property)->create();
    }
}
