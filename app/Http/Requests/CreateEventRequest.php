<?php

namespace App\Http\Requests;

use App\Event;
use App\Services\Calendar\EventManager;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:255',
            'date' => 'required|date',
            'remind_at' => 'nullable|date',
            'is_global' => 'nullable|boolean',
        ];
    }

    public function persist()
    {
        $eventManager = EventManager::make($this->title, $this->date);

        if ($this->remind_at) {
            $eventManager->remindAt(Carbon::parse($this->remind_at)->setTimeFromTimeString('09:00'));
        }

        if ($this->user()->hasRole(['super-admin', 'admin']) && $this->is_global) {
            $eventManager->isGlobal();
        }

        return $eventManager->create();
    }
}
