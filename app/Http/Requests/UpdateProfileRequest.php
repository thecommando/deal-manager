<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|max:255',
            'username'     => 'required|max:20|unique:users,username,' . \Auth::id(),
            'email'        => 'required|email|max:255|unique:users,email,' . \Auth::id(),
            'website'      => 'url|nullable',
            'mobile_phone' => 'phone:LENIENT,US|nullable',
            'office_phone' => 'phone:LENIENT,US|nullable',
        ];
    }
}
