<?php

namespace App\Http\Requests;

use App\Contact;
use App\ContactGroup;
use Illuminate\Foundation\Http\FormRequest;
use Mail;

class SendEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist()
    {
        $user = $this->user();

        $user->setSendInBlueCredentials();

        $subject = $this->subject;
        $body = nl2br($this->message);

        $recipients = [];

        if ($this->to == 'contacts') {
            $recipients = Contact::whereIn('id', $this->contact)->get();
        } elseif ($this->to == 'group') {
            $recipients = ContactGroup::find($this->group)->contacts;
        } elseif ($this->to == 'contact_category') {
            $recipients = Contact::where('user_id', $user->id)->where('category_id', $this->contact_category)->get();
        } elseif ($this->to == 'other') {
            $recipient = $this->other;

            Mail::send('emails.contact',
                ['user' => $user, 'body' => $body],
                function ($m) use ($user, $recipient, $subject) {
                    // $m->from($user->email_address, $user->name);

                    $m->to($recipient, $recipient)->subject($subject);
                }
            );
        }

        foreach ($recipients as $recipient) {
            if (!empty($recipient->email_address)) {
                Mail::send('emails.contact',
                    ['user' => $user, 'recipient' => $recipient, 'body' => $body],
                    function ($m) use ($user, $recipient, $subject, $body) {
                        // $m->from($user->email_address, $user->name);

                        $m->to($recipient->email_address, $recipient->full_name)->subject($subject)->setBody($body, 'text/html');
                    }
                );
            }
        }

        alert()->success('Success!', 'Mail sent successfully.');
    }
}
