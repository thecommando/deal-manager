<?php

namespace App\Http\Requests;

use App\Rules\FileExcelRule;
use Illuminate\Foundation\Http\FormRequest;

class UploadContactsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|file',
//            'file' => 'required|file|mimetypes:application/octet-stream,application/vnd.ms-excel,xlsx,xls,csv,docx,ppt,odt,ods,odp',
        ];
    }
}
