<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealType extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'name',
	];
}
