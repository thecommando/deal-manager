<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	public $timestamps = false;

    protected $fillable = [
	    'name',
    ];

	public function states()
	{
		return $this->hasMany(State::class);
	}

	public function scopeActive($query)
	{
		return $query->whereIsActive(true);
	}
}
