<?php

namespace App\Observers;

use App\Property;

class PropertyObserver
{
    public function saved(Property $property)
    {
        $property->syncPropertyEvents();
    }
}