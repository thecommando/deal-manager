<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactCategory extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
	    'name',
    ];

	public function contacts()
	{
		return $this->hasMany(Contact::class, 'category_id');
	}
}
