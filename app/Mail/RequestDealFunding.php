<?php

namespace App\Mail;

use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestDealFunding extends Mailable
{
	use Queueable, SerializesModels;

	public $property;

	public $data;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(Property $property, $data)
	{
		$this->property = $property;        
		$this->data = $data;
	} 

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->subject($this->getSubject())->markdown('emails.request_deal_funding', [
			'property' => $this->property,
            'data' => $this->data,
		]);
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return "Request Deal Funding";
	}
}
