<?php

namespace App\Mail;

use App\Property;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DealFundingRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Property
     */
    public $property;

    public $user;

    /**
     * Create a new message instance.
     *
     * @param Property $property
     * @param $user
     */
    public function __construct(Property $property, $user)
    {
        $this->property = $property;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->getSubject())->markdown('emails.deal-funding-request');
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return "Deal Funding Requested - {$this->user->name}";
    }
}
