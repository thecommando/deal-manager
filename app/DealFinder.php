<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealFinder extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'user_id', 'deal_finder_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
