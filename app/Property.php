<?php

namespace App;

use App\Traits\HasNotes;
use App\Traits\Imageable;
use App\Traits\HasEvents;
use App\Traits\Blacklistable;
use App\Traits\PropertyHelpers;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
	use Imageable, LogsActivity, SoftDeletes, Blacklistable, HasNotes, PropertyHelpers, HasEvents;

	protected $fillable = [
		// Property Information
		'address', 'city', 'state', 'country', 'postal_code', 'bedrooms', 'bathrooms', 'type', 'year_built', 'square_feet', 'lot_size', 'description', 'reason_for_selling',

		// Property Status
		'status', 'deal_type', 'stage', 'contract_status', 'inspection_status', 'condition',

		// Financial Information
		'asking_price_flag', 'asking_price', 'estimated_rehab_flag', 'rehab_estimate', 'rehab_estimate_category', 'appraised_value_flag', 'appraised_value', 'wholesale_fee', 'asking_price_source',

		// Wholesale & Rehab Calculations
		'estimated_rehab', 'comparable_price_per_sqft', 'after_repair_value', 'offer_scale_percentage', 'suggested_maximum_offer',

		// Buy & Hold Calculations
		'fair_market_rent', 'actual_monthly_rent', 'annual_taxes', 'annual_insurance', 'annual_expenses', 'annual_gross_income', 'annual_net_operating_income', 'actual_net_profit', 'delinquent_mortgage_amount',

		// Equity Position
		'equity_position', 'signal',

		// Special Note
		'wholesale_value', 'retail_value', 'offer_price', 'purchase', 'rehab',

		// Seller Information
		'seller_name', 'seller_email', 'seller_phone', 'seller_mobile', 'seller_address', 'seller_city', 'seller_state', 'seller_country', 'seller_postal_code',

        'finder_id', 'transferred_by', 'contract_period_days', 'contract_period', 'transfer_note', 'cost_template_id'
	];

	protected $dates = [
	    'contract_period',
    ];

	protected $appends = [
	    'full_address', 'short_address', 'map_address',
    ];

    protected $with = [
        'images'
    ];
	protected static $recordEvents = ['updated', 'deleted'];

	public function user()
	{
		return $this->belongsTo(User::class)->withTrashed();
	}

    public function finder()
    {
        return $this->belongsTo(User::class, 'finder_id');
	}

	public function transferredBy()
    {
        return $this->belongsTo(User::class, 'transferred_by');
	}

	public function type()
	{
		return $this->belongsTo(PropertyType::class, 'type');
	}

	public function status()
	{
		return $this->belongsTo(PropertyStatus::class, 'status');
	}

    public function condition()
    {
        return $this->belongsTo(PropertyStatus::class, 'condition');
    }

	public function stage()
	{
		return $this->belongsTo(PropertyStage::class, 'stage');
	}

	public function signal()
	{
		return $this->belongsTo(PropertySignal::class, 'signal');
	}

	public function deal_type()
	{
		return $this->belongsTo(DealType::class, 'deal_type');
	}

	public function bedrooms()
	{
		return $this->belongsTo(Bedroom::class, 'bedrooms');
	}

	public function bathrooms()
	{
		return $this->belongsTo(Bathroom::class, 'bathrooms');
	}

	public function comparable_properties()
	{
		return $this->hasMany(ComparableProperty::class);
	}


	/******************************************************************************
	 *                              Query Scopes
	 *****************************************************************************/

	public function scopePresentation($query)
	{
		return $query->leftJoin('property_types', 'properties.type', '=', 'property_types.id')
			->leftJoin('property_statuses', 'properties.status', '=', 'property_statuses.id')
			->leftJoin('property_conditions', 'properties.condition', '=', 'property_conditions.id')
			->leftJoin('property_stages', 'properties.stage', '=', 'property_stages.id')
			->leftJoin('property_signals', 'properties.signal', '=', 'property_signals.id')
			->leftJoin('deal_types', 'properties.deal_type', '=', 'deal_types.id')
			->leftJoin('bedrooms', 'properties.bedrooms', '=', 'bedrooms.id')
			->leftJoin('bathrooms', 'properties.bathrooms', '=', 'bathrooms.id')
			->select(
				'properties.*',
				'property_types.name as type',
				'property_statuses.name as status',
				'property_conditions.name as condition',
				'property_stages.name as stage',
				'property_signals.name as signal',
				'deal_types.name as deal_type',
				'bedrooms.name as bedrooms',
				'bathrooms.name as bathrooms'
			);
	}

	public function scopeOpen($query)
	{
		return $query->where('stage', '<>', 8);
	}

    public function scopeWholesale($query)
    {
        return $query->whereHas('deal_type', function($q) {
            $q->where('name', 'Wholesale');
        });
    }

    public function scopeNew($query)
    {
        return $query->where('stage', 1);
    }

    public function scopeOfferSubmitted($query)
    {
        return $query->where('stage', 2);
    }

    public function scopeOfferAccepted($query)
    {
        return $query->where('stage', 3);
    }

    public function scopeContactAssigned($query)
    {
        return $query->where('stage', 4);
    }

    public function scopeFundingRequested($query)
    {
        return $query->where('stage', 5);
    }

    public function scopeFunded($query)
    {
        return $query->where('stage', 6);
    }

    public function scopePending($query)
    {
        return $query->where('stage', 7);
    }

    public function scopeClosed($query)
	{
		return $query->where('stage', 8);
	}

    public function scopeNoDeal($query)
    {
        return $query->where('stage', 9);
	}

	/******************************************************************************
	 *                                Helpers
	 *****************************************************************************/

	public function getFullAddress()
	{
		return sprintf("%s %s, %s %s", $this->address, $this->city, $this->state, $this->postal_code);
	}

    public function getMapAddressAttribute()
    {
        return sprintf("%s %s %s %s", $this->address, $this->city, $this->state, $this->postal_code);
    }

    public function getFullAddressAttribute()
    {
        return $this->getFullAddress();
	}

    public function getShortAddressAttribute()
    {
        if(strlen($this->address) > 25) {
            return substr($this->address, 0, 25) . '...';
        }

        return $this->address;
	}

	public function getStatus()
	{
		return PropertyStatus::find($this->status)->name;
	}

    public function getCondition()
    {
        return PropertyCondition::find($this->condition)->name;
    }

	public function getBedrooms()
	{
		return Bedroom::find($this->bedrooms)->name;
	}

	public function getBathrooms()
	{
		return Bathroom::find($this->bedrooms)->name;
	}

	public function saveComparables()
	{
		if (session()->has('comparables')) {
			$comparables = session('comparables');

			$this->comparable_properties()->delete();

			foreach ($comparables as $comparable) {
				if ($comparable['sqft'] && $comparable['sold_price']) {
					$this->comparable_properties()->create($comparable);
				}
			}
		}
	}

	public function setPending()
	{
		$this->stage = 7;

		return $this->save();
	}
}
