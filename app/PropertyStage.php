<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyStage extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'name',
	];
}
