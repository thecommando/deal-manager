<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Document extends Model
{
	use LogsActivity, SoftDeletes;

	protected $fillable = [
		'title',
		'description',
		'path',
        'is_global',
	];

	protected $casts = [
	    'is_global' => 'boolean',
    ];

	protected $baseDir = 'uploads/documents';

	public function users()
	{
		return $this->belongsToMany(User::class);
	}

	/**
	 * Upload document file
	 *
	 * @param UploadedFile $file
	 * @return $this
	 */
	public function uploadFile(UploadedFile $file)
	{
		$fileName = sprintf("%s-%s", generateUUID(), $file->getClientOriginalName());

		$file->move($this->baseDir, $fileName);

		$this->path = sprintf("%s/%s", $this->baseDir, $fileName);

		$this->save();

		return $this;
	}

	/**
	 * Attach users to document
	 *
	 * @param array $usersIds
	 *
	 * @return array
	 */
	public function attachUsers(array $usersIds)
	{
		return $this->users()->sync($usersIds);
	}

	public function getUrl()
	{
		return url($this->path);
	}

	public function hasFile()
	{
		return (bool)$this->path;
	}

	public function removeFile()
	{
		if ($this->path) {
			unlink($this->path);
		}
	}
}
