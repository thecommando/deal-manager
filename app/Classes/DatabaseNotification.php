<?php

namespace App\Classes;

use App\Traits\Makeable;

class DatabaseNotification
{
    use Makeable;

    protected $collection;

    public function __construct($title)
    {
        $this->collection = collect(compact('title'));
    }

    public function description($description)
    {
        $this->collection->put('message', $description);

        return $this;
    }

    public function route($name, $parameters = [])
    {
        $this->collection->put('route_name', $name);
        $this->collection->put('route_parameters', $parameters);

        return $this;
    }

    public function toArray($extra = [])
    {
        $this->collection = $this->collection->merge($extra);

        return $this->collection->toArray();
    }
}
