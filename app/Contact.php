<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
	use LogsActivity, SoftDeletes, Notifiable;
	
	protected $fillable = [
		'category_id', 'first_name', 'last_name', 'email_address', 'phone', 'alternate_phone', 'site_address', 'site_city', 'site_state',
        'site_zip_code', 'price_range_min', 'price_range_max', 'location',
        'mailing_city','site_zip_code', 'mailing_address', 'mailing_state', 'mailing_zip', 'site_county'
	];

    const DEFAULT_UPLOAD_CONTACTS_FIELDS = [
        'first_name',
        'last_name',
        'email_address',
        'phone',
        'alternate_phone',
        'site_address',
        'site_city',
        'site_state',
        'site_zip_code',
        'mailing_address',
        'mailing_state',
        'mailing_zip',
        'mailing_city',
        'site_county',
    ];

	protected static $recordEvents = ['updated', 'deleted'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function category()
	{
		return $this->belongsTo(ContactCategory::class);
	}

	public function getFullNameAttribute()
	{
		return $this->last_name ? sprintf("%s %s", $this->first_name, $this->last_name) : $this->first_name;
	}

    public function contact_groups()
    {
        return $this->belongsToMany(ContactGroup::class);
	}

    public function routeNotificationForTwilio()
    {
        return $this->phone;
    }
}
