<?php

namespace App\Notifications;

use App\Property;
use Illuminate\Bus\Queueable;
use App\Classes\DatabaseNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class NewSellerLead extends Notification
{
    use Queueable;

    /**
     * @var Property
     */
    protected $deal;

    /**
     * Create a new notification instance.
     *
     * @param Property $deal
     */
    public function __construct(Property $deal)
    {
        $this->deal = $deal;

        $deal->user->setSendInBlueCredentials();
        $deal->user->setTwilioCredentials();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $medium = collect(['database']);

        if($notifiable->hasTwilioCredentials()) {
            $medium->push(TwilioChannel::class);
        }

        if($notifiable->hasSendInBlueCredentials()) {
            $medium->push('mail');
        }

        return $medium->toArray();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('You Have A New Seller Lead')
            ->greeting("Hello! {$notifiable->name}")
            ->line('Awesome - you have a new lead in your Deal Manager Pro account!')
            ->line('Log in to deal manager pro and see the lead details here:')
            ->action('View Deal', route('deals.show', $this->deal->id));
    }

    public function toTwilio($notifiable)
    {
        $dealUrl = route('deals.show', $this->deal->id);

        return (new TwilioSmsMessage)
            ->content("You have a new lead in your Deal Manager Pro account! View deal at $dealUrl to view. Nice work!");
    }

    public function toArray($notifiable)
    {
        return DatabaseNotification::make('You Have A New Seller Lead')
            ->description("You have a new lead in your Deal Manager Pro account!.")
            ->route('deals.show', [$this->deal->id])
            ->toArray();
    }
}
