<?php

namespace App\Notifications;

use App\Classes\DatabaseNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactsUploaded extends Notification
{
    use Queueable;

    protected $count;

    /**
     * Create a new notification instance.
     *
     * @param $count
     */
    public function __construct($count)
    {
        $this->count = $count;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Contact Uploaded!')
            ->greeting("Hello! {$notifiable->name}")
            ->line('New Contact(s) have been added to your account.')
            ->action('View Contacts', route('contacts.index'))
            ->line('Thank you for using Deal Manager PRO!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return DatabaseNotification::make('New Contact(s) Uploaded!')
            ->description("{$this->count} new contact(s) were uploaded to your account.")
            ->route('contacts.index')
            ->toArray();
    }
}
