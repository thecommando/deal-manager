<?php

namespace App\Notifications;

use App\Classes\DatabaseNotification;
use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DealWasTransferred extends Notification
{
    use Queueable;

    /**
     * @var Property
     */
    protected $deal;

    /**
     * Create a new notification instance.
     *
     * @param Property $deal
     */
    public function __construct(Property $deal)
    {
        $this->deal = $deal;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Deal Transferred!')
            ->greeting("Hello! {$notifiable->name}")
            ->line("A new deal {$this->deal->address} in {$this->deal->city}, {$this->deal->state} has been added into your Deal Manager Pro account by {$notifiable->name}. This deal can now be viewed or edited at any time. Please verify and validate this property at your earliest convenience.")
            ->action('View Deal', route('deals.show', $this->deal->id))
            ->line('Thank you for using Deal Manager PRO!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return DatabaseNotification::make('New Deal Transferred!')
            ->description("A new deal at {$this->deal->full_address} has been transferred to you. Transfer From: {$this->deal->transferredBy->name}")
            ->route('deals.show', [$this->deal->id])
            ->toArray();
    }
}
