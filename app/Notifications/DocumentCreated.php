<?php

namespace App\Notifications;

use App\Classes\DatabaseNotification;
use App\Document;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DocumentCreated extends Notification
{
    use Queueable;

    /**
     * @var Document
     */
    protected $document;

    /**
     * Create a new notification instance.
     *
     * @param Document $document
     */
    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Document Created!')
            ->greeting("Hello! {$notifiable->name}")
            ->line('You have been given access to a “new document.” This document can be found in the in the downloads section of Deal Manager Pro. This document is available for download at anytime.')
            ->line('If there are any additional documents, contracts, or checklists you feel would be helpful, we encourage your feedback to make Deal Manager Pro better for everyone.')
            ->action('View Documents', route('documents.index', $this->document->id))
            ->line('Thank you for using Deal Manager PRO!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return DatabaseNotification::make('New Document Added!')
            ->description(sprintf("A new document titled <a href='%s' target='_blank'>%s</a> has been added.",
                $this->document->hasFile() ? $this->document->getUrl() : '#',
                $this->document->title
            ))
            ->route('documents.index')
            ->toArray();
    }
}
