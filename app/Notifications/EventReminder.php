<?php

namespace App\Notifications;

use App\Classes\DatabaseNotification;
use App\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EventReminder extends Notification
{
    use Queueable;

    /**
     * @var Event
     */
    public $event;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Upcoming event!')
            ->greeting("Hello! {$notifiable->name}")
            ->line("An event is due on {$this->event->date->format(DATE_FORMAT)}. Please visit your calendar for more details.")
            ->action('Open Calendar', route('calendar.index'))
            ->line('Thank you for using Deal Manager PRO!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return DatabaseNotification::make($this->event->title)
            ->description('Reminder for event due on ' . $this->event->date->format(DATE_FORMAT))
            ->route('calendar.index')
            ->toArray();
    }
}
