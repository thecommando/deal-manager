<?php

namespace App\Notifications;

use App\Classes\DatabaseNotification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DealFinderCreated extends Notification
{
    use Queueable;

    /**
     * @var User
     */
    protected $finder;

    /**
     * Create a new notification instance.
     *
     * @param User $finder
     */
    public function __construct(User $finder)
    {
        $this->finder = $finder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Deal Finder Created!')
                    ->greeting("Hello! {$notifiable->name}")
                    ->line("A new deal finder {$this->finder->name} attached to your Deal Manager Pro account.")
                    ->line('Thank you for using Deal Manager PRO!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return DatabaseNotification::make('Deal Finder Added!')
            ->description("A new deal finder {$this->finder->name} has been added to your account.")
            ->route('dashboard')
            ->toArray();
    }
}
