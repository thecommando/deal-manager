<?php

namespace App\Notifications;

use App\Classes\DatabaseNotification;
use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SellerLocatorRequested extends Notification
{
    use Queueable;

    /**
     * @var Property
     */
    public $deal;

    /**
     * Create a new notification instance.
     *
     * @param Property $deal
     */
    public function __construct(Property $deal)
    {
        $this->deal = $deal;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Seller Locator Requested!')
            ->greeting("Hello! {$notifiable->name}")
            ->line("You have requested seller locator for deal at {$this->deal->address} in {$this->deal->city}, {$this->deal->state}.")
            ->action('View Deal', route('deals.show', $this->deal->id))
            ->line('Thank you for using Deal Manager PRO!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return DatabaseNotification::make('Seller Locator Requested!')
            ->description("You have requested seller locator for deal at {$this->deal->full_address}.")
            ->route('deals.show', [$this->deal->id])
            ->toArray();
    }
}
