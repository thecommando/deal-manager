<?php

namespace App\Notifications;

use App\Classes\DatabaseNotification;
use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactCreated extends Notification
{
    use Queueable;

    /**
     * @var Contact
     */
    protected $contact;

    /**
     * Create a new notification instance.
     *
     * @param Contact $contact
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Contact Created!')
                    ->greeting("Hello! {$notifiable->name}")
                    ->line("A new contact {$this->contact->full_name} has been uploaded into your Deal Manager Pro account. This contact can now be viewed or edited at any time. Please verify and validate this contact at your earliest convenience.")
                    ->action('View Contact', route('contacts.edit', $this->contact->id))
                    ->line('Thank you for using Deal Manager PRO!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return DatabaseNotification::make('New Contact Added!')
            ->description("A new contact {$this->contact->full_name} has been added to your account.")
            ->route('contacts.show', [$this->contact->id])
            ->toArray();
    }
}
