# Deal Manager

New Changes:
1. New User manually created the error page appears on "submit" but the user is created. When I choose standard "user" role
2. Update new stripe billing information for new stripe account
3. "Upload contact list" error page, using the excel template
4. Upload single contact error page
5. uploading the property list into system error page using template
6. uploading contact manually error page
7. Error adding new property or deal into system. when going back the info does not save on screen.
8. Maybe the errors are coming from manually entered "user accounts"  Did I create the wrong user role for manually entering user


## Done
- Project setup
- Setting up dev site on the server for testing purposes
- Fixed node version on server
- Installed `wkhtmltopdf` to fix pdf download bug
- Fixed errors and added validation on send email modal on deal show page
- Traced issue with email sending with sendgrid
- Fixed errors mentioned by chad over mail

- Transfer deal => there's no bug, can you share screenshot pls?
- All users lists are in alphabetical order
- Deal finders list improved
- New property statuses added

Auth Redirect URL:
- https://accounts.infusionsoft.com/app/oauth/authorize?client_id=5dvZIJwPv5e5m3AWXW9s1n4Uv9oarVY5xig0RYs7wEcf0iGT&redirect_uri=https://app.dealmanagerpro.com/login&response_type=code&scope=full
- copy access code from url
- generate new token with script
```
(new App\Services\Infusionsoft\Infusionsoft)->requestAccessToken('token_from_above_url')
```
